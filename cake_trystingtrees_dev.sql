-- phpMyAdmin SQL Dump
-- version 3.3.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Dec 15, 2011 at 08:51 AM
-- Server version: 5.1.54
-- PHP Version: 5.3.5-1ubuntu7.3

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `cake_trystingtrees_dev`
--

-- --------------------------------------------------------

--
-- Table structure for table `acos`
--

CREATE TABLE IF NOT EXISTS `acos` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) DEFAULT NULL,
  `model` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `foreign_key` int(10) DEFAULT NULL,
  `alias` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `lft` int(10) DEFAULT NULL,
  `rght` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `acos`
--


-- --------------------------------------------------------

--
-- Table structure for table `agent_media`
--

CREATE TABLE IF NOT EXISTS `agent_media` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `listing_agent_id` int(10) unsigned NOT NULL,
  `mime_type` varchar(20) NOT NULL,
  `medium` varchar(10) NOT NULL,
  `src_media_uid` int(11) DEFAULT NULL,
  `caption` varchar(255) DEFAULT NULL,
  `media_type_id` int(10) unsigned NOT NULL,
  `media_url` varchar(255) NOT NULL,
  `thumb_url` varchar(255) DEFAULT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `listing_agent_id` (`listing_agent_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `agent_media`
--

INSERT INTO `agent_media` (`id`, `listing_agent_id`, `mime_type`, `medium`, `src_media_uid`, `caption`, `media_type_id`, `media_url`, `thumb_url`, `created`, `modified`) VALUES
(1, 1, 'image/jpeg', 'image', NULL, 'Agent', 1, 'http://media.point2.com/p2a/user/6814/31fc/097a/1c17651e5c1ab592db43/w160h120.jpg', '', '2011-12-13 20:39:03', '2011-12-13 20:39:03'),
(2, 1, 'image/jpeg', 'image', NULL, 'Agency', 1, 'http://media.point2.com/p2a/agency/b25d/4f61/e7dd/3ed130e9a082149183b6/w160h120.jpg', '', '2011-12-13 20:39:03', '2011-12-13 20:39:03'),
(3, 2, 'image/jpeg', 'image', NULL, 'Agent', 1, 'http://media.point2.com/p2a/user/a3a1/0d9f/859e/7595d6a04e4d378bf260/w160h120.jpg', '', '2011-12-13 20:39:05', '2011-12-13 20:39:05'),
(4, 2, 'image/jpeg', 'image', NULL, 'Agency', 1, 'http://media.point2.com/p2a/agency/0b0f/adb5/b13a/b296ba2e4e571c630fe7/w160h120.jpg', '', '2011-12-13 20:39:05', '2011-12-13 20:39:05'),
(5, 3, 'image/jpeg', 'image', NULL, 'Agent', 1, 'http://media.point2.com/p2a/user/bdd5/b40f/df82/787a2afe8f8f5f791667/w160h120.jpg', '', '2011-12-13 20:39:06', '2011-12-13 20:39:06'),
(6, 3, 'image/jpeg', 'image', NULL, 'Agency', 1, 'http://media.point2.com/p2a/agency/ba68/af58/15d9/c31fdf14d567315a86f9/w160h120.jpg', '', '2011-12-13 20:39:06', '2011-12-13 20:39:06'),
(7, 4, 'image/jpeg', 'image', NULL, 'Agent', 1, 'http://media.point2.com/p2a/user/644a/39bd/8d2e/21b4517493bbffb9042d/w160h120.jpg', '', '2011-12-13 20:39:09', '2011-12-13 20:39:09'),
(8, 4, 'image/jpeg', 'image', NULL, 'Agency', 1, 'http://media.point2.com/p2a/agency/0ccc/06d2/4b20/038b1d80fae0c682f7d1/w160h120.jpg', '', '2011-12-13 20:39:09', '2011-12-13 20:39:09'),
(9, 5, 'image/jpeg', 'image', NULL, 'Agent', 1, 'http://media.point2.com/p2a/user/606a/1334/b522/a6655007810897c95b54/w160h120.jpg', '', '2011-12-13 20:39:10', '2011-12-13 20:39:10'),
(10, 5, 'image/jpeg', 'image', NULL, 'Agency', 1, 'http://media.point2.com/p2a/agency/37ee/d858/7c30/13cb7e8a3784c77d04e5/w160h120.jpg', '', '2011-12-13 20:39:10', '2011-12-13 20:39:10'),
(11, 9, 'image/jpeg', 'image', NULL, 'Agent', 1, 'http://media.point2.com/p2a/user/b75f/66a2/cdd4/8892a0c4a43ba788b0e8/w160h120.jpg', '', '2011-12-13 20:39:14', '2011-12-13 20:39:14'),
(12, 9, 'image/jpeg', 'image', NULL, 'Agency', 1, 'http://media.point2.com/p2a/agency/7d3d/f719/9321/5f6128f47e6d1070997d/w160h120.jpg', '', '2011-12-13 20:39:14', '2011-12-13 20:39:14');

-- --------------------------------------------------------

--
-- Table structure for table `aros`
--

CREATE TABLE IF NOT EXISTS `aros` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) DEFAULT NULL,
  `model` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `foreign_key` int(10) DEFAULT NULL,
  `alias` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `lft` int(10) DEFAULT NULL,
  `rght` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `aros`
--

INSERT INTO `aros` (`id`, `parent_id`, `model`, `foreign_key`, `alias`, `lft`, `rght`) VALUES
(1, NULL, 'Group', 1, NULL, 1, 2),
(2, NULL, 'Group', 2, NULL, 3, 6),
(3, NULL, 'Group', 3, NULL, 7, 8),
(4, NULL, 'Group', 4, NULL, 9, 10),
(5, 2, 'User', 2, NULL, 4, 5);

-- --------------------------------------------------------

--
-- Table structure for table `aros_acos`
--

CREATE TABLE IF NOT EXISTS `aros_acos` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `aro_id` int(10) NOT NULL,
  `aco_id` int(10) NOT NULL,
  `_create` varchar(2) CHARACTER SET latin1 NOT NULL DEFAULT '0',
  `_read` varchar(2) CHARACTER SET latin1 NOT NULL DEFAULT '0',
  `_update` varchar(2) CHARACTER SET latin1 NOT NULL DEFAULT '0',
  `_delete` varchar(2) CHARACTER SET latin1 NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ARO_ACO_KEY` (`aro_id`,`aco_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `aros_acos`
--


-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE IF NOT EXISTS `countries` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `short_name` varchar(10) NOT NULL,
  `long_name` varchar(60) NOT NULL,
  PRIMARY KEY (`id`),
  FULLTEXT KEY `short_name` (`short_name`),
  FULLTEXT KEY `long_name` (`long_name`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=240 ;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `short_name`, `long_name`) VALUES
(1, 'AF', 'Afghanistan'),
(2, 'AL', 'Albania'),
(3, 'DZ', 'Algeria'),
(4, 'AS', 'American Samoa'),
(5, 'AD', 'Andorra'),
(6, 'AO', 'Angola'),
(7, 'AI', 'Anguilla'),
(8, 'AQ', 'Antarctica'),
(9, 'AG', 'Antigua and Barbuda'),
(10, 'AR', 'Argentina'),
(11, 'AM', 'Armenia'),
(12, 'AW', 'Aruba'),
(13, 'AU', 'Australia'),
(14, 'AT', 'Austria'),
(15, 'AZ', 'Azerbaijan'),
(16, 'BS', 'Bahamas'),
(17, 'BH', 'Bahrain'),
(18, 'BD', 'Bangladesh'),
(19, 'BB', 'Barbados'),
(20, 'BY', 'Belarus'),
(21, 'BE', 'Belgium'),
(22, 'BZ', 'Belize'),
(23, 'BJ', 'Benin'),
(24, 'BM', 'Bermuda'),
(25, 'BT', 'Bhutan'),
(26, 'BO', 'Bolivia'),
(27, 'BA', 'Bosnia and Herzegowina'),
(28, 'BW', 'Botswana'),
(29, 'BV', 'Bouvet Island'),
(30, 'BR', 'Brazil'),
(31, 'IO', 'British Indian Ocean Territory'),
(32, 'BN', 'Brunei Darussalam'),
(33, 'BG', 'Bulgaria'),
(34, 'BF', 'Burkina Faso'),
(35, 'BI', 'Burundi'),
(36, 'KH', 'Cambodia'),
(37, 'CM', 'Cameroon'),
(38, 'CA', 'Canada'),
(39, 'CV', 'Cape Verde'),
(40, 'KY', 'Cayman Islands'),
(41, 'CF', 'Central African Republic'),
(42, 'TD', 'Chad'),
(43, 'CL', 'Chile'),
(44, 'CN', 'China'),
(45, 'CX', 'Christmas Island'),
(46, 'CC', 'Cocos (Keeling) Islands'),
(47, 'CO', 'Colombia'),
(48, 'KM', 'Comoros'),
(49, 'CG', 'Congo'),
(50, 'CD', 'Congo, the Democratic Republic of the'),
(51, 'CK', 'Cook Islands'),
(52, 'CR', 'Costa Rica'),
(53, 'CI', 'Cote d''Ivoire'),
(54, 'HR', 'Croatia (Hrvatska)'),
(55, 'CU', 'Cuba'),
(56, 'CY', 'Cyprus'),
(57, 'CZ', 'Czech Republic'),
(58, 'DK', 'Denmark'),
(59, 'DJ', 'Djibouti'),
(60, 'DM', 'Dominica'),
(61, 'DO', 'Dominican Republic'),
(62, 'TP', 'East Timor'),
(63, 'EC', 'Ecuador'),
(64, 'EG', 'Egypt'),
(65, 'SV', 'El Salvador'),
(66, 'GQ', 'Equatorial Guinea'),
(67, 'ER', 'Eritrea'),
(68, 'EE', 'Estonia'),
(69, 'ET', 'Ethiopia'),
(70, 'FK', 'Falkland Islands (Malvinas)'),
(71, 'FO', 'Faroe Islands'),
(72, 'FJ', 'Fiji'),
(73, 'FI', 'Finland'),
(74, 'FR', 'France'),
(75, 'FX', 'France, Metropolitan'),
(76, 'GF', 'French Guiana'),
(77, 'PF', 'French Polynesia'),
(78, 'TF', 'French Southern Territories'),
(79, 'GA', 'Gabon'),
(80, 'GM', 'Gambia'),
(81, 'GE', 'Georgia'),
(82, 'DE', 'Germany'),
(83, 'GH', 'Ghana'),
(84, 'GI', 'Gibraltar'),
(85, 'GR', 'Greece'),
(86, 'GL', 'Greenland'),
(87, 'GD', 'Grenada'),
(88, 'GP', 'Guadeloupe'),
(89, 'GU', 'Guam'),
(90, 'GT', 'Guatemala'),
(91, 'GN', 'Guinea'),
(92, 'GW', 'Guinea-Bissau'),
(93, 'GY', 'Guyana'),
(94, 'HT', 'Haiti'),
(95, 'HM', 'Heard and Mc Donald Islands'),
(96, 'VA', 'Holy See (Vatican City State)'),
(97, 'HN', 'Honduras'),
(98, 'HK', 'Hong Kong'),
(99, 'HU', 'Hungary'),
(100, 'IS', 'Iceland'),
(101, 'IN', 'India'),
(102, 'ID', 'Indonesia'),
(103, 'IR', 'Iran (Islamic Republic of)'),
(104, 'IQ', 'Iraq'),
(105, 'IE', 'Ireland'),
(106, 'IL', 'Israel'),
(107, 'IT', 'Italy'),
(108, 'JM', 'Jamaica'),
(109, 'JP', 'Japan'),
(110, 'JO', 'Jordan'),
(111, 'KZ', 'Kazakhstan'),
(112, 'KE', 'Kenya'),
(113, 'KI', 'Kiribati'),
(114, 'KP', 'Korea, Democratic People''s Republic of'),
(115, 'KR', 'Korea, Republic of'),
(116, 'KW', 'Kuwait'),
(117, 'KG', 'Kyrgyzstan'),
(118, 'LA', 'Lao People''s Democratic Republic'),
(119, 'LV', 'Latvia'),
(120, 'LB', 'Lebanon'),
(121, 'LS', 'Lesotho'),
(122, 'LR', 'Liberia'),
(123, 'LY', 'Libyan Arab Jamahiriya'),
(124, 'LI', 'Liechtenstein'),
(125, 'LT', 'Lithuania'),
(126, 'LU', 'Luxembourg'),
(127, 'MO', 'Macau'),
(128, 'MK', 'Macedonia'),
(129, 'MG', 'Madagascar'),
(130, 'MW', 'Malawi'),
(131, 'MY', 'Malaysia'),
(132, 'MV', 'Maldives'),
(133, 'ML', 'Mali'),
(134, 'MT', 'Malta'),
(135, 'MH', 'Marshall Islands'),
(136, 'MQ', 'Martinique'),
(137, 'MR', 'Mauritania'),
(138, 'MU', 'Mauritius'),
(139, 'YT', 'Mayotte'),
(140, 'MX', 'Mexico'),
(141, 'FM', 'Micronesia, Federated States of'),
(142, 'MD', 'Moldova, Republic of'),
(143, 'MC', 'Monaco'),
(144, 'MN', 'Mongolia'),
(145, 'MS', 'Montserrat'),
(146, 'MA', 'Morocco'),
(147, 'MZ', 'Mozambique'),
(148, 'MM', 'Myanmar'),
(149, 'NA', 'Namibia'),
(150, 'NR', 'Nauru'),
(151, 'NP', 'Nepal'),
(152, 'NL', 'Netherlands'),
(153, 'AN', 'Netherlands Antilles'),
(154, 'NC', 'New Caledonia'),
(155, 'NZ', 'New Zealand'),
(156, 'NI', 'Nicaragua'),
(157, 'NE', 'Niger'),
(158, 'NG', 'Nigeria'),
(159, 'NU', 'Niue'),
(160, 'NF', 'Norfolk Island'),
(161, 'MP', 'Northern Mariana Islands'),
(162, 'NO', 'Norway'),
(163, 'OM', 'Oman'),
(164, 'PK', 'Pakistan'),
(165, 'PW', 'Palau'),
(166, 'PA', 'Panama'),
(167, 'PG', 'Papua New Guinea'),
(168, 'PY', 'Paraguay'),
(169, 'PE', 'Peru'),
(170, 'PH', 'Philippines'),
(171, 'PN', 'Pitcairn'),
(172, 'PL', 'Poland'),
(173, 'PT', 'Portugal'),
(174, 'PR', 'Puerto Rico'),
(175, 'QA', 'Qatar'),
(176, 'RE', 'Reunion'),
(177, 'RO', 'Romania'),
(178, 'RU', 'Russian Federation'),
(179, 'RW', 'Rwanda'),
(180, 'KN', 'Saint Kitts and Nevis'),
(181, 'LC', 'Saint LUCIA'),
(182, 'VC', 'Saint Vincent and the Grenadines'),
(183, 'WS', 'Samoa'),
(184, 'SM', 'San Marino'),
(185, 'ST', 'Sao Tome and Principe'),
(186, 'SA', 'Saudi Arabia'),
(187, 'SN', 'Senegal'),
(188, 'SC', 'Seychelles'),
(189, 'SL', 'Sierra Leone'),
(190, 'SG', 'Singapore'),
(191, 'SK', 'Slovakia (Slovak Republic)'),
(192, 'SI', 'Slovenia'),
(193, 'SB', 'Solomon Islands'),
(194, 'SO', 'Somalia'),
(195, 'ZA', 'South Africa'),
(196, 'GS', 'South Georgia and the South Sandwich Islands'),
(197, 'ES', 'Spain'),
(198, 'LK', 'Sri Lanka'),
(199, 'SH', 'St. Helena'),
(200, 'PM', 'St. Pierre and Miquelon'),
(201, 'SD', 'Sudan'),
(202, 'SR', 'Suriname'),
(203, 'SJ', 'Svalbard and Jan Mayen Islands'),
(204, 'SZ', 'Swaziland'),
(205, 'SE', 'Sweden'),
(206, 'CH', 'Switzerland'),
(207, 'SY', 'Syrian Arab Republic'),
(208, 'TW', 'Taiwan, Province of China'),
(209, 'TJ', 'Tajikistan'),
(210, 'TZ', 'Tanzania, United Republic of'),
(211, 'TH', 'Thailand'),
(212, 'TG', 'Togo'),
(213, 'TK', 'Tokelau'),
(214, 'TO', 'Tonga'),
(215, 'TT', 'Trinidad and Tobago'),
(216, 'TN', 'Tunisia'),
(217, 'TR', 'Turkey'),
(218, 'TM', 'Turkmenistan'),
(219, 'TC', 'Turks and Caicos Islands'),
(220, 'TV', 'Tuvalu'),
(221, 'UG', 'Uganda'),
(222, 'UA', 'Ukraine'),
(223, 'AE', 'United Arab Emirates'),
(224, 'GB', 'United Kingdom'),
(225, 'US', 'United States'),
(226, 'UM', 'United States Minor Outlying Islands'),
(227, 'UY', 'Uruguay'),
(228, 'UZ', 'Uzbekistan'),
(229, 'VU', 'Vanuatu'),
(230, 'VE', 'Venezuela'),
(231, 'VN', 'Viet Nam'),
(232, 'VG', 'Virgin Islands (British)'),
(233, 'VI', 'Virgin Islands (U.S.)'),
(234, 'WF', 'Wallis and Futuna Islands'),
(235, 'EH', 'Western Sahara'),
(236, 'YE', 'Yemen'),
(237, 'YU', 'Yugoslavia'),
(238, 'ZM', 'Zambia'),
(239, 'ZW', 'Zimbabwe');

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE IF NOT EXISTS `groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) CHARACTER SET latin1 NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`, `created`, `modified`) VALUES
(1, 'users', '2011-12-07 15:12:09', '2011-12-07 15:12:09'),
(2, 'agents', '2011-12-07 15:12:13', '2011-12-07 15:12:13'),
(3, 'admins', '2011-12-07 15:12:16', '2011-12-07 15:12:27'),
(4, 'superusers', '2011-12-07 15:12:22', '2011-12-07 15:12:22');

-- --------------------------------------------------------

--
-- Table structure for table `listings`
--

CREATE TABLE IF NOT EXISTS `listings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `source_id` int(11) DEFAULT NULL,
  `listing_status_id` int(11) DEFAULT NULL,
  `listing_category` tinyint(3) unsigned NOT NULL,
  `listing_subcategory` tinyint(3) unsigned NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_listings_on_source_id` (`source_id`),
  KEY `index_listings_on_user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `listings`
--

INSERT INTO `listings` (`id`, `uuid`, `source_id`, `listing_status_id`, `listing_category`, `listing_subcategory`, `user_id`, `created`, `modified`) VALUES
(1, '4ee7fe36-cda8-40f9-9035-13bb0882eaeb', 1, 3, 1, 0, 1, '2011-12-13 20:39:02', '2011-12-13 20:39:02'),
(2, '4ee7fe37-756c-466f-a68f-13bb0882eaeb', 1, 3, 1, 0, 2, '2011-12-13 20:39:03', '2011-12-13 20:39:03'),
(3, '4ee7fe39-f088-4e8e-8827-13bb0882eaeb', 1, 3, 1, 0, 3, '2011-12-13 20:39:05', '2011-12-13 20:39:05'),
(4, '4ee7fe3b-6318-4596-b961-13bb0882eaeb', 1, 1, 1, 0, 4, '2011-12-13 20:39:07', '2011-12-13 20:39:07'),
(5, '4ee7fe3d-ff84-465c-8f41-13bb0882eaeb', 1, 2, 2, 0, 5, '2011-12-13 20:39:09', '2011-12-13 20:39:09'),
(6, '4ee7fe3e-1d8c-4ac1-951e-13bb0882eaeb', 1, 1, 1, 0, 6, '2011-12-13 20:39:10', '2011-12-13 20:39:10'),
(7, '4ee7fe40-e930-4d49-8041-13bb0882eaeb', 1, 1, 1, 0, 7, '2011-12-13 20:39:12', '2011-12-13 20:39:12'),
(8, '4ee7fe41-58ac-46f2-85d3-13bb0882eaeb', 1, 1, 1, 0, 8, '2011-12-13 20:39:13', '2011-12-13 20:39:13'),
(9, '4ee7fe41-7ed0-40c8-89b6-13bb0882eaeb', 1, 1, 1, 0, 9, '2011-12-13 20:39:13', '2011-12-13 20:39:13'),
(10, '4ee7fe42-0c74-44b0-ba4c-13bb0882eaeb', 1, 1, 1, 0, 10, '2011-12-13 20:39:14', '2011-12-13 20:39:14');

-- --------------------------------------------------------

--
-- Table structure for table `listing_agents`
--

CREATE TABLE IF NOT EXISTS `listing_agents` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `uuid` varchar(255) DEFAULT NULL,
  `src_agent_uid` varchar(255) DEFAULT NULL,
  `src_office_uid` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `contact_cell` varchar(255) DEFAULT NULL,
  `contact_office_phone` varchar(255) DEFAULT NULL,
  `contact_fax` varchar(255) DEFAULT NULL,
  `website` varchar(255) DEFAULT NULL,
  `twitter` varchar(255) DEFAULT NULL,
  `facebook` varchar(255) DEFAULT NULL,
  `linkedin` varchar(255) DEFAULT NULL,
  `youtube` varchar(255) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_listing_agents_on_user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `listing_agents`
--

INSERT INTO `listing_agents` (`id`, `user_id`, `uuid`, `src_agent_uid`, `src_office_uid`, `name`, `contact_cell`, `contact_office_phone`, `contact_fax`, `website`, `twitter`, `facebook`, `linkedin`, `youtube`, `created`, `modified`) VALUES
(1, 0, '4ee7fe36-e550-4c0c-8117-13bb0882eaeb', '163817', '64996', 'Duane Neufeldt', '(306) 948-8055', NULL, '(306) 948-2763', NULL, NULL, NULL, NULL, NULL, '2011-12-13 20:39:02', '2011-12-13 20:39:02'),
(2, 0, '4ee7fe37-d780-49b8-879b-13bb0882eaeb', '194994', '21268', 'Roy  Hjelte', '(306) 527-7778', NULL, '(306) 761-1499', NULL, NULL, NULL, NULL, NULL, '2011-12-13 20:39:03', '2011-12-13 20:39:03'),
(3, 0, '4ee7fe39-7de8-46b2-99b1-13bb0882eaeb', '32980', '5317', 'Susan Romanello', '(386)569-1569', NULL, '(386)672-9524', NULL, NULL, NULL, NULL, NULL, '2011-12-13 20:39:05', '2011-12-13 20:39:05'),
(4, 0, '4ee7fe3b-bf78-4bd3-a978-13bb0882eaeb', '2390', '14261', 'Cyd  Weeks', '386-793-7302', NULL, '386-951-1102', NULL, NULL, NULL, NULL, NULL, '2011-12-13 20:39:07', '2011-12-13 20:39:07'),
(5, 0, '4ee7fe3d-5098-468f-b1df-13bb0882eaeb', '156380', '55244', 'Lisa Blake', '248-224-9237', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2011-12-13 20:39:09', '2011-12-13 20:39:09'),
(6, 0, '4ee7fe3e-42a8-4dad-9bbe-13bb0882eaeb', '139495', '51735', 'Amanda Salmon', '(907) 841-2124', NULL, '(907) 352-5151', NULL, NULL, NULL, NULL, NULL, '2011-12-13 20:39:10', '2011-12-13 20:39:10'),
(7, 0, '4ee7fe40-6b6c-48dd-9bd6-13bb0882eaeb', '100313', '42893', 'Rita W. Thompson', '(615) 400-7865', NULL, '(615) 893-3246', NULL, NULL, NULL, NULL, NULL, '2011-12-13 20:39:12', '2011-12-13 20:39:12'),
(8, 0, '4ee7fe41-9560-455d-a217-13bb0882eaeb', '99451', '43039', 'Jim Brinkley', '(615) 476-0636', NULL, '(615) 672-2218', NULL, NULL, NULL, NULL, NULL, '2011-12-13 20:39:13', '2011-12-13 20:39:13'),
(9, 0, '4ee7fe41-ea68-4b0b-a27d-13bb0882eaeb', '17670', '29072', 'Lucy Sarubbi', '(813) 300-1645', NULL, '(813) 774-7824', NULL, NULL, NULL, NULL, NULL, '2011-12-13 20:39:13', '2011-12-13 20:39:13'),
(10, 0, '4ee7fe42-2828-4ccd-96d0-13bb0882eaeb', '147687', '53646', 'Carol Coffin', '508-560-2916', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2011-12-13 20:39:14', '2011-12-13 20:39:14');

-- --------------------------------------------------------

--
-- Table structure for table `listing_details`
--

CREATE TABLE IF NOT EXISTS `listing_details` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `listing_id` int(10) unsigned NOT NULL,
  `title` varchar(255) CHARACTER SET latin1 NOT NULL,
  `description` text CHARACTER SET latin1 NOT NULL,
  `detail_view_url` text CHARACTER SET latin1,
  `virtual_tour_url` text CHARACTER SET latin1,
  `broker_name` varchar(60) CHARACTER SET latin1 DEFAULT NULL,
  `property_type_id` int(10) unsigned NOT NULL,
  `property_style_id` int(10) unsigned NOT NULL,
  `lot_legal_name` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `lot_comment` text CHARACTER SET latin1,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `listing_id` (`listing_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `listing_details`
--

INSERT INTO `listing_details` (`id`, `listing_id`, `title`, `description`, `detail_view_url`, `virtual_tour_url`, `broker_name`, `property_type_id`, `property_style_id`, `lot_legal_name`, `lot_comment`, `created`, `modified`) VALUES
(1, 1, 'Farms and Acreages for Sale in Biggar No. 347, Sheldon Watson Grazing Package, Saskatchewan $105,000', 'Three excellent quarters of rolling pasture land 8 miles southwest of Biggar Saskatchewan.<br> <br>This grazing package is includes 320 acres of deeded land and 160 acres of crown lease land. <br><br>Pasture package contains 2 dugouts, fence and the carrying capacity of approximately 40 cow calf pairs for 6 months.<br><br>Land Taxes on the 3 quarters - $477.00 for 2009.   <br><br>Crown Lease Rent - $251.00 for 2009. <br><br>Crown Pasture Lease Details:<br>The Seller has a long term grazing lease with the provincial government.  These 160 acres, which is beside the southwest end of the deeded land, is entirely comprised of native pasture. The grazing lease expires December 31, 2025. The Seller will transfer this attractive lease to qualifying purchasers.<br><br>See photo of Biggar  R.M. 347 Map Section 17-35-15 W3 for the “potential full package” that is available.<br><br/><br/>Brokered And Advertised By: RE/MAX<br/>Listing Agent: Duane Neufeldt', 'http://platform.point2.com/Report/lct.ashx?lid=10104335&aid=XX&url=http%3A%2F%2Fwww.duaneneufeldt.com%2FListing%2FViewListingDetails.aspx%3FListingID%3D10104335%26Bb%3DXX%26Cc%3D10104335', 'http://www.duaneneufeldt.com/Listing/VirtualTour.aspx?ListingID=10104335', NULL, 1, 1, NULL, 'Additional 160 "Crown Lease" acres available', '2011-12-13 20:39:02', '2011-12-13 20:39:02'),
(2, 2, 'Farms and Acreages for Sale in Rural SE Saskatchewan, Jansen, Saskatchewan $795,000', 'Cattleman’s Dream ! !<br><br>This incredible ranch features 20 quarters in a block. 10 quarters are deeded and 10 quarters are leased (9 from the provincial government on preferential terms). This ranching package is fenced and cross-fenced possessing ample water with 10 flowing wells and 4 springs. With access to approx. 250 acres of lakes and ponds, it is no more than half a mile from anywhere on the ranch to plentiful, fresh, clear water for the cattle to drink. Gravel is readily available from pits on the property.<br><br>Residence<br>The 1,050 SqFt house (1993) shows nicely with 2 bedrooms and 1 bathroom. It is heated by fuel oil heat. <br><br>Cattle Facilities<br>- 30 x 40 Storage Shed (1998), metal clad with 16 x 30 insulated & heated (electricity) area<br>- 36 x 40 Barn (metal clad), 4 box stalls (steel pipe 7’ high) including maternity pen with head gate, small insulated room, wired<br>- Three 32 x 64 Open Front Shelters , metal clad, lined with planks 5’ high<br>- One 32 x 20 Open Front Shelter, metal clad, lined with planks 5’ high<br>- extensive cattle handling facilities with loading facilities, sorting pens, lanes, 3 watering bowls<br>- Penning includes 15 various sized pens. The smaller pens are about 50 x 70 and the largest ones cover approx. 2 acres. The exposed sides are covered with slabs for a windbreak. <br>- All gates are made of steel<br>- All corals & sheds have been built between 1994 & 2002.<br><br>One well in the yard is capped and there is enough pressure from it to supply water to the three watering bowls and five hydrants (approx. 40 igpm). <br><br>Capacity (approx.)<br>- 350 cow/calf with feed<br>- 1200 Yearlings (grazing hay acres)<br><br>Partial herd is negotiable (150 red angus cows)<br><br>*** See Seller''s Comments Tab for YouTube Video ***<br/><br/>Brokered And Advertised By: Tim Hammond Realty<br/>Listing Agent: Roy  Hjelte', 'http://platform.point2.com/Report/lct.ashx?lid=10071262&aid=XX&url=http%3A%2F%2Fwww.timhammond.ca%2FListing%2FViewListingDetails.aspx%3FListingID%3D10071262%26Bb%3DXX%26Cc%3D10071262', 'http://www.timhammond.ca/Listing/VirtualTour.aspx?ListingID=10071262', NULL, 1, 2, NULL, '1,333 ac Deeded & 1,516 ac. Leased', '2011-12-13 20:39:03', '2011-12-13 20:39:03'),
(3, 3, 'Homes for Sale in Marineland Acres, Marineland, Florida $229,900', 'Reduced! 2004 beautiful BEACH HOME!! Bring your boat! Simply beachy with clean lines, volume ceilings, all tile floors, and rounded corners in a bright, airy space.  3/2/2 WITH private community INTRACOASTAL DOCK AND PVT OCEAN WALKOVER--! CLEAN, BRIGHT AND AIRY HOME WITH, PVT WOODED BACK YARD--Plenty room for a pool! CUL-DE-SAC BLOCK. BACKS TO utility easement for privacy.  HIS/HER CLOSETS IN MASTER W/3 sets of FRENCH DOORS from Master, Living Room and Kitchen TO  ENCL SCREENED PATIO,   FULL DINING ROOM AND FULL LIVING ROOM!  MOVE IN CONDITION! NEAR MARINELAND AND PUBLIX, PUBLIC BOAT LAUNCH TOO!  EASY ACCESS TO ST AUGUSTINE AND DAYTONA. OFF SCENIC A1A!<br/><br/>Brokered And Advertised By: A1A Realty & Dev., Inc.<br/>Listing Agent: Susan Romanello', 'http://platform.point2.com/Report/lct.ashx?lid=10041758&aid=XX&url=http%3A%2F%2Fsusanromanello.point2agent.com%2FListing%2FViewListingDetails.aspx%3FListingID%3D10041758%26Bb%3DXX%26Cc%3D10041758', 'http://susanromanello.point2agent.com/Listing/VirtualTour.aspx?ListingID=10041758', NULL, 2, 3, 'Maritime Estates I', NULL, '2011-12-13 20:39:06', '2011-12-13 20:39:06'),
(4, 4, 'Homes for Sale in Ocean Hammock-Northshore, Palm Coast, Florida $849,900', 'HIgh end executive home in the exclusive gated community of Ocean Hammock with ocean view, lake view, 2 balconies, upgraded construction sitting on a corner lot.  <br>Can''t get that Jumbo Loan?  OWNER WILL HOLD to assist!<br>High pillars greet you when you drive into the circular driveway.  Italian marble and solid cherry hardwood flooring with high ceilings great you as you walk in.  Large open kitchen over looking the family room with built in cabinetry.  Upgraded 42" cabinets in kitchen. Each bedroom has it''s own bath, with 1/2 bath off living room for guests.  Trey ceilings.  Bedroom downstairs is perfect for mother in law or guests.  Tankless hot water system.  Too many extras to list, you have to see this beauty. All appliances stay. Large back patio with pool for elegant entertaining.   Membership upgradable to full golf package.  All offers will be considered.   <br><br>listing courtesy Art Ryan, Watson Realty.<br/><br/>Brokered And Advertised By: Palmcoasting.com Real Estate Corp<br/>Listing Agent: Cyd  Weeks', 'http://platform.point2.com/Report/lct.ashx?lid=10009621&aid=XX&url=http%3A%2F%2Fwww.palmcoasting.com%2FListing%2FViewListingDetails.aspx%3FListingID%3D10009621%26Bb%3DXX%26Cc%3D10009621', 'http://www.palmcoasting.com/Listing/VirtualTour.aspx?ListingID=10009621', NULL, 2, 4, NULL, 'corner lot', '2011-12-13 20:39:07', '2011-12-13 20:39:07'),
(5, 6, 'Lot/land for sale in Trapper Creek, AK 44,000 USD', 'Pristine privacy w/ endless recreational opportunities! Over 130 lots to choose from. 4000Ft. Airstrip access and Road Access in the Summer months! Historical gold mining area - recreational gold mining allowed on your lot(BTV). Beautiful country, abounding wildlife, recreational riding! Owner Finance Available! Make Sure to Check Out the VIDEO in the Photos Section!!<br/><br/>Brokered And Advertised By: CENTURY 21 North Homes Realty - Commercial<br/>Listing Agent: Amanda Salmon', 'http://platform.point2.com/Report/lct.ashx?lid=1000545916&aid=XX&url=http%3A%2F%2Famandasalmon.point2agent.com%2FListing%2FViewListingDetails.aspx%3FListingID%3D1000545916%26Bb%3DXX%26Cc%3D1000545916', NULL, NULL, 3, 5, '5331000L002', 'Size in Sqft: 399880.80, Size in Acres: 9.18', '2011-12-13 20:39:10', '2011-12-13 20:39:10'),
(6, 7, 'Lot/land for sale in Murfreesboro, TN 45,000 USD', 'NOT YOUR USUAL LOTS!  Catering to your CUSTOM Builders and clients.  Section III of LIBERTY VALLEY has much wider Road Frontage that  allows for rear entry garages.<br/><br/>Brokered And Advertised By: Coldwell Banker Snow & Wall<br/>Listing Agent: Rita W. Thompson', 'http://platform.point2.com/Report/lct.ashx?flid=1000228459&aid=XX&url=http%3A%2F%2Flistings.point2.com%2F1000228459', NULL, NULL, 3, 6, NULL, 'Dimensions: 103 x 125', '2011-12-13 20:39:12', '2011-12-13 20:39:12'),
(7, 8, 'Lot/land for sale in White House, TN 91,500 USD', 'HAS BEEN APPROVED FOR 3 BR SEPTIC SYSTEM;<br/><br/>Brokered And Advertised By: Jim Brinkley, REALTORS, Inc.<br/>Listing Agent: Jim Brinkley', 'http://platform.point2.com/Report/lct.ashx?lid=1000228483&aid=XX&url=http%3A%2F%2Fjimbrinkley.point2agent.com%2FListing%2FViewListingDetails.aspx%3FListingID%3D1000228483%26Bb%3DXX%26Cc%3D1000228483', NULL, NULL, 3, 7, NULL, NULL, '2011-12-13 20:39:13', '2011-12-13 20:39:13');

-- --------------------------------------------------------

--
-- Table structure for table `listing_features`
--

CREATE TABLE IF NOT EXISTS `listing_features` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `listing_id` int(10) unsigned NOT NULL,
  `year_built` smallint(5) unsigned DEFAULT NULL,
  `bathrooms` tinyint(3) unsigned DEFAULT NULL,
  `bathroom_comments` varchar(255) DEFAULT NULL,
  `bedrooms` tinyint(3) unsigned DEFAULT NULL,
  `bedroom_comments` varchar(255) DEFAULT NULL,
  `garage_stalls` tinyint(3) unsigned DEFAULT NULL,
  `garage_style` varchar(255) DEFAULT NULL,
  `garage_comments` varchar(255) DEFAULT NULL,
  `living_area` decimal(10,0) DEFAULT NULL,
  `living_area_units` varchar(15) DEFAULT NULL,
  `other_features` text,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `listing_features`
--

INSERT INTO `listing_features` (`id`, `listing_id`, `year_built`, `bathrooms`, `bathroom_comments`, `bedrooms`, `bedroom_comments`, `garage_stalls`, `garage_style`, `garage_comments`, `living_area`, `living_area_units`, `other_features`, `created`, `modified`) VALUES
(1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'a:0:{}', '2011-12-13 20:39:02', '2011-12-13 20:39:02'),
(2, 2, 1, 1, '1', 2, '2', NULL, NULL, NULL, 1050, 'square feet', 'a:0:{}', '2011-12-13 20:39:03', '2011-12-13 20:39:03'),
(3, 3, 2, 2, '2', 3, '3', 2, 'Detached', 'SIDE LOAD GARAGE', 1730, 'square feet', 'a:0:{}', '2011-12-13 20:39:06', '2011-12-13 20:39:06'),
(4, 4, 2003, 4, 'and one half', 4, 'one down stairs for ''mom''', 3, 'Attached', 'double door', 4672, 'square feet', 'a:0:{}', '2011-12-13 20:39:07', '2011-12-13 20:39:07'),
(5, 5, 1977, 1, '1', 1, '1', 1, 'Detached', 'CARPORT', 793, 'square feet', 'a:0:{}', '2011-12-13 20:39:09', '2011-12-13 20:39:09'),
(6, 6, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'a:0:{}', '2011-12-13 20:39:10', '2011-12-13 20:39:10'),
(7, 7, NULL, 0, '0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'a:0:{}', '2011-12-13 20:39:12', '2011-12-13 20:39:12'),
(8, 8, NULL, 0, '0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'a:0:{}', '2011-12-13 20:39:13', '2011-12-13 20:39:13'),
(9, 9, 1, 2, '2', 3, '3', 1, 'Detached', NULL, 1156, 'square feet', 'a:0:{}', '2011-12-13 20:39:13', '2011-12-13 20:39:13'),
(10, 10, 1, 2, '2', 3, '3', NULL, NULL, NULL, 1554, 'square feet', 'a:0:{}', '2011-12-13 20:39:14', '2011-12-13 20:39:14');

-- --------------------------------------------------------

--
-- Table structure for table `listing_financials`
--

CREATE TABLE IF NOT EXISTS `listing_financials` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `listing_id` int(10) unsigned NOT NULL,
  `price_amount` decimal(10,2) NOT NULL,
  `price_currency` varchar(15) CHARACTER SET latin1 NOT NULL COMMENT 'Must be same as tax currency if any',
  `price_period` varchar(25) CHARACTER SET latin1 DEFAULT NULL,
  `tax_amount` decimal(10,2) DEFAULT NULL COMMENT 'Must be same as price currency',
  `tax_currency` varchar(15) CHARACTER SET latin1 DEFAULT NULL,
  `tax_year` int(10) unsigned DEFAULT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `listing_id` (`listing_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `listing_financials`
--

INSERT INTO `listing_financials` (`id`, `listing_id`, `price_amount`, `price_currency`, `price_period`, `tax_amount`, `tax_currency`, `tax_year`, `created`, `modified`) VALUES
(1, 1, 105000.00, 'CAD', '0', 477.00, 'CAD', 2009, '2011-12-13 20:39:02', '2011-12-13 20:39:02'),
(2, 2, 795000.00, 'CAD', '0', 1768.00, 'CAD', 2009, '2011-12-13 20:39:03', '2011-12-13 20:39:03'),
(3, 3, 229900.00, 'USD', '0', 2559.00, 'USD', 2008, '2011-12-13 20:39:06', '2011-12-13 20:39:06'),
(4, 4, 849900.00, 'USD', '0', 10000.00, 'USD', 2009, '2011-12-13 20:39:07', '2011-12-13 20:39:07'),
(5, 5, 700.00, 'USD', 'Monthly', NULL, NULL, NULL, '2011-12-13 20:39:09', '2011-12-13 20:39:09'),
(6, 6, 44000.00, 'USD', '0', NULL, NULL, NULL, '2011-12-13 20:39:10', '2011-12-13 20:39:10'),
(7, 7, 45000.00, 'USD', '0', NULL, NULL, NULL, '2011-12-13 20:39:12', '2011-12-13 20:39:12'),
(8, 8, 91500.00, 'USD', '0', NULL, NULL, NULL, '2011-12-13 20:39:13', '2011-12-13 20:39:13'),
(9, 9, 149900.00, 'USD', '0', 2992.00, 'USD', 2006, '2011-12-13 20:39:13', '2011-12-13 20:39:13'),
(10, 10, 1350000.00, 'USD', '0', 3488.00, 'USD', 2009, '2011-12-13 20:39:14', '2011-12-13 20:39:14');

-- --------------------------------------------------------

--
-- Table structure for table `listing_locations`
--

CREATE TABLE IF NOT EXISTS `listing_locations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `listing_id` int(10) unsigned NOT NULL,
  `address` varchar(100) CHARACTER SET latin1 NOT NULL,
  `city` varchar(45) CHARACTER SET latin1 NOT NULL,
  `county` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `state_id` int(10) unsigned NOT NULL,
  `postal_code` varchar(7) CHARACTER SET latin1 NOT NULL,
  `country_id` int(10) unsigned NOT NULL,
  `latitude` varchar(30) CHARACTER SET latin1 DEFAULT NULL,
  `longitude` varchar(30) CHARACTER SET latin1 DEFAULT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  FULLTEXT KEY `city` (`city`),
  FULLTEXT KEY `postal_code` (`postal_code`),
  FULLTEXT KEY `county` (`county`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `listing_locations`
--

INSERT INTO `listing_locations` (`id`, `listing_id`, `address`, `city`, `county`, `state_id`, `postal_code`, `country_id`, `latitude`, `longitude`, `created`, `modified`) VALUES
(1, 1, 'Sheldon Watson R.M. 347 Biggar', 'Sheldon Watson Grazing Package', '', 71, 'SOK OMO', 38, '52.00137', '-108.101735', '2011-12-13 20:39:03', '2011-12-13 20:39:03'),
(2, 2, 'Evenson Ranch - 2,849 acres', 'Jansen', 'Rural SE Saskatchewan', 71, 'S0K 2B0', 38, '51.706894', '-104.615633', '2011-12-13 20:39:03', '2011-12-13 20:39:03'),
(3, 3, '20 BEACHSIDE DR', 'Marineland', 'Flagler County', 12, '32137', 225, '29.65871', '-81.213056', '2011-12-13 20:39:06', '2011-12-13 20:39:06'),
(4, 4, '14  Cypress Wood Dr', 'Palm Coast', 'Flagler County', 12, '32137', 225, '29.589791', '-81.183225', '2011-12-13 20:39:07', '2011-12-13 20:39:07'),
(5, 5, '15999 11 MILE UNIT 2', 'Southfield', 'Oakland County', 27, '48076', 225, '42.488302', '-83.206679', '2011-12-13 20:39:09', '2011-12-13 20:39:09'),
(6, 6, 'L2 Cache Creek Recreational', 'Trapper Creek', '', 2, '99683', 225, NULL, NULL, '2011-12-13 20:39:10', '2011-12-13 20:39:10'),
(7, 7, '207 Liberty Valley', 'Murfreesboro', 'Rutherford County', 50, '37129', 225, '35.886972017218', '-86.394001695966', '2011-12-13 20:39:12', '2011-12-13 20:39:12'),
(8, 8, '436 Fern Valley Rd', 'White House', 'Sumner County', 50, '37188', 225, '36.4619824174955', '-86.6163746226371', '2011-12-13 20:39:13', '2011-12-13 20:39:13'),
(9, 9, '1335 Coolridge Dr', 'Brandon', 'Hillsborough County', 12, '33511', 225, '27.915664', '-82.317157', '2011-12-13 20:39:13', '2011-12-13 20:39:13'),
(10, 10, '3 Eagle Lane', 'Town', '', 26, '02554', 225, '41.278913', '-70.09813', '2011-12-13 20:39:14', '2011-12-13 20:39:14');

-- --------------------------------------------------------

--
-- Table structure for table `listing_media`
--

CREATE TABLE IF NOT EXISTS `listing_media` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `listing_id` int(10) unsigned NOT NULL,
  `mime_type` varchar(20) NOT NULL,
  `medium` varchar(10) NOT NULL,
  `src_media_uid` int(11) DEFAULT NULL,
  `caption` varchar(255) DEFAULT NULL,
  `media_type_id` int(10) unsigned NOT NULL,
  `media_url` varchar(255) NOT NULL,
  `thumb_url` varchar(255) DEFAULT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `listing_id` (`listing_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=165 ;

--
-- Dumping data for table `listing_media`
--

INSERT INTO `listing_media` (`id`, `listing_id`, `mime_type`, `medium`, `src_media_uid`, `caption`, `media_type_id`, `media_url`, `thumb_url`, `created`, `modified`) VALUES
(1, 1, 'image/jpeg', 'jpeg', NULL, 'Land Location in R.M. of Biggar 347', 1, 'http://media.point2.com/cdr/listing/9052/f5b4/8b99/b004e65794e1e562bfd8/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/9052/f5b4/8b99/b004e65794e1e562bfd8/w64h48.jpg', '2011-12-13 20:39:03', '2011-12-13 20:39:03'),
(2, 1, 'image/jpeg', 'jpeg', NULL, 'IMGP0102', 1, 'http://media.point2.com/cdr/listing/cb04/bd0e/818a/1146a88cb8f13fa62eb0/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/cb04/bd0e/818a/1146a88cb8f13fa62eb0/w64h48.jpg', '2011-12-13 20:39:03', '2011-12-13 20:39:03'),
(3, 1, 'image/jpeg', 'jpeg', NULL, 'IMGP0103', 1, 'http://media.point2.com/cdr/listing/bc08/3bdf/891f/f402d8872946b57dc1c2/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/bc08/3bdf/891f/f402d8872946b57dc1c2/w64h48.jpg', '2011-12-13 20:39:03', '2011-12-13 20:39:03'),
(4, 1, 'image/jpeg', 'jpeg', NULL, 'IMGP0104', 1, 'http://media.point2.com/cdr/listing/8ed6/fdca/1f5d/1b04a1e344bbec4fe601/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/8ed6/fdca/1f5d/1b04a1e344bbec4fe601/w64h48.jpg', '2011-12-13 20:39:03', '2011-12-13 20:39:03'),
(5, 1, 'image/jpeg', 'jpeg', NULL, 'IMGP0105', 1, 'http://media.point2.com/cdr/listing/d0a0/5a3c/bc46/a6d4863824882c49fd22/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/d0a0/5a3c/bc46/a6d4863824882c49fd22/w64h48.jpg', '2011-12-13 20:39:03', '2011-12-13 20:39:03'),
(6, 1, 'image/jpeg', 'jpeg', NULL, 'IMGP0106', 1, 'http://media.point2.com/cdr/listing/aff3/a30d/aa31/e3c3c8aea6f4da5a1a00/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/aff3/a30d/aa31/e3c3c8aea6f4da5a1a00/w64h48.jpg', '2011-12-13 20:39:03', '2011-12-13 20:39:03'),
(7, 2, 'image/jpeg', 'jpeg', NULL, 'Evenson Ranch', 1, 'http://media.point2.com/cdr/listing/ecaa/0604/39be/ee90ac53c4262b81a951/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/ecaa/0604/39be/ee90ac53c4262b81a951/w64h48.jpg', '2011-12-13 20:39:03', '2011-12-13 20:39:03'),
(8, 2, 'image/jpeg', 'jpeg', NULL, 'RM 309 Prairie Rose', 1, 'http://media.point2.com/cdr/listing/bb9b/351a/e1ca/467187de2dcbbf142181/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/bb9b/351a/e1ca/467187de2dcbbf142181/w64h48.jpg', '2011-12-13 20:39:03', '2011-12-13 20:39:03'),
(9, 2, 'image/jpeg', 'jpeg', NULL, 'RM Map (deeded land highlighted in green)', 1, 'http://media.point2.com/cdr/listing/1832/ba5b/3341/90b975895379fc832215/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/1832/ba5b/3341/90b975895379fc832215/w64h48.jpg', '2011-12-13 20:39:03', '2011-12-13 20:39:03'),
(10, 2, 'image/jpeg', 'jpeg', NULL, 'RM Map deeded (green) & leased (yellow)', 1, 'http://media.point2.com/cdr/listing/ecaa/0604/39be/ee90ac53c4262b81a951/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/ecaa/0604/39be/ee90ac53c4262b81a951/w64h48.jpg', '2011-12-13 20:39:03', '2011-12-13 20:39:03'),
(11, 2, 'image/jpeg', 'jpeg', NULL, 'Yard Site', 1, 'http://media.point2.com/cdr/listing/9a62/77f8/2d94/6cf240c5a75feb8c53de/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/9a62/77f8/2d94/6cf240c5a75feb8c53de/w64h48.jpg', '2011-12-13 20:39:03', '2011-12-13 20:39:03'),
(12, 2, 'image/jpeg', 'jpeg', NULL, 'Yard', 1, 'http://media.point2.com/cdr/listing/eee7/8350/2da5/96ad875ca934f9a2a7c5/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/eee7/8350/2da5/96ad875ca934f9a2a7c5/w64h48.jpg', '2011-12-13 20:39:04', '2011-12-13 20:39:04'),
(13, 2, 'image/jpeg', 'jpeg', NULL, '36 x 40 Barn (front)', 1, 'http://media.point2.com/cdr/listing/d1ba/adb0/8be1/7b32a0881db112e9952f/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/d1ba/adb0/8be1/7b32a0881db112e9952f/w64h48.jpg', '2011-12-13 20:39:04', '2011-12-13 20:39:04'),
(14, 2, 'image/jpeg', 'jpeg', NULL, '36 x 40 Barn (back)', 1, 'http://media.point2.com/cdr/listing/43af/59c7/33c0/af6c9b631d07de541744/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/43af/59c7/33c0/af6c9b631d07de541744/w64h48.jpg', '2011-12-13 20:39:04', '2011-12-13 20:39:04'),
(15, 2, 'image/jpeg', 'jpeg', NULL, '30 x 40 Storage Shed', 1, 'http://media.point2.com/cdr/listing/6949/fb95/c92c/2fcaea63448fc4281c6d/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/6949/fb95/c92c/2fcaea63448fc4281c6d/w64h48.jpg', '2011-12-13 20:39:04', '2011-12-13 20:39:04'),
(16, 2, 'image/jpeg', 'jpeg', NULL, '30 x 40 Storage Shed', 1, 'http://media.point2.com/cdr/listing/9a8c/404a/c08c/f14e5e00449c70e6f56f/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/9a8c/404a/c08c/f14e5e00449c70e6f56f/w64h48.jpg', '2011-12-13 20:39:04', '2011-12-13 20:39:04'),
(17, 2, 'image/jpeg', 'jpeg', NULL, '16 x 30 heated bay', 1, 'http://media.point2.com/cdr/listing/e6af/8f38/dcff/7c751d4af6ec280ec776/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/e6af/8f38/dcff/7c751d4af6ec280ec776/w64h48.jpg', '2011-12-13 20:39:04', '2011-12-13 20:39:04'),
(18, 2, 'image/jpeg', 'jpeg', NULL, '24 x 30 work shop area', 1, 'http://media.point2.com/cdr/listing/ebf9/1d0e/5c8f/da7677441ecd61cd60ec/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/ebf9/1d0e/5c8f/da7677441ecd61cd60ec/w64h48.jpg', '2011-12-13 20:39:04', '2011-12-13 20:39:04'),
(19, 2, 'image/jpeg', 'jpeg', NULL, 'Open Front Shelter', 1, 'http://media.point2.com/cdr/listing/7f47/c867/259d/4d17e7a73c2f68016e7f/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/7f47/c867/259d/4d17e7a73c2f68016e7f/w64h48.jpg', '2011-12-13 20:39:04', '2011-12-13 20:39:04'),
(20, 2, 'image/jpeg', 'jpeg', NULL, 'Open Front Shelter', 1, 'http://media.point2.com/cdr/listing/c53f/0efa/8234/a273b9b3251a090a2e65/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/c53f/0efa/8234/a273b9b3251a090a2e65/w64h48.jpg', '2011-12-13 20:39:04', '2011-12-13 20:39:04'),
(21, 2, 'image/jpeg', 'jpeg', NULL, '32 x 64 Open Front Shelter', 1, 'http://media.point2.com/cdr/listing/9c61/f330/de2b/c98fe100daae3affe7df/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/9c61/f330/de2b/c98fe100daae3affe7df/w64h48.jpg', '2011-12-13 20:39:04', '2011-12-13 20:39:04'),
(22, 2, 'image/jpeg', 'jpeg', NULL, 'Loading Facilities', 1, 'http://media.point2.com/cdr/listing/c15f/2bdf/37dd/a20c8a71fe4f6b71adbd/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/c15f/2bdf/37dd/a20c8a71fe4f6b71adbd/w64h48.jpg', '2011-12-13 20:39:04', '2011-12-13 20:39:04'),
(23, 2, 'image/jpeg', 'jpeg', NULL, 'Alley & Penning', 1, 'http://media.point2.com/cdr/listing/4446/34c5/ef26/be31e243d25a28fc1975/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/4446/34c5/ef26/be31e243d25a28fc1975/w64h48.jpg', '2011-12-13 20:39:04', '2011-12-13 20:39:04'),
(24, 2, 'image/jpeg', 'jpeg', NULL, 'Water bowl / Artesian well', 1, 'http://media.point2.com/cdr/listing/5b89/5950/192e/b80d169036c0cd619ecd/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/5b89/5950/192e/b80d169036c0cd619ecd/w64h48.jpg', '2011-12-13 20:39:04', '2011-12-13 20:39:04'),
(25, 2, 'image/jpeg', 'jpeg', NULL, 'Abundant Water from Artesion Wells', 1, 'http://media.point2.com/cdr/listing/2d8d/0555/f72b/1918cbf5409ee0a7c71b/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/2d8d/0555/f72b/1918cbf5409ee0a7c71b/w64h48.jpg', '2011-12-13 20:39:04', '2011-12-13 20:39:04'),
(26, 2, 'image/jpeg', 'jpeg', NULL, 'Clover', 1, 'http://media.point2.com/cdr/listing/c332/355c/111a/b32de35f6b32f24f459b/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/c332/355c/111a/b32de35f6b32f24f459b/w64h48.jpg', '2011-12-13 20:39:04', '2011-12-13 20:39:04'),
(27, 2, 'image/jpeg', 'jpeg', NULL, 'Clover / Hay field', 1, 'http://media.point2.com/cdr/listing/f75d/c269/7d75/7c10dc936699eedcc87b/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/f75d/c269/7d75/7c10dc936699eedcc87b/w64h48.jpg', '2011-12-13 20:39:04', '2011-12-13 20:39:04'),
(28, 2, 'image/jpeg', 'jpeg', NULL, 'Complete Fencing & Cross Fenced', 1, 'http://media.point2.com/cdr/listing/e9e0/e478/fb32/80187b42b02c076c17d0/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/e9e0/e478/fb32/80187b42b02c076c17d0/w64h48.jpg', '2011-12-13 20:39:04', '2011-12-13 20:39:04'),
(29, 2, 'image/jpeg', 'jpeg', NULL, 'Hay', 1, 'http://media.point2.com/cdr/listing/da4e/2609/73e7/fb476f76207abe2cdb86/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/da4e/2609/73e7/fb476f76207abe2cdb86/w64h48.jpg', '2011-12-13 20:39:04', '2011-12-13 20:39:04'),
(30, 2, 'image/jpeg', 'jpeg', NULL, 'Hay & Water', 1, 'http://media.point2.com/cdr/listing/20b8/b895/ba9d/a6e5ab6a3e426b159bb8/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/20b8/b895/ba9d/a6e5ab6a3e426b159bb8/w64h48.jpg', '2011-12-13 20:39:05', '2011-12-13 20:39:05'),
(31, 2, 'image/jpeg', 'jpeg', NULL, 'Crown Water', 1, 'http://media.point2.com/cdr/listing/ff36/b63d/0f9f/28fe8076393165bef88a/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/ff36/b63d/0f9f/28fe8076393165bef88a/w64h48.jpg', '2011-12-13 20:39:05', '2011-12-13 20:39:05'),
(32, 2, 'image/jpeg', 'jpeg', NULL, 'Artesian Well in Pasture w/concrete pad', 1, 'http://media.point2.com/cdr/listing/dc2b/2ee9/9fbe/b7610a52279f062083f8/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/dc2b/2ee9/9fbe/b7610a52279f062083f8/w64h48.jpg', '2011-12-13 20:39:05', '2011-12-13 20:39:05'),
(33, 2, 'image/jpeg', 'jpeg', NULL, 'House - side view', 1, 'http://media.point2.com/cdr/listing/b0c0/81d7/66b3/8362a89ec1390cbd619f/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/b0c0/81d7/66b3/8362a89ec1390cbd619f/w64h48.jpg', '2011-12-13 20:39:05', '2011-12-13 20:39:05'),
(34, 2, 'image/jpeg', 'jpeg', NULL, '1050 SqFt Bungalow', 1, 'http://media.point2.com/cdr/listing/b34f/bb2f/48b5/60e35eb4265e47a92af2/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/b34f/bb2f/48b5/60e35eb4265e47a92af2/w64h48.jpg', '2011-12-13 20:39:05', '2011-12-13 20:39:05'),
(35, 2, 'image/jpeg', 'jpeg', NULL, 'Kitchen', 1, 'http://media.point2.com/cdr/listing/53ce/acb5/8a13/0211ceff3539742b37f4/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/53ce/acb5/8a13/0211ceff3539742b37f4/w64h48.jpg', '2011-12-13 20:39:05', '2011-12-13 20:39:05'),
(36, 2, 'image/jpeg', 'jpeg', NULL, 'Kitchen/Dining Area', 1, 'http://media.point2.com/cdr/listing/c1f1/cfe7/a341/8f86324ac45a80a19115/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/c1f1/cfe7/a341/8f86324ac45a80a19115/w64h48.jpg', '2011-12-13 20:39:05', '2011-12-13 20:39:05'),
(37, 2, 'image/jpeg', 'jpeg', NULL, 'Dining Area', 1, 'http://media.point2.com/cdr/listing/0d61/069e/4a79/05b22735584fec391603/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/0d61/069e/4a79/05b22735584fec391603/w64h48.jpg', '2011-12-13 20:39:05', '2011-12-13 20:39:05'),
(38, 2, 'image/jpeg', 'jpeg', NULL, 'Wood Burning Stove', 1, 'http://media.point2.com/cdr/listing/256c/8030/42f8/86243a8a623d1b3c109d/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/256c/8030/42f8/86243a8a623d1b3c109d/w64h48.jpg', '2011-12-13 20:39:05', '2011-12-13 20:39:05'),
(39, 2, 'image/jpeg', 'jpeg', NULL, 'Living Room', 1, 'http://media.point2.com/cdr/listing/0f35/2b39/f6f1/8fc8ffa92fe88077b589/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/0f35/2b39/f6f1/8fc8ffa92fe88077b589/w64h48.jpg', '2011-12-13 20:39:05', '2011-12-13 20:39:05'),
(40, 2, 'image/jpeg', 'jpeg', NULL, 'Living Room', 1, 'http://media.point2.com/cdr/listing/8ec6/2298/bc88/2cbd83d953e963464110/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/8ec6/2298/bc88/2cbd83d953e963464110/w64h48.jpg', '2011-12-13 20:39:05', '2011-12-13 20:39:05'),
(41, 2, 'image/jpeg', 'jpeg', NULL, 'Bedroom', 1, 'http://media.point2.com/cdr/listing/07d4/6c3b/0dea/6280688f0cd85d2248bc/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/07d4/6c3b/0dea/6280688f0cd85d2248bc/w64h48.jpg', '2011-12-13 20:39:05', '2011-12-13 20:39:05'),
(42, 2, 'image/jpeg', 'jpeg', NULL, 'Bathroom', 1, 'http://media.point2.com/cdr/listing/3e1b/c216/5bc7/f150751eb5c9e141e375/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/3e1b/c216/5bc7/f150751eb5c9e141e375/w64h48.jpg', '2011-12-13 20:39:05', '2011-12-13 20:39:05'),
(43, 3, 'image/jpeg', 'jpeg', NULL, 'oversized driveway', 1, 'http://media.point2.com/cdr/listing/426c/b87e/9610/19f0ff76b7428cece6b1/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/426c/b87e/9610/19f0ff76b7428cece6b1/w64h48.jpg', '2011-12-13 20:39:06', '2011-12-13 20:39:06'),
(44, 3, 'image/jpeg', 'jpeg', NULL, 'Welcome Home', 1, 'http://media.point2.com/cdr/listing/f16c/1556/f33d/9aa1d7cef145fcfd29c9/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/f16c/1556/f33d/9aa1d7cef145fcfd29c9/w64h48.jpg', '2011-12-13 20:39:06', '2011-12-13 20:39:06'),
(45, 3, 'image/jpeg', 'jpeg', NULL, ' dining room', 1, 'http://media.point2.com/cdr/listing/11ac/122c/cb78/603c1d30da5517e6800e/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/11ac/122c/cb78/603c1d30da5517e6800e/w64h48.jpg', '2011-12-13 20:39:06', '2011-12-13 20:39:06'),
(46, 3, 'image/jpeg', 'jpeg', NULL, ' living room', 1, 'http://media.point2.com/cdr/listing/e46f/5cc8/b470/3183b916298a0932e88a/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/e46f/5cc8/b470/3183b916298a0932e88a/w64h48.jpg', '2011-12-13 20:39:06', '2011-12-13 20:39:06'),
(47, 3, 'image/jpeg', 'jpeg', NULL, ' covered patio', 1, 'http://media.point2.com/cdr/listing/4798/02de/1c6b/61f3f1e1f7045fbdef89/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/4798/02de/1c6b/61f3f1e1f7045fbdef89/w64h48.jpg', '2011-12-13 20:39:06', '2011-12-13 20:39:06'),
(48, 3, 'image/jpeg', 'jpeg', NULL, 'kitchen table nook', 1, 'http://media.point2.com/cdr/listing/2abf/82d4/43ea/70f49558296111e0019b/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/2abf/82d4/43ea/70f49558296111e0019b/w64h48.jpg', '2011-12-13 20:39:06', '2011-12-13 20:39:06'),
(49, 3, 'image/jpeg', 'jpeg', NULL, ' kitchen bar area', 1, 'http://media.point2.com/cdr/listing/4bdd/01a8/5792/f00d605d158b855f2fd8/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/4bdd/01a8/5792/f00d605d158b855f2fd8/w64h48.jpg', '2011-12-13 20:39:06', '2011-12-13 20:39:06'),
(50, 3, 'image/jpeg', 'jpeg', NULL, 'maple cabinets', 1, 'http://media.point2.com/cdr/listing/1d9d/aa60/0bad/2cafad07778f7f5c3d6b/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/1d9d/aa60/0bad/2cafad07778f7f5c3d6b/w64h48.jpg', '2011-12-13 20:39:06', '2011-12-13 20:39:06'),
(51, 3, 'image/jpeg', 'jpeg', NULL, ' kitchen', 1, 'http://media.point2.com/cdr/listing/fccc/d6a4/126f/5caa960c3395a359f1e9/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/fccc/d6a4/126f/5caa960c3395a359f1e9/w64h48.jpg', '2011-12-13 20:39:06', '2011-12-13 20:39:06'),
(52, 3, 'image/jpeg', 'jpeg', NULL, 'master bath', 1, 'http://media.point2.com/cdr/listing/6acf/0fa8/5b92/5639cce2e05d2614fe22/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/6acf/0fa8/5b92/5639cce2e05d2614fe22/w64h48.jpg', '2011-12-13 20:39:06', '2011-12-13 20:39:06'),
(53, 3, 'image/jpeg', 'jpeg', NULL, 'master bathroom', 1, 'http://media.point2.com/cdr/listing/8a13/eb9d/c0fc/6dbd252dda62f4ef74b7/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/8a13/eb9d/c0fc/6dbd252dda62f4ef74b7/w64h48.jpg', '2011-12-13 20:39:06', '2011-12-13 20:39:06'),
(54, 3, 'image/jpeg', 'jpeg', NULL, ' Master w transom slider', 1, 'http://media.point2.com/cdr/listing/c9e4/ab24/54d4/97b3fa7d614524c43637/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/c9e4/ab24/54d4/97b3fa7d614524c43637/w64h48.jpg', '2011-12-13 20:39:06', '2011-12-13 20:39:06'),
(55, 3, 'image/jpeg', 'jpeg', NULL, '20 Beachside Drive back yard', 1, 'http://media.point2.com/cdr/listing/c2e7/b85d/77d2/3f7219744b5c27279b2e/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/c2e7/b85d/77d2/3f7219744b5c27279b2e/w64h48.jpg', '2011-12-13 20:39:06', '2011-12-13 20:39:06'),
(56, 3, 'image/jpeg', 'jpeg', NULL, 'Intracoastal fishing dock', 1, 'http://media.point2.com/cdr/listing/bd78/00ab/47aa/4ac4f4a416a964a35daa/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/bd78/00ab/47aa/4ac4f4a416a964a35daa/w64h48.jpg', '2011-12-13 20:39:06', '2011-12-13 20:39:06'),
(57, 3, 'image/jpeg', 'jpeg', NULL, 'Beach! Ocean! Swim!', 1, 'http://media.point2.com/cdr/listing/0425/09cc/a8f0/1d79df09049f2f441975/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/0425/09cc/a8f0/1d79df09049f2f441975/w64h48.jpg', '2011-12-13 20:39:06', '2011-12-13 20:39:06'),
(58, 4, 'image/jpeg', 'jpeg', NULL, NULL, 1, 'http://media.point2.com/cdr/listing/7387/23c5/3eb8/5529d2c4220dfb2a0e47/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/7387/23c5/3eb8/5529d2c4220dfb2a0e47/w64h48.jpg', '2011-12-13 20:39:07', '2011-12-13 20:39:07'),
(59, 4, 'image/jpeg', 'jpeg', NULL, NULL, 1, 'http://media.point2.com/cdr/listing/8cfd/0dab/a80f/a9184e96197f0f60c8b2/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/8cfd/0dab/a80f/a9184e96197f0f60c8b2/w64h48.jpg', '2011-12-13 20:39:07', '2011-12-13 20:39:07'),
(60, 4, 'image/jpeg', 'jpeg', NULL, NULL, 1, 'http://media.point2.com/cdr/listing/e62a/9d3b/abd6/3d6a7f706a2de0e937b1/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/e62a/9d3b/abd6/3d6a7f706a2de0e937b1/w64h48.jpg', '2011-12-13 20:39:07', '2011-12-13 20:39:07'),
(61, 4, 'image/jpeg', 'jpeg', NULL, NULL, 1, 'http://media.point2.com/cdr/listing/4299/eab0/15b3/403b84d67bb4d4c68386/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/4299/eab0/15b3/403b84d67bb4d4c68386/w64h48.jpg', '2011-12-13 20:39:07', '2011-12-13 20:39:07'),
(62, 4, 'image/jpeg', 'jpeg', NULL, NULL, 1, 'http://media.point2.com/cdr/listing/5427/2a3f/abae/64d828f8b87810147206/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/5427/2a3f/abae/64d828f8b87810147206/w64h48.jpg', '2011-12-13 20:39:07', '2011-12-13 20:39:07'),
(63, 4, 'image/jpeg', 'jpeg', NULL, NULL, 1, 'http://media.point2.com/cdr/listing/d92e/57cb/81f4/b5e97b3de25b7b3613f8/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/d92e/57cb/81f4/b5e97b3de25b7b3613f8/w64h48.jpg', '2011-12-13 20:39:07', '2011-12-13 20:39:07'),
(64, 4, 'image/jpeg', 'jpeg', NULL, NULL, 1, 'http://media.point2.com/cdr/listing/4c1a/3cc7/49bd/0afe8e33d42cafa45380/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/4c1a/3cc7/49bd/0afe8e33d42cafa45380/w64h48.jpg', '2011-12-13 20:39:07', '2011-12-13 20:39:07'),
(65, 4, 'image/jpeg', 'jpeg', NULL, NULL, 1, 'http://media.point2.com/cdr/listing/138b/0ee9/678c/1fef2356a899e1baabee/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/138b/0ee9/678c/1fef2356a899e1baabee/w64h48.jpg', '2011-12-13 20:39:07', '2011-12-13 20:39:07'),
(66, 4, 'image/jpeg', 'jpeg', NULL, NULL, 1, 'http://media.point2.com/cdr/listing/46b3/1b27/777e/27772168c3a320243d3d/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/46b3/1b27/777e/27772168c3a320243d3d/w64h48.jpg', '2011-12-13 20:39:07', '2011-12-13 20:39:07'),
(67, 4, 'image/jpeg', 'jpeg', NULL, NULL, 1, 'http://media.point2.com/cdr/listing/7c72/b38a/556c/139bd518c6ba364b8583/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/7c72/b38a/556c/139bd518c6ba364b8583/w64h48.jpg', '2011-12-13 20:39:07', '2011-12-13 20:39:07'),
(68, 4, 'image/jpeg', 'jpeg', NULL, NULL, 1, 'http://media.point2.com/cdr/listing/4c0f/8730/4776/254a58c675a986a7ab33/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/4c0f/8730/4776/254a58c675a986a7ab33/w64h48.jpg', '2011-12-13 20:39:07', '2011-12-13 20:39:07'),
(69, 4, 'image/jpeg', 'jpeg', NULL, NULL, 1, 'http://media.point2.com/cdr/listing/fd76/b5de/a03a/9b6e0faea7a9ac1e7359/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/fd76/b5de/a03a/9b6e0faea7a9ac1e7359/w64h48.jpg', '2011-12-13 20:39:07', '2011-12-13 20:39:07'),
(70, 4, 'image/jpeg', 'jpeg', NULL, NULL, 1, 'http://media.point2.com/cdr/listing/5d6f/9056/202f/5f954bed56225d6de50c/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/5d6f/9056/202f/5f954bed56225d6de50c/w64h48.jpg', '2011-12-13 20:39:08', '2011-12-13 20:39:08'),
(71, 4, 'image/jpeg', 'jpeg', NULL, NULL, 1, 'http://media.point2.com/cdr/listing/a74a/0b7c/4a82/a8b6ff8a323d071a4794/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/a74a/0b7c/4a82/a8b6ff8a323d071a4794/w64h48.jpg', '2011-12-13 20:39:08', '2011-12-13 20:39:08'),
(72, 4, 'image/jpeg', 'jpeg', NULL, NULL, 1, 'http://media.point2.com/cdr/listing/f661/4f55/e665/7b1f54fbe12d4600cbe2/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/f661/4f55/e665/7b1f54fbe12d4600cbe2/w64h48.jpg', '2011-12-13 20:39:08', '2011-12-13 20:39:08'),
(73, 4, 'image/jpeg', 'jpeg', NULL, NULL, 1, 'http://media.point2.com/cdr/listing/0c64/b897/e456/d462e5721b63df176176/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/0c64/b897/e456/d462e5721b63df176176/w64h48.jpg', '2011-12-13 20:39:08', '2011-12-13 20:39:08'),
(74, 4, 'image/jpeg', 'jpeg', NULL, NULL, 1, 'http://media.point2.com/cdr/listing/5e05/6aa6/e060/ce52c271ffa006daf72c/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/5e05/6aa6/e060/ce52c271ffa006daf72c/w64h48.jpg', '2011-12-13 20:39:08', '2011-12-13 20:39:08'),
(75, 4, 'image/jpeg', 'jpeg', NULL, NULL, 1, 'http://media.point2.com/cdr/listing/d3d0/f635/3496/3ebe429787388f917c6f/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/d3d0/f635/3496/3ebe429787388f917c6f/w64h48.jpg', '2011-12-13 20:39:08', '2011-12-13 20:39:08'),
(76, 4, 'image/jpeg', 'jpeg', NULL, NULL, 1, 'http://media.point2.com/cdr/listing/7b4d/1780/b580/6b1077196bbd255d1d2c/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/7b4d/1780/b580/6b1077196bbd255d1d2c/w64h48.jpg', '2011-12-13 20:39:08', '2011-12-13 20:39:08'),
(77, 4, 'image/jpeg', 'jpeg', NULL, NULL, 1, 'http://media.point2.com/cdr/listing/0f6b/fe33/66a5/f20ca025e34e28713743/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/0f6b/fe33/66a5/f20ca025e34e28713743/w64h48.jpg', '2011-12-13 20:39:08', '2011-12-13 20:39:08'),
(78, 4, 'image/jpeg', 'jpeg', NULL, NULL, 1, 'http://media.point2.com/cdr/listing/8315/e4e9/27df/da5790dfa2ad01b5ef2a/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/8315/e4e9/27df/da5790dfa2ad01b5ef2a/w64h48.jpg', '2011-12-13 20:39:08', '2011-12-13 20:39:08'),
(79, 4, 'image/jpeg', 'jpeg', NULL, NULL, 1, 'http://media.point2.com/cdr/listing/b2cd/8dcb/3be7/e873f6896d346e723d85/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/b2cd/8dcb/3be7/e873f6896d346e723d85/w64h48.jpg', '2011-12-13 20:39:08', '2011-12-13 20:39:08'),
(80, 4, 'image/jpeg', 'jpeg', NULL, NULL, 1, 'http://media.point2.com/cdr/listing/90b2/29f6/3c06/49fa2e1dbfca0ea7b2d5/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/90b2/29f6/3c06/49fa2e1dbfca0ea7b2d5/w64h48.jpg', '2011-12-13 20:39:08', '2011-12-13 20:39:08'),
(81, 4, 'image/jpeg', 'jpeg', NULL, NULL, 1, 'http://media.point2.com/cdr/listing/71a3/9851/dfa4/a08fcb18883f45c967a9/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/71a3/9851/dfa4/a08fcb18883f45c967a9/w64h48.jpg', '2011-12-13 20:39:08', '2011-12-13 20:39:08'),
(82, 4, 'image/jpeg', 'jpeg', NULL, NULL, 1, 'http://media.point2.com/cdr/listing/39de/1805/e599/a73b5d53e90f11812027/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/39de/1805/e599/a73b5d53e90f11812027/w64h48.jpg', '2011-12-13 20:39:08', '2011-12-13 20:39:08'),
(83, 4, 'image/jpeg', 'jpeg', NULL, NULL, 1, 'http://media.point2.com/cdr/listing/40ca/98d9/ea10/884179d9babafbdb73a4/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/40ca/98d9/ea10/884179d9babafbdb73a4/w64h48.jpg', '2011-12-13 20:39:08', '2011-12-13 20:39:08'),
(84, 4, 'image/jpeg', 'jpeg', NULL, NULL, 1, 'http://media.point2.com/cdr/listing/99e3/c4e3/53e0/95ef5de6ee6bf97bbd13/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/99e3/c4e3/53e0/95ef5de6ee6bf97bbd13/w64h48.jpg', '2011-12-13 20:39:08', '2011-12-13 20:39:08'),
(85, 4, 'image/jpeg', 'jpeg', NULL, NULL, 1, 'http://media.point2.com/cdr/listing/63c8/9282/6125/06bc2c07fe9d4368cf99/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/63c8/9282/6125/06bc2c07fe9d4368cf99/w64h48.jpg', '2011-12-13 20:39:08', '2011-12-13 20:39:08'),
(86, 4, 'image/jpeg', 'jpeg', NULL, NULL, 1, 'http://media.point2.com/cdr/listing/15d9/c695/0a5a/c2dab03405735d2bc645/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/15d9/c695/0a5a/c2dab03405735d2bc645/w64h48.jpg', '2011-12-13 20:39:08', '2011-12-13 20:39:08'),
(87, 4, 'image/jpeg', 'jpeg', NULL, NULL, 1, 'http://media.point2.com/cdr/listing/687b/f755/885f/0ac704c0d5d2f2a7af12/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/687b/f755/885f/0ac704c0d5d2f2a7af12/w64h48.jpg', '2011-12-13 20:39:08', '2011-12-13 20:39:08'),
(88, 4, 'image/jpeg', 'jpeg', NULL, NULL, 1, 'http://media.point2.com/cdr/listing/9270/3d91/3b2e/ff33c8eb3f9a0f58974b/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/9270/3d91/3b2e/ff33c8eb3f9a0f58974b/w64h48.jpg', '2011-12-13 20:39:09', '2011-12-13 20:39:09'),
(89, 4, 'image/jpeg', 'jpeg', NULL, NULL, 1, 'http://media.point2.com/cdr/listing/e497/281d/6a04/e55378cf3c56182188ba/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/e497/281d/6a04/e55378cf3c56182188ba/w64h48.jpg', '2011-12-13 20:39:09', '2011-12-13 20:39:09'),
(90, 4, 'image/jpeg', 'jpeg', NULL, NULL, 1, 'http://media.point2.com/cdr/listing/b4bc/00ae/08e2/0d440387124d7ebf3789/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/b4bc/00ae/08e2/0d440387124d7ebf3789/w64h48.jpg', '2011-12-13 20:39:09', '2011-12-13 20:39:09'),
(91, 4, 'image/jpeg', 'jpeg', NULL, NULL, 1, 'http://media.point2.com/cdr/listing/625d/2287/9581/90dd837a4ac4bf47e4a0/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/625d/2287/9581/90dd837a4ac4bf47e4a0/w64h48.jpg', '2011-12-13 20:39:09', '2011-12-13 20:39:09'),
(92, 4, 'image/jpeg', 'jpeg', NULL, NULL, 1, 'http://media.point2.com/cdr/listing/9657/57c6/a558/b58ea1b5e74b4f542c2a/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/9657/57c6/a558/b58ea1b5e74b4f542c2a/w64h48.jpg', '2011-12-13 20:39:09', '2011-12-13 20:39:09'),
(93, 4, 'image/jpeg', 'jpeg', NULL, NULL, 1, 'http://media.point2.com/cdr/listing/407a/4c37/8b4d/249cffbf04c32bc5a237/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/407a/4c37/8b4d/249cffbf04c32bc5a237/w64h48.jpg', '2011-12-13 20:39:09', '2011-12-13 20:39:09'),
(94, 5, 'image/jpeg', 'jpeg', NULL, '11 Mile', 1, 'http://media.point2.com/cdr/listing/f4a6/6e73/3eef/74254e65a11ed35260db/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/f4a6/6e73/3eef/74254e65a11ed35260db/w64h48.jpg', '2011-12-13 20:39:09', '2011-12-13 20:39:09'),
(95, 5, 'image/jpeg', 'jpeg', NULL, 'HPIM4796', 1, 'http://media.point2.com/cdr/listing/c4ae/cd0e/781a/1d56941b6501cf521ea7/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/c4ae/cd0e/781a/1d56941b6501cf521ea7/w64h48.jpg', '2011-12-13 20:39:09', '2011-12-13 20:39:09'),
(96, 5, 'image/jpeg', 'jpeg', NULL, 'HPIM4797', 1, 'http://media.point2.com/cdr/listing/b2aa/cf58/1626/13889da94593bb81b153/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/b2aa/cf58/1626/13889da94593bb81b153/w64h48.jpg', '2011-12-13 20:39:10', '2011-12-13 20:39:10'),
(97, 5, 'image/jpeg', 'jpeg', NULL, 'HPIM4798', 1, 'http://media.point2.com/cdr/listing/3fff/1b0c/8e80/da836e41d2a5b0a27b32/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/3fff/1b0c/8e80/da836e41d2a5b0a27b32/w64h48.jpg', '2011-12-13 20:39:10', '2011-12-13 20:39:10'),
(98, 5, 'image/jpeg', 'jpeg', NULL, 'HPIM4801', 1, 'http://media.point2.com/cdr/listing/d61c/31d2/d0af/12af22bf751fba501d44/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/d61c/31d2/d0af/12af22bf751fba501d44/w64h48.jpg', '2011-12-13 20:39:10', '2011-12-13 20:39:10'),
(99, 5, 'image/jpeg', 'jpeg', NULL, 'HPIM4799', 1, 'http://media.point2.com/cdr/listing/5801/ceef/2405/0b93f46669d3179b7fe5/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/5801/ceef/2405/0b93f46669d3179b7fe5/w64h48.jpg', '2011-12-13 20:39:10', '2011-12-13 20:39:10'),
(100, 5, 'image/jpeg', 'jpeg', NULL, 'HPIM4800', 1, 'http://media.point2.com/cdr/listing/425b/c9f3/6133/b6b2191190d16d964ad8/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/425b/c9f3/6133/b6b2191190d16d964ad8/w64h48.jpg', '2011-12-13 20:39:10', '2011-12-13 20:39:10'),
(101, 5, 'image/jpeg', 'jpeg', NULL, 'HPIM4802', 1, 'http://media.point2.com/cdr/listing/0da4/0c96/c121/9570c39d5eaab2263390/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/0da4/0c96/c121/9570c39d5eaab2263390/w64h48.jpg', '2011-12-13 20:39:10', '2011-12-13 20:39:10'),
(102, 5, 'image/jpeg', 'jpeg', NULL, 'HPIM4803', 1, 'http://media.point2.com/cdr/listing/e822/1891/d273/b2445724ee6a5d05a8a0/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/e822/1891/d273/b2445724ee6a5d05a8a0/w64h48.jpg', '2011-12-13 20:39:10', '2011-12-13 20:39:10'),
(103, 6, 'image/jpeg', 'jpeg', NULL, 'Cache Creek Recreational Subdivision', 1, 'http://media.point2.com/cdr/listing/8ba1/c754/2f6d/b3129e51123ad54713d1/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/8ba1/c754/2f6d/b3129e51123ad54713d1/w64h48.jpg', '2011-12-13 20:39:10', '2011-12-13 20:39:10'),
(104, 6, 'image/jpeg', 'jpeg', NULL, 'Cache Creek Recreational Subdivision', 1, 'http://media.point2.com/cdr/listing/a257/841c/8591/ddb09ad70a118ddc78a4/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/a257/841c/8591/ddb09ad70a118ddc78a4/w64h48.jpg', '2011-12-13 20:39:10', '2011-12-13 20:39:10'),
(105, 6, 'image/jpeg', 'jpeg', NULL, 'Cache Creek Recreational Subdivision', 1, 'http://media.point2.com/cdr/listing/8088/2be1/76cc/feff1ea9c8ab0dc68f4e/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/8088/2be1/76cc/feff1ea9c8ab0dc68f4e/w64h48.jpg', '2011-12-13 20:39:10', '2011-12-13 20:39:10'),
(106, 6, 'image/jpeg', 'jpeg', NULL, 'Cache Creek Recreational Subdivision', 1, 'http://media.point2.com/cdr/listing/a6bf/e1e1/4fb5/f05fc65de8eb8400090d/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/a6bf/e1e1/4fb5/f05fc65de8eb8400090d/w64h48.jpg', '2011-12-13 20:39:10', '2011-12-13 20:39:10'),
(107, 6, 'image/jpeg', 'jpeg', NULL, 'Cache Creek Recreational Subdivision', 1, 'http://media.point2.com/cdr/listing/8fab/2362/7298/117a3f3f7e3210d289da/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/8fab/2362/7298/117a3f3f7e3210d289da/w64h48.jpg', '2011-12-13 20:39:10', '2011-12-13 20:39:10'),
(108, 6, 'image/jpeg', 'jpeg', NULL, 'Cache Creek Recreational Subdivision', 1, 'http://media.point2.com/cdr/listing/eb92/b19a/f771/e4a92b95aa478aa167e2/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/eb92/b19a/f771/e4a92b95aa478aa167e2/w64h48.jpg', '2011-12-13 20:39:11', '2011-12-13 20:39:11'),
(109, 6, 'image/jpeg', 'jpeg', NULL, 'Cache Creek Recreational Subdivision', 1, 'http://media.point2.com/cdr/listing/01e9/621c/99ff/91a1a9044175b7cdbb92/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/01e9/621c/99ff/91a1a9044175b7cdbb92/w64h48.jpg', '2011-12-13 20:39:11', '2011-12-13 20:39:11'),
(110, 6, 'image/jpeg', 'jpeg', NULL, 'Airstrip', 1, 'http://media.point2.com/cdr/listing/ecd8/848a/3f9c/e4aad306d423e7b0d387/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/ecd8/848a/3f9c/e4aad306d423e7b0d387/w64h48.jpg', '2011-12-13 20:39:11', '2011-12-13 20:39:11'),
(111, 6, 'image/jpeg', 'jpeg', NULL, 'Airstrip', 1, 'http://media.point2.com/cdr/listing/d881/a9ca/ef1b/e01a7d0235de7e13d9fb/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/d881/a9ca/ef1b/e01a7d0235de7e13d9fb/w64h48.jpg', '2011-12-13 20:39:11', '2011-12-13 20:39:11'),
(112, 6, 'image/jpeg', 'jpeg', NULL, 'Airstrip with Plane', 1, 'http://media.point2.com/cdr/listing/d0f2/e473/6974/a35228162e1cb871acec/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/d0f2/e473/6974/a35228162e1cb871acec/w64h48.jpg', '2011-12-13 20:39:11', '2011-12-13 20:39:11'),
(113, 6, 'image/jpeg', 'jpeg', NULL, 'Airstrip with Plane', 1, 'http://media.point2.com/cdr/listing/f835/f72d/c1da/6ea46a354165c3a57cbe/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/f835/f72d/c1da/6ea46a354165c3a57cbe/w64h48.jpg', '2011-12-13 20:39:11', '2011-12-13 20:39:11'),
(114, 6, 'image/jpeg', 'jpeg', NULL, 'Cache Creek Recreational Subdivision', 1, 'http://media.point2.com/cdr/listing/2c0a/c0b0/fbcc/e903a136789983182ac5/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/2c0a/c0b0/fbcc/e903a136789983182ac5/w64h48.jpg', '2011-12-13 20:39:11', '2011-12-13 20:39:11'),
(115, 6, 'image/jpeg', 'jpeg', NULL, 'Cache Creek Recreational Subdivision', 1, 'http://media.point2.com/cdr/listing/b9bc/f2d7/2e68/6b80244746e91d612a52/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/b9bc/f2d7/2e68/6b80244746e91d612a52/w64h48.jpg', '2011-12-13 20:39:11', '2011-12-13 20:39:11'),
(116, 6, 'image/jpeg', 'jpeg', NULL, 'Cache Creek Recreational Subdivision', 1, 'http://media.point2.com/cdr/listing/6ea2/0e3b/a140/b27a3a463e38f074a703/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/6ea2/0e3b/a140/b27a3a463e38f074a703/w64h48.jpg', '2011-12-13 20:39:11', '2011-12-13 20:39:11'),
(117, 6, 'image/jpeg', 'jpeg', NULL, 'Cache Creek Recreational Subdivision', 1, 'http://media.point2.com/cdr/listing/858a/0282/c1f1/6579c2f64765fc6410d0/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/858a/0282/c1f1/6579c2f64765fc6410d0/w64h48.jpg', '2011-12-13 20:39:11', '2011-12-13 20:39:11'),
(118, 6, 'image/jpeg', 'jpeg', NULL, 'Cache Creek Recreational Subdivision', 1, 'http://media.point2.com/cdr/listing/e292/1b9d/4379/55af6b8a86486b4e9268/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/e292/1b9d/4379/55af6b8a86486b4e9268/w64h48.jpg', '2011-12-13 20:39:11', '2011-12-13 20:39:11'),
(119, 6, 'image/jpeg', 'jpeg', NULL, 'Cache Creek Recreational Subdivision', 1, 'http://media.point2.com/cdr/listing/247e/ea57/2aea/404201c1803b847fd416/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/247e/ea57/2aea/404201c1803b847fd416/w64h48.jpg', '2011-12-13 20:39:11', '2011-12-13 20:39:11'),
(120, 6, 'image/jpeg', 'jpeg', NULL, 'Cache Creek Recreational Subdivision', 1, 'http://media.point2.com/cdr/listing/200b/cfa7/6742/85ddc7b5e2e84abd8585/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/200b/cfa7/6742/85ddc7b5e2e84abd8585/w64h48.jpg', '2011-12-13 20:39:11', '2011-12-13 20:39:11'),
(121, 6, 'image/jpeg', 'jpeg', NULL, 'Cache Creek Recreational Subdivision', 1, 'http://media.point2.com/cdr/listing/09b4/fe86/1b81/ecc7185b63dbfac073f3/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/09b4/fe86/1b81/ecc7185b63dbfac073f3/w64h48.jpg', '2011-12-13 20:39:11', '2011-12-13 20:39:11'),
(122, 6, 'image/jpeg', 'jpeg', NULL, 'Cache Creek Recreational Subdivision', 1, 'http://media.point2.com/cdr/listing/7ee1/0c21/053e/f0ba03246461e1923739/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/7ee1/0c21/053e/f0ba03246461e1923739/w64h48.jpg', '2011-12-13 20:39:11', '2011-12-13 20:39:11'),
(123, 6, 'image/jpeg', 'jpeg', NULL, 'Cache Creek Recreational Subdivision', 1, 'http://media.point2.com/cdr/listing/f84d/26ce/5ebc/a56315d270d18dbaf4e2/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/f84d/26ce/5ebc/a56315d270d18dbaf4e2/w64h48.jpg', '2011-12-13 20:39:11', '2011-12-13 20:39:11'),
(124, 6, 'image/jpeg', 'jpeg', NULL, 'Cache Creek Recreational Subdivision', 1, 'http://media.point2.com/cdr/listing/b71b/5f31/0dbf/57ceea56e4511413871b/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/b71b/5f31/0dbf/57ceea56e4511413871b/w64h48.jpg', '2011-12-13 20:39:11', '2011-12-13 20:39:11'),
(125, 6, 'image/jpeg', 'jpeg', NULL, 'Cache Creek Recreational Subdivision', 1, 'http://media.point2.com/cdr/listing/2cd8/a857/a0d5/5f9473b465113bf9e82e/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/2cd8/a857/a0d5/5f9473b465113bf9e82e/w64h48.jpg', '2011-12-13 20:39:11', '2011-12-13 20:39:11'),
(126, 6, 'image/jpeg', 'jpeg', NULL, 'Cache Creek Recreational Subdivision', 1, 'http://media.point2.com/cdr/listing/3664/bea8/5b0a/158a5ce201d9f3856ed7/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/3664/bea8/5b0a/158a5ce201d9f3856ed7/w64h48.jpg', '2011-12-13 20:39:11', '2011-12-13 20:39:11'),
(127, 6, 'image/jpeg', 'jpeg', NULL, 'Cache Creek Recreational Subdivision', 1, 'http://media.point2.com/cdr/listing/e8d6/cf14/129c/6f2f69d21fd8418c7a3d/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/e8d6/cf14/129c/6f2f69d21fd8418c7a3d/w64h48.jpg', '2011-12-13 20:39:12', '2011-12-13 20:39:12'),
(128, 6, 'image/jpeg', 'jpeg', NULL, 'Cache Creek Recreational Subdivision', 1, 'http://media.point2.com/cdr/listing/41ec/b027/d967/1816d9f57da1b3fef1d5/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/41ec/b027/d967/1816d9f57da1b3fef1d5/w64h48.jpg', '2011-12-13 20:39:12', '2011-12-13 20:39:12'),
(129, 6, 'image/jpeg', 'jpeg', NULL, 'Cache Creek Recreational Subdivision', 1, 'http://media.point2.com/cdr/listing/4205/9031/de52/f9e91228da1ec6728ac3/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/4205/9031/de52/f9e91228da1ec6728ac3/w64h48.jpg', '2011-12-13 20:39:12', '2011-12-13 20:39:12'),
(130, 6, 'image/jpeg', 'jpeg', NULL, 'Cache Creek Recreational Subdivision', 1, 'http://media.point2.com/cdr/listing/9736/9a21/bccf/61c66dea8a0f153a6481/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/9736/9a21/bccf/61c66dea8a0f153a6481/w64h48.jpg', '2011-12-13 20:39:12', '2011-12-13 20:39:12'),
(131, 6, 'image/jpeg', 'jpeg', NULL, 'Cache Creek Recreational Subdivision', 1, 'http://media.point2.com/cdr/listing/8e0b/63a4/b5bb/67047f79646eec70417b/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/8e0b/63a4/b5bb/67047f79646eec70417b/w64h48.jpg', '2011-12-13 20:39:12', '2011-12-13 20:39:12'),
(132, 6, 'image/jpeg', 'jpeg', NULL, 'Cache Creek Recreational Subdivision', 1, 'http://media.point2.com/cdr/listing/feb0/80ae/7c85/62d6c154ef6d1e555db4/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/feb0/80ae/7c85/62d6c154ef6d1e555db4/w64h48.jpg', '2011-12-13 20:39:12', '2011-12-13 20:39:12'),
(133, 6, 'image/jpeg', 'jpeg', NULL, 'Cache Creek Recreational Subdivision', 1, 'http://media.point2.com/cdr/listing/076c/f53b/34c4/e99cdf76a004982ca1a0/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/076c/f53b/34c4/e99cdf76a004982ca1a0/w64h48.jpg', '2011-12-13 20:39:12', '2011-12-13 20:39:12'),
(134, 6, 'image/jpeg', 'jpeg', NULL, 'Cache Creek Recreational Subdivision', 1, 'http://media.point2.com/cdr/listing/0a16/8a9e/47f5/647eb74d52a286032c53/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/0a16/8a9e/47f5/647eb74d52a286032c53/w64h48.jpg', '2011-12-13 20:39:12', '2011-12-13 20:39:12'),
(135, 6, 'image/jpeg', 'jpeg', NULL, 'Cache Creek Recreational Subdivision', 1, 'http://media.point2.com/cdr/listing/6bc1/d895/baf8/6ac79b3b451389de8a27/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/6bc1/d895/baf8/6ac79b3b451389de8a27/w64h48.jpg', '2011-12-13 20:39:12', '2011-12-13 20:39:12'),
(136, 6, 'image/jpeg', 'jpeg', NULL, 'Leaving Cache Creek', 1, 'http://media.point2.com/cdr/listing/8051/c7b6/8d04/5303527fd55b3b9ce36a/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/8051/c7b6/8d04/5303527fd55b3b9ce36a/w64h48.jpg', '2011-12-13 20:39:12', '2011-12-13 20:39:12'),
(137, 7, 'image/jpeg', 'jpeg', NULL, NULL, 1, 'http://media.point2.com/cdr/listing/06a5/3918/a039/0d76a3c45655c6b6a991/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/06a5/3918/a039/0d76a3c45655c6b6a991/w64h48.jpg', '2011-12-13 20:39:12', '2011-12-13 20:39:12'),
(138, 8, 'image/jpeg', 'jpeg', NULL, NULL, 1, 'http://media.point2.com/cdr/listing/4835/1ec1/8a7f/5fe5733310519a750e79/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/4835/1ec1/8a7f/5fe5733310519a750e79/w64h48.jpg', '2011-12-13 20:39:13', '2011-12-13 20:39:13'),
(139, 8, 'image/jpeg', 'jpeg', NULL, NULL, 1, 'http://media.point2.com/cdr/listing/455c/e9f9/d34b/2a185fc8c41d645cfd0e/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/455c/e9f9/d34b/2a185fc8c41d645cfd0e/w64h48.jpg', '2011-12-13 20:39:13', '2011-12-13 20:39:13'),
(140, 8, 'image/jpeg', 'jpeg', NULL, NULL, 1, 'http://media.point2.com/cdr/listing/70a0/0308/8688/ee3fe16dd546a78ae03f/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/70a0/0308/8688/ee3fe16dd546a78ae03f/w64h48.jpg', '2011-12-13 20:39:13', '2011-12-13 20:39:13'),
(141, 8, 'image/jpeg', 'jpeg', NULL, NULL, 1, 'http://media.point2.com/cdr/listing/c2dc/b928/48d8/9040bef04c4ca73178f1/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/c2dc/b928/48d8/9040bef04c4ca73178f1/w64h48.jpg', '2011-12-13 20:39:13', '2011-12-13 20:39:13'),
(142, 8, 'image/jpeg', 'jpeg', NULL, NULL, 1, 'http://media.point2.com/cdr/listing/f8eb/a395/9d0e/c23bbe81184d6df95531/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/f8eb/a395/9d0e/c23bbe81184d6df95531/w64h48.jpg', '2011-12-13 20:39:13', '2011-12-13 20:39:13'),
(143, 8, 'image/jpeg', 'jpeg', NULL, NULL, 1, 'http://media.point2.com/cdr/listing/fe43/576a/0f76/228478f029c3608bc37c/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/fe43/576a/0f76/228478f029c3608bc37c/w64h48.jpg', '2011-12-13 20:39:13', '2011-12-13 20:39:13'),
(144, 9, 'image/jpeg', 'jpeg', NULL, 'photo1', 1, 'http://media.point2.com/cdr/listing/92fa/4fee/db8a/4f421331f98f7485f6db/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/92fa/4fee/db8a/4f421331f98f7485f6db/w64h48.jpg', '2011-12-13 20:39:13', '2011-12-13 20:39:13'),
(145, 9, 'image/jpeg', 'jpeg', NULL, 'photo6', 1, 'http://media.point2.com/cdr/listing/2942/5f53/773f/d6eb57aaae5e2b4c86fd/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/2942/5f53/773f/d6eb57aaae5e2b4c86fd/w64h48.jpg', '2011-12-13 20:39:13', '2011-12-13 20:39:13'),
(146, 9, 'image/jpeg', 'jpeg', NULL, 'photo5', 1, 'http://media.point2.com/cdr/listing/69d9/fb5e/7009/99898eb5eae4bc74b7f5/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/69d9/fb5e/7009/99898eb5eae4bc74b7f5/w64h48.jpg', '2011-12-13 20:39:13', '2011-12-13 20:39:13'),
(147, 9, 'image/jpeg', 'jpeg', NULL, 'photo4', 1, 'http://media.point2.com/cdr/listing/b498/969a/6592/1f10586704c3b76e2ebd/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/b498/969a/6592/1f10586704c3b76e2ebd/w64h48.jpg', '2011-12-13 20:39:14', '2011-12-13 20:39:14'),
(148, 9, 'image/jpeg', 'jpeg', NULL, 'Photo3', 1, 'http://media.point2.com/cdr/listing/7836/e4f1/156e/293ba40cc7085177c0b4/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/7836/e4f1/156e/293ba40cc7085177c0b4/w64h48.jpg', '2011-12-13 20:39:14', '2011-12-13 20:39:14'),
(149, 9, 'image/jpeg', 'jpeg', NULL, 'Photo2', 1, 'http://media.point2.com/cdr/listing/b6c7/b48a/c684/340cb6cdfaafbb4f127e/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/b6c7/b48a/c684/340cb6cdfaafbb4f127e/w64h48.jpg', '2011-12-13 20:39:14', '2011-12-13 20:39:14'),
(150, 9, 'image/jpeg', 'jpeg', NULL, 'photo10', 1, 'http://media.point2.com/cdr/listing/671a/6bfc/846c/4e0e1a8eabf678a982c9/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/671a/6bfc/846c/4e0e1a8eabf678a982c9/w64h48.jpg', '2011-12-13 20:39:14', '2011-12-13 20:39:14'),
(151, 9, 'image/jpeg', 'jpeg', NULL, 'photo8', 1, 'http://media.point2.com/cdr/listing/4867/a301/6454/d5d5d0195e3346bac43f/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/4867/a301/6454/d5d5d0195e3346bac43f/w64h48.jpg', '2011-12-13 20:39:14', '2011-12-13 20:39:14'),
(152, 9, 'image/jpeg', 'jpeg', NULL, 'photo9', 1, 'http://media.point2.com/cdr/listing/d04b/5a41/b631/2c70fe8105bc49209140/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/d04b/5a41/b631/2c70fe8105bc49209140/w64h48.jpg', '2011-12-13 20:39:14', '2011-12-13 20:39:14'),
(153, 10, 'image/jpeg', 'jpeg', NULL, NULL, 1, 'http://media.point2.com/cdr/listing/e0cf/abcd/a4a9/f8c38edc3de610aa1b47/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/e0cf/abcd/a4a9/f8c38edc3de610aa1b47/w64h48.jpg', '2011-12-13 20:39:14', '2011-12-13 20:39:14'),
(154, 10, 'image/jpeg', 'jpeg', NULL, NULL, 1, 'http://media.point2.com/cdr/listing/d30c/65ec/0b00/e56fe7adf31c0153ed24/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/d30c/65ec/0b00/e56fe7adf31c0153ed24/w64h48.jpg', '2011-12-13 20:39:14', '2011-12-13 20:39:14'),
(155, 10, 'image/jpeg', 'jpeg', NULL, NULL, 1, 'http://media.point2.com/cdr/listing/dc51/3867/c3c0/f0b34e1e705ec15d9483/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/dc51/3867/c3c0/f0b34e1e705ec15d9483/w64h48.jpg', '2011-12-13 20:39:14', '2011-12-13 20:39:14'),
(156, 10, 'image/jpeg', 'jpeg', NULL, NULL, 1, 'http://media.point2.com/cdr/listing/4c2b/adf2/8d67/29e56f159090f77aecc2/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/4c2b/adf2/8d67/29e56f159090f77aecc2/w64h48.jpg', '2011-12-13 20:39:14', '2011-12-13 20:39:14'),
(157, 10, 'image/jpeg', 'jpeg', NULL, NULL, 1, 'http://media.point2.com/cdr/listing/bbe0/9c4a/725c/35c263a73a701f6f5976/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/bbe0/9c4a/725c/35c263a73a701f6f5976/w64h48.jpg', '2011-12-13 20:39:14', '2011-12-13 20:39:14'),
(158, 10, 'image/jpeg', 'jpeg', NULL, NULL, 1, 'http://media.point2.com/cdr/listing/3576/13db/36b0/f7f1c96d9a1042cb8379/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/3576/13db/36b0/f7f1c96d9a1042cb8379/w64h48.jpg', '2011-12-13 20:39:14', '2011-12-13 20:39:14'),
(159, 10, 'image/jpeg', 'jpeg', NULL, NULL, 1, 'http://media.point2.com/cdr/listing/4ccd/292b/f1f7/2b2e30bdb8d81a32a70f/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/4ccd/292b/f1f7/2b2e30bdb8d81a32a70f/w64h48.jpg', '2011-12-13 20:39:14', '2011-12-13 20:39:14'),
(160, 10, 'image/jpeg', 'jpeg', NULL, NULL, 1, 'http://media.point2.com/cdr/listing/e910/70ee/de2f/c59702ff0fb66d916299/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/e910/70ee/de2f/c59702ff0fb66d916299/w64h48.jpg', '2011-12-13 20:39:14', '2011-12-13 20:39:14'),
(161, 10, 'image/jpeg', 'jpeg', NULL, NULL, 1, 'http://media.point2.com/cdr/listing/ea95/16f5/96e0/0df6ab6c8b4b44394164/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/ea95/16f5/96e0/0df6ab6c8b4b44394164/w64h48.jpg', '2011-12-13 20:39:15', '2011-12-13 20:39:15'),
(162, 10, 'image/jpeg', 'jpeg', NULL, NULL, 1, 'http://media.point2.com/cdr/listing/e143/05e4/adcd/d229075bbf911d4385d9/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/e143/05e4/adcd/d229075bbf911d4385d9/w64h48.jpg', '2011-12-13 20:39:15', '2011-12-13 20:39:15'),
(163, 10, 'image/jpeg', 'jpeg', NULL, NULL, 1, 'http://media.point2.com/cdr/listing/993b/b07a/8e50/5e754293919fe785815c/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/993b/b07a/8e50/5e754293919fe785815c/w64h48.jpg', '2011-12-13 20:39:15', '2011-12-13 20:39:15'),
(164, 10, 'image/jpeg', 'jpeg', NULL, NULL, 1, 'http://media.point2.com/cdr/listing/7d6a/88e8/1cd0/080b945e1bac5846e387/w475h356wm.jpg', 'http://media.point2.com/cdr/listing/7d6a/88e8/1cd0/080b945e1bac5846e387/w64h48.jpg', '2011-12-13 20:39:15', '2011-12-13 20:39:15');

-- --------------------------------------------------------

--
-- Table structure for table `listing_meta_details`
--

CREATE TABLE IF NOT EXISTS `listing_meta_details` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `listing_id` int(10) unsigned NOT NULL,
  `src_listing_uid` int(10) unsigned DEFAULT NULL,
  `src_provider_listing_uid` int(10) unsigned DEFAULT NULL,
  `src_provider_name` varchar(255) DEFAULT NULL,
  `src_list_date` datetime DEFAULT NULL,
  `src_last_update_date` datetime DEFAULT NULL,
  `regional_mls_number` varchar(25) DEFAULT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `listing_id` (`listing_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `listing_meta_details`
--

INSERT INTO `listing_meta_details` (`id`, `listing_id`, `src_listing_uid`, `src_provider_listing_uid`, `src_provider_name`, `src_list_date`, `src_last_update_date`, `regional_mls_number`, `created`, `modified`) VALUES
(1, 1, 10104335, 10104335, 'OnlineOffice', '2010-12-09 01:00:00', '2011-05-25 23:26:00', NULL, '2011-12-13 20:39:03', '2011-12-13 20:39:03'),
(2, 2, 10071262, 10071262, 'OnlineOffice', '2010-12-01 01:00:00', '2011-03-17 01:08:00', NULL, '2011-12-13 20:39:05', '2011-12-13 20:39:05'),
(3, 3, 10041758, 10041758, 'OnlineOffice', '2011-05-29 02:00:00', '2011-05-29 11:09:00', NULL, '2011-12-13 20:39:07', '2011-12-13 20:39:07'),
(4, 4, 10009621, 10009621, 'OnlineOffice', '2010-02-08 01:00:00', '2011-05-09 12:01:00', NULL, '2011-12-13 20:39:09', '2011-12-13 20:39:09'),
(5, 5, 10009575, 10009575, 'OnlineOffice', '2010-01-10 01:00:00', '2010-01-10 09:55:00', NULL, '2011-12-13 20:39:10', '2011-12-13 20:39:10'),
(6, 6, 1000545916, 4294967295, 'Alaska', '1969-12-31 19:00:00', '2010-10-06 16:59:16', NULL, '2011-12-13 20:39:12', '2011-12-13 20:39:12'),
(7, 7, 1000228459, 1011526, 'MidTennessee', '2010-03-20 10:00:45', '2011-01-27 16:27:59', NULL, '2011-12-13 20:39:13', '2011-12-13 20:39:13'),
(8, 8, 1000228483, 1011817, 'MidTennessee', '2010-03-20 10:00:46', '2011-04-27 17:08:31', NULL, '2011-12-13 20:39:13', '2011-12-13 20:39:13'),
(9, 9, 1100084351, 579542, 'OnlineOffice', '2008-01-24 01:00:00', '2008-01-24 15:46:00', NULL, '2011-12-13 20:39:14', '2011-12-13 20:39:14'),
(10, 10, 1000610045, 72816, 'LINK', '2009-04-21 14:42:54', '2011-05-28 06:00:01', NULL, '2011-12-13 20:39:15', '2011-12-13 20:39:15');

-- --------------------------------------------------------

--
-- Table structure for table `listing_statuses`
--

CREATE TABLE IF NOT EXISTS `listing_statuses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `active` tinyint(1) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `listing_statuses`
--

INSERT INTO `listing_statuses` (`id`, `name`, `active`, `created`, `modified`) VALUES
(1, 'for_sale', 1, '2011-12-12 20:17:47', '2011-12-12 20:17:47'),
(2, 'for_rent', 1, '2011-12-12 20:17:47', '2011-12-12 20:17:47'),
(3, 'sale_pending', 1, '2011-12-12 20:18:04', '2011-12-12 20:18:04');

-- --------------------------------------------------------

--
-- Table structure for table `media_types`
--

CREATE TABLE IF NOT EXISTS `media_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `active` tinyint(1) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `media_types`
--

INSERT INTO `media_types` (`id`, `name`, `active`, `created`, `modified`) VALUES
(1, 'image', 1, '2011-12-13 16:31:24', '2011-12-13 16:31:24'),
(2, 'audio', 1, '2011-12-13 16:31:42', '2011-12-13 16:31:42'),
(3, 'video', 1, '2011-12-13 16:31:42', '2011-12-13 16:31:42'),
(4, 'avatar', 1, '2011-12-13 16:52:33', '2011-12-13 16:52:33'),
(5, 'logo', 1, '2011-12-13 16:52:33', '2011-12-13 16:52:33');

-- --------------------------------------------------------

--
-- Table structure for table `profiles`
--

CREATE TABLE IF NOT EXISTS `profiles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `last_name` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_profiles_on_user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `profiles`
--

INSERT INTO `profiles` (`id`, `first_name`, `last_name`, `user_id`, `created`, `modified`) VALUES
(1, 'Luckner Jr.', 'Jean-Baptiste', 1, '2011-11-29 04:18:52', '2011-11-29 04:18:52');

-- --------------------------------------------------------

--
-- Table structure for table `property_styles`
--

CREATE TABLE IF NOT EXISTS `property_styles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `active` tinyint(1) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  FULLTEXT KEY `name` (`name`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `property_styles`
--

INSERT INTO `property_styles` (`id`, `name`, `active`, `created`, `modified`) VALUES
(1, 'ranch', 1, '2011-12-13 18:06:59', '2011-12-13 18:06:59'),
(2, 'bungalow', 1, '2011-12-13 18:07:00', '2011-12-13 18:07:00'),
(3, 'single_story', 1, '2011-12-13 18:07:00', '2011-12-13 18:07:00'),
(4, '2_story', 1, '2011-12-13 18:07:01', '2011-12-13 18:07:01'),
(5, 'recreational,remote', 1, '2011-12-13 18:07:02', '2011-12-13 18:07:02'),
(6, 'residential_lot', 1, '2011-12-13 18:07:02', '2011-12-13 18:07:02'),
(7, 'unimproved_tract', 1, '2011-12-13 18:07:03', '2011-12-13 18:07:03');

-- --------------------------------------------------------

--
-- Table structure for table `property_types`
--

CREATE TABLE IF NOT EXISTS `property_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `active` tinyint(1) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  FULLTEXT KEY `name` (`name`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `property_types`
--

INSERT INTO `property_types` (`id`, `name`, `active`, `created`, `modified`) VALUES
(1, 'farm_and_ranch', 1, '2011-12-13 18:35:47', '2011-12-13 18:35:47'),
(2, 'residential', 1, '2011-12-13 18:35:48', '2011-12-13 18:35:48'),
(3, 'lots_and_land', 1, '2011-12-13 18:35:49', '2011-12-13 18:35:49');

-- --------------------------------------------------------

--
-- Table structure for table `sources`
--

CREATE TABLE IF NOT EXISTS `sources` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET latin1 NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `sources`
--

INSERT INTO `sources` (`id`, `name`, `created`, `modified`) VALUES
(1, 'Point2.com', '2011-12-07 15:25:39', '2011-12-07 15:25:39');

-- --------------------------------------------------------

--
-- Table structure for table `states`
--

CREATE TABLE IF NOT EXISTS `states` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `short_name` varchar(2) NOT NULL,
  `long_name` varchar(60) NOT NULL,
  `country_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `country_id` (`country_id`),
  FULLTEXT KEY `short_name` (`short_name`),
  FULLTEXT KEY `long_name` (`long_name`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=73 ;

--
-- Dumping data for table `states`
--

INSERT INTO `states` (`id`, `short_name`, `long_name`, `country_id`) VALUES
(1, 'AL', 'Alabama', 225),
(2, 'AK', 'Alaska', 225),
(3, 'AS', 'American Samoa', 225),
(4, 'AZ', 'Arizona', 225),
(5, 'AR', 'Arkansas', 225),
(6, 'CA', 'California', 225),
(7, 'CO', 'Colorado', 225),
(8, 'CT', 'Connecticut', 225),
(9, 'DE', 'Delaware', 225),
(10, 'DC', 'District of Columbia', 225),
(11, 'FM', 'Federated statess of Micronesia', 225),
(12, 'FL', 'Florida', 225),
(13, 'GA', 'Georgia', 225),
(14, 'GU', 'Guam', 225),
(15, 'HI', 'Hawaii', 225),
(16, 'ID', 'Idaho', 225),
(17, 'IL', 'Illinois', 225),
(18, 'IN', 'Indiana', 225),
(19, 'IA', 'Iowa', 225),
(20, 'KS', 'Kansas', 225),
(21, 'KY', 'Kentucky', 225),
(22, 'LA', 'Louisiana', 225),
(23, 'ME', 'Maine', 225),
(24, 'MH', 'Marshall Islands', 225),
(25, 'MD', 'Maryland', 225),
(26, 'MA', 'Massachusetts', 225),
(27, 'MI', 'Michigan', 225),
(28, 'MN', 'Minnesota', 225),
(29, 'MS', 'Mississippi', 225),
(30, 'MO', 'Missouri', 225),
(31, 'MT', 'Montana', 225),
(32, 'NE', 'Nebraska', 225),
(33, 'NV', 'Nevada', 225),
(34, 'NH', 'New Hampshire', 225),
(35, 'NJ', 'New Jersey', 225),
(36, 'NM', 'New Mexico', 225),
(37, 'NY', 'New York', 225),
(38, 'NC', 'North Carolina', 225),
(39, 'ND', 'North Dakota', 225),
(40, 'MP', 'Northern Mariana Islands', 225),
(41, 'OH', 'Ohio', 225),
(42, 'OK', 'Oklahoma', 225),
(43, 'OR', 'Oregon', 225),
(44, 'PW', 'Palau', 225),
(45, 'PA', 'Pennsylvania', 225),
(46, 'PR', 'Puerto Rico', 225),
(47, 'RI', 'Rhode Island', 225),
(48, 'SC', 'South Carolina', 225),
(49, 'SD', 'South Dakota', 225),
(50, 'TN', 'Tennessee', 225),
(51, 'TX', 'Texas', 225),
(52, 'UT', 'Utah', 225),
(53, 'VT', 'Vermont', 225),
(54, 'VI', 'Virgin Islands', 225),
(55, 'VA', 'Virginia', 225),
(56, 'WA', 'Washington', 225),
(57, 'WV', 'West Virginia', 225),
(58, 'WI', 'Wisconsin', 225),
(59, 'WY', 'Wyoming', 225),
(60, 'AB', 'Alberta', 38),
(61, 'BC', 'British Columbia', 38),
(62, 'MB', 'Manitoba', 38),
(63, 'NB', 'New Brunswick', 38),
(64, 'NL', 'Newfoundland and Labrador', 38),
(65, 'NT', 'Northwest Territories', 38),
(66, 'NS', 'Nova Scotia', 38),
(67, 'NU', 'Nunavut', 38),
(68, 'ON', 'Ontario', 38),
(69, 'PE', 'Prince Edward Island', 38),
(70, 'QC', 'Quebec', 38),
(71, 'SK', 'Saskatchewan', 38),
(72, 'YT', 'Yukon', 38);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) CHARACTER SET latin1 NOT NULL,
  `username` varchar(255) CHARACTER SET latin1 NOT NULL,
  `password` char(40) CHARACTER SET latin1 NOT NULL,
  `group_id` int(11) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `username`, `password`, `group_id`, `created`, `modified`) VALUES
(1, 'lucknerjb@gmail.com', 'lucknerjb', 'f3556878c21fd76fe840f2f46ebc476b52e79632', 1, '2011-12-06 23:31:35', '2011-12-06 23:31:35'),
(2, 'admin@trystingtrees.com', 'admin', '682951acea4c4af0c16125771245a3e6d4289ffa', 2, '2011-12-07 15:17:58', '2011-12-07 15:17:58');

-- --------------------------------------------------------

--
-- Table structure for table `xml_uploads`
--

CREATE TABLE IF NOT EXISTS `xml_uploads` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `source_id` int(11) DEFAULT NULL,
  `xml_file` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `num_listings` int(11) DEFAULT NULL,
  `processed` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `xml_uploads`
--

INSERT INTO `xml_uploads` (`id`, `source_id`, `xml_file`, `num_listings`, `processed`, `created`, `modified`) VALUES
(1, 1, 'ListingDataFeed.xml', 10, 1, '2011-12-11 09:05:39', '2011-12-13 20:39:15');
