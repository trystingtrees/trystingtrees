<?php
App::uses('AppModel', 'Model');
/**
 * ListingAgent Model
 *
 * @property User $User
 */
class ListingAgent extends AppModel {

	//The Associations below have been created with all possible keys, those that are not needed can be removed

	public $hasMany = array(
		'AgentMedia' => array(
			'className' => 'AgentMedia', 
			'foreignKey' => 'listing_agent_id'
		), 
		'AgentArea'
	);

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		), 
		'Listing' => array(
			'className' => 'Listing',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		), 
		'State', 'Country'
	);

	public function is_duplicate($agent_id){
		$row = $this->findBySrcAgentUid($agent_id, array('fields' => 'ListingAgent.id'));

		return ($row == false) ? false : $row['ListingAgent']['id'];
	}

	public function findByUsername($username){
		$this->loadModel('User');
      $user = $this->User->getUserByUsername($username);

      if (isset($user['User']['id'])){
         if ($user['User']['group_id'] !== '12') return false; //Not an agent
         //Find the Agent Listing Id
         $this->loadModel('ListingAgent');
         return $this->ListingAgent->findByUserId($user['User']['id']);
      }else{
         return false;
      }
	}

	public function findById(){
	}

	public function findByUserId($id){
		$options = array(
			'conditions' => array(
				'ListingAgent.user_id = ' . $id
			)
		);
   	$row = $this->find('first', $options);

		return (!empty($row)) ? $row : false;
	}

	function searchByLocation($location, $states = null){
		//See if there are any states that match this search criteria
		$options = array();
		$conditions = array();
		if (!is_null($states)){
			foreach($states as $state){
				$conditions['OR'][] =  'ListingAgent.state_id = ' . $state;
			}
		}

		//Add location
		if (isset($conditions['OR']) && sizeof($conditions['OR']) > 0){
			$conditions['OR'][] = array('ListingAgent.city LIKE \'%' . $location . '%\'');
		}else{
			$conditions = array('ListingAgent.city LIKE \'%' . $location . '%\'');
		}

		$options = array('conditions' => $conditions);
		$rows = $this->find('all', $options);

		return $rows;
	}

	public function afterFind($data){
		$default_pic = Configure::read('DEFAULT_PROFILE_PIC_URL');

		foreach($data as &$d){
			if (!isset($d['ListingAgent']['picture_file_name'])) continue;
			if ($d['ListingAgent']['picture_file_name'] == '') $d['ListingAgent']['picture_file_name'] = $default_pic;
		}
		return $data;
	}
}
