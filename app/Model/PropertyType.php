<?php
App::uses('AppModel', 'Model');
/**
 * PropertyType Model
 *
 * @property ListingDetail $ListingDetail
 */
class PropertyType extends AppModel {

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'ListingDetail' => array(
			'className' => 'ListingDetail',
			'foreignKey' => 'property_type_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

	public function find_by_name($name){
		$conditions = array(
			"MATCH(PropertyType.name) AGAINST(\"{$name}\" IN BOOLEAN MODE)"
		);

		$row = $this->find('first', array('conditions' => $conditions, 'fields' => 'PropertyType.id'));

		return ($row == false) ? false : $row['PropertyType']['id'];
	}

	public function get_as_array(){
		$rows = $this->find('list');
		return $rows;
	}

}
