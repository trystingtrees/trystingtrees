<?php
App::uses('AppModel', 'Model');
/**
 * ListingStatus Model
 *
 * @property Listing $Listing
 */
class ListingStatus extends AppModel {

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'Listing' => array(
			'className' => 'Listing',
			'foreignKey' => 'listing_status_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

	public function find_by_name($name){
		$row = $this->findByName($name, array('fields' => 'ListingStatus.id'));

		return ($row == false) ? false : $row['ListingStatus']['id'];
	}

	public function get_as_array(){
		$rows = $this->find('list');
		return $rows;
	}

}
