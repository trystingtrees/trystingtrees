<?php
App::uses('AppModel', 'Model');
/**
 * Country Model
 *
 * @property ListingLocation $ListingLocation
 * @property State $State
 */
class Country extends AppModel {
	public $displayField = 'short_name';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'short_name' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'long_name' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'ListingLocation' => array(
			'className' => 'ListingLocation',
			'foreignKey' => 'country_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'State' => array(
			'className' => 'State',
			'foreignKey' => 'country_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

	public function find_by_short_name($name){
		$row = $this->findByShortName($name, array('fields' => 'Country.id'));

		return ($row == false) ? -1 : $row['Country']['id'];
	}

	public function get_as_array($display = 'short_name'){
		$this->displayField = $display;
		$rows = $this->find('list');

		//@TODO: Remove temp array
		return array(
			'38' => 'Canada', 
			'225' => 'United States'
		);

		//return $rows;
	}

}
