<?php
class User extends AppModel {
	public $name = "User";
	public $belongsTo = array('Group');
	public $actsAs = array('Acl' => array('type' => 'requester'));

	function parentNode(){
		if (!$this->id && empty($this->data)){
			return null;
		}

		if (isset($this->data['User']['group_id'])) {
			$groupId = $this->data['User']['group_id'];
	  	} else {
			$groupId = $this->field('group_id');
	  	}

	  	if (!$groupId) {
			return null;
	  	} else {
			return array('Group' => array('id' => $groupId));
      }
	}

	public function beforeSave(){
		$this->data['User']['password'] = AuthComponent::password($this->data['User']['passwd']);
		unset($this->data['User']['passwd_confirm']);

		return true;
	}

	public function username_exists($username){
		$row = $this->findByUsername($username);

		return ($row) ? true : false;
	}

	public function email_exists($email){
		$row = $this->findByEmail($email);

		return ($row) ? true : false;
	}

	function getUserByUsername($username){
		return $this->find('first', array('conditions' => array("User.username LIKE '%{$username}%'")));
	}
}
