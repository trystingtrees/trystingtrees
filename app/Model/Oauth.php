<?php
	class Oauth extends AppModel {
		public $name = 'Oauth';

		/**
		 * Check to see if a user is registered with us
		 * 
		 */
		public function is_registered_user($social_id){
			$row = $this->findBySocialId($social_id);

			return $row;
		}
	}
