<?php
App::uses('AppModel', 'Model');
/**
 * AgentArea Model
 *
 */
class AgentArea extends AppModel {
	public $name = 'AgentArea';
	public $belongsTo = array('ListingAgent', 'Map');

	/**
	 * Fetch the row id of the row matching the agent id and the map id
	 *
	 * @param int agent id
	 * @param int map id
	 * 
	 * @return int row id
	 */
	function findAreaId($agent_id, $map_id){
		$options = array(
			'conditions' => array(
				'AgentArea.listing_agent_id = ' . $agent_id, 
				'AgentArea.map_id = ' . $map_id
			)
		);
		$row = $this->find('first', $options);

		return (!empty($row)) ? $row['AgentArea']['id'] : false;
	}

	/**
	  * Find the areas that an agent has selected already
	  * 
	  * @param agent ID
	  * 
	  * @return mixed areas array
	  */
	function findAgentSelectedAreas($agent_id){
		$options = array(
			'conditions' => array(
				'AgentArea.listing_agent_id = ' . $agent_id
			)
		);

		$areas = $this->find('all', $options);
		return $areas;
	}

	function findAgentSelectedAreasOverview($agent_id){
		$options = array(
			'conditions' => array(
				'AgentArea.listing_agent_id = ' . $agent_id, 
				'Map.status = 1'	//Active map
			), 
			'fields' => array(
				'Map.id, Map.nice_name, Map.city, Map.state_id, Map.country_id'
			)
		);

		$areas = $this->find('all', $options);
		return $areas;
	}

	/**
	 * Find out whether or not an area is selected
	*/
	function areaSelected($map_id, $agent_id){
		$options = array(
			'conditions' => array(
				'AgentArea.map_id = ' . $map_id, 
				'AgentArea.listing_agent_id = ' . $agent_id
			)
		);

		$map = $this->find('first', $options);

		return (empty($map)) ? false : true;
	}

	function getNumberOfSelectedAreas($agent_id){
		$options = array(
			'conditions' => array(
				'AgentArea.listing_agent_id = ' . $agent_id
			)
		);
		$count = $this->find('count', $options);

		return $count;
	}

	function getMapAgents($map_id){
		$conditions = array('AgentArea.map_id = ' . $map_id);
		$fields = array(
			'DISTINCT (AgentArea.listing_agent_id), 
			ListingAgent.id, ListingAgent.user_id, ListingAgent.uuid, ListingAgent.name, ListingAgent.city, ListingAgent.state_id, ListingAgent.country_id, ListingAgent.picture_file_name'
		);

		$rows = $this->find('all', array('conditions' => $conditions, 'fields' => $fields));

		return ($rows) ? $rows : false;
	}
}
