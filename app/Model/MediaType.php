<?php
App::uses('AppModel', 'Model');
/**
 * MediaType Model
 *
 * @property AgentMedia $AgentMedia
 * @property ListingMedia $ListingMedia
 */
class MediaType extends AppModel {

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'AgentMedia' => array(
			'className' => 'AgentMedia',
			'foreignKey' => 'media_type_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'ListingMedia' => array(
			'className' => 'ListingMedia',
			'foreignKey' => 'media_type_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

	public function find_by_name($name){
		$name = strtolower($name);
		$row = $this->findByName($name, array('fields' => 'MediaType.id'));

		return ($row == false) ? false : $row['MediaType']['id'];
	}

	public function get_as_array(){
		$rows = $this->find('list');
		return $rows;
	}

}
