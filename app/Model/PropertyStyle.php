<?php
App::uses('AppModel', 'Model');
/**
 * PropertyStyle Model
 *
 * @property ListingDetail $ListingDetail
 */
class PropertyStyle extends AppModel {

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'ListingDetail' => array(
			'className' => 'ListingDetail',
			'foreignKey' => 'property_style_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

	public function find_by_name($name){
		$conditions = array(
			"MATCH(PropertyStyle.name) AGAINST(\"{$name}\" IN BOOLEAN MODE)"
		);

		$row = $this->find('first', array('conditions' => $conditions, 'fields' => 'PropertyStyle.id'));

		return ($row == false) ? false : $row['PropertyStyle']['id'];
	}

	public function get_as_array(){
		$rows = $this->find('list');
		return $rows;
	}

}
