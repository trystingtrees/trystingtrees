<?php
App::uses('AppModel', 'Model');
/**
 * State Model
 *
 * @property Country $Country
 * @property ListingLocation $ListingLocation
 */
class State extends AppModel {
	public $displayField = 'short_name';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'short_name' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'long_name' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'country_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Country' => array(
			'className' => 'Country',
			'foreignKey' => 'country_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'ListingLocation' => array(
			'className' => 'ListingLocation',
			'foreignKey' => 'state_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

	public function find_by_short_name($name){
		$row = $this->findByShortName($name, array('fields' => 'State.id'));
		
		/*
		$log = $this->getDataSource()->getLog(false, false);
		debug($log);
		*/

		return ($row == false) ? -1 : $row['State']['id'];
	}

	public function get_as_array(){
		$rows = $this->find('list');
		return $rows;
	}

	public function search($name){
		$options = array(
			'conditions' => array(
				'OR' => array(
					'State.short_name LIKE "%' . $name . '%"', 
					'State.long_name LIKE "%' . $name . '%"', 
				)
			)
		);

		return $this->find('all', $options);
	}

}
