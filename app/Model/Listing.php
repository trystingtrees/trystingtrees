<?php
App::uses('AppModel', 'Model');
/**
 * Listing Model
 *
 * @property Source $Source
 * @property ListingStatus $ListingStatus
 * @property User $User
 * @property ListingDetail $ListingDetail
 * @property ListingFinancial $ListingFinancial
 * @property ListingLocation $ListingLocation
 * @property ListingMedia $ListingMedia
 * @property ListingMetaDetail $ListingMetaDetail
 */
class Listing extends AppModel {
	public $actsAs = array('Containable');

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'listing_category' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'listing_subcategory' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Source' => array(
			'className' => 'Source',
			'foreignKey' => 'source_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'ListingStatus' => array(
			'className' => 'ListingStatus',
			'foreignKey' => 'listing_status_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'ListingAgent' => array(
			'className' => 'ListingAgent',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'ListingDetail' => array(
			'className' => 'ListingDetail',
			'foreignKey' => 'listing_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'ListingFeature' => array(
			'className' => 'ListingFeature',
			'foreignKey' => 'listing_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'ListingFinancial' => array(
			'className' => 'ListingFinancial',
			'foreignKey' => 'listing_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'ListingLocation' => array(
			'className' => 'ListingLocation',
			'foreignKey' => 'listing_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'ListingMedia' => array(
			'className' => 'ListingMedia',
			'foreignKey' => 'listing_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'ListingMetaDetail' => array(
			'className' => 'ListingMetaDetail',
			'foreignKey' => 'listing_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

	public function findByUserId($id){
		$this->contain(
			'ListingLocation', 
			'ListingAgent', 
			'ListingFinancial', 
			'ListingMedia', 
			'ListingFeature', 
			'ListingDetail', 
			'ListingStatus'
		);

/*
		$listing_states_array = array(
			'table' => 'states', 
			'alias' => 'State', 
			'type' => 'INNER', 
			'conditions' => 'ListingLocation.state_id = State.id'
		);

		$listing_countries_array = array(
			'table' => 'countries', 
			'alias' => 'Country', 
			'type' => 'INNER', 
			'conditions' => 'ListingLocation.country_id = Country.id'
		);

		$listing_locations_array = array(
			'table' => 'listing_locations', 
			'alias' => 'ListingLocation', 
			'type' => 'INNER', 
			'conditions' => 'Listing.id = ListingLocation.listing_id'
		);
*/

      //=========================================================== JOIN ARRAYS ====================================================
      $listing_locations_array = array(
         'table' => 'listing_locations',
         'alias' => 'ListingLocation',
         'type' => 'INNER',
         'conditions' => 'ListingLocation.listing_id = Listing.id'
      );

      $listing_states_array = array(
         'table' => 'states',
         'alias' => 'State',
         'type' => 'INNER',
         'conditions' => 'ListingLocation.state_id = State.id'
      );

      $listing_countries_array = array(
         'table' => 'countries',
         'alias' => 'Country',
         'type' => 'INNER',
         'conditions' => 'ListingLocation.country_id = Country.id'
      );

      $listing_agents_array = array(
         'table' => 'listing_agents',
         'alias' => 'ListingAgent',
         'type' => 'INNER',
         'conditions' => 'ListingAgent.user_id = Listing.user_id'
      );

      $listing_details_array = array(
         'table' => 'listing_details',
         'alias' => 'ListingDetail',
         'type' => 'INNER',
         'conditions' => 'ListingDetail.listing_id = Listing.id'
      );

      $listing_medias_array = array(
         'table' => 'listing_media',
         'alias' => 'ListingMedia',
         'type' => 'INNER',
         'conditions' => 'ListingMedia.listing_id = Listing.id'
      );

      $listing_features_array = array(
         'table' => 'listing_features',
         'alias' => 'ListingFeature',
         'type' => 'INNER',
         'conditions' => 'ListingFeature.listing_id = Listing.id'
      );

      $listing_financials_array = array(
         'table' => 'listing_financials',
         'alias' => 'ListingFinancial',
         'type' => 'INNER',
         'conditions' => 'ListingFinancial.listing_id = Listing.id'
      );

      $listing_meta_details_array = array(
         'table' => 'listing_meta_details',
         'alias' => 'ListingMetaDetail',
         'type' => 'INNER',
         'conditions' => 'ListingMetaDetail.listing_id = Listing.id'
      );

      $property_styles_array = array(
         'table' => 'property_styles',
         'alias' => 'PropertyStyle',
         'type' => 'INNER',
         'conditions' => 'PropertyStyle.id = ListingDetail.property_style_id'
      );

		$options['fields'] = array(
					      );

		$options = array(
			'conditions' => array(
				'Listing.user_id = ' . $id
			), 
			'fields' => 'DISTINCT (Listing.id), Listing.uuid, Listing.user_id, ListingAgent.id, ListingAgent.uuid, ListingAgent.name, PropertyStyle.name, ListingFeature.*, State.*'
		);

		$options['joins'] = array();

		//Add the default joins
      $options['joins'][] = $listing_financials_array;
      $options['joins'][] = $listing_locations_array;
      $options['joins'][] = $listing_meta_details_array;
      $options['joins'][] = $listing_details_array;
      $options['joins'][] = $listing_features_array;
      $options['joins'][] = $property_styles_array;
      $options['joins'][] = $listing_states_array;

   	$row = $this->find('all', $options);

		return (!empty($row)) ? $row : false;
	}

}
