<?php
	class Map extends AppModel {
		public $name = 'Map';
		public $belongsTo = array('State', 'Country');

		/**
		 * Find maps related to the array of map_id's given 
		 * 
		 * @param array map_ids
		 *
		 * @return array maps
		 */
		function findMaps($ids){
			$returnArray = array();

			foreach($ids as $id){
				$map = $this->read(null, $id);
				array_push($returnArray, $map);
			}

			return $returnArray;
		}

		function searchByLocation($location, $states = null){
			//See if there are any states that match this search criteria
			$options = array();
			$conditions = array();
			if (!is_null($states)){
				foreach($states as $state){
					$tmp = array('Map.state_id' => $state);
					$conditions['OR'][] =  'Map.state_id = ' . $state;
				}
			}

			//Add location
			if (isset($conditions['OR']) && sizeof($conditions['OR']) > 0){
				$conditions['OR'][] = array('Map.city LIKE \'%' . $location . '%\'');
			}else{
				$conditions = array('Map.city LIKE \'%' . $location . '%\'');
			}

			$options = array('conditions' => $conditions);
			$rows = $this->find('all', $options);

			return $rows;
		}

	}
