<?php
App::uses('AppModel', 'Model');
/**
 * Question Model
 *
 */
class Question extends AppModel {
	public $displayField = 'short_name';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'short_name' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Country' => array(
			'className' => 'Country',
			'foreignKey' => 'country_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

	public function find_by_short_name($name){
		$row = $this->findByShortName($name, array('fields' => 'Question.id'));
		
		/*
		$log = $this->getDataSource()->getLog(false, false);
		debug($log);
		*/

		return ($row == false) ? -1 : $row['Question']['id'];
	}

	public function get_as_array(){
		$rows = $this->find('list');
		return $rows;
	}

}
