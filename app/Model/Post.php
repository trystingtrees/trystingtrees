<?php
	class Post extends AppModel {
		public $name = 'Post';

		/*
		public $validate = array(
			'title' => VALID_NOT_EMPTY, 
			'body' => VALID_NOT_EMPTY
		);
		*/

		function beforeSave(){
			if (empty($this->id)){
				$this->data[$this->name]['url'] = $this->getUniqueUrl($this->data[$this->name]['title'], 'url');
			}

			return true;
		}

		function getUniqueUrl($string, $field) { 
      // Build URL 
         
      $currentUrl = $this->_getStringAsURL($string); 
         
      // Look for same URL, if so try until we find a unique one 
         
      $conditions = array($this->name . '.' . $field => 'LIKE "' . $currentUrl . '%"'); 
         
      $result = $this->find('all', array('conditions' => $conditions));
         
      if ($result !== false && count($result) > 0) { 
      	$sameUrls = array(); 
			 
			foreach($result as $record) { 
				$sameUrls[] = $record[$this->name][$field]; 
			} 
      } 
     
		if (isset($sameUrls) && count($sameUrls) > 0) { 
      	$currentBegginingUrl = $currentUrl; 
     
         $currentIndex = 1; 
     
         while($currentIndex > 0) {
         	if (!in_array($currentBegginingUrl . '_' . $currentIndex, $sameUrls)) {
            	$currentUrl = $currentBegginingUrl . '_' . $currentIndex; 
              	$currentIndex = -1; 
            } 
     
           	$currentIndex++; 
         } 
      } 
         
        	return $currentUrl; 
    	} 
     
    	function _getStringAsURL($string) { 
         // Define the maximum number of characters allowed as part of the URL 
         
         $currentMaximumURLLength = 100; 
         
         $string = strtolower($string); 
         
         // Any non valid characters will be treated as _, also remove duplicate _ 
         
         $string = preg_replace('/[^a-z0-9_]/i', '_', $string); 
         $string = preg_replace('/_[_]*/i', '_', $string); 
         
         // Cut at a specified length 
         
         if (strlen($string) > $currentMaximumURLLength) { 
             $string = substr($string, 0, $currentMaximumURLLength); 
         } 
         
         // Remove beggining and ending signs 
         
         $string = preg_replace('/_$/i', '', $string); 
         $string = preg_replace('/^_/i', '', $string); 
         
      	return $string; 
		}

		function findPost($title, $date, $agent_id){
			$title = $this->_getStringAsUrl($title);
			$options = array(
				'conditions' => array(
					"Post.listing_agent_id = {$agent_id}", 
					"Post.created LIKE '{$date}%'", 
					"Post.url LIKE '{$title}'"
				)
			);

			$post = $this->find('first', $options);

			return $post;
		}
	}
