function agentPost(){
	this.id = null;
	this.new_caption = '';
	this.elem_id = null;
}

//Set the ID of the current Post
agentPost.prototype.setId = function(id){
	this.id = id
}

//Set the ID of the current Element
agentPost.prototype.setElemId = function(id){
	this.elem_id = id
}

agentPost.prototype.getNewCaption = function(){
	return this.new_caption;
}

//Publish the current post
agentPost.prototype.publish = function(){
	if (this.id == null){
		alert('Could not publish Post: Invalid ID');
	}else{
		//Publish the post
		elem_id = this.elem_id;
		$.post(
			App.server + '/posts/publish', {id: this.id}, 
			function(response){
				success = response.status;
				message = response.message;

				if(success){
					//Remove publish class and add unpublish class and change link text
					/*
					App.removeClass(elem_id, 'publish_agent_post');
					App.addClass(elem_id, 'unpublish_agent_post');
					document.getElementById(elem_id).innerHTML = 'Unpublish';
					*/
					$( '#' + elem_id ).removeClass('publish_agent_post').addClass('unpublish_agent_post').html( 'Unpublish' );
					
					App.showSuccess(message);
				}else{
					App.showError(message);
				}
			}, 
			'json'
		);
	}
}

//Unpublish the current post
agentPost.prototype.unpublish = function(){
	if (this.id == null){
		alert('Could not unpublish Post: Invalid ID');
	}else{
		//Unpublish the post
		elem_id = this.elem_id;
		$.post(
			App.server + '/posts/unpublish', {id: this.id}, 
			function(response){
				success = response.status;
				message = response.message;
					
				if (success){
					//Remove unpublish class and add publish class and change link text
					$( '#' + elem_id ).removeClass('unpublish_agent_post').addClass('publish_agent_post').html( 'Publish' );
					
					App.showSuccess(message);
				}else{
					App.showError(message);
				}
			}, 
			'json'
		);
	}
}
