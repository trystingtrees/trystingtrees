jQuery.noConflict();


// Animated Image Preload Effect
// Courtesy of Sean Corey at http://5-squared.com
jQuery(document).ready(function(){	
	jQuery('.imgfade').hide();//hide all the images on the page
});

var i = 0;//initialize
var int=0;//Internet Explorer Fix
jQuery(window).bind("load", function() {//The load event will only fire if the entire page or document is fully loaded
	int = setInterval("doThis(i)",500);
});

function doThis() {
	var images = jQuery('.imgfade').length;//count the number of images on the page
	if (i >= images) {// Loop the images
		clearInterval(int);//When it reaches the last image the loop ends
		delete int;
	}
	jQuery('.imgfade:hidden').eq(0).fadeIn(500);//fades in the hidden images one by one
	i++;//add 1 to the count
}

//Core Functions
jQuery(document).ready(function(){
	
	jQuery(".menu li:last-child").addClass("last");
	jQuery(".sub-menu li:last-child").removeClass("last");
	jQuery(".widget:first-child").addClass("first");
	jQuery(".widget li:last-child").addClass("item-last");
	jQuery("ul.tweet_list li:last-child").addClass("item-last");
	
	//Insert commas in numbers as user types
	jQuery('input.number').keyup(function(){
		var jQuerythis = jQuery(this);
		var num = jQuerythis.val().replace(/,/g, '');
		jQuerythis.val(num.replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,"));
	});
});