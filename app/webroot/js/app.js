//Class: App.js

var App = {
	server: '', 
	/*
	 * Get the ID of the DB row from the object's ID attribute. 
	 *
	 * @param $obj The object
	 * @param $pos The position in the ID in the object's id array
	 * @param $del The delimiter on which to split the object's ID on
	 *
	 * @return ID
	 *
	 */
	getElementCustomId: function($obj, $pos, $del){
		$id_string = $obj.attr( 'id' );
		$id = $id_string.split( $del )[$pos];

		return $id;
	}, 

	getServer: function(){
		location_url = document.location.href;
		if (location_url.indexOf('/') !== -1){
			var path = location_url.split('/');
			www = path[2].substr(0, 3);
			domain = path[2];
		}else{
			var path = location_url;
			www = '';
			domain = path;
		}

		if (domain.indexOf('/') !== -1){
			//Split the domain on the periods and grab the first element. This will allow us to know what the environment is
			domain_parts = domain.split('/');
			env = domain_parts[0];
		}else{
			env = domain;
		}

		switch(env){
			case 'www':
				server_val = 'http://trystingtrees.com';
				break;
			case 'trystingtrees.localhost':
				server_val = 'http://trystingtrees.localhost';
				break;
			case '63.141.238.195':
				server_val = 'http://63.141.238.195/Trystingtrees';
				break;
			case 'thewebsandbox.com':
				server_val = 'http://thewebsandbox.com/Trystingtrees';
				break;
			case 'www.thewebsandbox.com':
				server_val = 'http://www.thewebsandbox.com/Trystingtrees';
				break;
			case 'dev01.trystingtrees.com':
				server_val = 'http://dev01.trystingtrees.com';
				break;
		}

		this.server = server_val;
	}, 

	fieldExists: function(field){
		return ($( field ).length > 0);
	}, 

	addClass: function(obj_id, class_name){
		if (App.hasClass(obj_id, class_name)) return false;

		document.getElementById( obj_id ).className += " " + class_name;
	}, 

	removeClass: function(obj_id, class_name){
		var reg = new RegExp('\\s|^' + class_name + '\\s|$');
		document.getElementById( obj_id ).className = document.getElementById( obj_id ).className.replace( reg , ' ' )
	}, 

	hasClass: function(obj_id, class_name){
		return document.getElementById( obj_id ).className.match(new RegExp('(\\s|^)' + class_name + '(\\s|$)'));
	}, 

	showError: function(err_msg){
		//Clear msg box
		$( '#success_message_box' ).hide('fast').html('');
		$( '#error_message_box' ).hide('fast').html( err_msg ).fadeIn('slow');
	}, 

	showSuccess: function(success_msg){
		//Clear msg box
		$( '#error_message_box' ).hide('fast').html('');
		$( '#success_message_box' ).hide('fast').html( success_msg ).fadeIn('slow');
	}, 

	check_social_actions: function(){
		$twitter_action = $( '#twitter_action' ).val();
		$facebook_action = $( '#facebook_action' ).val();

		if ($twitter_action == 'welcome') this.show_twitter_welcome();

		if ($facebook_action == 'welcome') this.show_facebook_welcome();
	}, 

	show_twitter_welcome: function(){
		alert('Work on twitter welcome');
		this.remove_social_action('twitter');
	}, 

	show_facebook_welcome: function(){
		alert('Work on facebook welcome');
		this.remove_social_action('facebook');
	}, 

	remove_social_action: function($action){
		$.ajax({
			type: 'POST', 
			url: this.server + '/social_auths/remove_actions', 
			data: 'action=' + $action, 
			dataType: 'json', 
			success: function(data){
				if (data.status == 'error') alert(data.message);
			}, 
			error: function(data){
				alert('Error: Could not remove actions');
			}
		});
	}
}

