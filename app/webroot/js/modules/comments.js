
function Comments(blog_uuid, post_id){
	this.blog_uuid = blog_uuid;
	this.post_id = post_id;
	this.comment_id = null;

	//Get the server
	App.getServer();
}

/**
 * Get the current blog's UUID
 *
 */
Comments.prototype.getBlogUuid = function(){
	return this.blog_uuid;
}

/**
 * Get the current Post ID
 *
 */
Comments.prototype.getPostID = function(){
	return this.post_id;
}

/**
 * Create a new comment
 *
 */
Comments.prototype.createComment = function(name, email, message, $reply_to){
	//Simple validation
	if (name == null || name == ''){
		alert('Please enter a valid name');
		return;
	}

	if (email == null || email == ''){
		alert('Please enter a valid email');
		return;
	}

	if (message == null || message == ''){
		alert('Please enter a valid message');
		return;
	}

	//Serialize info for data
	name = encodeURIComponent(name);
	email = encodeURIComponent(email);
	message = encodeURIComponent(message);

	//Save the comment to the DB
	$.ajax({
		type: 'POST', 
		url: App.server + '/posts/add_comment', 
		data: 'name=' + name + '&email=' + email + '&message=' + message + '&id=' + this.blog_uuid + '&post_id=' + this.post_id + '&reply_to=' + $reply_to, 
		dataType: 'json', 
		timeout: 5000, 
		success: function(data){
			console.log(data);
		}, 
		error: function(data){
			console.log(data);
		}
	});
}
