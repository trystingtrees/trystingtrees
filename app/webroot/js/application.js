//Application.js

jQuery( document ).ready(function(){
	//Rebind the dollar sign for jQuery
	$ = jQuery.noConflict();

	//Get the environment server value for ajax calls
	App.getServer();

	//Check Social Actions
	App.check_social_actions();

	//User card trigger
	$( '#user_card .trigger' ).hover(
		function(){ $( 'ul', this ).show(); }, 
		function(){ $( 'ul', this ).hide(); }
	);

	//Character count for Post Title
	$('input#PostTitle').bind('keyup', function() {
		var maxchar = 50;
		var cnt = $(this).val().length;
		var remainingchar = maxchar - cnt;
		if(remainingchar < 0){
			$('#PostTitleCharCount').html('0');
			$(this).val($(this).val().slice(0, 50));
		}else{
			$('#PostTitleCharCount').html(remainingchar);
		}
	});

	//Publish Agent Post
	$( '.publish_agent_post' ).live('click', function($e){
		$e.preventDefault();

		//Get ID
		$id = App.getElementCustomId( $(this), 1, '_' );

		//Create an agentPost object using the ID
		$Post = new agentPost();
		$Post.setId($id);
		$Post.setElemId( $(this).attr('id') );
		$response = $Post.publish();

		delete $Post;
	});

	//Unpublish Agent Post
	$( '.unpublish_agent_post' ).live('click', function($e){
		$e.preventDefault();

		//Get ID
		$id = App.getElementCustomId( $(this), 1, '_' );

		//Create an agentPost object using the ID
		$Post = new agentPost();
		$Post.setId($id);
		$Post.setElemId( $(this).attr('id') );
		$Post.unpublish();
		$caption = $Post.getNewCaption();

		delete $Post;
	});

	//Find Agent autocomplete
	/*
	$( '#ListingAgentLocation' ).autocomplete({
		source: App.server + '/states/autocomplete'
	});
	*/
});
