$( document ).ready(function(){
	
	//Save a new comment
	$( '#addComment' ).click(function(){
		//Get some information from the form
		$uuid = $( '#uuid;', '#addCommentForm' ).val();
		$post_id = $( '#post_id;', '#addCommentForm' ).val();
		$name = $( '#name', '#addCommentForm' ).val();
		$email = $( '#email', '#addCommentForm' ).val();
		$message = $( '#message', '#addCommentForm' ).val();
		$reply_to = '';

		//Create a new Comments object
		$Comments = new Comments($uuid, $post_id);

		//Create the comment
		$comment_created = $Comments.createComment($name, $email, $message, $reply_to);
	
		return false;
	});

	$( '.replyToComment' ).live('click', function($e){
		$e.preventDefault();
		$obj = $( this );
		$id = $obj.attr( 'id' );
		$id = $id.split('_')[1];

		$reply_container = $obj.parent().next( '.reply-container' );

		if ($reply_container.is(':visible')){
			$reply_container.html('');
			$reply_container.fadeOut();
		}else{
			$( '#reply_content .reply_name' ).html( $( '#commenter_' + $id ).html() );
			$reply_container.html( $('#reply_content').html() );
			$reply_container.fadeIn();
		}
		
		return false;
	});

});
