//Geocoder
var geocoder;
var poly, map;
var markers = [];
var coordinates = [];
var polygons_array = [];
var polygon_names = [];
var path = new google.maps.MVCArray;
var geocoder;
var location_marker;
var disablePoints = false;
var save_data = '';

$( document ).ready(function(){
	initialize();

	//Google Map address locator
	$( '#gmap_address' ).autocomplete({
		//Use the geocoder to fetch address values
		source: function(request, response){
			geocoder.geocode( { 'address': request.term }, function(results, status){
				response($.map(results, function(item){
					return {
						label: item.formatted_address, 
						value: item.formatted_address, 
						latitude: item.geometry.location.lat(), 
						longitude: item.geometry.location.lng()
					}
				}));
			})
		}, 
		//Address selected
		select: function(event, ui){
			var location = new google.maps.LatLng(ui.item.latitude, ui.item.longitude);
			//location_marker.setPosition(location);
			map.setCenter(location);
		}
	});

	//Create map
	$('#create_map').click(function(){
		$coords = '';
		//Get map coordinates
		for(var i = 0, a = coordinates.length; i < a; ++i){
			$coords = $coords + coordinates[i][0] + ',' + coordinates[i][1] + '|'
		}

		//Add coordinates to form
		$( '#MapCoordinates' ).val( $coords );

		//Let form submit as usual 
		return true;
	});

	//Check the coordinates of a map
	$('#create_map2').click(function($e){
		alert(coordinates);

		//Let form submit as usual 
		$e.preventDefault();
		return false;
	});
});

//Initialize the map fucntions.
//Set the map center
//Create a new map, geocoder
function initialize(){
	//MAP
	var map_center = new google.maps.LatLng(43.653226, -79.383184);
	markers = [];
	coordinates = [];
	path = new google.maps.MVCArray;

	map= new google.maps.Map(document.getElementById('map'), {
		zoom: 14, 
		center: map_center, 
		mapTypeId: google.maps.MapTypeId.ROADMAP
	});

	//Geocoder
	geocoder = new google.maps.Geocoder();

	//Location marker
	location_marker = new google.maps.Marker({
		map: map, 
		draggable: true
	});

	//Polygon
	poly = new google.maps.Polygon({
		strokeWeight: 2, 
		fillColor: '#5555FF'
	});
	poly.setMap(map);
	poly.setPaths(new google.maps.MVCArray([path]));

	google.maps.event.addListener(map, 'click', addPoint);
}

//Draw the map used to edit map areas in the database. Read coordinates from hidden input
function drawEditMap(){
	//Read coordinates
	draw_coordinates = document.getElementById('map_coordinates').value;

	//Split coordinates on "|"
	coordinates_array = draw_coordinates.split('|');

	//Define vars
	var area;
	var area_coords = [];
	var x_coords = [];
	var y_coords = [];

	//Loop through coordinates and draw them on the map
	for(var i = 0; i < coordinates_array.length; i++){
		if (coordinates_array[i] == '') continue;
		coords = coordinates_array[i].split(',');
		c = new google.maps.LatLng(coords[0], coords[1] + '');

		//Push coordinates to temp arrays to calculate min and max - for polygon center
		x_coords.push(coords[0]);
		y_coords.push(coords[1]);

		//Save coordinates
		area_coords.push(c);

		//Draw point
		addDrawPoint(c, coords);
	}

	//find the center of the polygon created
	center_coords = findAreaCenter(x_coords, y_coords);
	center_x = center_coords[0];
	center_y = center_coords[1];
	
	map.setCenter(new google.maps.LatLng(center_x, center_y));
	map.setZoom(12);
}

function drawSelectionMaps(){
	disablePoints = true;
	counter = 0;

	//Get list of already selected maps
	selected_maps = document.getElementById('selected_area_ids').value;
	selected_maps = selected_maps.split(',');

	//Read the map names into an array
	$( '.map_names' ).each(function(){
		console.log($(this));
		/*
		polygon_names.push( 
			{
				id: App.getElementCustomId( $(this), 1, '_' ), 
				name: $(this).val()
			} 
		);
		*/
		id = App.getElementCustomId( $(this), 1, '_' );
		polygon_names[ id ] = $(this).val();

	});

	//Loop through coordinates and draw maps
	$( '.map_coordinates' ).each(function(){
		var area;
		var area_coords = [];
		var x_coords = [];
		var y_coords = [];
		var fill_color = "#FF0000";

		//Get the map id used in the DB
		id = App.getElementCustomId( $(this), 1, '_' );

		//Find whether the current map is already selected
		in_array = $.inArray(id, selected_maps);
		if (in_array !== -1){
			//Map selected, add it to the list of selected areas
			//Set the fill color to black
			fill_color = "#000000";

			//Add the polygon to the array of selected areas
			polygons_array.push(id);	

			//Update the list of selected areas
			//updateSelectedAreasList();
		}

		//	draw_coordinates = document.getElementById('map_coordinates').value;
		draw_coordinates = $( this ).val();

		//Split coordinates on "|"
		coordinates_array = draw_coordinates.split('|');

		for(var i = 0; i < coordinates_array.length; i++){
			if (coordinates_array[i] == '') continue;
			coords = coordinates_array[i].split(',');
			c = new google.maps.LatLng(coords[0], coords[1] + '');

			//Push coordinates to temp arrays to calculate min and max - for polygon center
			x_coords.push(coords[0]);
			y_coords.push(coords[1]);

			area_coords.push(c);
		}

		//Get the center coordinates of the polygon	
		center_coords = findAreaCenter(x_coords, y_coords);

		//Create the area polygon
		area = new google.maps.Polygon({
			paths: area_coords, 
			strokeColor: "#FF000", 
			strokeOpacity: 0.8, 
			strokeWeight: 2, 
			fillColor: fill_color, 
			fillOpacity: 0.35, 
			clickable: true
		});

		area.setMap(map);

		//Draw a marker right in the center of the polygon.
		//@TODO: Use could be for adding information about the areas if the user clicks on them
		//addPolygonCenterMarker(area, center_coords[0], center_coords[1], counter);

		//Add the click event for the polygon
		//addPolygonClick(area, counter);
		addPolygonClick(area, id);

		//Add the mouseover event for the polygon
		//addPolygonHover(area, center_coords, polygon_names[counter].name);
		addPolygonHover(area, center_coords, polygon_names[id]);

		counter++;
	});
}

function areaSelected(event){
	console.log( event );
}

/**
 * Add a marker in the center of a polygon
 *
 */
function addPolygonCenterMarker(polygon, x, y, id){
	var infowindow = new google.maps.InfoWindow({ content: 'test string' });

	pos = new google.maps.LatLng(x, y);

	var marker = new google.maps.Marker({
		position: pos, 
		map: map, 
		title: 'Map #' + id
	});

	//Add click event for marker
	google.maps.event.addListener(marker, 'click', function(){
		infowindow.open(map, marker);
	});
}

/**
 * On click, change the color of the polygon and add / remove it to/from the polygons array
 *
 */
function addPolygonClick(poly, id) {
	//Check to see if the polygon is selected or not
	google.maps.event.addListener(poly,'click', function(event) {
		index = polygons_array.indexOf(id);
		if (index == -1){	//not selected
			this.setOptions({fillColor: "#000000"});
			//Add the polygon to the array of selected areas
			polygons_array.push(id);	
			updateSelectedAreasList();
		}else{
			this.setOptions({fillColor: "#FF0000"});
			polygons_array.splice(index, 1);//Remove the point from the markers array. Return the point
			updateSelectedAreasList();
		}
	}); 
}

function updateSelectedAreasList(){
	document.getElementById('selected_areas').innerHTML = '';
	save_data = '';

	if(polygons_array.length == 0){
		document.getElementById('selected_areas').innerHTML = '<span> No Areas Selected </span>';
		return;
	}

	for(var i = 0; i < polygons_array.length; i++){
		span = '<span>' + polygon_names[ polygons_array[i] ] + '</span>';
		save_data = save_data + polygons_array[i] + ',';
		document.getElementById('selected_areas').innerHTML = document.getElementById('selected_areas').innerHTML + span;
	}
}

function saveSelectedAreas(){
	server = App.server;
	/*	
	var data = [];

	for(var i = 0; i < polygons_array.length; i++){
		data = data + polygon_names[ i ]  + ',';
	}
	*/

	//Remove the last comma
	data = save_data.slice(0, save_data.length - 1);

	if (data == ''){
		App.showError('Please select at least one area before updating your selected areas.');
		return;
	}

	$.ajax({
		type: 'POST', 
		url: server + '/areas/update', 
		data: 'areas=' + data, 
		dataType: 'json', 
		success: function(resp){
			console.log(resp);
			//Render the message from the ajax call
			message = resp.message;
			class_name = resp.cls;

			//show the right message
			(resp.status == 'success') ? App.showSuccess(message) : App.showError(message);
		}, 
		error: function(resp){
		}
	});
}

/**
 * On hover, show the polygon's info window with the area name
 * Also change the fillcolor
 *
 */
function addPolygonHover(poly, center_coords, name){
	poly.infoWindow = new google.maps.InfoWindow({ content: '<strong>Area: </strong>' + name });

	google.maps.event.addListener(poly, 'mouseover', function(e){
		var latLng = new google.maps.LatLng(center_coords[0], center_coords[1]);
		this.setOptions({ fillOpacity: 0.1 });
		poly.infoWindow.setPosition(latLng);
		poly.infoWindow.open(map);
	});

	google.maps.event.addListener(poly, 'mouseout', function(e){
		this.setOptions({ fillOpacity: 0.35 });
		poly.infoWindow.close();
	});
}

function drawViewMap(){
	draw_coordinates = document.getElementById('map_coordinates').value;

	//Split coordinates on "|"
	coordinates_array = draw_coordinates.split('|');

	var area;
	var area_coords = [];
	var x_coords = [];
	var y_coords = [];

	for(var i = 0; i < coordinates_array.length; i++){
		if (coordinates_array[i] == '') continue;
		coords = coordinates_array[i].split(',');
		c = new google.maps.LatLng(coords[0], coords[1] + '');

		//Push coordinates to temp arrays to calculate min and max - for polygon center
		x_coords.push(coords[0]);
		y_coords.push(coords[1]);

		area_coords.push(c);
	}

	area = new google.maps.Polygon({
		paths: area_coords, 
		strokeColor: "#FF000", 
		strokeOpacity: 0.8, 
		strokeWeight: 2, 
		fillColor: "#FF0000", 
		fillOpacity: 0.35
	});

	area.setMap(map);

	center_coords = findAreaCenter(x_coords, y_coords);
	center_x = center_coords[0];
	center_y = center_coords[1];
	
	map.setCenter(new google.maps.LatLng(center_x, center_y));
	map.setZoom(12);
}

function findAreaCenter(x, y){
	//Lowest X and Y
	x1 = Math.min.apply(null, x);
	y1 = Math.min.apply(null, y);

	//Highest X and Y
	x2 = Math.max.apply(null, x);
	y2 = Math.max.apply(null, y);

	center_x = x1 + ((x2 - x1) / 2);
	center_y = y1 + ((y2 - y1) / 2);

	return_array = Array(center_x, center_y);

	return return_array;
}

function addDrawPoint(c, coords){
	path.insertAt(path.length, c);

	var marker = new google.maps.Marker({
		position: c, 
		map: map, 
		draggable: true
	});
	markers.push(marker);
	marker.setTitle("#" + path.length);
	//coordinates.push(coords);

	//Get the point and then get the X (Lat) and Y (Lng)
	x = coords[0];
	y = coords[1];
	point_array = [x,y];

	//Push to coordinates array
	coordinates.push(point_array);

	google.maps.event.addListener(marker, 'click', function(){
		marker.setMap(null);
		for(var i = 0, a = markers.length; i < a && markers[i] != marker; ++i);
			markers.splice(i, 1);//Remove the point from the markers array. Return the point
			path.removeAt(i);//Remove the point from the map

			//Remove from coordinates
			coordinates.splice(i, 1);
	});

	google.maps.event.addListener(marker, 'dragend', function(){
		for(var i = 0, b = markers.length; i < b && markers[i] != marker; ++i);
			//Move the line to the new location of this point
			path.setAt(i, marker.getPosition());

			//Update the coordinates
			point = marker.getPosition();
			x = point.lat();
			y = point.lng();
			point_array = [x,y];
			coordinates[i] = point_array;
	});
}

function addPoint(event){
	if (disablePoints) return;	//If the user is not allowed to add points to this map, return

	path.insertAt(path.length, event.latLng);

	var marker = new google.maps.Marker({
		position: event.latLng, 
		map: map, 
		draggable: true
	});
	markers.push(marker);
	marker.setTitle("#" + path.length);

	//Get the point and then get the X (Lat) and Y (Lng)
	point = event.latLng;
	x = point.lat();
	y = point.lng();
	point_array = [x,y];

	//Push to coordinates array
	coordinates.push(point_array);

	google.maps.event.addListener(marker, 'click', function(){
		marker.setMap(null);
		for(var i = 0, a = markers.length; i < a && markers[i] != marker; ++i);
			markers.splice(i, 1);//Remove the point from the markers array. Return the point
			path.removeAt(i);//Remove the point from the map

			//Remove from coordinates
			coordinates.splice(i, 1);
	});

	google.maps.event.addListener(marker, 'dragend', function(){
		for(var i = 0, b = markers.length; i < b && markers[i] != marker; ++i);
			//Move the line to the new location of this point
			path.setAt(i, marker.getPosition());
	});
}

function showPoints(){
	for(var i = 0, a = coordinates.length; i < a; ++i){
		console.log(coordinates[i]['Pa']);
	}
}

function setMapAddress(address) {
	var geocoder = new google.maps.Geocoder();
	geocoder.geocode( { address : address }, function( results, status ) {
		 if( status == google.maps.GeocoderStatus.OK ) {
							var latlng = results[0].geometry.location;
								  var options = {
					zoom: 15,
					center: latlng,
					mapTypeId: google.maps.MapTypeId.ROADMAP, 
					streetViewControl: true
			  };
			  var mymap = new google.maps.Map( document.getElementById( 'map' ), options );
			  var marker = new google.maps.Marker({
			map: mymap,
									position: results[0].geometry.location
								});
		
		 }
	} );
//setMapAddress( "1325 Pacific Hwy, San Diego, CA, 92101" );
}
