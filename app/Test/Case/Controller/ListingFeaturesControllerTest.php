<?php
/* ListingFeatures Test cases generated on: 2011-12-13 20:30:58 : 1323826258*/
App::uses('ListingFeaturesController', 'Controller');

/**
 * TestListingFeaturesController *
 */
class TestListingFeaturesController extends ListingFeaturesController {
/**
 * Auto render
 *
 * @var boolean
 */
	public $autoRender = false;

/**
 * Redirect action
 *
 * @param mixed $url
 * @param mixed $status
 * @param boolean $exit
 * @return void
 */
	public function redirect($url, $status = null, $exit = true) {
		$this->redirectUrl = $url;
	}
}

/**
 * ListingFeaturesController Test Case
 *
 */
class ListingFeaturesControllerTestCase extends CakeTestCase {
/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array('app.listing_feature', 'app.listing', 'app.source', 'app.xml_upload', 'app.listing_status', 'app.user', 'app.group', 'app.listing_detail', 'app.property_type', 'app.property_style', 'app.listing_financial', 'app.listing_location', 'app.state', 'app.country', 'app.listing_media', 'app.media_type', 'app.agent_media', 'app.listing_agent', 'app.listing_meta_detail');

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();

		$this->ListingFeatures = new TestListingFeaturesController();
		$this->ListingFeatures->constructClasses();
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->ListingFeatures);

		parent::tearDown();
	}

/**
 * testIndex method
 *
 * @return void
 */
	public function testIndex() {

	}

/**
 * testView method
 *
 * @return void
 */
	public function testView() {

	}

/**
 * testAdd method
 *
 * @return void
 */
	public function testAdd() {

	}

/**
 * testEdit method
 *
 * @return void
 */
	public function testEdit() {

	}

/**
 * testDelete method
 *
 * @return void
 */
	public function testDelete() {

	}

}
