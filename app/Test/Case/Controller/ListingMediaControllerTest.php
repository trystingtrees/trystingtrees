<?php
/* ListingMedia Test cases generated on: 2011-12-12 21:56:42 : 1323745002*/
App::uses('ListingMediaController', 'Controller');

/**
 * TestListingMediaController *
 */
class TestListingMediaController extends ListingMediaController {
/**
 * Auto render
 *
 * @var boolean
 */
	public $autoRender = false;

/**
 * Redirect action
 *
 * @param mixed $url
 * @param mixed $status
 * @param boolean $exit
 * @return void
 */
	public function redirect($url, $status = null, $exit = true) {
		$this->redirectUrl = $url;
	}
}

/**
 * ListingMediaController Test Case
 *
 */
class ListingMediaControllerTestCase extends CakeTestCase {
/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array('app.listing_media', 'app.listing', 'app.source', 'app.xml', 'app.listing_status', 'app.user', 'app.group', 'app.media_type', 'app.agent_media');

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();

		$this->ListingMedia = new TestListingMediaController();
		$this->ListingMedia->constructClasses();
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->ListingMedia);

		parent::tearDown();
	}

/**
 * testIndex method
 *
 * @return void
 */
	public function testIndex() {

	}

/**
 * testView method
 *
 * @return void
 */
	public function testView() {

	}

/**
 * testAdd method
 *
 * @return void
 */
	public function testAdd() {

	}

/**
 * testEdit method
 *
 * @return void
 */
	public function testEdit() {

	}

/**
 * testDelete method
 *
 * @return void
 */
	public function testDelete() {

	}

}
