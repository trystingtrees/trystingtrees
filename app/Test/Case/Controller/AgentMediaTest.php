<?php
/* AgentMedia Test cases generated on: 2011-12-12 21:56:47 : 1323745007*/
App::uses('AgentMedia', 'Controller');

/**
 * TestAgentMedia *
 */
class TestAgentMedia extends AgentMedia {
/**
 * Auto render
 *
 * @var boolean
 */
	public $autoRender = false;

/**
 * Redirect action
 *
 * @param mixed $url
 * @param mixed $status
 * @param boolean $exit
 * @return void
 */
	public function redirect($url, $status = null, $exit = true) {
		$this->redirectUrl = $url;
	}
}

/**
 * AgentMedia Test Case
 *
 */
class AgentMediaTestCase extends CakeTestCase {
/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array('app.agent_media', 'app.listing_agent', 'app.user', 'app.group', 'app.media_type', 'app.listing_media', 'app.listing', 'app.source', 'app.xml', 'app.listing_status');

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();

		$this->AgentMedia = new TestAgentMedia();
		$this->->constructClasses();
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->AgentMedia);

		parent::tearDown();
	}

}
