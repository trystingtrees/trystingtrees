<?php
/* MediaTypes Test cases generated on: 2011-12-12 21:55:55 : 1323744955*/
App::uses('MediaTypes', 'Controller');

/**
 * TestMediaTypes *
 */
class TestMediaTypes extends MediaTypes {
/**
 * Auto render
 *
 * @var boolean
 */
	public $autoRender = false;

/**
 * Redirect action
 *
 * @param mixed $url
 * @param mixed $status
 * @param boolean $exit
 * @return void
 */
	public function redirect($url, $status = null, $exit = true) {
		$this->redirectUrl = $url;
	}
}

/**
 * MediaTypes Test Case
 *
 */
class MediaTypesTestCase extends CakeTestCase {
/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();

		$this->MediaTypes = new TestMediaTypes();
		$this->->constructClasses();
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->MediaTypes);

		parent::tearDown();
	}

}
