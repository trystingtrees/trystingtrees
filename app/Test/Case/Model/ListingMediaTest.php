<?php
/* ListingMedia Test cases generated on: 2011-12-12 21:56:42 : 1323745002*/
App::uses('ListingMedia', 'Model');

/**
 * ListingMedia Test Case
 *
 */
class ListingMediaTestCase extends CakeTestCase {
/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array('app.listing_media', 'app.listing', 'app.source', 'app.xml', 'app.listing_status', 'app.user', 'app.group', 'app.media_type', 'app.agent_media');

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();

		$this->ListingMedia = ClassRegistry::init('ListingMedia');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->ListingMedia);

		parent::tearDown();
	}

}
