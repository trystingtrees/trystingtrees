<?php
/* BlogSetting Test cases generated on: 2012-02-14 20:49:13 : 1329270553*/
App::uses('BlogSetting', 'Model');

/**
 * BlogSetting Test Case
 *
 */
class BlogSettingTestCase extends CakeTestCase {
/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array('app.blog_setting');

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();

		$this->BlogSetting = ClassRegistry::init('BlogSetting');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->BlogSetting);

		parent::tearDown();
	}

}
