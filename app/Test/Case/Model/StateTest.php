<?php
/* State Test cases generated on: 2011-12-12 21:54:05 : 1323744845*/
App::uses('State', 'Model');

/**
 * State Test Case
 *
 */
class StateTestCase extends CakeTestCase {
/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array('app.state', 'app.country', 'app.listing_location');

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();

		$this->State = ClassRegistry::init('State');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->State);

		parent::tearDown();
	}

}
