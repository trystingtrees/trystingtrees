<?php
/* ListingAgent Test cases generated on: 2011-12-12 19:59:24 : 1323737964*/
App::uses('ListingAgent', 'Model');

/**
 * ListingAgent Test Case
 *
 */
class ListingAgentTestCase extends CakeTestCase {
/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array('app.listing_agent', 'app.user', 'app.group');

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();

		$this->ListingAgent = ClassRegistry::init('ListingAgent');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->ListingAgent);

		parent::tearDown();
	}

}
