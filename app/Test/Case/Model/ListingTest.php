<?php
/* Listing Test cases generated on: 2011-12-12 20:18:58 : 1323739138*/
App::uses('Listing', 'Model');

/**
 * Listing Test Case
 *
 */
class ListingTestCase extends CakeTestCase {
/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array('app.listing', 'app.source', 'app.xml', 'app.listing_status', 'app.user', 'app.group');

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();

		$this->Listing = ClassRegistry::init('Listing');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Listing);

		parent::tearDown();
	}

}
