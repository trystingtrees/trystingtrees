<?php
/* PostComment Test cases generated on: 2012-02-13 21:23:08 : 1329186188*/
App::uses('PostComment', 'Model');

/**
 * PostComment Test Case
 *
 */
class PostCommentTestCase extends CakeTestCase {
/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array('app.post_comment', 'app.user', 'app.group', 'app.post');

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();

		$this->PostComment = ClassRegistry::init('PostComment');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->PostComment);

		parent::tearDown();
	}

}
