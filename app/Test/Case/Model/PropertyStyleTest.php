<?php
/* PropertyStyle Test cases generated on: 2011-12-12 21:58:03 : 1323745083*/
App::uses('PropertyStyle', 'Model');

/**
 * PropertyStyle Test Case
 *
 */
class PropertyStyleTestCase extends CakeTestCase {
/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array('app.property_style', 'app.listing_detail', 'app.listing', 'app.source', 'app.xml', 'app.listing_status', 'app.user', 'app.group', 'app.property_type');

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();

		$this->PropertyStyle = ClassRegistry::init('PropertyStyle');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->PropertyStyle);

		parent::tearDown();
	}

}
