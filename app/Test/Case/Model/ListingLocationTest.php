<?php
/* ListingLocation Test cases generated on: 2011-12-12 21:55:05 : 1323744905*/
App::uses('ListingLocation', 'Model');

/**
 * ListingLocation Test Case
 *
 */
class ListingLocationTestCase extends CakeTestCase {
/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array('app.listing_location', 'app.listing', 'app.source', 'app.xml', 'app.listing_status', 'app.user', 'app.group', 'app.state', 'app.country');

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();

		$this->ListingLocation = ClassRegistry::init('ListingLocation');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->ListingLocation);

		parent::tearDown();
	}

}
