<?php
/* PropertyType Test cases generated on: 2011-12-12 20:19:57 : 1323739197*/
App::uses('PropertyType', 'Model');

/**
 * PropertyType Test Case
 *
 */
class PropertyTypeTestCase extends CakeTestCase {
/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array('app.property_type');

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();

		$this->PropertyType = ClassRegistry::init('PropertyType');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->PropertyType);

		parent::tearDown();
	}

}
