<?php
/* UserProfile Test cases generated on: 2012-02-20 12:49:03 : 1329763743*/
App::uses('UserProfile', 'Model');

/**
 * UserProfile Test Case
 *
 */
class UserProfileTestCase extends CakeTestCase {
/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array('app.user_profile', 'app.user', 'app.group');

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();

		$this->UserProfile = ClassRegistry::init('UserProfile');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->UserProfile);

		parent::tearDown();
	}

}
