<?php
/* MediaType Test cases generated on: 2011-12-12 21:55:54 : 1323744954*/
App::uses('MediaType', 'Model');

/**
 * MediaType Test Case
 *
 */
class MediaTypeTestCase extends CakeTestCase {
/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array('app.media_type', 'app.agent_media', 'app.listing_media');

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();

		$this->MediaType = ClassRegistry::init('MediaType');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->MediaType);

		parent::tearDown();
	}

}
