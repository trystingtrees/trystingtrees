<?php
/* ListingDetail Test cases generated on: 2011-12-12 21:54:16 : 1323744856*/
App::uses('ListingDetail', 'Model');

/**
 * ListingDetail Test Case
 *
 */
class ListingDetailTestCase extends CakeTestCase {
/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array('app.listing_detail', 'app.listing', 'app.source', 'app.xml', 'app.listing_status', 'app.user', 'app.group', 'app.property_type', 'app.property_style');

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();

		$this->ListingDetail = ClassRegistry::init('ListingDetail');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->ListingDetail);

		parent::tearDown();
	}

}
