<?php
/* ListingFeature Test cases generated on: 2011-12-13 20:30:58 : 1323826258*/
App::uses('ListingFeature', 'Model');

/**
 * ListingFeature Test Case
 *
 */
class ListingFeatureTestCase extends CakeTestCase {
/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array('app.listing_feature', 'app.listing', 'app.source', 'app.xml_upload', 'app.listing_status', 'app.user', 'app.group', 'app.listing_detail', 'app.property_type', 'app.property_style', 'app.listing_financial', 'app.listing_location', 'app.state', 'app.country', 'app.listing_media', 'app.media_type', 'app.agent_media', 'app.listing_agent', 'app.listing_meta_detail');

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();

		$this->ListingFeature = ClassRegistry::init('ListingFeature');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->ListingFeature);

		parent::tearDown();
	}

}
