<?php
/* ListingFinancial Test cases generated on: 2011-12-12 21:54:21 : 1323744861*/
App::uses('ListingFinancial', 'Model');

/**
 * ListingFinancial Test Case
 *
 */
class ListingFinancialTestCase extends CakeTestCase {
/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array('app.listing_financial', 'app.listing', 'app.source', 'app.xml', 'app.listing_status', 'app.user', 'app.group');

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();

		$this->ListingFinancial = ClassRegistry::init('ListingFinancial');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->ListingFinancial);

		parent::tearDown();
	}

}
