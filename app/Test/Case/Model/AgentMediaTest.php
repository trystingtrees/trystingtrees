<?php
/* AgentMedia Test cases generated on: 2011-12-12 21:56:46 : 1323745006*/
App::uses('AgentMedia', 'Model');

/**
 * AgentMedia Test Case
 *
 */
class AgentMediaTestCase extends CakeTestCase {
/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array('app.agent_media', 'app.listing_agent', 'app.user', 'app.group', 'app.media_type', 'app.listing_media', 'app.listing', 'app.source', 'app.xml', 'app.listing_status');

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();

		$this->AgentMedia = ClassRegistry::init('AgentMedia');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->AgentMedia);

		parent::tearDown();
	}

}
