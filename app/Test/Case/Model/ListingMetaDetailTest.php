<?php
/* ListingMetaDetail Test cases generated on: 2011-12-12 21:55:00 : 1323744900*/
App::uses('ListingMetaDetail', 'Model');

/**
 * ListingMetaDetail Test Case
 *
 */
class ListingMetaDetailTestCase extends CakeTestCase {
/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array('app.listing_meta_detail', 'app.listing', 'app.source', 'app.xml', 'app.listing_status', 'app.user', 'app.group');

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();

		$this->ListingMetaDetail = ClassRegistry::init('ListingMetaDetail');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->ListingMetaDetail);

		parent::tearDown();
	}

}
