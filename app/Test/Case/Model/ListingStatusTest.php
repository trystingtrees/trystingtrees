<?php
/* ListingStatus Test cases generated on: 2011-12-12 20:18:25 : 1323739105*/
App::uses('ListingStatus', 'Model');

/**
 * ListingStatus Test Case
 *
 */
class ListingStatusTestCase extends CakeTestCase {
/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array('app.listing_status', 'app.listing');

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();

		$this->ListingStatus = ClassRegistry::init('ListingStatus');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->ListingStatus);

		parent::tearDown();
	}

}
