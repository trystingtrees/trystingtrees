<?php
/* Source Test cases generated on: 2011-12-07 15:24:51 : 1323289491*/
App::uses('Source', 'Model');

/**
 * Source Test Case
 *
 */
class SourceTestCase extends CakeTestCase {
/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array('app.source', 'app.listing', 'app.xml_upload');

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();

		$this->Source = ClassRegistry::init('Source');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Source);

		parent::tearDown();
	}

}
