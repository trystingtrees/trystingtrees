<?php
/* ListingMetaDetail Fixture generated on: 2011-12-12 21:55:00 : 1323744900 */

/**
 * ListingMetaDetailFixture
 *
 */
class ListingMetaDetailFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => NULL, 'length' => 10, 'key' => 'primary', 'collate' => NULL, 'comment' => ''),
		'listing_id' => array('type' => 'integer', 'null' => false, 'default' => NULL, 'length' => 10, 'key' => 'unique', 'collate' => NULL, 'comment' => ''),
		'src_listing_iud' => array('type' => 'integer', 'null' => true, 'default' => NULL, 'length' => 10, 'collate' => NULL, 'comment' => ''),
		'src_provider_listing_uid' => array('type' => 'integer', 'null' => true, 'default' => NULL, 'length' => 10, 'collate' => NULL, 'comment' => ''),
		'src_provider_name' => array('type' => 'string', 'null' => true, 'default' => NULL, 'collate' => 'utf8_general_ci', 'comment' => '', 'charset' => 'utf8'),
		'src_list_date' => array('type' => 'datetime', 'null' => true, 'default' => NULL, 'collate' => NULL, 'comment' => ''),
		'src_last_update_date' => array('type' => 'datetime', 'null' => true, 'default' => NULL, 'collate' => NULL, 'comment' => ''),
		'created' => array('type' => 'datetime', 'null' => false, 'default' => NULL, 'collate' => NULL, 'comment' => ''),
		'modified' => array('type' => 'datetime', 'null' => false, 'default' => NULL, 'collate' => NULL, 'comment' => ''),
		'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1), 'listing_id' => array('column' => 'listing_id', 'unique' => 1)),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'listing_id' => 1,
			'src_listing_iud' => 1,
			'src_provider_listing_uid' => 1,
			'src_provider_name' => 'Lorem ipsum dolor sit amet',
			'src_list_date' => '2011-12-12 21:55:00',
			'src_last_update_date' => '2011-12-12 21:55:00',
			'created' => '2011-12-12 21:55:00',
			'modified' => '2011-12-12 21:55:00'
		),
	);
}
