<?php
/* ListingFinancial Fixture generated on: 2011-12-12 21:54:20 : 1323744860 */

/**
 * ListingFinancialFixture
 *
 */
class ListingFinancialFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => NULL, 'length' => 10, 'key' => 'primary', 'collate' => NULL, 'comment' => ''),
		'listing_id' => array('type' => 'integer', 'null' => false, 'default' => NULL, 'length' => 10, 'key' => 'unique', 'collate' => NULL, 'comment' => ''),
		'price_amount' => array('type' => 'float', 'null' => false, 'default' => NULL, 'length' => '10,2', 'collate' => NULL, 'comment' => ''),
		'price_currency' => array('type' => 'string', 'null' => false, 'default' => NULL, 'length' => 15, 'collate' => 'latin1_swedish_ci', 'comment' => 'Must be same as tax currency if any', 'charset' => 'latin1'),
		'price_period' => array('type' => 'string', 'null' => true, 'default' => NULL, 'length' => 25, 'collate' => 'latin1_swedish_ci', 'comment' => '', 'charset' => 'latin1'),
		'tax_amount' => array('type' => 'float', 'null' => true, 'default' => NULL, 'length' => '10,2', 'collate' => NULL, 'comment' => 'Must be same as price currency'),
		'tax_currency' => array('type' => 'string', 'null' => true, 'default' => NULL, 'length' => 15, 'collate' => 'latin1_swedish_ci', 'comment' => '', 'charset' => 'latin1'),
		'tax_year' => array('type' => 'integer', 'null' => true, 'default' => NULL, 'length' => 10, 'collate' => NULL, 'comment' => ''),
		'created' => array('type' => 'datetime', 'null' => false, 'default' => NULL, 'collate' => NULL, 'comment' => ''),
		'modified' => array('type' => 'datetime', 'null' => false, 'default' => NULL, 'collate' => NULL, 'comment' => ''),
		'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1), 'listing_id' => array('column' => 'listing_id', 'unique' => 1)),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'listing_id' => 1,
			'price_amount' => 1,
			'price_currency' => 'Lorem ipsum d',
			'price_period' => 'Lorem ipsum dolor sit a',
			'tax_amount' => 1,
			'tax_currency' => 'Lorem ipsum d',
			'tax_year' => 1,
			'created' => '2011-12-12 21:54:20',
			'modified' => '2011-12-12 21:54:20'
		),
	);
}
