<?php
/* BlogSetting Fixture generated on: 2012-02-14 20:49:13 : 1329270553 */

/**
 * BlogSettingFixture
 *
 */
class BlogSettingFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'uuid' => array('type' => 'string', 'null' => false, 'default' => NULL, 'length' => 60, 'key' => 'index', 'collate' => 'utf8_general_ci', 'comment' => '', 'charset' => 'utf8'),
		'tagline' => array('type' => 'string', 'null' => false, 'default' => NULL, 'length' => 60, 'collate' => 'utf8_general_ci', 'comment' => '', 'charset' => 'utf8'),
		'email' => array('type' => 'string', 'null' => false, 'default' => NULL, 'length' => 60, 'collate' => 'utf8_general_ci', 'comment' => '', 'charset' => 'utf8'),
		'date_format' => array('type' => 'string', 'null' => false, 'default' => 'F j, Y', 'length' => 30, 'collate' => 'utf8_general_ci', 'comment' => '', 'charset' => 'utf8'),
		'time_format' => array('type' => 'string', 'null' => false, 'default' => 'g:i a', 'length' => 10, 'collate' => 'utf8_general_ci', 'comment' => '', 'charset' => 'utf8'),
		'allow_comments' => array('type' => 'boolean', 'null' => false, 'default' => '1', 'collate' => NULL, 'comment' => ''),
		'administer_comments' => array('type' => 'boolean', 'null' => false, 'default' => '1', 'collate' => NULL, 'comment' => 'Non-administered blogs will have comments auto-approved'),
		'allow_comment_replies' => array('type' => 'boolean', 'null' => false, 'default' => '1', 'collate' => NULL, 'comment' => ''),
		'indexes' => array('uuid' => array('column' => 'uuid', 'unique' => 0)),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'MyISAM')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'uuid' => 'Lorem ipsum dolor sit amet',
			'tagline' => 'Lorem ipsum dolor sit amet',
			'email' => 'Lorem ipsum dolor sit amet',
			'date_format' => 'Lorem ipsum dolor sit amet',
			'time_format' => 'Lorem ip',
			'allow_comments' => 1,
			'administer_comments' => 1,
			'allow_comment_replies' => 1
		),
	);
}
