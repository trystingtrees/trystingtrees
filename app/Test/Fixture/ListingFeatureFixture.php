<?php
/* ListingFeature Fixture generated on: 2011-12-13 20:30:58 : 1323826258 */

/**
 * ListingFeatureFixture
 *
 */
class ListingFeatureFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => NULL, 'length' => 10, 'key' => 'primary', 'collate' => NULL, 'comment' => ''),
		'listing_id' => array('type' => 'integer', 'null' => false, 'default' => NULL, 'length' => 10, 'collate' => NULL, 'comment' => ''),
		'year_built' => array('type' => 'integer', 'null' => true, 'default' => NULL, 'length' => 5, 'collate' => NULL, 'comment' => ''),
		'bathrooms' => array('type' => 'integer', 'null' => true, 'default' => NULL, 'length' => 3, 'collate' => NULL, 'comment' => ''),
		'bedrooms' => array('type' => 'integer', 'null' => true, 'default' => NULL, 'length' => 3, 'collate' => NULL, 'comment' => ''),
		'garage_stalls' => array('type' => 'integer', 'null' => true, 'default' => NULL, 'length' => 3, 'collate' => NULL, 'comment' => ''),
		'garage_style' => array('type' => 'string', 'null' => true, 'default' => NULL, 'collate' => 'utf8_general_ci', 'comment' => '', 'charset' => 'utf8'),
		'garage_comments' => array('type' => 'string', 'null' => true, 'default' => NULL, 'collate' => 'utf8_general_ci', 'comment' => '', 'charset' => 'utf8'),
		'living_area' => array('type' => 'float', 'null' => true, 'default' => NULL, 'length' => 10, 'collate' => NULL, 'comment' => ''),
		'living_area_units' => array('type' => 'string', 'null' => true, 'default' => NULL, 'length' => 15, 'collate' => 'utf8_general_ci', 'comment' => '', 'charset' => 'utf8'),
		'other_features' => array('type' => 'text', 'null' => true, 'default' => NULL, 'collate' => 'utf8_general_ci', 'comment' => '', 'charset' => 'utf8'),
		'created' => array('type' => 'datetime', 'null' => false, 'default' => NULL, 'collate' => NULL, 'comment' => ''),
		'modified' => array('type' => 'datetime', 'null' => false, 'default' => NULL, 'collate' => NULL, 'comment' => ''),
		'indexes' => array('PRIMARY' => array('column' => 'id', 'unique' => 1)),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'MyISAM')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'listing_id' => 1,
			'year_built' => 1,
			'bathrooms' => 1,
			'bedrooms' => 1,
			'garage_stalls' => 1,
			'garage_style' => 'Lorem ipsum dolor sit amet',
			'garage_comments' => 'Lorem ipsum dolor sit amet',
			'living_area' => 1,
			'living_area_units' => 'Lorem ipsum d',
			'other_features' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida, phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam, vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit, feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
			'created' => '2011-12-13 20:30:58',
			'modified' => '2011-12-13 20:30:58'
		),
	);
}
