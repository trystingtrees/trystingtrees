<?php
App::uses('AppController', 'Controller');
/**
 * AgentMedia Controller
 *
 * @property AgentMedia $AgentMedia
 */
class AgentMediaController extends AppController {


/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->AgentMedia->recursive = 0;
		$this->set('agentMedia', $this->paginate());
	}

/**
 * view method
 *
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->AgentMedia->id = $id;
		if (!$this->AgentMedia->exists()) {
			throw new NotFoundException(__('Invalid agent media'));
		}
		$this->set('agentMedia', $this->AgentMedia->read(null, $id));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->AgentMedia->create();
			if ($this->AgentMedia->save($this->request->data)) {
				$this->Session->setFlash(__('The agent media has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The agent media could not be saved. Please, try again.'));
			}
		}
		$listingAgents = $this->AgentMedia->ListingAgent->find('list');
		$mediaTypes = $this->AgentMedia->MediaType->find('list');
		$this->set(compact('listingAgents', 'mediaTypes'));
	}

/**
 * edit method
 *
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->AgentMedia->id = $id;
		if (!$this->AgentMedia->exists()) {
			throw new NotFoundException(__('Invalid agent media'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->AgentMedia->save($this->request->data)) {
				$this->Session->setFlash(__('The agent media has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The agent media could not be saved. Please, try again.'));
			}
		} else {
			$this->request->data = $this->AgentMedia->read(null, $id);
		}
		$listingAgents = $this->AgentMedia->ListingAgent->find('list');
		$mediaTypes = $this->AgentMedia->MediaType->find('list');
		$this->set(compact('listingAgents', 'mediaTypes'));
	}

/**
 * delete method
 *
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->AgentMedia->id = $id;
		if (!$this->AgentMedia->exists()) {
			throw new NotFoundException(__('Invalid agent media'));
		}
		if ($this->AgentMedia->delete()) {
			$this->Session->setFlash(__('Agent media deleted'));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Agent media was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
