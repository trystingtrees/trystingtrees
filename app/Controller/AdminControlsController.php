<?php
class AdminControlsController extends AppController {
	public $name = 'AdminControls';
	public $layout = 'admin';

	public function beforeFilter(){
		parent::beforeFilter();
		$this->Auth->allow(array('*'));
	}

	public function dashboard(){
	}
}
