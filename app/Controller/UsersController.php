<?php
class UsersController extends AppController {
	public $name = "Users";
	public $layout = 'inner_pages';

	function beforeFilter(){
		parent::beforeFilter();
		$this->Auth->allow(array('*'));
	}
	
	function index(){
		App::import('Vendor', 'facebook/facebook');

		$facebook=new Facebook(array(
		'appId'=>'319420798103380',
		'secret'=>'697edbb5badd169d77953cee326370af',
		'cookie'=>true
		));

		//FB Session
		$fb_session = $facebook->getSession();

		//Login URL
		$login_url = $facebook->getLoginUrl(array('next' => 'http://www.thewebsandbox.com/users/callback'));

		//Session available?
		if(!empty($fb_session)){
			$this->Session->write('FB.uid',$fb_session['uid']);
			$this->Session->write('FB.username',$fb_session['name']);
			$this->redirect('/',null,true);
		}else{
			$this->redirect($login_url);
		}

		exit();
	}

	function callback(){
		App::import('Vendor', 'facebook/facebook');

		$facebook=new Facebook(array(
		'appId'=>'319420798103380',
		'secret'=>'697edbb5badd169d77953cee326370af',
		'cookie'=>true
		));

		//FB Session
		$fb_session = $facebook->getSession();

		if (!empty($fb_session)){
			try{
				$user = json_decode(file_get_contents('https://graph.facebook.com/me?access_token='.$fb_session['access_token']));
			}catch(FacebookApiException $e){
				error_log($e);
			}
		}

		die();
	}

	function add(){
		if ($this->request->is('post')){
			$data = $this->data;

			//Increment group id
			$data['User']['group_id'] += 1;

			//Make sure the user does not exist
			$user_exists = $this->User->username_exists($data['User']['username']);
			if ($user_exists){
				$this->Session->setFlash('This username is already in use');
				$this->redirect('/signup');
			}

			//Make sure the email address is not already in use
			$email_exists = $this->User->email_exists($data['User']['email']);
			if ($email_exists){
				$this->Session->setFlash('This email is already in use');
				$this->redirect('/signup');
			}

			//Generate a UUID
			$uuid = $this->AppControls->generate_uuid();

			//Register the user
			$data['User']['uuid'] = $uuid;

			if ($this->User->save($data)){
				echo 'user saved';
			}else{
				echo 'user saved';
			}

			//If the user is an agent, create an entry in the ListingAgents table
			if ($data['User']['group_id'] == 2){
				$agent = array(
					'ListingAgent' => array(
						'user_id' => $this->User->id, 
						'uuid' => $uuid
					)
				);
				$this->loadModel('ListingAgent');
				$this->ListingAgent->save($agent);
			}
		}
	}

	function signup(){
	}

	function ajax_login(){
		$this->layout = false;

		$redir = $this->request->query['redir'];
		$this->set('redir', $redir);
	}

	function login(){
		/**
		@TODO : Redirect users to the page they were on before the sign in occured
		*/

		//If the user is already signed in, redirect to the home page
		if ($this->Session->check('Auth.User')) $this->redirect('/', true);
		
		if ($this->request->is('post')){
			$data = $this->data;

			//Check for @ sign in email_username
			$at_sign = substr_count($this->data['User']['email_username'], '@');

			//If there is an "@" sign, we need to sign in the user on the email address
			if ($at_sign == 0){
				//Username
				$data['User']['username'] = $data['User']['email_username'];
			}else{
				//Email
				$email = $data['User']['email_username'];
				$row = $this->User->findByEmail($email);
				$username = $row['User']['username'];
				$data['User']['username'] = $username;
			}
			unset($data['User']['email_username']);

			$this->data = $data;

			if ($this->Auth->login()){
				$this->_doPostLogin();
				$this->Session->setFlash('You are now logged in');
				$this->redirect('/');
			}else{
				$this->Session->setFlash('Login failed');
				$this->redirect('/login');
			}

			exit();
		}
	}

	function _doPostLogin(){
		$auth = $this->Session->read('Auth.User');
		$gid = $auth['group_id'];

		switch($gid){
			case Configure::read('USER_GROUP'):
				break;
			case Configure::read('AGENT_GROUP'):
				//Get the agent's agent Id and add it to the session
				$this->loadModel('ListingAgent');
				//Get logged in agent
				$uid = $this->Session->read('Auth.User.id');
				$agent = $this->ListingAgent->findByUserId($uid);
				$agent_id = $agent['ListingAgent']['id'];
				$this->Session->write('Auth.User.agent_id', $agent_id);
				break;
			case Configure::read('ADMIN_GROUP'):
				break;
			default:
		}
	}

	function logout(){
		$this->Auth->logout();
		$this->Session->setFlash('You have logged out');
		$this->redirect($this->Auth->logout());
	}

	function delete(){
	}

	function update_password(){
	}

	function reset_password(){
	}
}
