<?php
App::uses('AppController', 'Controller');
/**
 * PropertyStyles Controller
 *
 * @property PropertyStyle $PropertyStyle
 */
class PropertyStylesController extends AppController {


/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->PropertyStyle->recursive = 0;
		$this->set('propertyStyles', $this->paginate());
	}

/**
 * view method
 *
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->PropertyStyle->id = $id;
		if (!$this->PropertyStyle->exists()) {
			throw new NotFoundException(__('Invalid property style'));
		}
		$this->set('propertyStyle', $this->PropertyStyle->read(null, $id));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->PropertyStyle->create();
			if ($this->PropertyStyle->save($this->request->data)) {
				$this->Session->setFlash(__('The property style has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The property style could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->PropertyStyle->id = $id;
		if (!$this->PropertyStyle->exists()) {
			throw new NotFoundException(__('Invalid property style'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->PropertyStyle->save($this->request->data)) {
				$this->Session->setFlash(__('The property style has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The property style could not be saved. Please, try again.'));
			}
		} else {
			$this->request->data = $this->PropertyStyle->read(null, $id);
		}
	}

/**
 * delete method
 *
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->PropertyStyle->id = $id;
		if (!$this->PropertyStyle->exists()) {
			throw new NotFoundException(__('Invalid property style'));
		}
		if ($this->PropertyStyle->delete()) {
			$this->Session->setFlash(__('Property style deleted'));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Property style was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
