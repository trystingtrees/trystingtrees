<?php
App::uses('AppController', 'Controller');
/**
 * ListingStatuses Controller
 *
 * @property ListingStatus $ListingStatus
 */
class ListingStatusesController extends AppController {


/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->ListingStatus->recursive = 0;
		$this->set('listingStatuses', $this->paginate());
	}

/**
 * view method
 *
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->ListingStatus->id = $id;
		if (!$this->ListingStatus->exists()) {
			throw new NotFoundException(__('Invalid listing status'));
		}
		$this->set('listingStatus', $this->ListingStatus->read(null, $id));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->ListingStatus->create();
			if ($this->ListingStatus->save($this->request->data)) {
				$this->Session->setFlash(__('The listing status has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The listing status could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->ListingStatus->id = $id;
		if (!$this->ListingStatus->exists()) {
			throw new NotFoundException(__('Invalid listing status'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->ListingStatus->save($this->request->data)) {
				$this->Session->setFlash(__('The listing status has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The listing status could not be saved. Please, try again.'));
			}
		} else {
			$this->request->data = $this->ListingStatus->read(null, $id);
		}
	}

/**
 * delete method
 *
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->ListingStatus->id = $id;
		if (!$this->ListingStatus->exists()) {
			throw new NotFoundException(__('Invalid listing status'));
		}
		if ($this->ListingStatus->delete()) {
			$this->Session->setFlash(__('Listing status deleted'));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Listing status was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
