<?php
class ImportController extends AppController {
	public $name = 'Import';
	public $uses = array('PostalCode');

	public function beforeFilter(){
		parent::beforeFilter();
		$this->Auth->allow(array('*'));
	}

	function us_postal_codes(){
		ini_set('max_execution_time', 300); //300 seconds = 5 minutes
		//$file = APP . 'webroot' . DS . 'files' . DS . 'postal_codes' . DS . 'postal_codes.csv';
		$file = APP . 'webroot' . DS . 'files' . DS . 'postal_codes' . DS . 'CA' . DS . 'postal_codes.csv';

		setlocale(LC_ALL, 'fr_FR.UTF-8');

		$handle = fopen($file, 'r');
		$header = fgetcsv($handle);

		$return = array(
			'messages' => array(), 
			'errors' => array()
		);

		$i = 0;
		// read each data row in the file
 		while (($row = fgetcsv($handle)) !== FALSE) {
 			$i++;
 			$data = array();

 			// for each header field 
 			foreach ($header as $k=>$head) {
 				// get the data field from Model.field
 				if (strpos($head,'.')!==false) {
	 				$h = explode('.',$head);
	 				$data[$h[0]][$h[1]]=(isset($row[$k])) ? utf8_encode($row[$k]) : '';
				}
 				// get the data field from field
				else {
	 				$data['PostalCode'][$head]=(isset($row[$k])) ? utf8_encode($row[$k]) : '';
				}
 			}

/*
			// see if we have an id 			
 			$id = isset($data['PostalCode']['id']) ? $data['PostalCode']['id'] : 0;

			// we have an id, so we update
 			if ($id) {
 				// there is 2 options here, 
				 
				// option 1:
				// load the current row, and merge it with the new data
	 			//$this->recursive = -1;
	 			//$post = $this->read(null,$id);
	 			//$data['PostalCode'] = array_merge($post['PostalCode'],$data['PostalCode']);
	 			
				// option 2:
	 			// set the model id
	 			$this->id = $id;
			}
			
			// or create a new record
			else {
	 			$this->PostalCode->create();
			}
			*/
		
			$data['PostalCode']['area'] = @str_replacE('?', '', $data['PostalCode']['area']);
			pr($data);
			
			$this->PostalCode->create();
 			
			// see what we have
			// debug($data);
			
 			// validate the row
			$this->set($data);
			$this->PostalCode->save($data);
			echo $i . '<br />';
			/*
			if (!$this->validates()) {
				//$this->_flash('warning');
				$return['errors'][] = __(sprintf('PostalCode for Row %d failed to validate.',$i), true);
			}

 			// save the row
			if (!$error && !$this->PostalCode->save($data)) {
				$return['errors'][] = __(sprintf('PostalCode for Row %d failed to save.',$i), true);
			}

 			// success message!
			if (!$error) {
				$return['messages'][] = __(sprintf('PostalCode for Row %d was saved.',$i), true);
			}
			*/
 		}
 		
 		// close the file
 		fclose($handle);
 		
 		// return the messages
 		return $return;
	
	}
}
