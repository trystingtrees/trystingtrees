<?php
App::uses('AppController', 'Controller');
/**
 * Area Controller
 *
 * @property AgentArea $AgentArea
 * @property UserArea $UserArea
 */
class AreasController extends AppController {
	public $uses = array('AgentArea');

/**
 * agent index method
 *
 * @return void
 */
	public function agent_index() {
		//$this->loadModel('AgentArea');
		$agent_id = $this->Session->read('Auth.User.agent_id');

		//Find the map for each area selected
		$map_ids = array();
		$this->loadModel('Map');

		//Grab all the IDs
		$options = array(
			'conditions' => array(
				'AgentArea.listing_agent_id = ' . $agent_id
			)
		);
		$areas = $this->AgentArea->find('all', $options);

		//Add all the map IDs
		foreach($areas as $area){
			array_push($map_ids, $area['AgentArea']['map_id']);
		}

		//Fetch the maps from the Map model
		$maps = $this->Map->findMaps($map_ids);

		//Set the maps
		$this->set('maps', $maps);
	}

/**
 * user index method
 *
 * @return void
 */
	public function user_index() {
		$this->UserArea->recursive = 0;
		$this->set('userAreas', $this->paginate());
	}

	/*
	 * Remove a map from an agent's selected areas
	 *
	 * @param int map id
	 * @return bool
	 */
	function agent_unselect($id){
		//Get agent id
		$agent_id = $this->Session->read('Auth.User.agent_id');

		//Remove the map
		$area_id = $this->AgentArea->findAreaId($agent_id, $id);

		//Row found?
		if ($area_id === FALSE){
			parent::custom_error('The area could not be removed: Invalid agent ID or area ID.', '/agent/areas/index');
		}

		//Delete the row
		$delete = $this->AgentArea->delete($area_id);

		//Did th delete work?
		if ($delete){
			parent::custom_success('The area has been successfully removed', 'index');
		}else{
			parent::custom_error('The area could not be removed', 'index');
		}
	}

	public function agent_select(){
		$this->set('jsIncludes', array('http://maps.google.com/maps/api/js?sensor=false', 'maps/googletool.js?' . time()));
		$this->set('body_function', 'drawSelectionMaps()');

		//Get agent id
		$agent_id = $this->Session->read('Auth.User.agent_id');

		//Find the areas that the agent has already selected
		$agent_areas_raw = $this->AgentArea->findAgentSelectedAreas($agent_id);
		$agent_areas = array();
		foreach($agent_areas_raw as $area){
			$tmp = array(
				'id' => $area['AgentArea']['id'], 
				'map_id' => $area['AgentArea']['map_id'], 
				'name' => $area['Map']['nice_name'], 
			);
			array_push($agent_areas, $tmp);
		}

		$this->loadModel('Map');
		$this->set('maps', $this->Map->find('all'));
		$this->set('selected_areas', $agent_areas);
	}

	public function update(){
		$agent_id = $this->Session->read('Auth.User.agent_id');

		$areas = $this->request->data['areas'];
		$this->loadModel('AgentArea');
		$saves_made = 0;

		$maps = explode(',', $areas);
		$areas_to_save = array();

		//Get the number of areas currently selected
		$num_selected = $this->AgentArea->getNumberOfSelectedAreas($agent_id);

		//Get the max number of areas agents are allowed to select
		$this->loadModel('Setting');
		$max_areas = $this->Setting->findSetting('max_agent_areas');
		$max_areas = $max_areas['Setting']['value'];

		//Loop though the maps and check to see which ones have already been selected
		foreach($maps as $map_id){
			$selected = $this->AgentArea->areaSelected($map_id, $agent_id);
			//If the area is not selected yet by the agent in the DB, add it to the list of areas to add
			if ($selected === FALSE) $areas_to_save[] = $map_id;
		}

		//Calculate how many areas the agent would have should we save all the valid ones
		$new_total = sizeof($areas_to_save) + $num_selected;

		//Already at max?
		if ($max_areas == $num_selected){
			$msg = 'You have reached the maximum (' . $max_areas . ') of areas allowed'; 
			$returnArray = array(
				'message' => $msg, 
				'cls' => 'flash_failure', 
				'status' => 'fail', 
				'data' => ''
			);
			echo json_encode($returnArray);
			exit();
		}
		//Combining currently saved areas and new ones will go over the limit
		else if ($new_total > $max_areas){
			$num_left = $max_areas - $num_selected;
			$msg = 'You are allowed to select ' . $max_areas . ' area(s) and currently have ' . $num_selected . '. Please only select ' . $num_left . ' more area(s).';
			$returnArray = array(
				'message' => $msg, 
				'msg_class' => 'flash_failure', 
				'status' => 'fail', 
				'data' => ''
			);
			echo json_encode($returnArray);
			exit();
		}

		foreach($areas_to_save as $id){
			$saves_made++;
			$save_data = array(
				'AgentArea' => array(
					'listing_agent_id' => $agent_id, 
					'map_id' => $id
				)
			);
			$this->AgentArea->create();
			$this->AgentArea->save($save_data);
		}

		if ($saves_made == 0){
			$msg = 'There were no changes to save';
			$status = 'fail';
			$class = 'flash_failure';
		}else{
			$msg = 'Your selected areas have been updated';
			$status = 'success';
			$class = 'flash_success';
		}

		$returnArray = array(
			'message' => $msg, 
			'msg_class' => $class, 
			'status' => $status, 
			'data' => ''
		);
		echo json_encode($returnArray);
		exit();
	}














/**
 * view method
 *
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->AgentArea->id = $id;
		if (!$this->AgentArea->exists()) {
			throw new NotFoundException(__('Invalid agent media'));
		}
		$this->set('agentAreas', $this->AgentArea->read(null, $id));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->AgentArea->create();
			if ($this->AgentArea->save($this->request->data)) {
				$this->Session->setFlash(__('The agent media has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The agent media could not be saved. Please, try again.'));
			}
		}
		$listingAgents = $this->AgentArea->ListingAgent->find('list');
		$mediaTypes = $this->AgentArea->MediaType->find('list');
		$this->set(compact('listingAgents', 'mediaTypes'));
	}

/**
 * edit method
 *
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->AgentArea->id = $id;
		if (!$this->AgentArea->exists()) {
			throw new NotFoundException(__('Invalid agent media'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->AgentArea->save($this->request->data)) {
				$this->Session->setFlash(__('The agent media has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The agent media could not be saved. Please, try again.'));
			}
		} else {
			$this->request->data = $this->AgentArea->read(null, $id);
		}
		$listingAgents = $this->AgentArea->ListingAgent->find('list');
		$mediaTypes = $this->AgentArea->MediaType->find('list');
		$this->set(compact('listingAgents', 'mediaTypes'));
	}

/**
 * delete method
 *
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->AgentArea->id = $id;
		if (!$this->AgentArea->exists()) {
			throw new NotFoundException(__('Invalid agent media'));
		}
		if ($this->AgentArea->delete()) {
			$this->Session->setFlash(__('Agent media deleted'));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Agent media was not deleted'));
		$this->redirect(array('action' => 'index'));
	}

	/**
	 * Displays a view
	 *
	 * @param mixed What page to display
	 * @return void
	 */
	public function display() {
		echo 1; die();
		$path = func_get_args();

		$count = count($path);
		if (!$count) {
			$this->redirect('/');
		}
		$page = $subpage = $title_for_layout = null;

		if (!empty($path[0])) {
			$page = $path[0];
		}
		if (!empty($path[1])) {
			$subpage = $path[1];
		}
		if (!empty($path[$count - 1])) {
			$title_for_layout = Inflector::humanize($path[$count - 1]);
		}

		echo $path; die();
		$this->set(compact('page', 'subpage', 'title_for_layout'));
		$this->render(implode('/', $path));
	}
}
