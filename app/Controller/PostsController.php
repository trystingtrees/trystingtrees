<?php
	class PostsController extends AppController {
		public $name = 'Posts';
	
		public function beforeFilter(){
			parent::beforeFilter();
			$this->Auth->allow(array('*'));
			$this->set('body_id', 'home');
			$this->set('body_classes', 'home blog');
		}

		public function index(){
			$this->set('posts', $this->Post->find('all'));
		}

		public function add(){
			if ($this->request->is('post')){
				if ($this->Post->save($this->request->data)){
					$this->Session->setFlash(__('Your post has been saved'), 'flash_success');
					$this->redirect(array('controller' => 'posts', 'action' => 'manage'));
				}else{
					$this->Session->setFlash(__('Your post could not be saved'), 'flash_failure');
				}
			}
		}

		public function view($id = null) {
			$this->Post->id = $id;
			$this->set('post', $this->Post->read());
		}

		public function manage(){
			/*
			//Get the user id
			$session = $this->Session->read();
			$uid = $session['Auth']['User']['id'];
				$this->set('username', $session['Auth']['User']['username']);

			//Get the agent id
			$this->loadModel('ListingAgent');
			$agent = $this->ListingAgent->findByUserId($uid);
			$agent_id = $agent['ListingAgent']['id'];
			*/
			
			//Get the agent id
			$agent_id = $this->Session->read('Auth.User.agent_id');
			$username = $this->Session->read('Auth.User.username');
			
			//Find this agent's posts
			$this->set('posts', $this->Post->find('all', array('conditions' => array("Post.listing_agent_id = {$agent_id}"))));

			$this->set('uid', $agent_id);
			$this->set('username', $username);
		}

		public function create(){
			//Get the user id
			$session = $this->Session->read();
			$uid = $session['Auth']['User']['id'];

			//Get the agent id
			$this->loadModel('ListingAgent');
			$agent = $this->ListingAgent->findByUserId($uid);

			$this->set('uid', $agent['ListingAgent']['id']);
		}

		public function publish(){
			$id = $this->request->data['id'];
			$this->Post->read(null, $id);
			$post = $this->Post->saveField('is_published', 1);

			$message = ($post['Post']['is_published'] == 1) ? __('The post was published') : __('The post could not be published');
			
			echo json_encode(array('status' => 1, 'message' => $message));

			exit();
		}

		public function unpublish(){
			$id = $this->request->data['id'];
			$this->Post->read(null, $id);
			$post = $this->Post->saveField('is_published', 0);

			$message = ($post['Post']['is_published'] == 0) ? __('The post was unpublished', false) : __('The post could not be unpublished', false);

			echo json_encode(array('status' => 1, 'message' => $message));

			exit();
		}

		public function add_comment(){
			//Get params
			$data = $this->request->data;
			$name = $data['name'];
			$email = $data['email'];
			$message = $data['message'];
			$uuid = $data['id'];
			$post_id = $data['post_id'];

			$user = parent::get_current_user();
			$user_id = ($user === false) ? 0 : $user['id'];

			$save_data = array(
				'PostComment' => array(
					'user_id' => $user_id, 
					'blog_uuid' => $uuid, 
					'post_id' => $post_id, 
					'name' => $name,
					'email' => $email, 
					'message' => $message, 
					'ip' => $_SERVER['REMOTE_ADDR']
				)
			);

			//Is this comment a reply?
			if (isset($data['reply_to']) && !is_null($data['reply_to']) && $data['reply_to'] !== ''){
				$save_data['PostComment']['reply_to'] = (int)$data['reply_to'];
			}

			//Get the blog settings
			$this->loadModel('BlogSetting');
			$blog_settings = $this->BlogSetting->getAllSettings($uuid);

			//Allow comments?
			if (!$blog_settings['BlogSetting']['allow_comments']){
				echo json_encode(array('status' => 0, 'message' => __('Comments are not allowed on this blog. Your comment was not saved')));
				exit();
			}

			//Should we auto-approve this comment?
			$administer = $blog_settings['BlogSetting']['administer_comments'];
			if ($administer){
				//Do not auto-approve the comment
				$save_data['PostComment']['status'] = -1;	//-1 means the comment needs to be reviewed
			}else{
				//Auto-approve comment
				$save_data['PostComment']['status'] = 1;	//1 means the comment is approved
			}

			//Save
			$this->loadModel('PostComment');
			if ($this->PostComment->save($save_data)){
				echo json_encode(array('status' => 1, 'message' => __('Your comment was successfully posted')));
				exit();
			}else{
				echo json_encode(array('status' => 0, 'message' => __('Your comment could not be posted')));
				exit();
			}

			exit();
		}
	}
