<?php
	class BlogsController extends AppController {
		public $name = 'Blogs';
		public $uses = array('Post', 'ListingAgent');
	
		public function beforeFilter(){
			parent::beforeFilter();
			$this->Auth->allow(array('*'));
			$this->set('body_id', 'home');
			$this->set('body_classes', 'home blog');
		}

		function view(){
			$this->set('jsIncludes', array('modules/comments.js', 'blogs.js'));

			$username = $this->request->params['username'];
			$year = $this->request->params['year'];
			$month = $this->request->params['month'];
			$day = $this->request->params['day'];
			$title = $this->request->params['title'];

			$blog_base = '/blog/' . $username;

			//Make sure user exists, get their ID to make finding the post easier
			$agent = parent::_is_valid_agent($username);

			//If the username or the group id is not valid, render the view for blogs not found and show an error message
			if ($agent === false){
				$this->redirect(array('controller' => 'pages', 'action' => 'not-found'));
			}

			//Build the datetime for the post
			$date = "{$year}-{$month}-{$day}";

			//Look for the post
			$post = $this->Post->findPost($title, $date, $agent['ListingAgent']['id']);
			$this->set('post', $post);
			$this->set('agent_uuid', $agent['ListingAgent']['uuid']);
			$this->data = array('uuid' => $agent['ListingAgent']['uuid'], 'post_id' => $post['Post']['id']);

			//Get the comments
			$this->loadModel('PostComment');
			$comments = $this->PostComment->getPostComments($agent['ListingAgent']['uuid'], $post['Post']['id']);
			$this->set('comments', $comments);
			$this->set('blog_base', $blog_base);
		}
	}
