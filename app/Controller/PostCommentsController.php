<?php
App::uses('AppController', 'Controller');
/**
 * PostComments Controller
 *
 * @property PostComment $PostComment
 */
class PostCommentsController extends AppController {


/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->PostComment->recursive = 0;
		$this->set('postComments', $this->paginate());
	}

/**
 * view method
 *
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->PostComment->id = $id;
		if (!$this->PostComment->exists()) {
			throw new NotFoundException(__('Invalid post comment'));
		}
		$this->set('postComment', $this->PostComment->read(null, $id));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->PostComment->create();
			if ($this->PostComment->save($this->request->data)) {
				$this->Session->setFlash(__('The post comment has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The post comment could not be saved. Please, try again.'));
			}
		}
		$users = $this->PostComment->User->find('list');
		$posts = $this->PostComment->Post->find('list');
		$this->set(compact('users', 'posts'));
	}

/**
 * edit method
 *
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->PostComment->id = $id;
		if (!$this->PostComment->exists()) {
			throw new NotFoundException(__('Invalid post comment'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->PostComment->save($this->request->data)) {
				$this->Session->setFlash(__('The post comment has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The post comment could not be saved. Please, try again.'));
			}
		} else {
			$this->request->data = $this->PostComment->read(null, $id);
		}
		$users = $this->PostComment->User->find('list');
		$posts = $this->PostComment->Post->find('list');
		$this->set(compact('users', 'posts'));
	}

/**
 * delete method
 *
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->PostComment->id = $id;
		if (!$this->PostComment->exists()) {
			throw new NotFoundException(__('Invalid post comment'));
		}
		if ($this->PostComment->delete()) {
			$this->Session->setFlash(__('Post comment deleted'));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Post comment was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
