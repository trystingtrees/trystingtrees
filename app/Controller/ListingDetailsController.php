<?php
App::uses('AppController', 'Controller');
/**
 * ListingDetails Controller
 *
 * @property ListingDetail $ListingDetail
 */
class ListingDetailsController extends AppController {


/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->ListingDetail->recursive = 0;
		$this->set('listingDetails', $this->paginate());
	}

/**
 * view method
 *
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->ListingDetail->id = $id;
		if (!$this->ListingDetail->exists()) {
			throw new NotFoundException(__('Invalid listing detail'));
		}
		$this->set('listingDetail', $this->ListingDetail->read(null, $id));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->ListingDetail->create();
			if ($this->ListingDetail->save($this->request->data)) {
				$this->Session->setFlash(__('The listing detail has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The listing detail could not be saved. Please, try again.'));
			}
		}
		$listings = $this->ListingDetail->Listing->find('list');
		$propertyTypes = $this->ListingDetail->PropertyType->find('list');
		$propertyStyles = $this->ListingDetail->PropertyStyle->find('list');
		$this->set(compact('listings', 'propertyTypes', 'propertyStyles'));
	}

/**
 * edit method
 *
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->ListingDetail->id = $id;
		if (!$this->ListingDetail->exists()) {
			throw new NotFoundException(__('Invalid listing detail'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->ListingDetail->save($this->request->data)) {
				$this->Session->setFlash(__('The listing detail has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The listing detail could not be saved. Please, try again.'));
			}
		} else {
			$this->request->data = $this->ListingDetail->read(null, $id);
		}
		$listings = $this->ListingDetail->Listing->find('list');
		$propertyTypes = $this->ListingDetail->PropertyType->find('list');
		$propertyStyles = $this->ListingDetail->PropertyStyle->find('list');
		$this->set(compact('listings', 'propertyTypes', 'propertyStyles'));
	}

/**
 * delete method
 *
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->ListingDetail->id = $id;
		if (!$this->ListingDetail->exists()) {
			throw new NotFoundException(__('Invalid listing detail'));
		}
		if ($this->ListingDetail->delete()) {
			$this->Session->setFlash(__('Listing detail deleted'));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Listing detail was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
