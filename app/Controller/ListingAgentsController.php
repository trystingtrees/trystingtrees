<?php
App::uses('AppController', 'Controller');
/**
 * ListingAgents Controller
 *
 * @property ListingAgent $ListingAgent
 */
class ListingAgentsController extends AppController {

	public $name = 'ListingAgents';
	public $layout = 'inner_pages';

	public function beforeFilter(){
		parent::beforeFilter();

		$this->Auth->allow(array('*'));
		$this->set('body_id', 'archive');
		$this->set('body_classes', 'archive author atuhor-crob author-1');
	}

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->ListingAgent->recursive = 0;
		$this->set('listingAgents', $this->paginate());
	}

/**
 * view method
 *
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		/*
		$this->ListingAgent->id = $id;
		if (!$this->ListingAgent->exists()) {
			throw new NotFoundException(__('Invalid listing agent'));
		}
		*/
		$agent = $this->ListingAgent->findByUuid($id);

		$areas = $agent['AgentArea'];
		if (sizeof($areas) > 0){
			foreach($areas as $area){
				$map_ids[] = $area['map_id'];
			}
			$this->loadModel('Map');
			$maps = $this->Map->findMaps($map_ids);
			$agent['Maps'] = $maps;
		}

		$this->set('listingAgent', $agent);
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->ListingAgent->create();
			if ($this->ListingAgent->save($this->request->data)) {
				$this->Session->setFlash(__('The listing agent has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The listing agent could not be saved. Please, try again.'));
			}
		}
		$users = $this->ListingAgent->User->find('list');
		$this->set(compact('users'));
	}

/**
 * edit method
 *
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->ListingAgent->id = $id;
		if (!$this->ListingAgent->exists()) {
			throw new NotFoundException(__('Invalid listing agent'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			//Is there an image to save?
			$img_file_name = '';
			if ($this->request->data['ListingAgent']['picture_file_name']['error'] == 0){
				$img_saved = $this->_update_profile_picture($this->request->data['ListingAgent']['picture_file_name']);

				//File saved to server?
				if ($img_saved == FILE_MOVE_SUCCESS){
					$img_file_name = $this->request->data['ListingAgent']['picture_file_name']['name'];
				}
			}

			//Remove the file key
			$data = $this->request->data;
			unset($data['ListingAgent']['picture_file_name']);
			if ($img_file_name !== '') $data['ListingAgent']['picture_file_name'] = $img_file_name;

			if ($this->ListingAgent->save($data)) {
				//$this->Session->setFlash(__('The listing agent has been saved'), 'flash_success');
				$this->Session->setFlash(__('Your profile has been saved'), 'flash_success');
				$this->redirect('/profile');
			} else {
				$this->Session->setFlash(__('Your profile could not be saved. Please, try again.'), 'flash_failure');
			}
		} else {
			$this->request->data = $this->ListingAgent->read(null, $id);
		}
		$users = $this->ListingAgent->User->find('list');
		$this->set(compact('users'));
	}

	private function _update_profile_picture($data){
		$name = $data['name'];
		$type = $data['type'];
		$tmp_name = $data['tmp_name'];
		$size = $data['size'];

		//Get the agent's uuid
		$uid = $this->Session->read('Auth.User.id');
		$agent = $this->ListingAgent->findByUserId($uid);
		$uuid = $agent['ListingAgent']['uuid'];

		//Save the file to the server
		$pic_path = Configure::read('PROFILE_PICS_BASE_PATH');
		if(!is_dir($pic_path . DS . $uuid)){
			if (mkdir($pic_path . DS . $uuid) === FALSE){
				return MKDIR_FAIL;
			}
		}

		if(move_uploaded_file($tmp_name, $pic_path . DS . $uuid . DS . $name) === FALSE){
			return FILE_MOVE_FAIL;
		}else{
			return FILE_MOVE_SUCCESS;
		}
	}

/**
 * delete method
 *
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->ListingAgent->id = $id;
		if (!$this->ListingAgent->exists()) {
			throw new NotFoundException(__('Invalid listing agent'));
		}
		if ($this->ListingAgent->delete()) {
			$this->Session->setFlash(__('Listing agent deleted'));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Listing agent was not deleted'));
		$this->redirect(array('action' => 'index'));
	}

	public function profile(){
		$uid = $this->Session->read('Auth.User.id');

		//Get Listing Agent
		$agent = $this->ListingAgent->findByUserId($uid);

		$this->set(compact('uid'));
		$this->data = $agent;
	}

	public function update_profile(){
		pr($this->request->data);
		die();
	}

	public function areas(){
		//Get logged in agent
		$uid = $this->Session->read('Auth.User.id');
		$agent = $this->ListingAgent->findByUserId($uid);
		$agent_id = $agent['ListingAgent']['id'];

		//Find the map for each area selected
		$map_ids = array();
		$this->loadModel('Map');

		//Grab all the IDs
		foreach($agent['AgentArea'] as $area){
			array_push($map_ids, $area['map_id']);
		}

		//Fetch the maps from the Map model
		$maps = $this->Map->findMaps($map_ids);

		//Set the maps
		$this->set('maps', $maps);
	}

	public function update_areas(){
		//@TODO: Remove extra DB check and save the agent's LISTING AGENT ID in the Auth Session on login
		//Get Listing Agent
		$uid = $this->Session->read('Auth.User.id');
		$agent = $this->ListingAgent->findByUserId($uid);
		
		$areas = $this->request->data['areas'];
		$lid = $agent['ListingAgent']['id'];
		$this->loadModel('AgentArea');
		$saves_made = 0;

		$maps = explode(',', $areas);

		foreach($maps as $mid){
			//Check to see if the agent already has this map selected
			$options = array(
				'conditions' => array(
					"AgentArea.listing_agent_id = {$lid}", 
					"AgentArea.map_id = {$mid}"
				)
			);

			$map = $this->AgentArea->find('first', $options);

			if (empty($map)){
				$saves_made++;
				$save_data = array(
					'AgentArea' => array(
						'listing_agent_id' => $lid, 
						'map_id' => $mid
					)
				);
				$this->AgentArea->create();
				$this->AgentArea->save($save_data);
			}
		}

		echo ($saves_made == 0) ? 'There were no changes to save' : 'Your selected areas have been updated';

		die();
	}

	public function profile_view($username = ''){
		//Get ID of currently logged in user if there is one
		if ($this->Session->check('Auth.User')){
			$user_id = $this->Session->read('Auth.User.id');

			//Check to see if this user is an agent. If so, only show them their profile
			$agent = $this->ListingAgent->findByUserId($user_id);

			//Show the profile for the username specified
			if ($agent === FALSE){
				//@TODO: Keep user on same page and show error. Propose some actions to do
				if (empty($username) || $username == '') parent::custom_error('The agent specified could not be found', 'index');

				//Fetch profile
				$agent = $this->ListingAgent->findByUsername($username);

				if ($agent === FALSE){
					parent::custom_error('The agent specified could not be found', 'index');
				}

				//Show profile
				$this->set('edit', false);	//Do not show edit link
				$this->set('agent', $agent);
			}else{
				//Show the profile for the current agent
				$agent = $this->ListingAgent->findByUserId($user_id);

				//Get listings for this agent
				$this->loadModel('Listing');
				$listings = $this->Listing->findByUserId($user_id);
				$this->set('edit', true);	//Show edit link
				$this->set(compact('agent', 'listings'));
			}
		
			//Profile picture
			$uuid = $agent['ListingAgent']['uuid'];
			$picture = $agent['ListingAgent']['picture_file_name'];
			$profile_pic_base_path = Configure::read('PROFILE_PICS_BASE_URL');

			if (!is_null($picture)){
				$picture_url = $profile_pic_base_path . DS . $uuid . DS . $picture;
			}else{
				$picture_url = Configure::read('DEFAULT_PROFILE_PIC_URL');
			}
			$this->set('picture_url', $picture_url);
		}
	}

	/**
	 * Edit the current user's profile (agent)
	*/
	public function profile_edit(){
		$uid = $this->Session->read('Auth.User.id');
		$agent = $this->ListingAgent->findByUserId($uid);

		//@TODO: Validate if the user is an agent

		$uuid = $agent['ListingAgent']['uuid'];
		$picture = $agent['ListingAgent']['picture_file_name'];

		if (!is_null($picture)){
			$picture_url = $this->AppControls->build_profile_pic_path($uuid, $picture);
		}else{
			$picture_url = Configure::read('DEFAULT_PROFILE_PIC_URL');
		}

		//Get the state options
		$states = $this->ListingAgent->State->get_as_array();

		//Get the country options
		$countries = $this->ListingAgent->Country->get_as_array();

		$this->data = $agent;
		$this->set(compact('picture_url', 'states', 'countries'));
	}

	public function find(){
		if (isset($this->request->query['search'])){
			//Build the results link
			$results_params = $this->AppControls->build_results_params($this->request->query);
			$link = '/agent-locator/results/' . $results_params;
			$this->redirect($link, true);
		}
	}

	public function find_results(){
		if ($this->request->is('get') && $this->passedArgs['search'] == 'true'){
			$location = $this->passedArgs['location'];
			$keyword = $this->passedArgs['keyword'];
			$map_agents = array();

			//============= LOCATION ===============
			//Given a location, search for agents who have this location marked in their agent profile
			//Also look for agents who have selected an area within this location
			if (!empty($location) && strlen($location) >= 3){
				//Return agents that match this location in their profiles
				$search_type = 'location';
				$this->loadModel('AgentArea');
				$this->loadModel('Map');
				$this->loadModel('State');
				$this->loadModel('ListingAgent');
				
				//Search for states that match this search
				$states = $this->State->search($location);
				$location_states = null;
				if (is_array($states) && sizeof($states) > 0){
					foreach($states as $state){
						$location_states[] = $state['State']['id'];
					}
				}

				//Any agents with these areas selected?
				$agents = $this->ListingAgent->searchByLocation($location, $location_states);

				//Return agents that match this location in their map selections
				$maps = $this->Map->searchByLocation($location, $location_states);

				//Get all the agents that are related to these map areas
				foreach($maps as $map){
					$tmp = $this->AgentArea->getMapAgents($map['Map']['id']);
					if ($tmp === false) continue;
					$map_agents[] = $tmp;
				}

				//Clean up the map_agents and remove duplicates
				$map_agent_ids = array();
				foreach($map_agents as $v => $m){
					foreach($m as $k => $a){
						$id = $a['AgentArea']['listing_agent_id'];
						if (in_array($id, $map_agent_ids)){
							unset($map_agents[$v][$k]);	//Remove duplicate agents as we only need one reference to them
							if (empty($map_agents[$v])) unset($map_agents[$v]);	//If there are no more agents in this map ID key, remove the key as it is not needed
							continue;
						}
						$map_agent_ids[] = $id;
					}
				}

				//Loop over the agents and remove any that already appear in the map agents. The map agents get priority since they are experts in these areas
				foreach($agents as $k => $agent){
					if (in_array($agent['ListingAgent']['id'], $map_agent_ids)){
						unset($agents[$k]);
					}
				}

				//For all the map_agents, get all the areas they have
				foreach($map_agents as $k => &$agent){
					foreach($agent as $k2 => &$a){
						$id = $a['AgentArea']['listing_agent_id'];
						$a['Areas'] = $this->AgentArea->findAgentSelectedAreasOverview($id);
					}
				}
				//$map_agents = $map_agents[0];

				//Set the view vars
				$this->set(compact('agents', 'map_agents', 'location', 'keyword', 'search_type'));
			}
		}
	}
}
