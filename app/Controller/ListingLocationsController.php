<?php
App::uses('AppController', 'Controller');
/**
 * ListingLocations Controller
 *
 * @property ListingLocation $ListingLocation
 */
class ListingLocationsController extends AppController {


/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->ListingLocation->recursive = 0;
		$this->set('listingLocations', $this->paginate());
	}

/**
 * view method
 *
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->ListingLocation->id = $id;
		if (!$this->ListingLocation->exists()) {
			throw new NotFoundException(__('Invalid listing location'));
		}
		$this->set('listingLocation', $this->ListingLocation->read(null, $id));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->ListingLocation->create();
			if ($this->ListingLocation->save($this->request->data)) {
				$this->Session->setFlash(__('The listing location has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The listing location could not be saved. Please, try again.'));
			}
		}
		$listings = $this->ListingLocation->Listing->find('list');
		$states = $this->ListingLocation->State->find('list');
		$countries = $this->ListingLocation->Country->find('list');
		$this->set(compact('listings', 'states', 'countries'));
	}

/**
 * edit method
 *
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->ListingLocation->id = $id;
		if (!$this->ListingLocation->exists()) {
			throw new NotFoundException(__('Invalid listing location'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->ListingLocation->save($this->request->data)) {
				$this->Session->setFlash(__('The listing location has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The listing location could not be saved. Please, try again.'));
			}
		} else {
			$this->request->data = $this->ListingLocation->read(null, $id);
		}
		$listings = $this->ListingLocation->Listing->find('list');
		$states = $this->ListingLocation->State->find('list');
		$countries = $this->ListingLocation->Country->find('list');
		$this->set(compact('listings', 'states', 'countries'));
	}

/**
 * delete method
 *
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->ListingLocation->id = $id;
		if (!$this->ListingLocation->exists()) {
			throw new NotFoundException(__('Invalid listing location'));
		}
		if ($this->ListingLocation->delete()) {
			$this->Session->setFlash(__('Listing location deleted'));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Listing location was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
