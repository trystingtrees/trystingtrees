<?php
App::uses('AppController', 'Controller');
/**
 * AgentArea Controller
 *
 * @property AgentArea $AgentArea
 */
class AgentAreasController extends AppController {


/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->AgentArea->recursive = 0;
		$this->set('agentAreas', $this->paginate());
	}

/**
 * view method
 *
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->AgentArea->id = $id;
		if (!$this->AgentArea->exists()) {
			throw new NotFoundException(__('Invalid agent media'));
		}
		$this->set('agentAreas', $this->AgentArea->read(null, $id));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->AgentArea->create();
			if ($this->AgentArea->save($this->request->data)) {
				$this->Session->setFlash(__('The agent media has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The agent media could not be saved. Please, try again.'));
			}
		}
		$listingAgents = $this->AgentArea->ListingAgent->find('list');
		$mediaTypes = $this->AgentArea->MediaType->find('list');
		$this->set(compact('listingAgents', 'mediaTypes'));
	}

/**
 * edit method
 *
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->AgentArea->id = $id;
		if (!$this->AgentArea->exists()) {
			throw new NotFoundException(__('Invalid agent media'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->AgentArea->save($this->request->data)) {
				$this->Session->setFlash(__('The agent media has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The agent media could not be saved. Please, try again.'));
			}
		} else {
			$this->request->data = $this->AgentArea->read(null, $id);
		}
		$listingAgents = $this->AgentArea->ListingAgent->find('list');
		$mediaTypes = $this->AgentArea->MediaType->find('list');
		$this->set(compact('listingAgents', 'mediaTypes'));
	}

/**
 * delete method
 *
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->AgentArea->id = $id;
		if (!$this->AgentArea->exists()) {
			throw new NotFoundException(__('Invalid agent media'));
		}
		if ($this->AgentArea->delete()) {
			$this->Session->setFlash(__('Agent media deleted'));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Agent media was not deleted'));
		$this->redirect(array('action' => 'index'));
	}

	/**
	 * Displays a view
	 *
	 * @param mixed What page to display
	 * @return void
	 */
	public function display() {
		echo 1; die();
		$path = func_get_args();

		$count = count($path);
		if (!$count) {
			$this->redirect('/');
		}
		$page = $subpage = $title_for_layout = null;

		if (!empty($path[0])) {
			$page = $path[0];
		}
		if (!empty($path[1])) {
			$subpage = $path[1];
		}
		if (!empty($path[$count - 1])) {
			$title_for_layout = Inflector::humanize($path[$count - 1]);
		}

		echo $path; die();
		$this->set(compact('page', 'subpage', 'title_for_layout'));
		$this->render(implode('/', $path));
	}
}
