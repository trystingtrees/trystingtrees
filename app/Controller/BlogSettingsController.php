<?php
App::uses('AppController', 'Controller');
/**
 * BlogSettings Controller
 *
 * @property BlogSetting $BlogSetting
 */
class BlogSettingsController extends AppController {


/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->BlogSetting->recursive = 0;
		$this->set('blogSettings', $this->paginate());
	}

/**
 * view method
 *
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->BlogSetting->id = $id;
		if (!$this->BlogSetting->exists()) {
			throw new NotFoundException(__('Invalid blog setting'));
		}
		$this->set('blogSetting', $this->BlogSetting->read(null, $id));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->BlogSetting->create();
			if ($this->BlogSetting->save($this->request->data)) {
				$this->Session->setFlash(__('The blog setting has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The blog setting could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->BlogSetting->id = $id;
		if (!$this->BlogSetting->exists()) {
			throw new NotFoundException(__('Invalid blog setting'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->BlogSetting->save($this->request->data)) {
				$this->Session->setFlash(__('The blog setting has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The blog setting could not be saved. Please, try again.'));
			}
		} else {
			$this->request->data = $this->BlogSetting->read(null, $id);
		}
	}

/**
 * delete method
 *
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->BlogSetting->id = $id;
		if (!$this->BlogSetting->exists()) {
			throw new NotFoundException(__('Invalid blog setting'));
		}
		if ($this->BlogSetting->delete()) {
			$this->Session->setFlash(__('Blog setting deleted'));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Blog setting was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
