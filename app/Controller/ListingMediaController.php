<?php
App::uses('AppController', 'Controller');
/**
 * ListingMedia Controller
 *
 * @property ListingMedia $ListingMedia
 */
class ListingMediaController extends AppController {


/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->ListingMedia->recursive = 0;
		$this->set('listingMedia', $this->paginate());
	}

/**
 * view method
 *
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->ListingMedia->id = $id;
		if (!$this->ListingMedia->exists()) {
			throw new NotFoundException(__('Invalid listing media'));
		}
		$this->set('listingMedia', $this->ListingMedia->read(null, $id));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->ListingMedia->create();
			if ($this->ListingMedia->save($this->request->data)) {
				$this->Session->setFlash(__('The listing media has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The listing media could not be saved. Please, try again.'));
			}
		}
		$listings = $this->ListingMedia->Listing->find('list');
		$mediaTypes = $this->ListingMedia->MediaType->find('list');
		$this->set(compact('listings', 'mediaTypes'));
	}

/**
 * edit method
 *
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->ListingMedia->id = $id;
		if (!$this->ListingMedia->exists()) {
			throw new NotFoundException(__('Invalid listing media'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->ListingMedia->save($this->request->data)) {
				$this->Session->setFlash(__('The listing media has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The listing media could not be saved. Please, try again.'));
			}
		} else {
			$this->request->data = $this->ListingMedia->read(null, $id);
		}
		$listings = $this->ListingMedia->Listing->find('list');
		$mediaTypes = $this->ListingMedia->MediaType->find('list');
		$this->set(compact('listings', 'mediaTypes'));
	}

/**
 * delete method
 *
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->ListingMedia->id = $id;
		if (!$this->ListingMedia->exists()) {
			throw new NotFoundException(__('Invalid listing media'));
		}
		if ($this->ListingMedia->delete()) {
			$this->Session->setFlash(__('Listing media deleted'));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Listing media was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
