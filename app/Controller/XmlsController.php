<?php
class XmlsController extends AppController {
	public $name = "Xmls";

	function beforeFilter(){
		parent::beforeFilter();
		$this->Auth->allow(array('*'));
	}

	function upload(){
	}

	function upload_xml(){
		pr($this->data);

		//Grab the information
		$data = array(
			'Xml' => array(
				'source_id' => $this->data['Xml']['source_id'], 
				'xml_file' => $this->data['Xml']['xml_file']['name']
			)
		);

		if ($this->Xml->save($data)){
			$id = $this->Xml->id;
			//Create dir
			$XML_UPLOADS_HOME = parent::$config['XML_UPLOADS_HOME'];
			mkdir($XML_UPLOADS_HOME . "/{$id}");

			//Move file
			move_uploaded_file($this->data['Xml']['xml_file']['tmp_name'], "{$XML_UPLOADS_HOME}/{$id}/{$this->data['Xml']['xml_file']['name']}");
		}else{
			echo 'wrong';
		}

		exit();
	}

	function parse_xml(){
	}
}
