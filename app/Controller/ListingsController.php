<?php
App::uses('AppController', 'Controller');
/**
 * Listings Controller
 *
 * @property Listing $Listing
 */
class ListingsController extends AppController {

	public $name = 'Listings';
	public $layout = 'inner_pages';

	function beforeFilter(){
		parent::beforeFilter();
		$this->Auth->allow('search', 'results', 'view');
		$this->set('body_id', 'home');
		$this->set('body_classes', 'home blog');
	}

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Listing->recursive = 0;
		$this->set('listings', $this->paginate());
	}

/**
 * view method
 *
 * @param string $id
 * @return void
 */
	public function view($uuid = null) {
		//Get the listing via the uuid

		$listing = $this->Listing->findByUuid($uuid);
		$agent_media = $this->Listing->ListingAgent->AgentMedia->findByListingAgentId($listing['ListingAgent']['id']);
		$state_id = $listing['ListingLocation'][0]['state_id'];
		$state = $this->Listing->ListingLocation->State->read(null, $state_id);

		$this->set(compact('listing', 'agent_media', 'state'));
		$this->set('body_id', 'listing');
		$this->set('body_classes', 'single single-listings');
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Listing->create();
			if ($this->Listing->save($this->request->data)) {
				$this->Session->setFlash(__('The listing has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The listing could not be saved. Please, try again.'));
			}
		}
		$sources = $this->Listing->Source->find('list');
		$listingStatuses = $this->Listing->ListingStatus->find('list');
		$users = $this->Listing->User->find('list');
		$this->set(compact('sources', 'listingStatuses', 'users'));
	}

/**
 * edit method
 *
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->Listing->id = $id;
		if (!$this->Listing->exists()) {
			throw new NotFoundException(__('Invalid listing'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Listing->save($this->request->data)) {
				$this->Session->setFlash(__('The listing has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The listing could not be saved. Please, try again.'));
			}
		} else {
			$this->request->data = $this->Listing->read(null, $id);
		}
		$sources = $this->Listing->Source->find('list');
		$listingStatuses = $this->Listing->ListingStatus->find('list');
		$users = $this->Listing->User->find('list');
		$this->set(compact('sources', 'listingStatuses', 'users'));
	}

/**
 * delete method
 *
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Listing->id = $id;
		if (!$this->Listing->exists()) {
			throw new NotFoundException(__('Invalid listing'));
		}
		if ($this->Listing->delete()) {
			$this->Session->setFlash(__('Listing deleted'));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Listing was not deleted'));
		$this->redirect(array('action' => 'index'));
	}

/**
 * search method
 * 
 * Search for listings based on POSTed parameters from the search form
*/
	public function search(){
		if ($this->request->is('post')) {
			$data = $this->data;
			$params = array();

			//Loop through the data and drop the keys with no entry
			foreach($data as $k => $v){
				//Add the key to the params if there is data
				if (!empty($v)){
					$params[$k] = $v;
				}
			}

			//Build the results link
			$results_params = $this->AppControls->build_results_params($params);

			//Redirect to results page
			$link = '/listings/results/' . $results_params;
			$this->redirect($link, true);
		}else{
			$this->redirect('/');
		}
	}

/**
 * results method
 * 
 * Show search results for listings based on the Get parameters from the search method
*/
	public function results(){
		$tmp_data = $this->passedArgs;
		$data = array();

		if (sizeof($tmp_data) > 0){
			foreach($tmp_data as $k => $v){
				$data[$k] = $v;
			}
		}else{
			$data = $tmp_data;
		}

		//@TODO: TRY USING SUB QUERIES
		//http://book.cakephp.org/2.0/en/models/retrieving-your-data.html?highlight=find#sub-queries

		//@TODO: Move logic to model

		$this->Listing->contain(
			'ListingLocation', 
			//'ListingAgent', 
			'ListingFinancial', 
			'ListingMedia', 
			'ListingFeature', 
			'ListingDetail', 
			'ListingStatus'
		);

		$conditions = array();

		$options['fields'] = array(
			'fields' => 'DISTINCT (Listing.id), Listing.uuid, Listing.user_id, ListingAgent.id, ListingAgent.user_id, ListingAgent.uuid, ListingAgent.name, PropertyStyle.name, ListingFeature.*, State.*'
		);

		//=========================================================== JOIN ARRAYS ====================================================
		$listing_locations_array = array(
			'table' => 'listing_locations', 
			'alias' => 'ListingLocation', 
			'type' => 'INNER', 
			'conditions' => 'ListingLocation.listing_id = Listing.id'
		);

		$listing_states_array = array(
			'table' => 'states', 
			'alias' => 'State', 
			'type' => 'INNER', 
			'conditions' => 'ListingLocation.state_id = State.id'
		);

		$listing_countries_array = array(
			'table' => 'countries', 
			'alias' => 'Country', 
			'type' => 'INNER', 
			'conditions' => 'ListingLocation.country_id = Country.id'
		);

		$listing_agents_array = array(
			'table' => 'listing_agents', 
			'alias' => 'ListingAgent', 
			'type' => 'INNER', 
			'conditions' => 'ListingAgent.user_id = Listing.user_id'
		);

		$listing_details_array = array(
			'table' => 'listing_details', 
			'alias' => 'ListingDetail', 
			'type' => 'INNER', 
			'conditions' => 'ListingDetail.listing_id = Listing.id'
		);

		$listing_medias_array = array(
			'table' => 'listing_media', 
			'alias' => 'ListingMedia', 
			'type' => 'INNER', 
			'conditions' => 'ListingMedia.listing_id = Listing.id'
		);

		$listing_features_array = array(
			'table' => 'listing_features', 
			'alias' => 'ListingFeature', 
			'type' => 'INNER', 
			'conditions' => 'ListingFeature.listing_id = Listing.id'
		);
		
		$listing_financials_array = array(
			'table' => 'listing_financials', 
			'alias' => 'ListingFinancial', 
			'type' => 'INNER', 
			'conditions' => 'ListingFinancial.listing_id = Listing.id'
		);
		
		$listing_meta_details_array = array(
			'table' => 'listing_meta_details', 
			'alias' => 'ListingMetaDetail', 
			'type' => 'INNER', 
			'conditions' => 'ListingMetaDetail.listing_id = Listing.id'
		);
		
		$property_styles_array = array(
			'table' => 'property_styles', 
			'alias' => 'PropertyStyle', 
			'type' => 'INNER', 
			'conditions' => 'PropertyStyle.id = ListingDetail.property_style_id'
		);


		//=========================================================== SEARCH PARAMS ====================================================
		//Define an array of the tables to include in the JOINS
		$options['joins'] = array();
		$joins_arrays = array();

		//Bathrooms
		$ct_baths = array_key_exists('ct_baths', $data);
		if (!empty($ct_baths)){
			$baths = $data['ct_baths'];
			$conditions['AND'][] = "ListingFeature.bathrooms = {$baths}";

			//Add the table to the joins_arrays
			/*
			if (!array_key_exists('listing_features', $joins_arrays)) {
				$joins_arrays['listing_features'] = 1;
				$options['joins'][] = $listing_features_array;
				$options['fields']['fields'] = $options['fields']['fields'] . ",ListingFeature.*";
			}
			*/
		}
		
		//Bedrooms
		$ct_beds = array_key_exists('ct_beds', $data);
		if (!empty($ct_beds)){
			$beds = $data['ct_beds'];
			$conditions['AND'][] = "ListingFeature.bedrooms = {$beds}";
		}
		
		//Status
		$ct_status = array_key_exists('ct_status', $data);
		if (!empty($ct_status)){
			$status = $data['ct_status'];
			if ($status == 'for-sale'){
				$conditions['OR'] = array(
					"ListingStatus.name = 'for_sale'", 
					"ListingStatus.name = 'sale_pending'"
				);
			}else{
				$status = strtolower($status);
				$status = str_replace('-', '_', $status);
				$conditions['AND'][] = "ListingStatus.name = '{$status}'";
			}
		}
		
		//Price From
		$ct_price_from = array_key_exists('ct_price_from', $data);
		if (!empty($ct_price_from)){
			$price_from = $data['ct_price_from'];
			$price_from = str_replace(',', '', $price_from);
			$conditions['AND'][] = "ListingFinancial.price_amount > {$price_from}";

			//Add the table to the joins_arrays
			if (!array_key_exists('listing_financials', $joins_arrays)) {
				$joins_arrays['listing_financials'] = 1;
				$options['joins'][] = $listing_financials_array;
			}
		}
		
		//Price To
		$ct_price_to = array_key_exists('ct_price_to', $data);
		if (!empty($ct_price_to)){
			$price_to = $data['ct_price_to'];
			$price_to = str_replace(',', '', $price_to);
			$conditions['AND'][] = "ListingFinancial.price_amount < {$price_to}";

			//Add the table to the joins_arrays
			if (!array_key_exists('listing_financials', $joins_arrays)) {
				$joins_arrays['listing_financials'] = 1;
				$options['joins'][] = $listing_financials_array;
			}
		}
		
		//MLS
		$ct_mls = array_key_exists('ct_mls', $data);
		if (!empty($ct_mls)){
			$mls = $data['ct_mls'];
			$conditions['AND'][] = "ListingMetaDetail.regional_mls_number = '{$mls}'";

			//Add the table to the joins_arrays
			/*
			if (!array_key_exists('listing_meta_details', $joins_arrays)) {
				$joins_arrays[] = 'listing_meta_details';
				$options['joins'][] = $listing_meta_details_array;
			}
			*/
		}

		//@TODO: Need to remove countries and states relationships to allow for better fulltext search on locations
		//City
		$ct_city = array_key_exists('ct_location', $data);
		if (!empty($ct_city)){
			$city = $data['ct_location'];
			$conditions['OR'] = array(
				"ListingLocation.city LIKE '{$city}'", 
				"ListingLocation.county LIKE '%{$city}%'", 
				"ListingLocation.postal_code LIKE '{$city}'",
				"State.short_name LIKE '{$city}'", 
				"State.long_name LIKE '{$city}'"
			);
		}

		//Add the default joins
		//$options['joins'][] = $listing_agents_array;
		$options['joins'][] = $listing_financials_array;
		$options['joins'][] = $listing_locations_array;
		$options['joins'][] = $listing_meta_details_array;
		$options['joins'][] = $listing_details_array;
		$options['joins'][] = $listing_features_array;
		$options['joins'][] = $property_styles_array;
		$options['joins'][] = $listing_states_array;
		$options['joins'][] = $listing_agents_array;

		$options['conditions'] = $conditions;

		$rows = $this->Listing->find('all', $options);

		$log = $this->Listing->getDataSource()->getLog(false, false);
		/*
		debug($log);
		die();
		*/

		$listings = array();
		//Get data in an array
		foreach($rows as $row){
			/*
			pr($row);
			continue;
			*/
			$listings[] = array(
				'id' => $row['Listing']['id'], 
				'price' => $row['ListingFinancial'][0]['price_amount'], 
				'city' => $row['ListingLocation'][0]['city'], 
				//'state' => $row['State'][0]['short_name'], 
				'postal_code' => $row['ListingLocation'][0]['postal_code'], 
				'beds' => $row['ListingFeature'][0]['bedrooms'],
				'baths' => $row['ListingFeature'][0]['bathrooms'],
			);
		}
		
		$this->set('listings', $rows);
	}
}
