<?php
	class MapsController extends AppController {
		public $name = 'Maps';
	
		public function beforeFilter(){
			parent::beforeFilter();

			$this->Auth->allow(array('*'));
			$this->set('body_id', 'home');
			$this->set('body_classes', 'home blog');
			$this->set('cssIncludes', array());
			$this->set('jsIncludes', array('http://maps.google.com/maps/api/js?sensor=false', 'modules/google_tools'));
			//$this->set('body_function', 'initialize()');
		}

		function admin_index(){
			$this->set('maps', $this->Map->find('all'));
		}

		function test(){
		}

		function admin_create(){
			$this->loadModel('State');
			$this->loadModel('Country');

			$this->set('states', $this->State->find('list'));
			$this->set('countries', array('38' => 'Canada', '225' => 'United States'));
			//$this->set('countries', $this->Country->find('list'));
		}

		function admin_add(){
			$data = $this->request->data;

			$data['Map']['status'] = 1;	//Active
			$data['Map']['type'] = 0;	//Regular map

			$coordinates = $data['Map']['coordinates'];
			if (substr_count($coordinates, 'undefined')){
				$this->Session->setFlash(__('Map could not be saved due to a javascript error'), 'flash_failure');
				echo 'Map could not be saved due to a javascript error';
				$this->redirect(array('action' => 'index'));
			}

			if ($this->Map->save($data)){
				$this->Session->setFlash(__('Map has been saved'), 'flash_success');
				$this->redirect(array('action' => 'index'));
			}else{
				$this->Session->setFlash(__('Map could not be saved'), 'flash_failure');
			}
		}

		function admin_view($id = null){
			$this->set('body_function', 'drawViewMap()');
			if(is_null($id)) exit();

			$map = $this->Map->findById($id);
			$this->set('map', $map);
		}

		function admin_edit($id = null){
			$this->set('body_function', 'drawEditMap()');
			if(is_null($id)) exit();

			if (is_numeric($id) && $this->request->is('post')){
				//Get the map
				$this->Map->id = $this->request->data['Map']['id'];

				if ($this->Map->save($this->request->data)){
					$this->Session->setFlash(__('Map has been saved'), 'flash_success');
				}else{
					$this->Session->setFlash(__('Map could not be saved'), 'flash_failure');
				}
			}

			$map = $this->Map->findById($id);

			$this->data = $map;

			$this->loadModel('State');
			$this->loadModel('Country');
			$states = $this->State->get_as_array();
			$countries = $this->Country->get_as_array();

			$this->set(compact('states','countries'));

			$this->set('map', $map);
		}

		/**
		 * delete method
		 *
		 * @param string $id
		 * @return void
		 */
		public function admin_delete($id = null) {
			if (!$this->request->is('post')) {
				throw new MethodNotAllowedException();
			}
			$this->Map->id = $id;
			if (!$this->Map->exists()) {
				throw new NotFoundException(__('Invalid map'));
			}
			if ($this->Map->delete()) {
				$this->Session->setFlash(__('Map deleted'));
				$this->redirect(array('action'=>'index'));
			}
			$this->Session->setFlash(__('Map was not deleted'));
			$this->redirect(array('action' => 'index'));
		}
	}
