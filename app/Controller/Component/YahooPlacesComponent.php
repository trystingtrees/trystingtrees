<?php
	/**
	 * Yahoo Places API Component
	 *
	 * @author Luckner Jr. Jean-Baptiste <lucknerjb [@] gmail [dot] com>
	 * @date 06 Feb 2012
	 */
	class YahooPlacesComponent extends Component {
		/**
		 * Available results formats. XML is defaults for Yahoo so there is no flag needed in the api call
		 */
		protected $formats = array(
			'json' => 'J', 
			'php' => 'P', 
			'xml' => ''
		);		

		public function geocode($address, $format = 'json'){
			//Available formats = json|xml|php
			//Get API info
			$endpoint = Configure::read('YAHOO_ENDPOINT');
			$api_key = Configure::read('YAHOO_KEY');
			$api_secret = Configure::read('YAHOO_SECRET');

			//Make the call
			$results = $this->_api_call($address, $endpoint, $api_key, $api_secret, $format);

			return $results;
		}

		private function _api_call($address, $endpoint, $api_key, $api_secret, $format){
			// Format to receive the results in?
			$flag = $this->formats[$format];
			$flag = ($flag == '') ? '' : '&flags=' . $flag;

			// Prepare the call URL
			$address = str_replace(' ', '+', $address);
			$call_url = $endpoint . 'geocode?location=' . $address . $flag . '&appid=' . $api_key;

			// Make the call and return the results
			$results = file_get_contents($call_url);
			return $results;
		}
	}
?>
