<?php
class AppControlsComponent extends Component {
	public static function generate_uuid(){
		return String::uuid();
	}

	public static function parse_xml($file){
		$xml_string = Xml::build($file);

		//To Array
		$xml_array = Xml::toArray( $xml_string );

		return $xml_array;
	}

	public function build_results_params($params){
		//Loop through the params and build the GET params such as "key:id&key:id"
		$get_params = '';
		foreach($params as $k => $v){
			//$get_params .= "{$k}:{$v}&";
			$get_params .= "{$k}:{$v}/";
		}

		//Remove extra "&"
		//$get_params = rtrim($get_params, "&");

		return $get_params;
	}

	//Build current page url
	public function get_current_page_url($request, $skip_keys = array()){
		$controller = $request->controller;
		$action = $request->action;
		$query = $request->query;

		//Rebuild the Query params if there are any and drop the reference to fb_auth
		$params = '';
		if (sizeof($query) > 1){
			foreach($query as $k => $v){
				if (in_array($k, $skip_keys)) continue;
				$params .= "$k=$v&";
			}
			$params = rtrim($params, '&');
			$params = '?' . urlencode($params);
		}

		$redir_link = '/' . $controller . '/' . $action . '/' . $params;

		return $redir_link;
	}

	public function build_profile_pic_path($uuid, $picture){
		$profile_pic_base_path = Configure::read('PROFILE_PICS_BASE_URL');
		$picture_url = $profile_pic_base_path . DS . $uuid . DS . $picture;

		return $picture_url;
	}
}
