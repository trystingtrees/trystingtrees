<?php
class AppController extends Controller {
	public $components = array(
		'Acl',
		'Auth' => array(
			'authorize' => array(
				'Actions' => array('actionPath' => 'controllers')
			)
		),
		'Session', 
		'AppControls', 
		'Twitter.Twitter', 
		'Facebook.Connect',
		'YahooPlaces'
	);
	
	public $helpers = array('Html', 'Form', 'Session', 'Facebook.Facebook', 'Formatter', 'Social');
	public static $config;

	private function _set_env_var(){
		//Get environment
		$env = $this->_get_env();

		switch($env){
			case 'thewebsandbox.com':	//Local DEV
				$HOME = 'http://thewebsandbox.com/Trystingtrees';
				$this->HOME = $HOME;
				break;
			case 'www.thewebsandbox.com':	//Local DEV
				$HOME = 'http://www.thewebsandbox.com/Trystingtrees';
				$this->HOME = $HOME;
				break;
			case 'trystingtrees.localhost':	//Local DEV
				$HOME = 'http://trystingtrees.localhost';
				$this->HOME = $HOME;
				break;
			case '63.141.238.195': //SSHVM Test
				$HOME = 'http://63.141.238.195/Trystingtrees';
				$this->HOME = $HOME;
				break;
			case 'dev01.trystingtrees.com': //Dev01
				$HOME = 'http://dev01.trystingtrees.com';
				$this->HOME = $HOME;
				break;
			case 'dev02.trystingtrees.com': //Dev01
				$HOME = 'http://dev02.trystingtrees.com';
				$this->HOME = $HOME;
				break;
			case 'qa01.trystingtrees.com': //Dev01
				$HOME = 'http://qa01.trystingtrees.com';
				$this->HOME = $HOME;
				break;
			case 'test01.trystingtrees.com': //Test01
				$HOME = 'http://test01.trystingtrees.com';
				$this->HOME = $HOME;
				break;
			case 'www.trystingtrees.com': // Live Server
				$HOME = 'http://www.trystingtrees.com';
				$this->HOME = $HOME;
				break;
			default:
		}

		$this->set('HOME', $HOME);
	}

	function beforeFilter() {
		//Configure AuthComponent
		$this->Auth->loginAction = array('controller' => 'users', 'action' => 'login');
		//$this->Auth->logoutRedirect = array('controller' => 'users', 'action' => 'login');
		$this->Auth->logoutRedirect = array('controller' => 'websites', 'action' => 'index');
		//$this->Auth->loginRedirect = array('controller' => 'posts', 'action' => 'add');
		self::$config = array(
			'XML_UPLOADS_HOME' => APP.'webroot/files/xml_uploads'
		);

		//Set some site-wide env vars
		$this->_set_env_var();

		//Facebook
		$fb_user = false;
		$FB = new FB();
		$fb_user = $FB->api('/me');
		$this->set('fb_user', $fb_user);
		$this->set('fb_flag', '');

		//Check for social logins
		$this->_check_social_logins();

		//Get the current page's URL
		$cur_page_url = $this->AppControls->get_current_page_url($this->request);
		$this->set('fb_redir', $cur_page_url . '?social_auth=true&auth=fb');
		$this->set('twitter_redir', $cur_page_url . '?social_auth=true&auth=twitter');

		//Keep the current page redir value in the Session for use later if need be
		$social_redirs = array(
			'facebook' => $cur_page_url . '?social_auth=true&auth=fb', 
			'twitter' => $cur_page_url . '?social_auth=true&auth=twitter', 
		);
		$this->Session->write('social_redirs', $social_redirs);

		//Change layout for admin routing
		if (isset($this->params['prefix']) && $this->params['prefix'] == 'admin') {
			$this->layout = 'admin';
		}

		//Get the current user
		$this->set('user', $this->get_current_user());

		//Twitter or FB Actions?
		$twitter_action = '';
		$facebook_action = '';
		if ($this->Session->check('twitter_action')){
			$twitter_action = $this->Session->read('twitter_action');
		}
		else if ($this->Session->check('fb_action')){
			$facebook_action = $this->Session->read('facebook_action');
		}
	
		//Set the actions for the views	
		$this->set(compact('twitter_action', 'facebook_action'));
	}

	//Check social logins
	private function _check_social_logins(){
		//Check FB login?
		if (isset($this->request->query['social_auth'])){
			switch($this->request->query['auth']){
				case 'fb':
					$this->_fb_auth();
					break;
				case 'twitter':
					//Preserve request with oauth token and verifier.
					//$this->_twitter_auth();
					break;
			}
		}
	}

	private function _fb_auth(){
		//Check to see if we have a regularly logged in user
		if ($this->Session->check('Auth.User')){
			//There is a user, attach this FB User to this regular user
			$redir_link = $this->AppControls->get_current_page_url($this->request);
			$this->redirect(Configure::read('HOME') . '/social_auths/fb_login/?user=true&redir=' . $redir_link, true);
		}else{
			//There is no user, create it
			$redir_link = $this->AppControls->get_current_page_url($this->request, array('fb_auth'));
			$this->redirect(Configure::read('HOME') . '/social_auths/fb_login/?user=false&redir=' . $redir_link, true);
		}
	}

	private function _twitter_auth(){
		//Check to see if we have a regularly logged in user
		if ($this->Session->check('Auth.User')){
			//There is a user, attach this Twitter User to this regular user
			$redir_link = $this->AppControls->get_current_page_url($this->request);
			$this->redirect(Configure::read('HOME') . '/social_auths/twitter_authorize/?user=true&redir=' . $redir_link, true);
		}else{
			//There is no user, create it
			$redir_link = $this->AppControls->get_current_page_url($this->request, array('fb_auth'));
			$this->redirect(Configure::read('HOME') . '/social_auths/twitter_login/?user=false&redir=' . $redir_link, true);
		}
	}

	private function _get_env(){
		//return getenv('REMOTE_ADDR');
		return $_SERVER['HTTP_HOST'];
	}

	function beforeRender(){
		$this->_set_env_var();
	}

	function _is_valid_agent($agent_username){
		$this->loadModel('User');
		$user = $this->User->getUserByUsername($agent_username);

		if (isset($user['User']['id'])){
			if ($user['User']['group_id'] !== '2') return false; //Not an agent
			//Find the Agent Listing Id
			$this->loadModel('ListingAgent');
			return $this->ListingAgent->findByUserId($user['User']['id']);
		}else{
			return false;
		}
	}

	public function get_current_user(){
		$sess = $this->Session->read();
		return isset($sess['Auth']['User']) ? $sess['Auth']['User'] : false;
	}

	/**
	 * Render a custom error message
	 * 
	 * @param string error message
	 * @param string redirect url
	 * 
	 * @return void
	*/
	function custom_error($error_msg, $redirect = ''){
		$this->Session->setFlash($error_msg, 'flash_failure');
		$this->redirect($redirect, true);
	}

	/**
	 * Render a custom success message
	 * 
	 * @param string success message
	 * @param string redirect url
	 * 
	 * @return void
	*/
	function custom_success($error_msg, $redirect = ''){
		$this->Session->setFlash($success_msg, 'flash_success');
		$this->redirect($redirect, true);
	}

}
