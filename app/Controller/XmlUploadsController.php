<?php
class XmlUploadsController extends AppController {
	public $name = "XmlUploads";

	function beforeFilter(){
		parent::beforeFilter();
		$this->Auth->allow(array('*'));
	}

	function upload(){
	}
	
	/** ADMIN ROUTING **/

	function admin_upload(){
		return $this->upload();
	}

	function admin_list(){
		$xml_files = $this->XmlUpload->find('all');
		$this->set(compact('xml_files'));
	}

	/** FUNCTIOMS **/

	function admin_upload_xml(){
		//The next formatting lines are importatnt for when we want to read the XML file. The main element in the array will be the name of the file
		//being read. We need a standard format so we can guess the node name via the file name.
		$file_name = $this->data['XmlUpload']['xml_file']['name'];
		$file_name = $str_replace('_', ' ', $file_name);
		$file_name = ucwords($file_name);
		$file_name = str_replace(' ', '', $file_name);

		//Grab the information
		$data = array(
			'XmlUpload' => array(
				'source_id' => $this->data['XmlUpload']['source_id'] + 1, 
				//'xml_file' => $this->data['XmlUpload']['xml_file']['name']
				'xml_file' => $file_name
			)
		);

		if ($this->XmlUpload->save($data)){
			$id = $this->XmlUpload->id;
			//Create dir
			$XML_UPLOADS_HOME = parent::$config['XML_UPLOADS_HOME'];
			mkdir($XML_UPLOADS_HOME . "/{$id}");

			//Move file
			move_uploaded_file($this->data['XmlUpload']['xml_file']['tmp_name'], "{$XML_UPLOADS_HOME}/{$id}/{$this->data['XmlUpload']['xml_file']['name']}");
		}else{
			$this->Session->setFlash('Could not upload Listings XML File');
		}

		$this->Session->setFlash('Listings XML File Uploaded Successfully');
		$this->redirect('/admin/xmls/list');

	}

	function admin_process($id = null){
		if (is_null($id)){
			$this->Session->setFlash('Invalid File ID');
			$this->redirect('/admin/xmls/list');
		}

		//Get file information
		$file = $this->XmlUpload->find('first', $id);
		$file_name = $file['XmlUpload']['xml_file'];

		//Check to make sure that this file was not already processed
		if ($file['XmlUpload']['processed'] == 1){
			$this->Session->setFlash('This file was already processed');
			//$this->redirect('/admin/xml_uploads/list', true);
		}

		$root_loc = parent::$config['XML_UPLOADS_HOME'];
		$file_location = "{$root_loc}/{$id}/{$file_name}";

		//Parse Xml
		$xml_array = $this->AppControls->parse_xml($file_location);

		//Source?
		$source = $file['Source']['id'];

		switch($source){
			case 1:	//Point2.com
					$process_status = $this->_parse_source_point2($id, $xml_array, $file_name);
				break;
			case 2:	//Other
				break;
			default:
				break;
		}
		
		if ($process_status){
			$this->XmlUpload->saveField('processed', 1);
		}

		$this->Session->setFlash('The Listings File was successfully processed and the listings created');
		$this->redirect('/admin/xml_uploads/list', true);
	}

	private function _parse_source_point2($file_id, $xml_array, $file_name){
		$file_name = str_replace('.xml', '', $file_name);
		$all_listings = $xml_array[$file_name]['Listings']['Listing'];
		$num_listings = 0;
	
		//Load some models needed
		$this->loadModel('Listing');
		$this->loadModel('ListingAgent');
		$this->loadModel('ListingStatus');
		$this->loadModel('ListingDetail');
		$this->loadModel('ListingFinancial');
		$this->loadModel('ListingLocation');
		$this->loadModel('ListingMedia');
		$this->loadModel('ListingMetaDetail');
		$this->loadModel('ListingFeature');
		$this->loadModel('AgentMedia');
		$this->loadModel('MediaType');
		$this->loadModel('PropertyType');
		$this->loadModel('PropertyStyle');
		$this->loadModel('Country');
		$this->loadModel('State');
		
		//Get some data once so we don't have to get it every time
		$this->v_listing_statuses = $this->ListingStatus->get_as_array();
		$this->v_countries = $this->Country->get_as_array();
		$this->v_states = $this->State->get_as_array();
		$this->v_property_types = $this->PropertyType->get_as_array();
		$this->v_property_styles = $this->PropertyStyle->get_as_array();
		$this->v_media_types = $this->MediaType->get_as_array();

		//==================================== Loop and gather info
		foreach($all_listings as $item){
			//Fetch the class vars again just in case they were updated in the previous pass on the listings array
			$listing_statuses = $this->v_listing_statuses;
			$countries = $this->v_countries;
			$states = $this->v_states;
			$property_types = $this->v_property_types;
			$property_styles = $this->v_property_styles;
			$media_types = $this->v_media_types;

			//============ Listing Metadata
			$listing_uid = $item['ListingID'];
			$provider_listing_id = $item['ProviderListingID'];
			$provider_name = $item['Provider'];
			$list_date = @$item['ListDate'];
			$last_update_date = @$item['LastUpdateDate'];
			$status = $item['Status'];
			$regional_mls_number = @$item['RegionalMLSNumber']['@'];
			$regional_mls_number_visible = $item['RegionalMLSNumber']['@publiclyVisible'];

			//============ Listing Title + URLs
			$listing_title = $item['Title'];
			$detail_view_url = $item['DetailViewUrl'];
			$virtual_tour_url = @$item['VirtualTourUrl'];
				
			//============ Listing Location
			$location = $item['Location'];
			$address = $location['Address']['@'];
			$address_visible = $location['Address']['@publiclyVisible'];
			$postal_code = $location['PostalCode'];
			$latitude = @$location['Latitude'];
			$longitude = @$location['Longitude'];
			$city = $location['City'];
			$state = $location['State']['@abbreviation'];
			$country = $location['Country']['@abbreviation'];

			//COUNTY
			$county = '';
			foreach($location['GeoArea'] as $k => $v){
				if ($v['@type'] == 'County'){
					$county = $v['@name'];
				}
			}

			//============ Listing Details
			$details = $item['Details'];
			$description = $details['Description'];
			$year_built = @$details['YearBuilt']['@'];
			
			$bathrooms = @$details['Bathrooms']['@'];
			$bathroom_comments = @$details['Bathrooms']['@comment'];
			
			$bedrooms = @$details['Bedrooms']['@'];
			$bedroom_comments = @$details['Bedrooms']['@comment'];

			$garage_stalls = @$details['Garage']['@stalls'];
			$garage_style = @$details['Garage']['@style'];
			$garage_comments = @$details['Garage']['@comment'];

			$lot_legal_name = @$details['Lot']['LotLegal'];
			$lot_comment = @$details['Lot']['@comment'];

			$living_area = @$details['LivingArea']['@'];
			$living_area_units = @$details['LivingArea']['@unit'];

			$list_price = @$details['ListPrice']['@'];
			$list_price_currency = @$details['ListPrice']['@currency'];

			$rental_price = @$details['RentalPrice']['@'];
			$rental_price_currency = @$details['RentalPrice']['@currency'];
			$rental_price_period = @$details['RentalPrice']['@period'];

			$tax_amount = @$details['TaxAmount']['@'];
			$tax_currency = @$details['TaxAmount']['@currency'];
			$tax_year = @$details['TaxAmount']['@taxYear'];

			$property_type = $details['PropertyType'];
			$property_style = @$details['Style'];

			//In case the property style is empty
			if ($property_style == '') $property_style = $property_type;


			//============ Listing Agent
			$agent = $item['ListingAgent'];
			$agent_id = $agent['AgentID'];
			$office_id = $agent['OfficeID'];
			$agent_name = $agent['Name'];

			$agent_avatar = @$agent['Media']['Item'][0]['Url']['@'];
			$agent_avatar_caption = @$agent['Media']['Item'][0]['@caption'];
			$agent_avatar_media_id = @$agent['Media']['Item'][0]['@mediaId'];

			$agent_logo = @$agent['Media']['Item'][0]['Url']['@'];
			$agent_logo_caption = @$agent['Media']['Item'][0]['@caption'];
			$agent_logo_media_id = @$agent['Media']['Item'][0]['@mediaId'];

			$broker_name = @$item['Broker']['name'];
			$agent_website = @$contact['Website'];

			//============ Listing Features
			$features = array();
			if (isset($item['Features'])){
				foreach($item['Features']['Feature'] as $f){
					$f_category = $f['@category'];
					$f_details = $f['@'];

					$features[] = array(
						'category' => $f_category, 
						'details' => $f_details
					);
				}
			}

			//============ Listing Telephones
			$telephone_numbers = array();
			if (isset($item['ListingAgent']['ContactInfo'])){
				foreach($item['ListingAgent']['ContactInfo']['Telephone'] as $f){
					$f_type = $f['@name'];
					$f_number = $f['@'];

					$telephone_numbers[$f_type] = array(
						'type' => $f_type, 
						'number' => $f_number
					);
				}
			}

			//============ Medias
			$medias = array();
			if (isset($item['Media']['Item'])){
				if (isset($item['Media']['Item'][0])){
					foreach($item['Media']['Item'] as $f){
						$f_mime_type	= $f['@mimeType'];
						$f_medium 		= $f['@medium'];
						$f_media_id 	= $f['@mediaId'];
						$f_caption 		= @$f['@caption'];
						$f_thumb_url 	= $f['Url'][0]['@'];
						$f_gallery_url	= $f['Url'][1]['@'];

						$medias[] = array(
							'mime_type' => $f_mime_type, 
							'medium' => $f_medium, 
							'media_id' => $f_media_id, 
							'caption' => $f_caption, 
							'thumb_url' => $f_thumb_url, 
							'image_url' => $f_gallery_url
						);
					}
				}else{
					$f_mime_type	= $item['Media']['Item']['@mimeType'];
					$f_medium 		= $item['Media']['Item']['@medium'];
					$f_media_id 	= $item['Media']['Item']['@mediaId'];
					$f_caption 		= @$item['Media']['Item']['@caption'];
					$f_thumb_url 	= $item['Media']['Item']['Url'][0]['@'];
					$f_gallery_url	= $item['Media']['Item']['Url'][1]['@'];

					$medias[] = array(
						'mime_type' => $f_mime_type, 
						'medium' => $f_medium, 
						'media_id' => $f_media_id, 
						'caption' => $f_caption, 
						'thumb_url' => $f_thumb_url, 
						'image_url' => $f_gallery_url
					);
				}
			}

			//============ Agent Medias
			$agent_medias = array();
			if (isset($item['ListingAgent']['Media']['Item'])){
				if (isset($item['ListingAgent']['Media']['Item'][0])){
					foreach($item['ListingAgent']['Media']['Item'] as $f){
						$f_mime_type	= $f['@mimeType'];
						$f_medium 		= $f['@medium'];
						$f_media_id 	= $f['@mediaId'];
						$f_caption 		= @$f['@caption'];
						$f_type		 	= $f['Url']['@style'];
						$f_gallery_url	= $f['Url']['@'];

						$agent_medias[] = array(
							'mime_type' => $f_mime_type, 
							'medium' => $f_medium, 
							'media_id' => $f_media_id, 
							'caption' => $f_caption, 
							'type' => $f_type, 
							'image_url' => $f_gallery_url
						);
					}
				}else{
					$f_mime_type	= $item['ListingAgent']['Media']['Item']['@mimeType'];
					$f_medium 		= $item['ListingAgent']['Media']['Item']['@medium'];
					$f_media_id 	= $item['ListingAgent']['Media']['Item']['@mediaId'];
					$f_caption 		= @$item['ListingAgent']['Media']['Item']['@caption'];
					$f_gallery_url	= $item['ListingAgent']['Media']['Item']['Url'][1]['@'];

					$agent_medias[] = array(
						'mime_type' => $f_mime_type, 
						'medium' => $f_medium, 
						'media_id' => $f_media_id, 
						'caption' => $f_caption, 
						'type' => $f_type, 
						'image_url' => $f_gallery_url
					);
				}
			}

			//============================================== Save data to DB
			
			//==============================================
			// AGENT
			//==============================================

			//Check if the agent exists first
			$agent_exists = $this->ListingAgent->is_duplicate($agent_id);

			//Cancel if agent exists
			if ($agent_exists == false) {
				//Create agent
				$agent_save_data = array(
					'ListingAgent' => array(
						'user_id' => '0', 
						'uuid' => $this->AppControls->generate_uuid(), 
						'src_agent_uid' => $agent_id, 
						'src_office_uid' => $office_id, 
						'name' => $agent_name, 
						'contact_cell' => $telephone_numbers['Cell']['number'], 
						'contact_office_phone' => @$telephone_numbers['Office']['number'], 
						'contact_fax' => @$telephone_numbers['Fax']['number'], 
						'website' => $agent_website 
					)
				);

				$this->ListingAgent->create();
				$agent_row = $this->ListingAgent->save($agent_save_data);
				$user_id = $agent_row['ListingAgent']['id'];
			}else{
				//Agent exists
				$user_id = $agent_exists;
			}
			
			//==============================================
			// LISTING
			//==============================================
		
			$source_id = 1;
			//1 = for sale, 2 = for rent
			$listing_category = ($rental_price == '') ? 1 : 2;
			$listing_subcategory = 0;

			//Get listing status id
			$status = strtolower($status);
			$status = str_replace(' ', '_', $status);

			//Find child element or create it if need be
			$listing_status_id = $this->_find_or_create_element($status, $listing_statuses, 'ListingStatus', 'v_listing_statuses');

			//@TODO: Need to check for invalid listing status id. Will most likely have to create it on the spot and send an email
			//so that we can look into it.

			$listing_save_data = array(
				'Listing' => array(
					'uuid' => $this->AppControls->generate_uuid(), 
					'source_id' => 1,
					'listing_status_id' => $listing_status_id,
					'listing_category' => $listing_category, 
					'listing_subcategory' => $listing_subcategory, 
					'user_id' => $user_id
				)
			);

			$this->Listing->create();
			$save = $this->Listing->save($listing_save_data);
			$listing_id = $save['Listing']['id'];
	
			//==============================================
			// LISTING FEATURES
			//==============================================
		
			$listing_features_save_data = array(
				'ListingFeature' => array(
					'listing_id' => $listing_id, 
					'year_built' => $year_built, 
					'bathrooms' => $bathrooms, 
					'bathroom_comments' => $bathroom_comments, 
					'bedrooms' => $bedrooms, 
					'bedroom_comments' => $bedroom_comments, 
					'garage_stalls' => $garage_stalls, 
					'garage_style' => $garage_style, 
					'garage_comments' => $garage_comments, 
					'living_area' => $living_area, 
					'living_area_units' => $living_area_units, 
					'other_features' => serialize($features)
				)
			);	

			$this->ListingFeature->create();
			$this->ListingFeature->save($listing_features_save_data);
			
			//==============================================
			// LISTING DETAILS
			//==============================================

			//Find the Property Type ID. Create a new one if one is not found
			$property_type = strtolower($property_type);
			$property_type = str_replace(' ', '_', $property_type);

			//Find child element or create it if need be
			$property_type_id = $this->_find_or_create_element($property_type, $property_types, 'PropertyType', 'v_property_types');
			
			if ($property_type_id == false){
				$this->PropertyType->create();
				$property_row = $this->PropertyType->save(array('name' => $property_type, 'active' => 1));
				$property_type_id = $property_row['PropertyType']['id'];
			}

			//Find the Property Style ID. Create a new one if one is not found
			$property_style = strtolower($property_style);
			$property_style = str_replace(' ', '_', $property_style);

			//Find child element or create it if need be
			$property_style_id = $this->_find_or_create_element($property_style, $property_styles, 'PropertyStyle', 'v_property_styles');
		
			if ($property_style_id == false && $property_style !== ''){
				$this->PropertyStyle->create();
				$property_row = $this->PropertyStyle->save(array('name' => $property_style, 'active' => 1));
				$property_style_id = $property_row['PropertyStyle']['id'];
			}

			//Build save data
			$listing_details_save_data = array(
				'ListingDetail' => array(
					'listing_id' => $listing_id, 
					'title' => $listing_title, 
					'description' => $description, 
					'detail_view_url' => $detail_view_url, 
					'virtual_tour_url' => $virtual_tour_url, 
					'broker_name' => $broker_name, 
					'property_type_id' => $property_type_id, 
					'property_style_id' => $property_style_id, 
					'lot_legal_name' => $lot_legal_name, 
					'lot_comment' => $lot_comment, 
				)
			);

			$this->ListingDetail->create();
			$save = $this->ListingDetail->save($listing_details_save_data);

			if ($listing_id == 5 || $listing_id == 9){
				/*
				pr($item);
				pr($listing_details_save_data);
				pr($save);
				*/
			}
			
			//==============================================
			// LISTING FINANCIALS
			//==============================================

			//FOR SALE
			if ($listing_category == 1){
				$price = $list_price;
				$currency = $list_price_currency;
				$period = 0;
			}else{
			//FOR RENT
				$price = $rental_price;
				
				$currency = $rental_price_currency;
				$period = $rental_price_period;
			}

			//Make sure currencies match
			if ($listing_category == 1){
				if ($currency !== $tax_currency){
					//@TODO: Flag listing to be reviewed. Add it to the DB but make it inactive and send an email with the details as to why it is inactive.
				}
			}

			$listing_financials_save_data = array(
				'ListingFinancial' => array(
					'listing_id' => $listing_id, 
					'price_amount' => $price, 
					'price_currency' => $currency, 
					'price_period' => $period, 
					'tax_amount' => $tax_amount, 
					'tax_currency' => $tax_currency, 
					'tax_year' => $tax_year
				)
			);

			$this->ListingFinancial->create();
			$save = $this->ListingFinancial->save($listing_financials_save_data);
			
			//==============================================
			// LISTING LOCATIONS
			//==============================================

			//Find Country and State IDs
			//Find child element or create it if need be
			$country_id = $this->_find_or_create_element($country, $countries, 'Country', 'v_countries');

			//Find child element or create it if need be
			$state_id = $this->_find_or_create_element($state, $states, 'State', 'v_states');

			$listing_locations_save_data = array(
				'ListingLocation' => array(
					'listing_id' => $listing_id, 
					'address' => $address,
					'city' => $city, 
					'county' => $county, 
					'state_id' => $state_id,
					'postal_code' => $postal_code, 
					'country_id' => $country_id, 
					'latitude' => $latitude, 
					'longitude' => $longitude 
				)
			);

			$this->ListingLocation->create();
			$save = $this->ListingLocation->save($listing_locations_save_data);
			
			//==============================================
			// LISTING MEDIA
			//==============================================
		
			foreach($medias as $media){
				$mime_type_array = explode('/', $media['mime_type']);
				$medium_type = $mime_type_array[0];

				//Find child element or create it if need be
				$media_type_id = $this->_find_or_create_element($medium_type, $media_types, 'MediaType', 'v_media_types');

				$listing_medias_save_data = array(
					'ListingMedia' => array(
						'listing_id' => $listing_id, 
						'mime_type' => $media['mime_type'], 
						'medium' => $media['medium'], 
						'src_media_id' => $media['media_id'], 
						'caption' => $media['caption'], 
						'media_type_id' => $media_type_id, 
						'media_url' => $media['image_url'], 
						'thumb_url' => $media['thumb_url']
					)
				);

				$this->ListingMedia->create();
				$this->ListingMedia->save($listing_medias_save_data);
			}
			
			//==============================================
			//AGENT MEDIA
			//==============================================
		
			foreach($agent_medias as $media){
				$mime_type_array = explode('/', $media['mime_type']);
				$medium_type = $mime_type_array[0];

				//Find child element or create it if need be
				$media_type_id = $this->_find_or_create_element($medium_type, $media_types, 'MediaType', 'v_media_types');

				$agent_medias_save_data = array(
					'AgentMedia' => array(
						'listing_agent_id' => $user_id, 
						'mime_type' => $media['mime_type'], 
						'medium' => $media['medium'], 
						'src_media_id' => $media['media_id'], 
						'caption' => $media['caption'], 
						'media_type_id' => $media_type_id, 
						'media_url' => $media['image_url'], 
						'thumb_url' => ''
					)
				);

				$this->AgentMedia->create();
				$this->AgentMedia->save($agent_medias_save_data);
			}
			
			//==============================================
			// LISTING META DETAILS
			//==============================================

			//Format dates
			$str_list_date = strtotime($list_date);
			$list_date = date('Y-m-d H:i:s', $str_list_date);

			$str_last_update_date = strtotime($last_update_date);
			$last_update_date = date('Y-m-d H:i:s', $str_last_update_date);

			$listing_meta_details_save_data = array(
				'ListingMetaDetail' => array(
					'listing_id' => $listing_id, 
					'src_listing_uid' => $listing_uid, 
					'src_provider_listing_uid' => $provider_listing_id, 
					'src_provider_name' => $provider_name, 
					'src_list_date' => $list_date, 
					'src_last_update_date' => $last_update_date, 
					'regional_mls_number' => $regional_mls_number
				)
			);

			$this->ListingMetaDetail->create();
			$save = $this->ListingMetaDetail->save($listing_meta_details_save_data);
			
			$num_listings++;

			$log = $this->Listing->getDataSource()->getLog(false, false);

		}	//End ForEach

		//Save the number of listings
		$this->XmlUpload->id = $file_id;
		$this->XmlUpload->saveField('num_listings', $num_listings);

		return true;

	}

	private function _find_or_create_element($needle, $haystack, $model, $class_var){
		if (in_array($needle, $haystack)){
			$ids = array_keys($haystack, $needle);
			$id = $ids[0];
		}else{
			//Create Model Entry if $needle is not empty
			if ($needle !== '' && !($model == 'Country' || $model == 'State')){
				$this->$model->create();
				$rs = $this->$model->save(array('name' => $needle, 'active' => 1));

				//Save the new array to a class var
				$new_data = $this->$model->find('list');
				$this->$class_var = $new_data;

				//Set the ID
				$id = $this->$model->id;
			}else{
				$id = '';
			}
		}

		return $id;
	}
}
