<?php
	class WebsitesController extends AppController {
		public $name = "Websites";

		function beforeFilter(){
			parent::beforeFilter();
			$this->Auth->allow(array('*'));
			$this->set('body_id', 'home');
			$this->set('body_classes', 'home blog');
		}

		function index(){
			$title = 'Home';
			$this->set(compact(
				'title'
			));
		}

		function index2(){
			$this->layout = 'version2';
			$title = 'Home';
			$this->set(compact(
				'title'
			));
		}

		function about(){
			$title = 'About Us';
			$this->set(compact(
				'title'
			));
		}

		function contact(){
			$title = 'Contact Us';
			$this->set(compact(
				'title'
			));
		}

		function api_test(){
			$address = '33 Lochinvar Cresc, North York, ON';
			$results = $this->YahooPlaces->geocode($address);
			$results = json_decode($results);

			pr($results);

			exit();
		}
	}
?>
