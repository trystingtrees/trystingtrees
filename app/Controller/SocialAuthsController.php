<?php
class SocialAuthsController extends AppController {
	public $name = "SocialAuths";
	public $uses = array('Oauth');

	public function beforeFilter(){
		$this->Auth->allow(array('*'));
	}

	/**
	 * Check to see if the newly logged in FB User has a user attached to them. If not, create one. If so, set their attached user as the logged in user
	 * 
	 */
	public function fb_login(){
		$sess = $this->Session->read();

		//Get the FB ID
		$me = $this->Connect->user();
		//if (isset($sess['FB'])){
			$fb_id = $sess['FB']['Me']['id'];
		//}

		//Check to see there is a logged in user
		if (isset($this->request->query['user']) && $this->request->query['user'] == 'true'){
			//Grab the current user
			$current_user = $this->Session->read('Auth.User');
			$uid = $current_user['id'];

			//Attach the user to the FB user
			$save_data = array(
				'Oauth' => array(
					'oauth_type' => 'facebook', 
					'user_id' => $uid, 
					'social_id' => $fb_id 
				)
			);

			$this->Oauth->create();
			$saved = $this->Oauth->save($save_data);

			//Set the modal flag for the main layout
			$this->set('fb_flag', 'accounts_merged');

			//If saved, redirect to redir
			if ($saved) $this->redirect($this->request->query['redir'], true);
		}else{
		//Not registered
			//See if the user is registered in the DB
			$user = $this->Oauth->is_registered_user($fb_id);

			if ($user === false){
				//Create the user
				//Get email
				if (isset($sess['FB']['Me']['email'])){
					$email = $sess['FB']['Me']['email'];
				}else{
					//No email for user, default to FB ID
					$email = $fb_id;
				}
				
				//Get username
				if (isset($sess['FB']['Me']['username'])){
					$username = $sess['FB']['Me']['username'];
				}else{
					//No username for user, default to FB ID
					$username = $fb_id;
				}

				//Generate a random password
				$password = $this->AppControls->generate_uuid();

				//Build save array
				$save_array = array(
					'User' => array(
						'email' => $email, 
						'username' => $fb_id, 
						'passwd' => $password, 
						'passwd_confirm' => $password, 
						'group_id' => 1	//Default is a regular user. Users can ask to be switched to agents from their account page
					)
				);
				
				//Check to see if there is a user with this email address
				/*
				$this->loadModel('User');
				if ($email !== $fb_id){
					$user = $this->User->findByEmail($email);
					if ($user !== false){
						$user_id = $user['User']['id'];
					}else{
						//Save the user
						$this->User->create();
						$user = $this->User->save($save_array);
						$user_id = $this->User->id;
					}
				}else{
					*/
					//There is no user in the DB with this email address since Email == FB_ID
					//Save the user
					$this->User->create();
					$user = $this->User->save($save_array);
					$user_id = $this->User->id;
				//}

				//Create the connection with the Oauth table
				$save_data = array(
					'Oauth' => array(
						'oauth_type' => 'facebook', 
						'user_id' => $user_id, 
						'social_id' => $fb_id 
					)
				);

				$this->Oauth->create();
				$saved = $this->Oauth->save($save_data);

				if ($saved){
					//Log user in
					$auth_array = $user['User'];
					$this->Session->write('Auth.User', $auth_array);

					//Set the modal flag for the main layout
					$this->set('fb_flag', 'account_created');

					//Set fb action in session
					$this->Session->write('fb_action', 'welcome');

					//redirect to redir
					$this->redirect($this->request->query['redir'], true);
				}
			}else{
				//We found a user, log the person in as this user by setting the Session's Auth data
				$user_id = $user['Oauth']['user_id'];
				$this->loadModel('User');
				$reg_user = $this->User->read(null, $user_id);

				$this->Session->write('Auth.User', $reg_user['User']);

				//Set the modal flag for the main layout
				$this->set('fb_flag', 'account_found');

				$this->redirect($this->request->query['redir'], true);
			}
		}
	}

	public function fb_logout(){
	}

	public function fb_finalize(){
		$query = $this->request->query;
		if (isset($query['code'])){
			//Get an access token from FB
			$appId = Configure::read('FB_APP_ID');
			$redir = Configure::read('HOME') . '/fb_finalize';
			$secret = Configure::read('FB_APP_SECRET');
			$code = $query['code'];

			$link = 'https://graph.facebook.com/oauth/access_token?client_id=' . $appId . '&redirect_uri=' . $redir . '&client_secret=' . $secret . '&code=' . $code;
			$this->redirect($link);
		}else{
			'User not logged in';
		}
		exit();
	}

	//=====================================  TWITTER  =========================================

	public function twitter_connect(){
		$key = Configure::read('TWITTER_APP_KEY');
		$secret = Configure::read('TWITTER_APP_SECRET');
		$callback_url = Configure::read('TWITTER_CALLBACK_URL');

		$this->Twitter->setupApp($key, $secret);
		$this->Twitter->connectApp($callback_url);
	}

	public function twitter_authorize(){
		$this->Twitter->authorizeTwitterUser($this->request->query['oauth_token'], $this->request->query['oauth_verifier']);
		$twitter_user = $this->Twitter->getTwitterUser(true);

		$twitter_id = $twitter_user['profile']['id'];
		$profile_pic = $twitter_user['profile']['profile_image_url'];
		$screen_name = $twitter_user['profile']['screen_name'];
		$location = $twitter_user['profile']['location'];
		$name = $twitter_user['profile']['name'];

		$profile_data = array(
			'UserProfile' => array(
				'user_id' => '', 
				'name' => $name, 
				'profile_pic' => $profile_pic, 
				'screen_name' => $screen_name, 
				'geo_location' => $location

			)
		);
			
		$redirs = $this->Session->read('social_redirs');
		$redir = $redirs['twitter'];

		//Create a new user
		//Check to see there is a logged in user
		if ($this->Session->check('Auth.User')){
			//Grab the current user
			$current_user = $this->Session->read('Auth.User');
			$uid = $current_user['id'];

			//Attach the user to the FB user
			$save_data = array(
				'Oauth' => array(
					'oauth_type' => 'twitter', 
					'user_id' => $uid, 
					'social_id' => $twitter_id 
				)
			);

			$this->Oauth->create();
			$saved = $this->Oauth->save($save_data);

			//Set the modal flag for the main layout
			$this->set('twitter_flag', 'accounts_merged');

			//Save the profile
			$profile_data['UserProfile']['user_id'] = $uid;
			$this->loadModel('UserProfile');
			$this->UserProfile->save($profile_data);

			//If saved, redirect to redir
			if ($saved){
				$this->redirect($redir, true);
			}else{
				//@TODO: LOG
				$this->redirect($redir, true);
			}
		}else{
		//Not registered
			//See if the user is registered in the DB
			$user = $this->Oauth->is_registered_user($twitter_id);

			if ($user === false){
				//Create the user
				$email = $twitter_id;
				
				//Generate a random password
				$password = $this->AppControls->generate_uuid();

				//Build save array
				$save_array = array(
					'User' => array(
						'email' => $email, 
						'username' => $twitter_id, 
						'passwd' => $password, 
						'passwd_confirm' => $password, 
						'group_id' => 1	//Default is a regular user. Users can ask to be switched to agents from their account page
					)
				);

				//Save the user
				$this->loadModel('User');
				$this->User->create();
				$user = $this->User->save($save_array);
				$user_id = $this->User->id;

				//Create the connection with the Oauth table
				$save_data = array(
					'Oauth' => array(
						'oauth_type' => 'twitter', 
						'user_id' => $user_id, 
						'social_id' => $twitter_id 
					)
				);

				$this->Oauth->create();
				$saved = $this->Oauth->save($save_data);

				//Save the profile
				$profile_data['UserProfile']['user_id'] = $user_id;
				$this->loadModel('UserProfile');
				$this->UserProfile->save($profile_data);

				if ($saved){
					//Log user in
					$auth_array = $user['User'];
					$auth_array['User']['username'] = $screen_name;
					$this->Session->write('Auth.User', $auth_array);

					//Set the modal flag for the main layout
					$this->set('twitter_flag', 'account_created');

					//Set twitter action in session
					$this->Session->write('twitter_action', 'welcome');

					$this->redirect($redir, true);
				}else{
					//@TODO: LOG
					$this->redirect($redir, true);
				}
			}else{
				//We found a user, log the person in as this user by setting the Session's Auth data
				$user_id = $user['Oauth']['user_id'];
				$this->loadModel('User');
				$reg_user = $this->User->read(null, $user_id);
				$reg_user['User']['username'] = $screen_name;

				$this->Session->write('Auth.User', $reg_user['User']);

				//Set the modal flag for the main layout
				$this->set('twitter_flag', 'account_found');

				$this->redirect($redir, true);
			}
		}

		$this->redirect('/');
	}

	public function twitter_user(){
		$user = $this->Twitter->getTwitterUser(true);
		pr($user);
		die();
	}

	public function remove_actions(){
		$action = $this->request->data['action'];
		switch($action){
			case 'twitter':
				$this->Session->delete('twitter_action');
				
				if ($this->Session->check('twitter_action')){
					echo json_encode(array('status' => 'fail', 'message' => 'Could not remove welcome flag'));
				}else{
					echo json_encode(array('status' => 'success', 'message' => ''));
				}
				break;
			
			case 'facebook':
				$this->Session->delete('facebook_action');
				
				if ($this->Session->check('twitter_action')){
					echo json_encode(array('status' => 'fail', 'message' => 'Could not remove welcome flag'));
				}else{
					echo json_encode(array('status' => 'success', 'message' => ''));
				}
				break;
		}
		exit();
	}
}
