<?php
App::uses('AppController', 'Controller');
/**
 * ListingFinancials Controller
 *
 * @property ListingFinancial $ListingFinancial
 */
class ListingFinancialsController extends AppController {


/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->ListingFinancial->recursive = 0;
		$this->set('listingFinancials', $this->paginate());
	}

/**
 * view method
 *
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->ListingFinancial->id = $id;
		if (!$this->ListingFinancial->exists()) {
			throw new NotFoundException(__('Invalid listing financial'));
		}
		$this->set('listingFinancial', $this->ListingFinancial->read(null, $id));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->ListingFinancial->create();
			if ($this->ListingFinancial->save($this->request->data)) {
				$this->Session->setFlash(__('The listing financial has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The listing financial could not be saved. Please, try again.'));
			}
		}
		$listings = $this->ListingFinancial->Listing->find('list');
		$this->set(compact('listings'));
	}

/**
 * edit method
 *
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->ListingFinancial->id = $id;
		if (!$this->ListingFinancial->exists()) {
			throw new NotFoundException(__('Invalid listing financial'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->ListingFinancial->save($this->request->data)) {
				$this->Session->setFlash(__('The listing financial has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The listing financial could not be saved. Please, try again.'));
			}
		} else {
			$this->request->data = $this->ListingFinancial->read(null, $id);
		}
		$listings = $this->ListingFinancial->Listing->find('list');
		$this->set(compact('listings'));
	}

/**
 * delete method
 *
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->ListingFinancial->id = $id;
		if (!$this->ListingFinancial->exists()) {
			throw new NotFoundException(__('Invalid listing financial'));
		}
		if ($this->ListingFinancial->delete()) {
			$this->Session->setFlash(__('Listing financial deleted'));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Listing financial was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
