<?php
App::uses('AppController', 'Controller');
/**
 * ListingMetaDetails Controller
 *
 * @property ListingMetaDetail $ListingMetaDetail
 */
class ListingMetaDetailsController extends AppController {


/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->ListingMetaDetail->recursive = 0;
		$this->set('listingMetaDetails', $this->paginate());
	}

/**
 * view method
 *
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->ListingMetaDetail->id = $id;
		if (!$this->ListingMetaDetail->exists()) {
			throw new NotFoundException(__('Invalid listing meta detail'));
		}
		$this->set('listingMetaDetail', $this->ListingMetaDetail->read(null, $id));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->ListingMetaDetail->create();
			if ($this->ListingMetaDetail->save($this->request->data)) {
				$this->Session->setFlash(__('The listing meta detail has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The listing meta detail could not be saved. Please, try again.'));
			}
		}
		$listings = $this->ListingMetaDetail->Listing->find('list');
		$this->set(compact('listings'));
	}

/**
 * edit method
 *
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->ListingMetaDetail->id = $id;
		if (!$this->ListingMetaDetail->exists()) {
			throw new NotFoundException(__('Invalid listing meta detail'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->ListingMetaDetail->save($this->request->data)) {
				$this->Session->setFlash(__('The listing meta detail has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The listing meta detail could not be saved. Please, try again.'));
			}
		} else {
			$this->request->data = $this->ListingMetaDetail->read(null, $id);
		}
		$listings = $this->ListingMetaDetail->Listing->find('list');
		$this->set(compact('listings'));
	}

/**
 * delete method
 *
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->ListingMetaDetail->id = $id;
		if (!$this->ListingMetaDetail->exists()) {
			throw new NotFoundException(__('Invalid listing meta detail'));
		}
		if ($this->ListingMetaDetail->delete()) {
			$this->Session->setFlash(__('Listing meta detail deleted'));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Listing meta detail was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
