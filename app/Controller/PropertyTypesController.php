<?php
App::uses('AppController', 'Controller');
/**
 * PropertyTypes Controller
 *
 * @property PropertyType $PropertyType
 */
class PropertyTypesController extends AppController {


/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->PropertyType->recursive = 0;
		$this->set('propertyTypes', $this->paginate());
	}

/**
 * view method
 *
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->PropertyType->id = $id;
		if (!$this->PropertyType->exists()) {
			throw new NotFoundException(__('Invalid property type'));
		}
		$this->set('propertyType', $this->PropertyType->read(null, $id));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->PropertyType->create();
			if ($this->PropertyType->save($this->request->data)) {
				$this->Session->setFlash(__('The property type has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The property type could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->PropertyType->id = $id;
		if (!$this->PropertyType->exists()) {
			throw new NotFoundException(__('Invalid property type'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->PropertyType->save($this->request->data)) {
				$this->Session->setFlash(__('The property type has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The property type could not be saved. Please, try again.'));
			}
		} else {
			$this->request->data = $this->PropertyType->read(null, $id);
		}
	}

/**
 * delete method
 *
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->PropertyType->id = $id;
		if (!$this->PropertyType->exists()) {
			throw new NotFoundException(__('Invalid property type'));
		}
		if ($this->PropertyType->delete()) {
			$this->Session->setFlash(__('Property type deleted'));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Property type was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
