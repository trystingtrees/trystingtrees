<?php
App::uses('AppController', 'Controller');
/**
 * ListingFeatures Controller
 *
 * @property ListingFeature $ListingFeature
 */
class ListingFeaturesController extends AppController {


/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->ListingFeature->recursive = 0;
		$this->set('listingFeatures', $this->paginate());
	}

/**
 * view method
 *
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->ListingFeature->id = $id;
		if (!$this->ListingFeature->exists()) {
			throw new NotFoundException(__('Invalid listing feature'));
		}
		$this->set('listingFeature', $this->ListingFeature->read(null, $id));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->ListingFeature->create();
			if ($this->ListingFeature->save($this->request->data)) {
				$this->Session->setFlash(__('The listing feature has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The listing feature could not be saved. Please, try again.'));
			}
		}
		$listings = $this->ListingFeature->Listing->find('list');
		$this->set(compact('listings'));
	}

/**
 * edit method
 *
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->ListingFeature->id = $id;
		if (!$this->ListingFeature->exists()) {
			throw new NotFoundException(__('Invalid listing feature'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->ListingFeature->save($this->request->data)) {
				$this->Session->setFlash(__('The listing feature has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The listing feature could not be saved. Please, try again.'));
			}
		} else {
			$this->request->data = $this->ListingFeature->read(null, $id);
		}
		$listings = $this->ListingFeature->Listing->find('list');
		$this->set(compact('listings'));
	}

/**
 * delete method
 *
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->ListingFeature->id = $id;
		if (!$this->ListingFeature->exists()) {
			throw new NotFoundException(__('Invalid listing feature'));
		}
		if ($this->ListingFeature->delete()) {
			$this->Session->setFlash(__('Listing feature deleted'));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Listing feature was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
