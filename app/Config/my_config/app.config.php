<?php
/**
 * Application config file. Based on HOST
 * 
 */

	//Constants
	define('FILE_MOVE_FAIL', 600);
	define('FILE_MOVE_SUCCESS', 601);
	define('MKDIR_FAIL', 602);

	$host = $_SERVER['HTTP_HOST'];
 	$valid_hosts = array('trystingtrees.localhost', 'www.thewebsandbox.com', 'dev01.trystingtrees.com', 'test01.trystingtrees.com', 'www.trystingtrees.com');

	//=========================================== Environment specific config
	switch($host){
		case 'dev01.trystingtrees.com':
			Configure::write('HOME', 'http://dev01.trystingtrees.com');
			
			//Twitter API
			Configure::write('TWITTER_APP_KEY', 'LxwhUdagwvtRKv3TrIrLw');
			Configure::write('TWITTER_APP_SECRET', '2ZQDiglfJs8sJDBdIaFZumdzMTucuseZosqxroBBE');
			Configure::write('TWITTER_CALLBACK_URL', Configure::read('HOME') . '/twitter/authorize');
			break;

		case 'trystingtrees.localhost':
			Configure::write('HOME', 'http://trystingtrees.localhost');

			//Facebook API
			Configure::write('FB_APP_ID', '319420798103380');
			Configure::write('FB_APP_SECRET', '697edbb5badd169d77953cee326370af');

			//Yahoo API
			Configure::write('YAHOO_ENDPOINT', 'http://where.yahooapis.com/');
			Configure::write('YAHOO_KEY', 'dj0yJmk9YU5NRVBhZzQ2UDRWJmQ9WVdrOVEybGpNSHBETnpRbWNHbzlNVGN3TURJek9EUTJNZy0tJnM9Y29uc3VtZXJzZWNyZXQmeD1hNg--');
			Configure::write('YAHOO_SECRET', '5dc0092e3c2c018e5e18b3a3c44637d2b4c59bc6');
			break;
	
	 	case 'www.thewebsandbox.com':
			Configure::write('HOME', 'http://www.thewebsandbox.com/Trystingtrees');

			//Facebook API
			Configure::write('FB_APP_ID', '269655156435163');
			Configure::write('FB_APP_SECRET', '158f22d331dd98b1c705e7b3a2d07362');

			//Yahoo API
			Configure::write('YAHOO_ENDPOINT', 'http://where.yahooapis.com/');
			Configure::write('YAHOO_KEY', 'dj0yJmk9djJZUUg4OW1OOTgzJmQ9WVdrOWJWWnBUMnhhTldFbWNHbzlNVFkyTWpJeE5qWTJNZy0tJnM9Y29uc3VtZXJzZWNyZXQmeD0yZg--');
			Configure::write('YAHOO_SECRET', '531eb457fafeb1762c3a7e0fdb2cfec8134d47e2');
	 		break;
	
	 	case 'thewebsandbox.com':
			Configure::write('HOME', 'http://thewebsandbox.com/Trystingtrees');
			Configure::write('FB_APP_ID', '269655156435163');
			Configure::write('FB_APP_SECRET', '158f22d331dd98b1c705e7b3a2d07362');
	 		break;
 	}

	//=========================================== Global config

	//User Roles
	Configure::write('USER_GROUP', 1);
	Configure::write('AGENT_GROUP', 2);
	Configure::write('ADMIN_GROUP', 3);
	Configure::write('SUPERUSER_GROUP', 4);

	//Paths
	Configure::write('PROFILE_PICS_BASE_PATH', APP . 'webroot' . DS . 'files' . DS . 'profile_pictures');
	Configure::write('DEFAULT_PROFILE_PIC_PATH', APP . 'webroot' . DS . 'img' . DS . 'no_img_available.png');
	Configure::write('PROFILE_PICS_BASE_URL', DS . 'files' . DS . 'profile_pictures');
	Configure::write('DEFAULT_PROFILE_PIC_URL', DS . 'img' . DS . 'no_img_available.png');

	//Facebook Login
	Configure::write('FB_FINALIZE_URL', Configure::read('HOME') . '/social_auths/fb_finalize');
	Configure::write('FB_LOGIN', 'https://www.facebook.com/dialog/oauth?client_id=' . Configure::read('FB_APP_ID') . 
		'&redirect_uri=' . Configure::read('HOME') . '/social_auths/fb_finalize&scope=email,publish_stream');
