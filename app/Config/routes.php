<?php
/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different urls to chosen controllers and their actions (functions).
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2011, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2011, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Config
 * @since         CakePHP(tm) v 0.2.9
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
/**
 * Here, we are connecting '/' (base path) to controller called 'Pages',
 * its action called 'display', and we pass a param to select the view file
 * to use (in this case, /app/View/Pages/home.ctp)...
 */
	Router::connect('/', array('controller' => 'websites', 'action' => 'index'));
	Router::connect('/contact', array('controller' => 'websites', 'action' => 'contact'));
	Router::connect('/signup', array('controller' => 'users', 'action' => 'signup'));
	Router::connect('/login', array('controller' => 'users', 'action' => 'login'));
	Router::connect('/logout', array('controller' => 'users', 'action' => 'logout'));
	Router::connect('/buy', array('controller' => 'listings', 'action' => 'buy'));
	Router::connect('/rent', array('controller' => 'listings', 'action' => 'rent'));
	Router::connect('/profile', array('controller' => 'listing_agents', 'action' => 'profile_view'));
	Router::connect('/profile/edit', array('controller' => 'listing_agents', 'action' => 'profile_edit'));
	Router::connect('/agents/view/*', array('controller' => 'listing_agents', 'action' => 'view'));
	Router::connect('/agents/profile', array('controller' => 'listing_agents', 'action' => 'profile'));
	Router::connect('/areas/select', array('controller' => 'listing_agents', 'action' => 'select_area', 'select-area'));
	//Router::connect('/areas', array('controller' => 'listing_agents', 'action' => 'areas', 'areas'));
	//Router::connect('/agent/areas/*', array('controller' => 'agent_areas', 'action' => 'display'));
	//Router::connect('/areas/update', array('controller' => 'listing_agents', 'action' => 'update_areas', 'areas/update'));
	Router::connect('/agent-locator', array('controller' => 'listing_agents', 'action' => 'find'));
	Router::connect('/agent-locator/results/*', array('controller' => 'listing_agents', 'action' => 'find_results'));
	Router::connect('/blog/:username/:year/:month/:day/:title', array('controller' => 'blogs', 'action' => 'view'));
	Router::connect('/my-blog', array('controller' => 'posts', 'action' => 'index'));

	//Twitter
	Router::connect('/twitter/connect', array('controller' => 'social_auths', 'action' => 'twitter_connect'));
	Router::connect('/twitter/authorize', array('controller' => 'social_auths', 'action' => 'twitter_authorize'));

	//ADMIN ROUTING
	Router::connect('/admin/dashboard', array('controller' => 'admin_controls', 'action' => 'dashboard', 'admin/dashboard'));
	Router::connect('/u/:username/*', array('controller' => 'listing_agents', 'action' => 'profile'));

/**
 * ...and connect the rest of 'Pages' controller's urls.
 */
	Router::connect('/pages/*', array('controller' => 'pages', 'action' => 'display'));

/**
 * Load all plugin routes.  See the CakePlugin documentation on 
 * how to customize the loading of plugin routes.
 */
	CakePlugin::routes();

/**
 * Load the CakePHP default routes. Remove this if you do not want to use
 * the built-in default routes.
 */
	require CAKE . 'Config' . DS . 'routes.php';
