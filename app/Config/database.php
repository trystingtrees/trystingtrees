<?php
/**
 * This is core configuration file.
 *
 * Use it to configure core behaviour of Cake.
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2011, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2011, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Config
 * @since         CakePHP(tm) v 0.2.9
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
/**
 * In this file you set up your database connection details.
 *
 * @package       cake.config
 */
/**
 * Database configuration class.
 * You can specify multiple configurations for production, development and testing.
 *
 * driver => The name of a supported driver; valid options are as follows:
 *		Database/Mysql 		- MySQL 4 & 5,
 *		Database/Sqlite		- SQLite (PHP5 only),
 *		Database/Postgres	- PostgreSQL 7 and higher,
 *		Database/Sqlserver	- Microsoft SQL Server 2005 and higher
 *
 * You can add custom database drivers (or override existing drivers) by adding the
 * appropriate file to app/Model/Datasource/Database.  Drivers should be named 'MyDriver.php',
 *
 *
 * persistent => true / false
 * Determines whether or not the database should use a persistent connection
 *
 * host =>
 * the host you connect to the database. To add a socket or port number, use 'port' => #
 *
 * prefix =>
 * Uses the given prefix for all the tables in this database.  This setting can be overridden
 * on a per-table basis with the Model::$tablePrefix property.
 *
 * schema =>
 * For Postgres specifies which schema you would like to use the tables in. Postgres defaults to 'public'.
 *
 * encoding =>
 * For MySQL, Postgres specifies the character encoding to use when connecting to the
 * database. Uses database default not specified.
 *
 * unix_socket =>
 * For MySQL to connect via socket specify the `unix_socket` parameter instead of `host` and `port`
 */
class DATABASE_CONFIG {

	public $production = array(
		'datasource' => 'Database/Mysql',
		'persistent' => false,
		'host' => 'localhost',
		'login' => 'treescom_prod',
		'password' => 'S33;JayN*2ZC',
		'database' => 'treescom_prod',
		'prefix' => '',
		'encoding' => 'utf8',
	);

	public $dev01 = array(
		'datasource' => 'Database/Mysql',
		'persistent' => false,
		'host' => 'localhost',
		'login' => 'treescom_dev01',
		'password' => '_kbRr=T_%=bb',
		'database' => 'treescom_dev01',
		'prefix' => '',
		'encoding' => 'utf8',
	);

	public $dev02 = array(
		'datasource' => 'Database/Mysql',
		'persistent' => false,
		'host' => 'localhost',
		'login' => 'treescom_dev02',
		'password' => 'qTWZ)tssGS@I',
		'database' => 'treescom_dev02',
		'prefix' => '',
		'encoding' => 'utf8',
	);

	public $test01 = array(
		'datasource' => 'Database/Mysql',
		'persistent' => false,
		'host' => 'localhost',
		'login' => 'treescom_test01',
		'password' => 'Dq1#1,%&V^k.',
		'database' => 'treescom_test01',
		'prefix' => '',
		'encoding' => 'utf8',
	);

	public $qa01 = array(
		'datasource' => 'Database/Mysql',
		'persistent' => false,
		'host' => 'localhost',
		'login' => 'treescom_qa01',
		'password' => '6$ZBxTg.Pz4O',
		'database' => 'treescom_qa01',
		'prefix' => '',
		'encoding' => 'utf8',
	);

	public function __construct(){
		//Change DB based on environment
		if (isset($_SERVER) && isset($_SERVER['SERVER_NAME'])){
			// Dev01
			if (strpos($_SERVER['SERVER_NAME'], 'dev01') !== false) $this->default = $this->dev01;

			// Dev02
			if (strpos($_SERVER['SERVER_NAME'], 'dev02') !== false) $this->default = $this->dev02;

			// Test01
			if (strpos($_SERVER['SERVER_NAME'], 'test01') !== false) $this->default = $this->test01;

			// Qa01
			if (strpos($_SERVER['SERVER_NAME'], 'qa01') !== false) $this->default = $this->qa01;

			// Prod
			if (strpos($_SERVER['SERVER_NAME'], 'www.trystingtrees.com') !== false) $this->default = $this->prod;

		}
	}
}
