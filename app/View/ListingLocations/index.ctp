<div class="listingLocations index">
	<h2><?php echo __('Listing Locations');?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id');?></th>
			<th><?php echo $this->Paginator->sort('listing_id');?></th>
			<th><?php echo $this->Paginator->sort('address');?></th>
			<th><?php echo $this->Paginator->sort('city');?></th>
			<th><?php echo $this->Paginator->sort('county');?></th>
			<th><?php echo $this->Paginator->sort('state_id');?></th>
			<th><?php echo $this->Paginator->sort('postal_code');?></th>
			<th><?php echo $this->Paginator->sort('country_id');?></th>
			<th><?php echo $this->Paginator->sort('latitude');?></th>
			<th><?php echo $this->Paginator->sort('longitude');?></th>
			<th><?php echo $this->Paginator->sort('created');?></th>
			<th><?php echo $this->Paginator->sort('modified');?></th>
			<th class="actions"><?php echo __('Actions');?></th>
	</tr>
	<?php
	$i = 0;
	foreach ($listingLocations as $listingLocation): ?>
	<tr>
		<td><?php echo h($listingLocation['ListingLocation']['id']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($listingLocation['Listing']['id'], array('controller' => 'listings', 'action' => 'view', $listingLocation['Listing']['id'])); ?>
		</td>
		<td><?php echo h($listingLocation['ListingLocation']['address']); ?>&nbsp;</td>
		<td><?php echo h($listingLocation['ListingLocation']['city']); ?>&nbsp;</td>
		<td><?php echo h($listingLocation['ListingLocation']['county']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($listingLocation['State']['id'], array('controller' => 'states', 'action' => 'view', $listingLocation['State']['id'])); ?>
		</td>
		<td><?php echo h($listingLocation['ListingLocation']['postal_code']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($listingLocation['Country']['id'], array('controller' => 'countries', 'action' => 'view', $listingLocation['Country']['id'])); ?>
		</td>
		<td><?php echo h($listingLocation['ListingLocation']['latitude']); ?>&nbsp;</td>
		<td><?php echo h($listingLocation['ListingLocation']['longitude']); ?>&nbsp;</td>
		<td><?php echo h($listingLocation['ListingLocation']['created']); ?>&nbsp;</td>
		<td><?php echo h($listingLocation['ListingLocation']['modified']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $listingLocation['ListingLocation']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $listingLocation['ListingLocation']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $listingLocation['ListingLocation']['id']), null, __('Are you sure you want to delete # %s?', $listingLocation['ListingLocation']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>

	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Listing Location'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Listings'), array('controller' => 'listings', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Listing'), array('controller' => 'listings', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List States'), array('controller' => 'states', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New State'), array('controller' => 'states', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Countries'), array('controller' => 'countries', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Country'), array('controller' => 'countries', 'action' => 'add')); ?> </li>
	</ul>
</div>
