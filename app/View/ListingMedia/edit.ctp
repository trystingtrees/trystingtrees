<div class="listingMedia form">
<?php echo $this->Form->create('ListingMedia');?>
	<fieldset>
		<legend><?php echo __('Edit Listing Media'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('listing_id');
		echo $this->Form->input('mime_type');
		echo $this->Form->input('medium');
		echo $this->Form->input('src_media_uid');
		echo $this->Form->input('caption');
		echo $this->Form->input('media_type_id');
		echo $this->Form->input('media_url');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit'));?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('ListingMedia.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('ListingMedia.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Listing Media'), array('action' => 'index'));?></li>
		<li><?php echo $this->Html->link(__('List Listings'), array('controller' => 'listings', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Listing'), array('controller' => 'listings', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Media Types'), array('controller' => 'media_types', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Media Type'), array('controller' => 'media_types', 'action' => 'add')); ?> </li>
	</ul>
</div>
