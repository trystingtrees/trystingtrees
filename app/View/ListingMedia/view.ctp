<div class="listingMedia view">
<h2><?php  echo __('Listing Media');?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($listingMedia['ListingMedia']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Listing'); ?></dt>
		<dd>
			<?php echo $this->Html->link($listingMedia['Listing']['id'], array('controller' => 'listings', 'action' => 'view', $listingMedia['Listing']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Mime Type'); ?></dt>
		<dd>
			<?php echo h($listingMedia['ListingMedia']['mime_type']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Medium'); ?></dt>
		<dd>
			<?php echo h($listingMedia['ListingMedia']['medium']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Src Media Uid'); ?></dt>
		<dd>
			<?php echo h($listingMedia['ListingMedia']['src_media_uid']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Caption'); ?></dt>
		<dd>
			<?php echo h($listingMedia['ListingMedia']['caption']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Media Type'); ?></dt>
		<dd>
			<?php echo $this->Html->link($listingMedia['MediaType']['name'], array('controller' => 'media_types', 'action' => 'view', $listingMedia['MediaType']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Media Url'); ?></dt>
		<dd>
			<?php echo h($listingMedia['ListingMedia']['media_url']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($listingMedia['ListingMedia']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($listingMedia['ListingMedia']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Listing Media'), array('action' => 'edit', $listingMedia['ListingMedia']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Listing Media'), array('action' => 'delete', $listingMedia['ListingMedia']['id']), null, __('Are you sure you want to delete # %s?', $listingMedia['ListingMedia']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Listing Media'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Listing Media'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Listings'), array('controller' => 'listings', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Listing'), array('controller' => 'listings', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Media Types'), array('controller' => 'media_types', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Media Type'), array('controller' => 'media_types', 'action' => 'add')); ?> </li>
	</ul>
</div>
