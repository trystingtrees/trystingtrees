<div class="listingDetails form">
<?php echo $this->Form->create('ListingDetail');?>
	<fieldset>
		<legend><?php echo __('Edit Listing Detail'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('listing_id');
		echo $this->Form->input('title');
		echo $this->Form->input('description');
		echo $this->Form->input('detail_view_url');
		echo $this->Form->input('virtual_tour_url');
		echo $this->Form->input('broker_name');
		echo $this->Form->input('property_type_id');
		echo $this->Form->input('property_style_id');
		echo $this->Form->input('lot_legal_name');
		echo $this->Form->input('lot_comment');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit'));?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('ListingDetail.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('ListingDetail.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Listing Details'), array('action' => 'index'));?></li>
		<li><?php echo $this->Html->link(__('List Listings'), array('controller' => 'listings', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Listing'), array('controller' => 'listings', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Property Types'), array('controller' => 'property_types', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Property Type'), array('controller' => 'property_types', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Property Styles'), array('controller' => 'property_styles', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Property Style'), array('controller' => 'property_styles', 'action' => 'add')); ?> </li>
	</ul>
</div>
