<div class="listingDetails view">
<h2><?php  echo __('Listing Detail');?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($listingDetail['ListingDetail']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Listing'); ?></dt>
		<dd>
			<?php echo $this->Html->link($listingDetail['Listing']['id'], array('controller' => 'listings', 'action' => 'view', $listingDetail['Listing']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Title'); ?></dt>
		<dd>
			<?php echo h($listingDetail['ListingDetail']['title']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Description'); ?></dt>
		<dd>
			<?php echo h($listingDetail['ListingDetail']['description']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Detail View Url'); ?></dt>
		<dd>
			<?php echo h($listingDetail['ListingDetail']['detail_view_url']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Virtual Tour Url'); ?></dt>
		<dd>
			<?php echo h($listingDetail['ListingDetail']['virtual_tour_url']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Broker Name'); ?></dt>
		<dd>
			<?php echo h($listingDetail['ListingDetail']['broker_name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Property Type'); ?></dt>
		<dd>
			<?php echo $this->Html->link($listingDetail['PropertyType']['name'], array('controller' => 'property_types', 'action' => 'view', $listingDetail['PropertyType']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Property Style'); ?></dt>
		<dd>
			<?php echo $this->Html->link($listingDetail['PropertyStyle']['name'], array('controller' => 'property_styles', 'action' => 'view', $listingDetail['PropertyStyle']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Lot Legal Name'); ?></dt>
		<dd>
			<?php echo h($listingDetail['ListingDetail']['lot_legal_name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Lot Comment'); ?></dt>
		<dd>
			<?php echo h($listingDetail['ListingDetail']['lot_comment']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($listingDetail['ListingDetail']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($listingDetail['ListingDetail']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Listing Detail'), array('action' => 'edit', $listingDetail['ListingDetail']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Listing Detail'), array('action' => 'delete', $listingDetail['ListingDetail']['id']), null, __('Are you sure you want to delete # %s?', $listingDetail['ListingDetail']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Listing Details'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Listing Detail'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Listings'), array('controller' => 'listings', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Listing'), array('controller' => 'listings', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Property Types'), array('controller' => 'property_types', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Property Type'), array('controller' => 'property_types', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Property Styles'), array('controller' => 'property_styles', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Property Style'), array('controller' => 'property_styles', 'action' => 'add')); ?> </li>
	</ul>
</div>
