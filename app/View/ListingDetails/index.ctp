<div class="listingDetails index">
	<h2><?php echo __('Listing Details');?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id');?></th>
			<th><?php echo $this->Paginator->sort('listing_id');?></th>
			<th><?php echo $this->Paginator->sort('title');?></th>
			<th><?php echo $this->Paginator->sort('description');?></th>
			<th><?php echo $this->Paginator->sort('detail_view_url');?></th>
			<th><?php echo $this->Paginator->sort('virtual_tour_url');?></th>
			<th><?php echo $this->Paginator->sort('broker_name');?></th>
			<th><?php echo $this->Paginator->sort('property_type_id');?></th>
			<th><?php echo $this->Paginator->sort('property_style_id');?></th>
			<th><?php echo $this->Paginator->sort('lot_legal_name');?></th>
			<th><?php echo $this->Paginator->sort('lot_comment');?></th>
			<th><?php echo $this->Paginator->sort('created');?></th>
			<th><?php echo $this->Paginator->sort('modified');?></th>
			<th class="actions"><?php echo __('Actions');?></th>
	</tr>
	<?php
	$i = 0;
	foreach ($listingDetails as $listingDetail): ?>
	<tr>
		<td><?php echo h($listingDetail['ListingDetail']['id']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($listingDetail['Listing']['id'], array('controller' => 'listings', 'action' => 'view', $listingDetail['Listing']['id'])); ?>
		</td>
		<td><?php echo h($listingDetail['ListingDetail']['title']); ?>&nbsp;</td>
		<td><?php echo h($listingDetail['ListingDetail']['description']); ?>&nbsp;</td>
		<td><?php echo h($listingDetail['ListingDetail']['detail_view_url']); ?>&nbsp;</td>
		<td><?php echo h($listingDetail['ListingDetail']['virtual_tour_url']); ?>&nbsp;</td>
		<td><?php echo h($listingDetail['ListingDetail']['broker_name']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($listingDetail['PropertyType']['name'], array('controller' => 'property_types', 'action' => 'view', $listingDetail['PropertyType']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($listingDetail['PropertyStyle']['name'], array('controller' => 'property_styles', 'action' => 'view', $listingDetail['PropertyStyle']['id'])); ?>
		</td>
		<td><?php echo h($listingDetail['ListingDetail']['lot_legal_name']); ?>&nbsp;</td>
		<td><?php echo h($listingDetail['ListingDetail']['lot_comment']); ?>&nbsp;</td>
		<td><?php echo h($listingDetail['ListingDetail']['created']); ?>&nbsp;</td>
		<td><?php echo h($listingDetail['ListingDetail']['modified']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $listingDetail['ListingDetail']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $listingDetail['ListingDetail']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $listingDetail['ListingDetail']['id']), null, __('Are you sure you want to delete # %s?', $listingDetail['ListingDetail']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>

	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Listing Detail'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Listings'), array('controller' => 'listings', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Listing'), array('controller' => 'listings', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Property Types'), array('controller' => 'property_types', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Property Type'), array('controller' => 'property_types', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Property Styles'), array('controller' => 'property_styles', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Property Style'), array('controller' => 'property_styles', 'action' => 'add')); ?> </li>
	</ul>
</div>
