<div class="listingStatuses form">
<?php echo $this->Form->create('ListingStatus');?>
	<fieldset>
		<legend><?php echo __('Edit Listing Status'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('name');
		echo $this->Form->input('active');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit'));?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('ListingStatus.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('ListingStatus.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Listing Statuses'), array('action' => 'index'));?></li>
		<li><?php echo $this->Html->link(__('List Listings'), array('controller' => 'listings', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Listing'), array('controller' => 'listings', 'action' => 'add')); ?> </li>
	</ul>
</div>
