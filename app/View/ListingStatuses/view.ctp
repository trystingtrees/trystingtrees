<div class="listingStatuses view">
<h2><?php  echo __('Listing Status');?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($listingStatus['ListingStatus']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($listingStatus['ListingStatus']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Active'); ?></dt>
		<dd>
			<?php echo h($listingStatus['ListingStatus']['active']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($listingStatus['ListingStatus']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($listingStatus['ListingStatus']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Listing Status'), array('action' => 'edit', $listingStatus['ListingStatus']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Listing Status'), array('action' => 'delete', $listingStatus['ListingStatus']['id']), null, __('Are you sure you want to delete # %s?', $listingStatus['ListingStatus']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Listing Statuses'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Listing Status'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Listings'), array('controller' => 'listings', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Listing'), array('controller' => 'listings', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Listings');?></h3>
	<?php if (!empty($listingStatus['Listing'])):?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Uuid'); ?></th>
		<th><?php echo __('Source Id'); ?></th>
		<th><?php echo __('Listing Status Id'); ?></th>
		<th><?php echo __('Listing Category'); ?></th>
		<th><?php echo __('Listing Subcategory'); ?></th>
		<th><?php echo __('User Id'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th class="actions"><?php echo __('Actions');?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($listingStatus['Listing'] as $listing): ?>
		<tr>
			<td><?php echo $listing['id'];?></td>
			<td><?php echo $listing['uuid'];?></td>
			<td><?php echo $listing['source_id'];?></td>
			<td><?php echo $listing['listing_status_id'];?></td>
			<td><?php echo $listing['listing_category'];?></td>
			<td><?php echo $listing['listing_subcategory'];?></td>
			<td><?php echo $listing['user_id'];?></td>
			<td><?php echo $listing['created'];?></td>
			<td><?php echo $listing['modified'];?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'listings', 'action' => 'view', $listing['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'listings', 'action' => 'edit', $listing['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'listings', 'action' => 'delete', $listing['id']), null, __('Are you sure you want to delete # %s?', $listing['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Listing'), array('controller' => 'listings', 'action' => 'add'));?> </li>
		</ul>
	</div>
</div>
