<div class="agentMedia view">
<h2><?php  echo __('Agent Media');?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($agentMedia['AgentMedia']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Listing Agent'); ?></dt>
		<dd>
			<?php echo $this->Html->link($agentMedia['ListingAgent']['name'], array('controller' => 'listing_agents', 'action' => 'view', $agentMedia['ListingAgent']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Mime Type'); ?></dt>
		<dd>
			<?php echo h($agentMedia['AgentMedia']['mime_type']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Medium'); ?></dt>
		<dd>
			<?php echo h($agentMedia['AgentMedia']['medium']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Src Media Uid'); ?></dt>
		<dd>
			<?php echo h($agentMedia['AgentMedia']['src_media_uid']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Caption'); ?></dt>
		<dd>
			<?php echo h($agentMedia['AgentMedia']['caption']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Media Type'); ?></dt>
		<dd>
			<?php echo $this->Html->link($agentMedia['MediaType']['name'], array('controller' => 'media_types', 'action' => 'view', $agentMedia['MediaType']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Media Url'); ?></dt>
		<dd>
			<?php echo h($agentMedia['AgentMedia']['media_url']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($agentMedia['AgentMedia']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($agentMedia['AgentMedia']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Agent Media'), array('action' => 'edit', $agentMedia['AgentMedia']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Agent Media'), array('action' => 'delete', $agentMedia['AgentMedia']['id']), null, __('Are you sure you want to delete # %s?', $agentMedia['AgentMedia']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Agent Media'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Agent Media'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Listing Agents'), array('controller' => 'listing_agents', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Listing Agent'), array('controller' => 'listing_agents', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Media Types'), array('controller' => 'media_types', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Media Type'), array('controller' => 'media_types', 'action' => 'add')); ?> </li>
	</ul>
</div>
