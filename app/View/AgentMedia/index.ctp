<div class="agentMedia index">
	<h2><?php echo __('Agent Media');?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id');?></th>
			<th><?php echo $this->Paginator->sort('listing_agent_id');?></th>
			<th><?php echo $this->Paginator->sort('mime_type');?></th>
			<th><?php echo $this->Paginator->sort('medium');?></th>
			<th><?php echo $this->Paginator->sort('src_media_uid');?></th>
			<th><?php echo $this->Paginator->sort('caption');?></th>
			<th><?php echo $this->Paginator->sort('media_type_id');?></th>
			<th><?php echo $this->Paginator->sort('media_url');?></th>
			<th><?php echo $this->Paginator->sort('created');?></th>
			<th><?php echo $this->Paginator->sort('modified');?></th>
			<th class="actions"><?php echo __('Actions');?></th>
	</tr>
	<?php
	$i = 0;
	foreach ($agentMedia as $agentMedia): ?>
	<tr>
		<td><?php echo h($agentMedia['AgentMedia']['id']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($agentMedia['ListingAgent']['name'], array('controller' => 'listing_agents', 'action' => 'view', $agentMedia['ListingAgent']['id'])); ?>
		</td>
		<td><?php echo h($agentMedia['AgentMedia']['mime_type']); ?>&nbsp;</td>
		<td><?php echo h($agentMedia['AgentMedia']['medium']); ?>&nbsp;</td>
		<td><?php echo h($agentMedia['AgentMedia']['src_media_uid']); ?>&nbsp;</td>
		<td><?php echo h($agentMedia['AgentMedia']['caption']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($agentMedia['MediaType']['name'], array('controller' => 'media_types', 'action' => 'view', $agentMedia['MediaType']['id'])); ?>
		</td>
		<td><?php echo h($agentMedia['AgentMedia']['media_url']); ?>&nbsp;</td>
		<td><?php echo h($agentMedia['AgentMedia']['created']); ?>&nbsp;</td>
		<td><?php echo h($agentMedia['AgentMedia']['modified']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $agentMedia['AgentMedia']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $agentMedia['AgentMedia']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $agentMedia['AgentMedia']['id']), null, __('Are you sure you want to delete # %s?', $agentMedia['AgentMedia']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>

	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Agent Media'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Listing Agents'), array('controller' => 'listing_agents', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Listing Agent'), array('controller' => 'listing_agents', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Media Types'), array('controller' => 'media_types', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Media Type'), array('controller' => 'media_types', 'action' => 'add')); ?> </li>
	</ul>
</div>
