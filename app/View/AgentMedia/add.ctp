<div class="agentMedia form">
<?php echo $this->Form->create('AgentMedia');?>
	<fieldset>
		<legend><?php echo __('Add Agent Media'); ?></legend>
	<?php
		echo $this->Form->input('listing_agent_id');
		echo $this->Form->input('mime_type');
		echo $this->Form->input('medium');
		echo $this->Form->input('src_media_uid');
		echo $this->Form->input('caption');
		echo $this->Form->input('media_type_id');
		echo $this->Form->input('media_url');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit'));?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Agent Media'), array('action' => 'index'));?></li>
		<li><?php echo $this->Html->link(__('List Listing Agents'), array('controller' => 'listing_agents', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Listing Agent'), array('controller' => 'listing_agents', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Media Types'), array('controller' => 'media_types', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Media Type'), array('controller' => 'media_types', 'action' => 'add')); ?> </li>
	</ul>
</div>
