<h1>Selected maps</h1>

<?php echo $this->Html->link('Add an area', array('action' => 'select')); ?>

<br />
<br />
<table class="list-table">
    <tr>
        <th>Area Name</th>
        <th>Selected On</th>
        <th>Actions</th>
    </tr>

    <!-- Here is where we loop through our $posts array, printing out post info -->

    <?php foreach ($maps as $map): ?>
    <tr>
        <td><?php echo $map['Map']['name']; ?></td>
        <td><?php echo substr($map['Map']['created'], 0, 11); ?></td>
        <td>
            <?php echo $this->Html->link('View', '/maps/view/' . $map['Map']['id']); ?> | 
            <?php echo $this->Html->link('Remove', array('controller' => 'areas', 'action' => 'agent_unselect', $map['Map']['id'])); ?>
        </td>
    </tr>
    <?php endforeach; ?>

</table>
