<?php 
	$area_ids = '';
?>

<div class="left dashboard-main">
	<h2>Select Area</h2>

	<fieldset>
		<legend><strong>Selected Areas</strong></legend>
		<div id="selected_areas">
			<?php if (empty($selected_areas)){ ?>
			<span class="name">No area selected</span>
			<?php }else{ 
				foreach($selected_areas as $area){
					echo '<span class="mame">' . $area['name'] . '</span>';
					$area_ids .= $area['map_id'] . ',';
				}
			?>
			<?php } ?>
		</div>
		<input type="hidden" id="selected_area_ids" value="<?php echo rtrim($area_ids, ','); ?>" />
	</fieldset>

	<br />
	<button id="save_selected_areas" style="padding: 10px;" onclick="saveSelectedAreas();">Update My Areas</button>
	<br />
	<br />

	<div id="map_canvas" style"float: left;">
		<div id="map" style="width: 900px; height: 500px;">Loading Map</div>
		<?php foreach($maps as $map): ?>
		<input type="hidden" class="map_coordinates" id="map_<?php echo $map['Map']['id']; ?>" value="<?php echo $map['Map']['coordinates']; ?>">
		<input type="hidden" class="map_names" id="name_<?php echo $map['Map']['id']; ?>" value="<?php echo $map['Map']['nice_name']; ?>">
		<?php endforeach; ?>
	</div>
</div>
