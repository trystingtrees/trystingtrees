<?php echo $this->Html->script('agentPosts'); ?>

<div class="left dashboard-main">
	<br />
	<h2>Agent Blog - Manage Posts</h2>

	<table class="list-table">
		<thead>
			<tr>
				<th>Post Title</th>
				<th>Views</th>
				<th>Comments</th>
				<th>Share</th>
				<th>Actions</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach($posts as $post): 
				$title = $post['Post']['title'];
				$date = substr($post['Post']['created'], 0, 11);
				$date = str_replace('-', '/', $date);
				$date = trim($date);
			?>
			<tr>
				<td><?php echo $post['Post']['title']; ?></td>
				<td>n/a</td>
				<td>n/a</td>
				<td class="share-buttons">
					<!-- Twitter -->
					<a href="#">
					<img src="<?php echo $HOME; ?>/img/icons/small/twitter2.png" />
					</a>
					<!-- Facebook -->
					<a href="#">
					<img src="<?php echo $HOME; ?>/img/icons/small/facebook.png" />
					</a>
					<!-- Linkedin -->
					<a href="#">
					<img src="<?php echo $HOME; ?>/img/icons/small/linkedin.png" />
					</a>
				</td>
				<td>
					<a href="#">Edit</a> | 
					<a href="<?php echo $HOME; ?>/blog/<?php echo $username . '/' . $date . '/' . $post['Post']['url']; ?>" target="_blank">View</a> | 
				<a href="#">Delete</a> | 
				<?php if ($post['Post']['is_published']){ ?>
					<a href="#" class="unpublish_agent_post" id="post_<?php echo $post['Post']['id']; ?>">Unpublish</a> | 
				<?php }else{ ?>
					<a href="#" class="publish_agent_post" id="post_<?php echo $post['Post']['id']; ?>">Publish</a> | 
				<?php } ?>
				<a href="#">View Comments</a> 
			</td>
		</tr>
		<?php endforeach; ?>
		</tbody>
	</table>
</div>
