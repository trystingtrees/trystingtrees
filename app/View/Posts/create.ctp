<div class="left dashboard-main">
	<br />
	<h2>Agent Blog - New Post</h2>

	<?php
		echo $this->Form->create('Post', array('action' => 'add'));
		echo '<div class="form-row">';
		echo $this->Form->input('title', array('size' => '50', 'maxlength' => '50'));
		echo "<small><span id='PostTitleCharCount'>0</span> / 50 characters left</small>";
		echo '</div>';
		echo '<div class="form-row">';
		echo $this->Form->label('Post Body');
		echo $this->Form->textarea('body', array('cols' => '70', 'rows' => '10'));
		echo '</div>';
		echo '<div class="form-row">';
		echo $this->Form->input('tags', array('size' => '70', 'maxlength' => '255'));
		echo '</div>';
		echo '<div class="form-row">';
		echo $this->Form->hidden('listing_agent_id', array('value' => $uid));
		echo $this->Form->button('Save Post', array('class' => 'form-button'));
		echo '</div>';
		echo $this->Form->end();
	?>
</div>
