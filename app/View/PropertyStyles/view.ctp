<div class="propertyStyles view">
<h2><?php  echo __('Property Style');?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($propertyStyle['PropertyStyle']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($propertyStyle['PropertyStyle']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Active'); ?></dt>
		<dd>
			<?php echo h($propertyStyle['PropertyStyle']['active']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($propertyStyle['PropertyStyle']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($propertyStyle['PropertyStyle']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Property Style'), array('action' => 'edit', $propertyStyle['PropertyStyle']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Property Style'), array('action' => 'delete', $propertyStyle['PropertyStyle']['id']), null, __('Are you sure you want to delete # %s?', $propertyStyle['PropertyStyle']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Property Styles'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Property Style'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Listing Details'), array('controller' => 'listing_details', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Listing Detail'), array('controller' => 'listing_details', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Listing Details');?></h3>
	<?php if (!empty($propertyStyle['ListingDetail'])):?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Listing Id'); ?></th>
		<th><?php echo __('Title'); ?></th>
		<th><?php echo __('Description'); ?></th>
		<th><?php echo __('Detail View Url'); ?></th>
		<th><?php echo __('Virtual Tour Url'); ?></th>
		<th><?php echo __('Broker Name'); ?></th>
		<th><?php echo __('Property Type Id'); ?></th>
		<th><?php echo __('Property Style Id'); ?></th>
		<th><?php echo __('Lot Legal Name'); ?></th>
		<th><?php echo __('Lot Comment'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th class="actions"><?php echo __('Actions');?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($propertyStyle['ListingDetail'] as $listingDetail): ?>
		<tr>
			<td><?php echo $listingDetail['id'];?></td>
			<td><?php echo $listingDetail['listing_id'];?></td>
			<td><?php echo $listingDetail['title'];?></td>
			<td><?php echo $listingDetail['description'];?></td>
			<td><?php echo $listingDetail['detail_view_url'];?></td>
			<td><?php echo $listingDetail['virtual_tour_url'];?></td>
			<td><?php echo $listingDetail['broker_name'];?></td>
			<td><?php echo $listingDetail['property_type_id'];?></td>
			<td><?php echo $listingDetail['property_style_id'];?></td>
			<td><?php echo $listingDetail['lot_legal_name'];?></td>
			<td><?php echo $listingDetail['lot_comment'];?></td>
			<td><?php echo $listingDetail['created'];?></td>
			<td><?php echo $listingDetail['modified'];?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'listing_details', 'action' => 'view', $listingDetail['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'listing_details', 'action' => 'edit', $listingDetail['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'listing_details', 'action' => 'delete', $listingDetail['id']), null, __('Are you sure you want to delete # %s?', $listingDetail['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Listing Detail'), array('controller' => 'listing_details', 'action' => 'add'));?> </li>
		</ul>
	</div>
</div>
