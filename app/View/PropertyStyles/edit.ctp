<div class="propertyStyles form">
<?php echo $this->Form->create('PropertyStyle');?>
	<fieldset>
		<legend><?php echo __('Edit Property Style'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('name');
		echo $this->Form->input('active');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit'));?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('PropertyStyle.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('PropertyStyle.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Property Styles'), array('action' => 'index'));?></li>
		<li><?php echo $this->Html->link(__('List Listing Details'), array('controller' => 'listing_details', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Listing Detail'), array('controller' => 'listing_details', 'action' => 'add')); ?> </li>
	</ul>
</div>
