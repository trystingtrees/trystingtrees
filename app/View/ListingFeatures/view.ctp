<div class="listingFeatures view">
<h2><?php  echo __('Listing Feature');?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($listingFeature['ListingFeature']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Listing'); ?></dt>
		<dd>
			<?php echo $this->Html->link($listingFeature['Listing']['id'], array('controller' => 'listings', 'action' => 'view', $listingFeature['Listing']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Year Built'); ?></dt>
		<dd>
			<?php echo h($listingFeature['ListingFeature']['year_built']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Bathrooms'); ?></dt>
		<dd>
			<?php echo h($listingFeature['ListingFeature']['bathrooms']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Bedrooms'); ?></dt>
		<dd>
			<?php echo h($listingFeature['ListingFeature']['bedrooms']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Garage Stalls'); ?></dt>
		<dd>
			<?php echo h($listingFeature['ListingFeature']['garage_stalls']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Garage Style'); ?></dt>
		<dd>
			<?php echo h($listingFeature['ListingFeature']['garage_style']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Garage Comments'); ?></dt>
		<dd>
			<?php echo h($listingFeature['ListingFeature']['garage_comments']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Living Area'); ?></dt>
		<dd>
			<?php echo h($listingFeature['ListingFeature']['living_area']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Living Area Units'); ?></dt>
		<dd>
			<?php echo h($listingFeature['ListingFeature']['living_area_units']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Other Features'); ?></dt>
		<dd>
			<?php echo h($listingFeature['ListingFeature']['other_features']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($listingFeature['ListingFeature']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($listingFeature['ListingFeature']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Listing Feature'), array('action' => 'edit', $listingFeature['ListingFeature']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Listing Feature'), array('action' => 'delete', $listingFeature['ListingFeature']['id']), null, __('Are you sure you want to delete # %s?', $listingFeature['ListingFeature']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Listing Features'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Listing Feature'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Listings'), array('controller' => 'listings', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Listing'), array('controller' => 'listings', 'action' => 'add')); ?> </li>
	</ul>
</div>
