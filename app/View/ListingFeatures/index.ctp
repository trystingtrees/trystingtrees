<div class="listingFeatures index">
	<h2><?php echo __('Listing Features');?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id');?></th>
			<th><?php echo $this->Paginator->sort('listing_id');?></th>
			<th><?php echo $this->Paginator->sort('year_built');?></th>
			<th><?php echo $this->Paginator->sort('bathrooms');?></th>
			<th><?php echo $this->Paginator->sort('bedrooms');?></th>
			<th><?php echo $this->Paginator->sort('garage_stalls');?></th>
			<th><?php echo $this->Paginator->sort('garage_style');?></th>
			<th><?php echo $this->Paginator->sort('garage_comments');?></th>
			<th><?php echo $this->Paginator->sort('living_area');?></th>
			<th><?php echo $this->Paginator->sort('living_area_units');?></th>
			<th><?php echo $this->Paginator->sort('other_features');?></th>
			<th><?php echo $this->Paginator->sort('created');?></th>
			<th><?php echo $this->Paginator->sort('modified');?></th>
			<th class="actions"><?php echo __('Actions');?></th>
	</tr>
	<?php
	$i = 0;
	foreach ($listingFeatures as $listingFeature): ?>
	<tr>
		<td><?php echo h($listingFeature['ListingFeature']['id']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($listingFeature['Listing']['id'], array('controller' => 'listings', 'action' => 'view', $listingFeature['Listing']['id'])); ?>
		</td>
		<td><?php echo h($listingFeature['ListingFeature']['year_built']); ?>&nbsp;</td>
		<td><?php echo h($listingFeature['ListingFeature']['bathrooms']); ?>&nbsp;</td>
		<td><?php echo h($listingFeature['ListingFeature']['bedrooms']); ?>&nbsp;</td>
		<td><?php echo h($listingFeature['ListingFeature']['garage_stalls']); ?>&nbsp;</td>
		<td><?php echo h($listingFeature['ListingFeature']['garage_style']); ?>&nbsp;</td>
		<td><?php echo h($listingFeature['ListingFeature']['garage_comments']); ?>&nbsp;</td>
		<td><?php echo h($listingFeature['ListingFeature']['living_area']); ?>&nbsp;</td>
		<td><?php echo h($listingFeature['ListingFeature']['living_area_units']); ?>&nbsp;</td>
		<td><?php echo h($listingFeature['ListingFeature']['other_features']); ?>&nbsp;</td>
		<td><?php echo h($listingFeature['ListingFeature']['created']); ?>&nbsp;</td>
		<td><?php echo h($listingFeature['ListingFeature']['modified']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $listingFeature['ListingFeature']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $listingFeature['ListingFeature']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $listingFeature['ListingFeature']['id']), null, __('Are you sure you want to delete # %s?', $listingFeature['ListingFeature']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>

	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Listing Feature'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Listings'), array('controller' => 'listings', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Listing'), array('controller' => 'listings', 'action' => 'add')); ?> </li>
	</ul>
</div>
