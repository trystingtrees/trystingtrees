<div class="listingFeatures form">
<?php echo $this->Form->create('ListingFeature');?>
	<fieldset>
		<legend><?php echo __('Edit Listing Feature'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('listing_id');
		echo $this->Form->input('year_built');
		echo $this->Form->input('bathrooms');
		echo $this->Form->input('bedrooms');
		echo $this->Form->input('garage_stalls');
		echo $this->Form->input('garage_style');
		echo $this->Form->input('garage_comments');
		echo $this->Form->input('living_area');
		echo $this->Form->input('living_area_units');
		echo $this->Form->input('other_features');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit'));?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('ListingFeature.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('ListingFeature.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Listing Features'), array('action' => 'index'));?></li>
		<li><?php echo $this->Html->link(__('List Listings'), array('controller' => 'listings', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Listing'), array('controller' => 'listings', 'action' => 'add')); ?> </li>
	</ul>
</div>
