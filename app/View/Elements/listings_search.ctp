<form id="advanced_search" name="property-search" action="<?php echo $HOME; ?>/listings/search" method="post">
	<div class="listings-search-area">
		<div class="bottom-row">
			<input type="text" class="span5" placeholder="Enter a Street, Neighbourhood or City" id="ct_location" name="ct_location" />

			<div class="left">
				<label for="ct_status">Type</label>
				<div class="clear"></div>
				<select class="span3" id="ct_property_type" name="ct_property_type">
					<option value="0">Any</option>
					<option value="single">Single Family Home</option>
					<option value="condo">Condo / Townhome</option>
					<option value="land">Land</option>
				</select>
			</div>

			<div class="left">
				<label for="ct_status">Status</label>
				<div class="clear"></div>
				<select id="ct_status" name="ct_status">
					<option value="0">Any</option>
					<!--
					<option value="active">Active</option>
					-->
					<option value="featured">Featured</option>
					<option value="for-rent">For Rent</option>
					<option value="for-sale">For Sale</option>
					<!--
					<option value="open-house">Open House</option>
					<option value="reduced">Reduced</option>
					<option value="rental">Rental</option>
					<option value="sold">Sold</option>
					-->
				</select>
			</div>
		</div><!-- End top row -->
		<div class="clear"></div>

		<div class="row bottom-row">
			<div class="left">
				<label for="ct_beds">Beds</label>
				<div class="clear"></div>
				<select id="ct_beds" name="ct_beds">
					<option value="0">Any</option>
					<option value="2">2</option>
					<option value="3">3</option>
					<option value="4">4+</option>
				</select>
			</div>
		
			<div class="left">	
				<label for="ct_baths">Baths</label>
				<div class="clear"></div>
				<select id="ct_baths" name="ct_baths">
					<option value="0">Any</option>
					<option value="1-5">1.5</option>
					<option value="2">2</option>
					<option value="2-5">2.5</option>
					<option value="3">3+</option>
				</select>
			</div>

			<div class="left">
				<label for="ct_price_from" style="width: auto;">Price From ($)</label>
				<div class="clear"></div>
				<div class="input-prepend">
					<span class="add-on">$</span>
					<input type="text" id="ct_price_from" class="number span2" name="ct_price_from" size="8" />
				</div>
			</div>

			<div class="left">
				<label for="ct_price_to" style="width: auto;">Price To ($)</label>
				<div class="clear"></div>
				<div class="input-prepend">
					<span class="add-on">$</span>
					<input type="text" id="ct_price_to" class="number span2" name="ct_price_to" size="8" />
				</div>
			</div>

			<div class="left">
				<label for="ct_mls">MLS #</label>
				<div class="clear"></div>
				<div class="input-prepend">
					<span class="add-on">#</span>
					<input type="text" id="ct_mls" name="ct_mls" size="12" class="span2" />
				</div>
			</div>

		</div><!-- End bottom-row -->

		<div class="row">
			<div class="right" style="margin-top: 10px;">
				<button type="submit" class="btn large primary">Search Listings</button>
			</div>
		</div>

	</div><!-- End listings-search-area -->
</form>
