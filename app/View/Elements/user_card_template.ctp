<div class="float-r">
<diV id="user_card" class="right" style="position: relative;">
	<ul>
		<li class="trigger">
			<a href="#">
				<?php 
					echo $user['username']; 
				?>
				&#9660;
			</a>
			<ul class="user_card_menu">
				<?php 
					switch($user['group_id']){
						case 1: //Users
							echo $this->Element('dashboard/users/card_menu', array('HOME' => $HOME));
							break;
						case 2: //Agents
							echo $this->Element('dashboard/agents/card_menu', array('HOME' => $HOME));
							break;
						case 3: //Admins
							break;
							echo $this->Element('dashboard/admins/card_menu', array('HOME' => $HOME));
					}
				?>
			</ul>
		</li>
	</ul>
</div>
</div>
