<nav class="left">
  <div id="nav" class="left">
      <ul id="menu-nav" class="menu">
        <li id="menu-item-1" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-1">
			 <?php echo $this->Html->link('My Dashboard', '/'); ?>
        </li>
        <li id="menu-item-2" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-2">
			 <?php echo $this->Html->link('My Listings', '#'); ?>
			 <ul class="sub-menu">
			 	<li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="<?php echo $HOME; ?>/listings/create">Create New Listing</a></li>
			 	<li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="<?php echo $HOME; ?>/listings/manage">Manage Listings</a></li>
			 </ul>
        </li>
        <li id="menu-item-2" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-2">
			 <?php echo $this->Html->link('My Blog', '#'); ?>
			 <ul class="sub-menu">
			 	<li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="<?php echo $HOME; ?>/posts/create">Create New Post</a></li>
			 	<li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="<?php echo $HOME; ?>/posts/manage">Manage Posts</a></li>
			 </ul>
        </li>
        <li id="menu-item-3" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-3">
			 <?php echo $this->Html->link('Q & A', '/rent'); ?>
        </li>
        <li id="menu-item-4" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-4">
			 <?php echo $this->Html->link('My Profile', $HOME . '/agents/profile'); ?>
        </li>
        <li id="menu-item-5" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-5 last">
			 <?php echo $this->Html->link('My Settings', '/contact'); ?>
        </li>
    </ul>
  </div>
</nav>
<div class="clear"></div>
