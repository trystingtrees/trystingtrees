<?php
	//OAuth
	$onclick = '';
	if ($fb_user){
		$onclick = 'FB.Logout(function(response){window.location = "/users/logout";});';
	}
?>

<li><?php echo $this->Html->link('My Blog', '/my-blog'); ?></li>
<li><?php echo $this->Html->link('My Areas', '/agent/areas'); ?></li>
<li class="divider"></li>
<li><?php echo $this->Html->link('My Q&A', '/answers'); ?></li>
<li><?php echo $this->Html->link('My Recommendations', '/recommendations'); ?></li>
<li class="divider"></li>
<li><?php echo $this->Html->link('Profile', '/profile'); ?></li>
<li><?php echo $this->Html->link('Notifications', '/notifications'); ?></li>
<li class="divider"></li>
<li><?php echo $this->Html->link('Help', '/help'); ?></li>
<?php
	if ($fb_user){
		echo '<li>' . $this->Facebook->logout(array('redirect' => '/users/logout', 'label' => 'Logout')) . '</li>';
	}else{
	?>
		<li><?php echo $this->Html->link('Logout', '/logout'); ?></li>
	<?php
	}
?>
