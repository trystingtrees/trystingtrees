<li><?php echo $this->Html->link('Activity Stream', '/activity-stream'); ?></li>
<li><?php echo $this->Html->link('My Agents', '/my/agents'); ?></li>
<li><?php echo $this->Html->link('My Contacts', '/my/contacts'); ?></li>
<li class="divider"></li>
<li><?php echo $this->Html->link('My Q&A', '/my/questions'); ?></li>
<li><?php echo $this->Html->link('My Searches', '/my/searches'); ?></li>
<li><?php echo $this->Html->link('My Recommendations', '/my/recommendations'); ?></li>
<li class="divider"></li>
<li><?php echo $this->Html->link('Profile', '/profile'); ?></li>
<li><?php echo $this->Html->link('Notifications', '/notifications'); ?></li>
<li class="divider"></li>
<li><?php echo $this->Html->link('Help', '/help'); ?></li>
<?php
	if ($fb_user){
		echo '<li>' . $this->Facebook->logout(array('redirect' => '/users/logout', 'label' => 'Logout')) . '</li>';
	}else{
	?>
		<li><?php echo $this->Html->link('Logout', '/logout'); ?></li>
	<?php
	}
?>
