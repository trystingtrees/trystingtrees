<nav class="left">
  <div id="nav" class="left">
      <ul id="menu-nav" class="menu">
        <li id="menu-item-1" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-1">
			 <?php echo $this->Html->link('Activity Stream', '/activity-stream'); ?>
        </li>
        <li id="menu-item-2" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-2">
			 <?php echo $this->Html->link('Followed Listings', 'followed-listings'); ?>
        </li>
        <li id="menu-item-3" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-3">
			 <?php echo $this->Html->link('Q & A', '/answers'); ?>
        </li>
        <li id="menu-item-4" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-4">
			 <?php echo $this->Html->link('My Profile', $HOME . '/users/profile'); ?>
        </li>
        <li id="menu-item-5" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-5 last">
			 <?php echo $this->Html->link('My Settings', '/settings'); ?>
        </li>
    </ul>
  </div>
</nav>
<div class="clear"></div>
