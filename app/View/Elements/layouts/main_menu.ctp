<nav class="right">
  <div id="nav" class="right">
      <ul id="menu-nav" class="menu">
        <li id="menu-item-1" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-1">
			 <?php echo $this->Html->link('Home', '/'); ?>
        </li>
        <li id="menu-item-4" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-4">
			 <?php echo $this->Html->link('Find an Agent', '/agents/find'); ?>
        </li>
        <li id="menu-item-2" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-2">
			 <?php echo $this->Html->link('Q & A', '/buy'); ?>
        </li>
        <li id="menu-item-3" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-3">
			 <?php echo $this->Html->link('Rent', '/rent'); ?>
        </li>
        <li id="menu-item-5" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-5">
			 <?php echo $this->Html->link('Contact', '/contact'); ?>
        </li>
      <?php 
			if (!$user && !$fb_user){ 
		?>
        <li id="menu-item-1" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-1">
			 <?php echo $this->Html->link('Signup', '/signup'); ?>
        </li>
        <li id="menu-item-1" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-1">
			 <?php echo $this->Html->link('Login', '/login', array('class' => 'osx-login')); ?>
			</li>
      <?php } ?>
    </ul>
	<div style="text-align: right;" id="social-networks-auth">
		<?php
		/*
		$redir_link = '/' . $this->request->controller . '/' . $this->request->action . '/?fb_auth=true';
		if ($fb_user){
			echo $this->Facebook->logout(array('redirect' => '/users/logout', 'img' => 'facebook-logout.png'));
		}else{
			echo $this->Facebook->login(array('perms' => 'email,publish_stream', 'show-faces' => false, 'redirect' => $redir_link, 'img' => 'connectwithfacebook.gif'));
		}
		?>
		<?php
			if ($fb_user === false){
				echo $this->Facebook->login();
			}else{
				echo $this->Facebook->logout();
			}
		*/
		?>
	</div>
  </div>
</nav>
<div class="cleartext"></div>
