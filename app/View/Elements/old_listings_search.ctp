                <div id="advanced-search" class="left">
	<div id="advanced-search-inner">

        <form id="advanced_search" name="property-search" action="<?php echo $HOME; ?>/listings/search" method="post">
            
            <h2>Find your new home</h2>
            <p>5 Real Estate Listings, Homes for Sale &amp; Rental Properties.</p>

            <div class="top-row left">
					<div class="left">
						<label for="ct_location">Location</label>
						<input type="text" name="ct_location" id="ct_location" size="50" />
						<br />
						<small>e.g. "Toront, ON", "New York", "85734" ...</small>
					</div>

					<div class="left">
                	<label for="ct_type">Type</label>
                	<select id="ct_property_type" name="ct_property_type">
							<option value="0">Any</option>
							<option value="condotownhome">Condo/Townhome</option>
							<option value="land">Land</option>
							<option value="single-family-home">Single Family Home</option>
						</select>
	            </div>
				</div>
            
				<div class="top-row left">
            <div class="left">
                <label for="ct_beds">Beds</label>
                	<select id="ct_beds" name="ct_beds">
							<option value="0">Any</option>
							<option value="2">2</option>
							<option value="3">3</option>
							<option value="4">4+</option>
						</select>
	            </div>
            
            <div class="left">
                <label for="ct_baths">Baths</label>
                	<select id="ct_baths" name="ct_baths">
							<option value="0">Any</option>
							<option value="1-5">1.5</option>
							<option value="2">2</option>
							<option value="2-5">2.5</option>
							<option value="3">3+</option>
						</select>
	            </div>
           
			  	<!-- 
            <div class="left">
                <label for="ct_city">City</label>
					 <input type="text" name="ct_city" id="ct_city" size="30" />
	            </div>
            
            <div class="left">
                <label for="ct_state">State</label>
                	<select id="ct_state" name="ct_state">
							<option value="0">Any</option>
							<option value="ca">CA</option>
						</select>
	            </div>
				-->
            
            <div class="top-row left">
					<!--
                <label for="ct_zipcode">Postcode</label>
					-->
                	<select id="ct_zipcode" name="ct_zipcode" style="display: none;">
							<option value="0">Any</option>
							<option value="91901">91901</option>
							<option value="92014">92014</option>
							<option value="92101">92101</option>
						</select>
            
                <label for="ct_status">Status</label>
                	<select id="ct_status" name="ct_status">
							<option value="0">Any</option>
						<!--
							<option value="active">Active</option>
							-->
							<option value="featured">Featured</option>
							<option value="for-rent">For Rent</option>
							<option value="for-sale">For Sale</option>
							<!--
							<option value="open-house">Open House</option>
							<option value="reduced">Reduced</option>
							<option value="rental">Rental</option>
							<option value="sold">Sold</option>
							-->
						</select>
	            </div>
            
            <div class="left">
					<!--
                <label for="ct_additional_features">Addtional Features</label>
					-->
                	<select id="ct_additional_features" name="ct_additional_features" style="display: none;">
		<option value="0">Any</option>
								<option value="1-car-garage">1 car garage</option>
								<option value="1-total-half-bath">1 total half bath</option>
								<option value="2-stories">2 stories</option>
								<option value="3-car-garages">3 car garage(s)</option>
								<option value="3-fireplaces">3 fireplaces</option>
								<option value="area-coastal-north">Area: Coastal North</option>
								<option value="area-metro-central">Area: Metro Central</option>
								<option value="automatic-security-gate">Automatic Security Gate</option>
								<option value="city-view">City view</option>
								<option value="closed-circuit-security-tv">Closed Circuit Security TV</option>
								<option value="community-clubhouses">Community clubhouse(s)</option>
								<option value="community-exercise-areas">Community exercise area(s)</option>
								<option value="community-security-features">Community security features</option>
								<option value="community-spahot-tubs">Community spa/hot tub(s)</option>
								<option value="complex-name-the-legend">Complex Name: The Legend</option>
								<option value="concierge">Concierge</option>
								<option value="county-san-diego">County: San Diego</option>
								<option value="den">Den</option>
								<option value="dining-room-is-20x12">Dining room is 20x12</option>
								<option value="dining-room-is-20x20">Dining room is 20x20</option>
								<option value="family-room">Family room</option>
								<option value="family-room-is-20x12">Family room is 20x12</option>
								<option value="family-room-is-28x20">Family room is 28x20</option>
								<option value="forced-air-heat">Forced air heat</option>
								<option value="kitchen-is-11x8">Kitchen is 11x8</option>
								<option value="kitchen-is-20x14">Kitchen is 20x14</option>
								<option value="living-room-is-20x12">Living room is 20x12</option>
								<option value="living-room-is-20x20">Living room is 20x20</option>
								<option value="master-bedroom-is-12x11">Master bedroom is 12x11</option>
								<option value="master-bedroom-is-20x14">Master Bedroom is 20x14</option>
								<option value="ocean-view">Ocean view</option>
								<option value="on-site-guard">On Site Guard</option>
								<option value="roofing-concrete">Roofing: Concrete</option>
								<option value="single-story">Single story</option>
								<option value="spahot-tubs">Spa/hot tub(s)</option>
								<option value="style-contemporary">Style: Contemporary</option>
								<option value="subdivision-olde-del-mar">Subdivision: Olde Del Mar</option>
								<option value="topography-bluffcanyon-rim">Topography: Bluff/Canyon Rim</option>
								<option value="type-detached">Type: Detached</option>
								<option value="view-evening-lights">View: Evening Lights</option>
								<option value="water-view">Water view</option>
								<option value="year-built-2007">Year Built: 2007</option>
			</select>
	            </div>
            
						<div class="left">
                <label for="ct_price_from">Price From ($)</label>
				<input type="text" id="ct_price_from" class="number" name="ct_price_from" size="8" />
            </div>    

			<div class="left">
                <label for="ct_price_to">Price To ($)</label>
				<input type="text" id="ct_price_to" class="number" name="ct_price_to" size="8" />
            </div>
                        
            <div class="left">
                <label for="ct_mls">MLS #</label>
                <input type="text" id="ct_mls" name="ct_mls" size="12" />
            </div>
            
                <div class="clear"></div>
            
            <input type="hidden" name="property-search" value="true" disabled="disabled" />
            <input id="submit" class="btn right" type="submit" value="Search" />
            

                <div class="clear"></div>
            
        </form>
		 </div>

