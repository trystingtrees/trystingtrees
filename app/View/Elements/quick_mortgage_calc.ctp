<aside id="loancalc" class="widget left">
	<a name="calc"></a>
    <h4>Mortgage Calculator</h4>
    <form id="loancalc">
        <fieldset>
          <input type="text" name="mcPrice" id="mcPrice" class="mortgageField text-input" value="Sale price ($)" onfocus="if(this.value=='Sale price ($)')this.value = '';" onblur="if(this.value=='')this.value = 'Sale price ($)';" />
          <input type="text" name="mcDown" id="mcDown" class="mortgageField text-input" value="Down payment (%)" onfocus="if(this.value=='Down payment (%)')this.value = '';" onblur="if(this.value=='')this.value = 'Down payment (%)';" />
          <input type="text" name="mcRate" id="mcRate" class="mortgageField text-input" value="Interest Rate (%)" onfocus="if(this.value=='Interest Rate (%)')this.value = '';" onblur="if(this.value=='')this.value = 'Interest Rate (%)';" />
          <input type="text" name="mcTerm" id="mcTerm" class="mortgageField text-input" value="Term (years)" onfocus="if(this.value=='Term (years)')this.value = '';" onblur="if(this.value=='')this.value = 'Term (years)';" />
          <button class="btn" id="mortgageCalc" onclick="return false">Calculate Monthly Payment</button>
          <input type="text" name="mcPayment" id="mcPayment" class="mortgageAnswer text-input" value="$" />
        </fieldset>
    </form>
	<script type="text/javascript" src="http://wp.contempographicdesign.com/wp_real_estate_2/wp-content/themes/realestate_2/js/loancalc.js"></script>

</aside>
