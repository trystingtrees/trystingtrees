<aside id="contact-agent" class="widget left">
    	<!-- Contact Form Validation and Ajax Submit -->
		<!--
	<link rel="stylesheet" href="http://wp.contempographicdesign.com/wp_real_estate_2/wp-content/themes/realestate_2/css/validationEngine.jquery.css" type="text/css" media="screen" charset="utf-8" />
    	<script src="http://wp.contempographicdesign.com/wp_real_estate_2/wp-content/themes/realestate_2/js/jquery.validationEngine.js" type="text/javascript"></script>
		-->
    <script type="text/javascript">
    // Ajax Submit
    // Full documentation on this can be found at http://www.position-absolute.com/articles/jquery-form-validator-because-form-validation-is-a-mess/
    jQuery(document).ready(function() {
        jQuery("#contactform").validationEngine({
            ajaxSubmit: true,
                ajaxSubmitFile: "http://wp.contempographicdesign.com/wp_real_estate_2/wp-content/themes/realestate_2/includes/ajaxSubmit.php",
                ajaxSubmitMessage: "Success! We\'ve received your message.",
            success :  false,
            failure : function() {}
        })
    });
	</script>
    <form id="contactform" class="formular" method="post">
        <fieldset>
            <input type="text" name="name" id="name" class="validate[required,custom[onlyLetter]] text-input" value="Name" onfocus="if(this.value=='Name')this.value = '';" onblur="if(this.value=='')this.value = 'Name';" />
            
            <input type="text" name="email" id="email" class="validate[required,custom[email]] text-input" value="Email" onfocus="if(this.value=='Email')this.value = '';" onblur="if(this.value=='')this.value = 'Email';" />
            
            <input type="text" name="ctphone" id="ctphone" class="text-input" value="Phone" onfocus="if(this.value=='Phone')this.value = '';" onblur="if(this.value=='')this.value = 'Phone';" />
            
            <textarea class="validate[required,length[2,500]] text-input" name="message" id="message" rows="5" cols="10"></textarea>
            
            <input type="hidden" id="ctyouremail" name="ctyouremail" value="chris@contempographicdesign.com" />
            <input type="hidden" id="ctproperty" name="ctproperty" value="100 Harbor Dr #3902, San Diego, CA 92101" />
            <input type="hidden" id="ctpermalink" name="ctpermalink" value="http://wp.contempographicdesign.com/wp_real_estate_2/?post_type=listings&p=691" />
            <input type="hidden" id="ctsubject" name="ctsubject" value="WP Pro Real Estate 2 - Demo Inquiry" />
				<div class="clear"></div>
            
            <input type="submit" name="Submit" value="Contact" id="submit" class="btn" />  
        </fieldset>
    </form>
</aside>
