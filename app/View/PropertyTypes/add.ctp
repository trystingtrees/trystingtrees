<div class="propertyTypes form">
<?php echo $this->Form->create('PropertyType');?>
	<fieldset>
		<legend><?php echo __('Add Property Type'); ?></legend>
	<?php
		echo $this->Form->input('name');
		echo $this->Form->input('active');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit'));?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Property Types'), array('action' => 'index'));?></li>
	</ul>
</div>
