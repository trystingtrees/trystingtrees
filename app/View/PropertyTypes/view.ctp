<div class="propertyTypes view">
<h2><?php  echo __('Property Type');?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($propertyType['PropertyType']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($propertyType['PropertyType']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Active'); ?></dt>
		<dd>
			<?php echo h($propertyType['PropertyType']['active']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($propertyType['PropertyType']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($propertyType['PropertyType']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Property Type'), array('action' => 'edit', $propertyType['PropertyType']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Property Type'), array('action' => 'delete', $propertyType['PropertyType']['id']), null, __('Are you sure you want to delete # %s?', $propertyType['PropertyType']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Property Types'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Property Type'), array('action' => 'add')); ?> </li>
	</ul>
</div>
