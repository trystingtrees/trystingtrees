<div class="listingMetaDetails form">
<?php echo $this->Form->create('ListingMetaDetail');?>
	<fieldset>
		<legend><?php echo __('Edit Listing Meta Detail'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('listing_id');
		echo $this->Form->input('src_listing_uid');
		echo $this->Form->input('src_provider_listing_uid');
		echo $this->Form->input('src_provider_name');
		echo $this->Form->input('src_list_date');
		echo $this->Form->input('src_last_update_date');
		echo $this->Form->input('regional_mls_number');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit'));?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('ListingMetaDetail.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('ListingMetaDetail.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Listing Meta Details'), array('action' => 'index'));?></li>
		<li><?php echo $this->Html->link(__('List Listings'), array('controller' => 'listings', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Listing'), array('controller' => 'listings', 'action' => 'add')); ?> </li>
	</ul>
</div>
