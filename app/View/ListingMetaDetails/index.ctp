<div class="listingMetaDetails index">
	<h2><?php echo __('Listing Meta Details');?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id');?></th>
			<th><?php echo $this->Paginator->sort('listing_id');?></th>
			<th><?php echo $this->Paginator->sort('src_listing_uid');?></th>
			<th><?php echo $this->Paginator->sort('src_provider_listing_uid');?></th>
			<th><?php echo $this->Paginator->sort('src_provider_name');?></th>
			<th><?php echo $this->Paginator->sort('src_list_date');?></th>
			<th><?php echo $this->Paginator->sort('src_last_update_date');?></th>
			<th><?php echo $this->Paginator->sort('regional_mls_number');?></th>
			<th><?php echo $this->Paginator->sort('created');?></th>
			<th><?php echo $this->Paginator->sort('modified');?></th>
			<th class="actions"><?php echo __('Actions');?></th>
	</tr>
	<?php
	$i = 0;
	foreach ($listingMetaDetails as $listingMetaDetail): ?>
	<tr>
		<td><?php echo h($listingMetaDetail['ListingMetaDetail']['id']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($listingMetaDetail['Listing']['id'], array('controller' => 'listings', 'action' => 'view', $listingMetaDetail['Listing']['id'])); ?>
		</td>
		<td><?php echo h($listingMetaDetail['ListingMetaDetail']['src_listing_uid']); ?>&nbsp;</td>
		<td><?php echo h($listingMetaDetail['ListingMetaDetail']['src_provider_listing_uid']); ?>&nbsp;</td>
		<td><?php echo h($listingMetaDetail['ListingMetaDetail']['src_provider_name']); ?>&nbsp;</td>
		<td><?php echo h($listingMetaDetail['ListingMetaDetail']['src_list_date']); ?>&nbsp;</td>
		<td><?php echo h($listingMetaDetail['ListingMetaDetail']['src_last_update_date']); ?>&nbsp;</td>
		<td><?php echo h($listingMetaDetail['ListingMetaDetail']['regional_mls_number']); ?>&nbsp;</td>
		<td><?php echo h($listingMetaDetail['ListingMetaDetail']['created']); ?>&nbsp;</td>
		<td><?php echo h($listingMetaDetail['ListingMetaDetail']['modified']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $listingMetaDetail['ListingMetaDetail']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $listingMetaDetail['ListingMetaDetail']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $listingMetaDetail['ListingMetaDetail']['id']), null, __('Are you sure you want to delete # %s?', $listingMetaDetail['ListingMetaDetail']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>

	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Listing Meta Detail'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Listings'), array('controller' => 'listings', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Listing'), array('controller' => 'listings', 'action' => 'add')); ?> </li>
	</ul>
</div>
