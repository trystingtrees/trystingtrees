<div class="listingMetaDetails view">
<h2><?php  echo __('Listing Meta Detail');?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($listingMetaDetail['ListingMetaDetail']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Listing'); ?></dt>
		<dd>
			<?php echo $this->Html->link($listingMetaDetail['Listing']['id'], array('controller' => 'listings', 'action' => 'view', $listingMetaDetail['Listing']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Src Listing Uid'); ?></dt>
		<dd>
			<?php echo h($listingMetaDetail['ListingMetaDetail']['src_listing_uid']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Src Provider Listing Uid'); ?></dt>
		<dd>
			<?php echo h($listingMetaDetail['ListingMetaDetail']['src_provider_listing_uid']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Src Provider Name'); ?></dt>
		<dd>
			<?php echo h($listingMetaDetail['ListingMetaDetail']['src_provider_name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Src List Date'); ?></dt>
		<dd>
			<?php echo h($listingMetaDetail['ListingMetaDetail']['src_list_date']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Src Last Update Date'); ?></dt>
		<dd>
			<?php echo h($listingMetaDetail['ListingMetaDetail']['src_last_update_date']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Regional Mls Number'); ?></dt>
		<dd>
			<?php echo h($listingMetaDetail['ListingMetaDetail']['regional_mls_number']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($listingMetaDetail['ListingMetaDetail']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($listingMetaDetail['ListingMetaDetail']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Listing Meta Detail'), array('action' => 'edit', $listingMetaDetail['ListingMetaDetail']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Listing Meta Detail'), array('action' => 'delete', $listingMetaDetail['ListingMetaDetail']['id']), null, __('Are you sure you want to delete # %s?', $listingMetaDetail['ListingMetaDetail']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Listing Meta Details'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Listing Meta Detail'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Listings'), array('controller' => 'listings', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Listing'), array('controller' => 'listings', 'action' => 'add')); ?> </li>
	</ul>
</div>
