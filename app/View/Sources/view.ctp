<div class="sources view">
<h2><?php  echo __('Source');?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($source['Source']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($source['Source']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($source['Source']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($source['Source']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Source'), array('action' => 'edit', $source['Source']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Source'), array('action' => 'delete', $source['Source']['id']), null, __('Are you sure you want to delete # %s?', $source['Source']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Sources'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Source'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Listings'), array('controller' => 'listings', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Listing'), array('controller' => 'listings', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Xml Uploads'), array('controller' => 'xml_uploads', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Xml Upload'), array('controller' => 'xml_uploads', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Listings');?></h3>
	<?php if (!empty($source['Listing'])):?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Uuid'); ?></th>
		<th><?php echo __('Source Id'); ?></th>
		<th><?php echo __('Listing Status Id'); ?></th>
		<th><?php echo __('User Id'); ?></th>
		<th><?php echo __('Created At'); ?></th>
		<th><?php echo __('Updated At'); ?></th>
		<th class="actions"><?php echo __('Actions');?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($source['Listing'] as $listing): ?>
		<tr>
			<td><?php echo $listing['id'];?></td>
			<td><?php echo $listing['uuid'];?></td>
			<td><?php echo $listing['source_id'];?></td>
			<td><?php echo $listing['listing_status_id'];?></td>
			<td><?php echo $listing['user_id'];?></td>
			<td><?php echo $listing['created_at'];?></td>
			<td><?php echo $listing['updated_at'];?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'listings', 'action' => 'view', $listing['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'listings', 'action' => 'edit', $listing['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'listings', 'action' => 'delete', $listing['id']), null, __('Are you sure you want to delete # %s?', $listing['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Listing'), array('controller' => 'listings', 'action' => 'add'));?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Xml Uploads');?></h3>
	<?php if (!empty($source['XmlUpload'])):?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Source Id'); ?></th>
		<th><?php echo __('Xml File'); ?></th>
		<th><?php echo __('Num Listings'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th class="actions"><?php echo __('Actions');?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($source['XmlUpload'] as $xmlUpload): ?>
		<tr>
			<td><?php echo $xmlUpload['id'];?></td>
			<td><?php echo $xmlUpload['source_id'];?></td>
			<td><?php echo $xmlUpload['xml_file'];?></td>
			<td><?php echo $xmlUpload['num_listings'];?></td>
			<td><?php echo $xmlUpload['created'];?></td>
			<td><?php echo $xmlUpload['modified'];?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'xml_uploads', 'action' => 'view', $xmlUpload['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'xml_uploads', 'action' => 'edit', $xmlUpload['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'xml_uploads', 'action' => 'delete', $xmlUpload['id']), null, __('Are you sure you want to delete # %s?', $xmlUpload['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Xml Upload'), array('controller' => 'xml_uploads', 'action' => 'add'));?> </li>
		</ul>
	</div>
</div>
