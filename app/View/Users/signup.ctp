<h3>Join Trystingtrees!</h3>
<?php
	echo $this->Form->create('User', array('action' => 'add', 'class' => 'formify', 'label' => ''));
	echo $this->Form->label('Group', 'I am a...');
	echo '<br />';
	echo $this->Form->select('group_id', array('Buyer / Seller', 'Agent'), array('class' => 'select-input', 'label' => ''));
	echo '<br />';

	echo $this->Form->label('Email');
	echo $this->Form->input('email', array('class' => 'text-input', 'label' => ''));

	echo $this->Form->label('Username');
	echo $this->Form->input('username', array('class' => 'text-input', 'label' => ''));
	
	echo $this->Form->label('Password');
	echo $this->Form->input('passwd', array('type' => 'password', 'class' => 'text-input', 'label' => ''));
	
	echo $this->Form->label('Password Confirm');
	echo $this->Form->input('passwd_confirm', array('type' => 'password', 'class' => 'text-input', 'label' => ''));

	echo '<br /><br />';
	echo $this->Form->submit('Join Trystingtrees', array('class' => 'btn'));
	echo $this->Form->end();
	echo '<br /><br />';
?>
