<section id="content">
	<div class="shady bott-27"></div>
	<div class="inner clearfix">
		<div class="inner-t">
			<div class="heading"><h4>Register or Sign In</h4></div>

			<div class="login-container" style="width: 500px; margin: 0 auto; margin-top: 10px;">
				<div class="tabbable">
					<ul class="nav nav-tabs">
						<li class="active"><a href="#register" data-toggle="tab">Register</a></li>
						<li class=""><a href="#sign-in" data-toggle="tab">Sign in</a></li>
						<li class=""><a href="#social-sign-in" data-toggle="tab">Social Sign in</a></li>
					</ul>
				</div>

				<div class="tab-content">
					<div class="tab-pane active" id="register">
						<?php
							echo $this->Form->create('User', array('action' => 'add', 'class' => 'formify', 'label' => ''));
							echo $this->Form->label('Group', 'I am a...');
							echo '<br />';
							echo $this->Form->select('group_id', array('Buyer / Seller', 'Agent'), array('class' => 'select-input', 'label' => ''));
							echo '<br />';

							echo $this->Form->label('Email');
							echo $this->Form->input('email', array('class' => 'text-input', 'label' => ''));

							echo $this->Form->label('Username');
							echo $this->Form->input('username', array('class' => 'text-input', 'label' => ''));
							
							echo $this->Form->label('Password');
							echo $this->Form->input('passwd', array('type' => 'password', 'class' => 'text-input', 'label' => ''));
							
							echo $this->Form->label('Password Confirm');
							echo $this->Form->input('passwd_confirm', array('type' => 'password', 'class' => 'text-input', 'label' => ''));

							echo '<br /><br />';
							echo $this->Form->submit('Join Trystingtrees', array('class' => 'btn'));
							echo $this->Form->end();
							echo '<br /><br />';
						?>
					</div>
					<div class="tab-pane" id="sign-in">
						<?php
							echo $this->Form->create('User', array('action' => 'login', 'class' => 'formify', 'label' => ''));
							echo $this->Form->label('', 'Email or username');
							echo $this->Form->input('email_username', array('class' => 'text-input', 'label' => ''));

							echo $this->Form->label('Password');
							echo $this->Form->input('password', array('type' => 'password', 'class' => 'text-input', 'label' => ''));

							echo $this->Html->link('Forgot your password?', '/forgot-password');
							echo '<br />';
							echo '<br />';
							echo $this->Form->checkbox('remember');
							echo '<span style="margin-left: 10px; top: 2px; position: relative;">Remember me</span>';
							
							echo '<br /><br />';
							echo $this->Form->submit('Login', array('class' => 'btn'));
							echo $this->Form->end();
							echo '<br /><br />';
						?>

					</div>
					<div class="tab-pane" id="social-sign-in">
						<?php echo $this->Element('oauth/facebook', array('rb_redir', $fb_redir)); ?>
						<br />
						<?php echo $this->Element('oauth/twitter'); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
