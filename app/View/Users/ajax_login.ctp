<div style="float: left;">
<?php
	echo $this->Form->create('User', array('action' => 'login', 'class' => 'formify', 'label' => ''));
	echo $this->Form->label('', 'Email or username');
	echo $this->Form->input('email_username', array('class' => 'text-input', 'label' => ''));

	echo $this->Form->label('Password');
	echo $this->Form->input('password', array('type' => 'password', 'class' => 'text-input', 'label' => ''));
	
	echo '<br /><br />';
	echo $this->Form->submit('Login', array('class' => 'btn'));
	echo $this->Form->end();
?>
</div>
<div style="float: left; margin: 60px 45px 0 35px;">
	<center><h3> - OR - </h3></center>
</div>
<div style="float: left;">
	<?php echo $this->Element('oauth/facebook', array('redir', $redir)); ?>
</div>
