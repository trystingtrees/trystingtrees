<div class="blogSettings view">
<h2><?php  echo __('Blog Setting');?></h2>
	<dl>
		<dt><?php echo __('Uuid'); ?></dt>
		<dd>
			<?php echo h($blogSetting['BlogSetting']['uuid']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Tagline'); ?></dt>
		<dd>
			<?php echo h($blogSetting['BlogSetting']['tagline']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Email'); ?></dt>
		<dd>
			<?php echo h($blogSetting['BlogSetting']['email']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Date Format'); ?></dt>
		<dd>
			<?php echo h($blogSetting['BlogSetting']['date_format']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Time Format'); ?></dt>
		<dd>
			<?php echo h($blogSetting['BlogSetting']['time_format']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Allow Comments'); ?></dt>
		<dd>
			<?php echo h($blogSetting['BlogSetting']['allow_comments']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Administer Comments'); ?></dt>
		<dd>
			<?php echo h($blogSetting['BlogSetting']['administer_comments']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Allow Comment Replies'); ?></dt>
		<dd>
			<?php echo h($blogSetting['BlogSetting']['allow_comment_replies']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Blog Setting'), array('action' => 'edit', $blogSetting['BlogSetting']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Blog Setting'), array('action' => 'delete', $blogSetting['BlogSetting']['id']), null, __('Are you sure you want to delete # %s?', $blogSetting['BlogSetting']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Blog Settings'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Blog Setting'), array('action' => 'add')); ?> </li>
	</ul>
</div>
