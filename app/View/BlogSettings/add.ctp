<div class="blogSettings form">
<?php echo $this->Form->create('BlogSetting');?>
	<fieldset>
		<legend><?php echo __('Add Blog Setting'); ?></legend>
	<?php
		echo $this->Form->input('uuid');
		echo $this->Form->input('tagline');
		echo $this->Form->input('email');
		echo $this->Form->input('date_format');
		echo $this->Form->input('time_format');
		echo $this->Form->input('allow_comments');
		echo $this->Form->input('administer_comments');
		echo $this->Form->input('allow_comment_replies');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit'));?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Blog Settings'), array('action' => 'index'));?></li>
	</ul>
</div>
