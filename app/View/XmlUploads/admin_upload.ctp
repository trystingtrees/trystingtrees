<h3>Upload a new Listings XML File</h3>
<?php
	echo $this->Form->create('XmlUpload', array('action' => 'upload_xml', 'class' => 'formify', 'label' => '', 'enctype' => 'multipart/form-data'));
	echo $this->Form->label('source_id', 'Source');
	echo '<br />';
	echo $this->Form->select('source_id', array('Point2.com'), array('class' => 'select-input', 'label' => ''));
	echo '<br /><br />';

	echo $this->Form->label('Xml File');
	echo '<br />';
	echo $this->Form->file('xml_file', array('label' => ''));
	
	echo '<br /><br />';
	echo $this->Form->submit('Upload File', array('class' => 'btn'));
	echo $this->Form->end();
	echo '<br /><br />';
?>

