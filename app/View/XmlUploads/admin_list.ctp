<h3>Uploaded Listing Files</h3>
<?php
	//pr($xml_files);
?>

<table>
	<thead>
		<tr>
			<td>ID</td>
			<td>Source</td>
			<td>File Name</td>
			<td>Actions</td>
		</tr>
	</thead>
	<tbody>
		<?php
			foreach($xml_files as $file){
				$id = $file['XmlUpload']['id'];
				$source = $file['Source']['name'];
				$name = $file['XmlUpload']['xml_file'];
		?>
		<tr>
			<td><?php echo $id; ?></td>
			<td><?php echo $source; ?></td>
			<td><?php echo $name; ?></td>
			<td>
				<a href="/admin/xml_uploads/process/<?php echo $id; ?>">Process</a> | 
				<a href="/admin/xml_uploads/view/<?php echo $id; ?>">View</a> | 
				<a href="/admin/xml_uploads/edit/<?php echo $id; ?>">Edit</a> | 
				<a href="/admin/xml_uploads/delete/<?php echo $id; ?>">Delete</a> | 
			</td>
		</tr>
		<?php
			}
		?>
	</tbody>
</table>
