<div class="countries view">
<h2><?php  echo __('Country');?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($country['Country']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Short Name'); ?></dt>
		<dd>
			<?php echo h($country['Country']['short_name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Long Name'); ?></dt>
		<dd>
			<?php echo h($country['Country']['long_name']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Country'), array('action' => 'edit', $country['Country']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Country'), array('action' => 'delete', $country['Country']['id']), null, __('Are you sure you want to delete # %s?', $country['Country']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Countries'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Country'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Listing Locations'), array('controller' => 'listing_locations', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Listing Location'), array('controller' => 'listing_locations', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List States'), array('controller' => 'states', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New State'), array('controller' => 'states', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Listing Locations');?></h3>
	<?php if (!empty($country['ListingLocation'])):?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Listing Id'); ?></th>
		<th><?php echo __('Address'); ?></th>
		<th><?php echo __('City'); ?></th>
		<th><?php echo __('County'); ?></th>
		<th><?php echo __('State Id'); ?></th>
		<th><?php echo __('Postal Code'); ?></th>
		<th><?php echo __('Country Id'); ?></th>
		<th><?php echo __('Latitude'); ?></th>
		<th><?php echo __('Longitude'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th class="actions"><?php echo __('Actions');?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($country['ListingLocation'] as $listingLocation): ?>
		<tr>
			<td><?php echo $listingLocation['id'];?></td>
			<td><?php echo $listingLocation['listing_id'];?></td>
			<td><?php echo $listingLocation['address'];?></td>
			<td><?php echo $listingLocation['city'];?></td>
			<td><?php echo $listingLocation['county'];?></td>
			<td><?php echo $listingLocation['state_id'];?></td>
			<td><?php echo $listingLocation['postal_code'];?></td>
			<td><?php echo $listingLocation['country_id'];?></td>
			<td><?php echo $listingLocation['latitude'];?></td>
			<td><?php echo $listingLocation['longitude'];?></td>
			<td><?php echo $listingLocation['created'];?></td>
			<td><?php echo $listingLocation['modified'];?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'listing_locations', 'action' => 'view', $listingLocation['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'listing_locations', 'action' => 'edit', $listingLocation['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'listing_locations', 'action' => 'delete', $listingLocation['id']), null, __('Are you sure you want to delete # %s?', $listingLocation['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Listing Location'), array('controller' => 'listing_locations', 'action' => 'add'));?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related States');?></h3>
	<?php if (!empty($country['State'])):?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Short Name'); ?></th>
		<th><?php echo __('Long Name'); ?></th>
		<th><?php echo __('Country Id'); ?></th>
		<th class="actions"><?php echo __('Actions');?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($country['State'] as $state): ?>
		<tr>
			<td><?php echo $state['id'];?></td>
			<td><?php echo $state['short_name'];?></td>
			<td><?php echo $state['long_name'];?></td>
			<td><?php echo $state['country_id'];?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'states', 'action' => 'view', $state['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'states', 'action' => 'edit', $state['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'states', 'action' => 'delete', $state['id']), null, __('Are you sure you want to delete # %s?', $state['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New State'), array('controller' => 'states', 'action' => 'add'));?> </li>
		</ul>
	</div>
</div>
