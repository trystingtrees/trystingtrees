       <h1 id="welcome">Feature Rich & Affordable. <em><strong>Buy it Now!</strong></em></h1>
        
            
            <div id="featured" class="left fourcol">
                <div class="snipe-lrg"><h6>Featured Property</h6></div>
                	<div id="sliderwrap" class="left">
                    	<div id="slider">
                						<a href="http://wp.contempographicdesign.com/wp_real_estate_2/?post_type=listings&p=617"><img src="http://wp.contempographicdesign.com/wp_real_estate_2/wp-content/themes/realestate_2/img_resize/timthumb.php?src=http://wp.contempographicdesign.com/wp_real_estate_2/wp-content/uploads/2010/07/4207805464_f704ccda77_b.jpg&h=227&w=453&zc=1" /></a>
					<a href="http://wp.contempographicdesign.com/wp_real_estate_2/?post_type=listings&p=617"><img src="http://wp.contempographicdesign.com/wp_real_estate_2/wp-content/themes/realestate_2/img_resize/timthumb.php?src=http://wp.contempographicdesign.com/wp_real_estate_2/wp-content/uploads/2010/07/3313607822_f17a5c13f5_o.jpg&h=227&w=453&zc=1" /></a>
					<a href="http://wp.contempographicdesign.com/wp_real_estate_2/?post_type=listings&p=617"><img src="http://wp.contempographicdesign.com/wp_real_estate_2/wp-content/themes/realestate_2/img_resize/timthumb.php?src=http://wp.contempographicdesign.com/wp_real_estate_2/wp-content/uploads/2010/07/3313607696_8815da1c84_o.jpg&h=227&w=453&zc=1" /></a>
					<a href="http://wp.contempographicdesign.com/wp_real_estate_2/?post_type=listings&p=617"><img src="http://wp.contempographicdesign.com/wp_real_estate_2/wp-content/themes/realestate_2/img_resize/timthumb.php?src=http://wp.contempographicdesign.com/wp_real_estate_2/wp-content/uploads/2010/07/3313607318_841057e41c_o.jpg&h=227&w=453&zc=1" /></a>
					<a href="http://wp.contempographicdesign.com/wp_real_estate_2/?post_type=listings&p=617"><img src="http://wp.contempographicdesign.com/wp_real_estate_2/wp-content/themes/realestate_2/img_resize/timthumb.php?src=http://wp.contempographicdesign.com/wp_real_estate_2/wp-content/uploads/2010/07/3313607068_94117288b6_o.jpg&h=227&w=453&zc=1" /></a>
		                        </div>
                    </div>
                <div id="info" class="left">
                    <div id="infoinner">
                        <h3><a href="http://wp.contempographicdesign.com/wp_real_estate_2/?post_type=listings&p=617">1325 Pacific Hwy</a></h3>
                        <h4 class="location">San Diego, CA 92101</h4>
						                       		<p class="price"><strong>$895,000</strong></p>
                                                <p class="propinfo">3 Bed, 2 Bath,  6,375 Sq Ft</p>
                        <p class="description">Pellentesque fermentum, sem nec condimentum pulvinar, odio lectus varius nibh, at luctus ligula ante nec nulla. Fusce et nulla id ligula porttitor convallis vel&hellip;</p>                       
                        
                    </div>
                </div>
                <div id="map" class="right">
							<script src="http://maps.google.com/maps/api/js?sensor=true"></script>		
		<script>
        function setMapAddress(address) {
            var geocoder = new google.maps.Geocoder();
            geocoder.geocode( { address : address }, function( results, status ) {
                if( status == google.maps.GeocoderStatus.OK ) {
										var latlng = results[0].geometry.location;
					                    var options = {
                        zoom: 15,
                        center: latlng,
                        mapTypeId: google.maps.MapTypeId.ROADMAP, 
                        streetViewControl: true
                    };
                    var mymap = new google.maps.Map( document.getElementById( 'map' ), options );
                    var marker = new google.maps.Marker({
						map: mymap,
												position: results[0].geometry.location
											});
					
                }
            } );
        }
        setMapAddress( "1325 Pacific Hwy, San Diego, CA, 92101" );
        </script>
        <div id="location">
            <div id="map">Loading...</div>
        </div>
                </div>
            </div>
            	<div class="clear"></div>
    		    
    <div id="leftcol-home" class="left">
        
                <div id="advanced-search" class="left">
	<div id="advanced-search-inner">

        <form id="advanced_search" name="property-search" action="http://wp.contempographicdesign.com/wp_real_estate_2">
            
            <h2>Find your new home</h2>
            <p>5 Real Estate Listings, Homes for Sale &amp; Rental Properties.</p>
            
            <div class="top-row left">
                <label for="ct_type">Type</label>
                	<select id="ct_property_type" name="ct_property_type">
		<option value="0">Any</option>
								<option value="condotownhome">Condo/Townhome</option>
								<option value="land">Land</option>
								<option value="single-family-home">Single Family Home</option>
			</select>
	            </div>
            
            <div class="left">
                <label for="ct_beds">Beds</label>
                	<select id="ct_beds" name="ct_beds">
		<option value="0">Any</option>
								<option value="2">2</option>
								<option value="3">3</option>
								<option value="4">4</option>
			</select>
	            </div>
            
            <div class="left">
                <label for="ct_baths">Baths</label>
                	<select id="ct_baths" name="ct_baths">
		<option value="0">Any</option>
								<option value="1-5">1.5</option>
								<option value="2">2</option>
								<option value="3">3</option>
								<option value="6">6</option>
			</select>
	            </div>
            
            <div class="left">
                <label for="ct_city">City</label>
                	<select id="ct_city" name="ct_city">
		<option value="0">Any</option>
								<option value="alpine">Alpine</option>
								<option value="del-mar">Del Mar</option>
								<option value="san-diego">San Diego</option>
			</select>
	            </div>
            
            <div class="left">
                <label for="ct_state">State</label>
                	<select id="ct_state" name="ct_state">
		<option value="0">Any</option>
								<option value="ca">CA</option>
			</select>
	            </div>
            
            <div class="top-row left">
                <label for="ct_zipcode">Postcode</label>
                	<select id="ct_zipcode" name="ct_zipcode">
		<option value="0">Any</option>
								<option value="91901">91901</option>
								<option value="92014">92014</option>
								<option value="92101">92101</option>
			</select>
	            </div>
            
                <div class="clear"></div>
            
            <div class="left">
                <label for="ct_status">Status</label>
                	<select id="ct_status" name="ct_status">
		<option value="0">Any</option>
								<option value="active">Active</option>
								<option value="featured">Featured</option>
								<option value="for-rent">For Rent</option>
								<option value="for-sale">For Sale</option>
								<option value="open-house">Open House</option>
								<option value="reduced">Reduced</option>
								<option value="rental">Rental</option>
								<option value="sold">Sold</option>
			</select>
	            </div>
            
            <div class="left">
                <label for="ct_additional_features">Addtional Features</label>
                	<select id="ct_additional_features" name="ct_additional_features">
		<option value="0">Any</option>
								<option value="1-car-garage">1 car garage</option>
								<option value="1-total-half-bath">1 total half bath</option>
								<option value="2-stories">2 stories</option>
								<option value="3-car-garages">3 car garage(s)</option>
								<option value="3-fireplaces">3 fireplaces</option>
								<option value="area-coastal-north">Area: Coastal North</option>
								<option value="area-metro-central">Area: Metro Central</option>
								<option value="automatic-security-gate">Automatic Security Gate</option>
								<option value="city-view">City view</option>
								<option value="closed-circuit-security-tv">Closed Circuit Security TV</option>
								<option value="community-clubhouses">Community clubhouse(s)</option>
								<option value="community-exercise-areas">Community exercise area(s)</option>
								<option value="community-security-features">Community security features</option>
								<option value="community-spahot-tubs">Community spa/hot tub(s)</option>
								<option value="complex-name-the-legend">Complex Name: The Legend</option>
								<option value="concierge">Concierge</option>
								<option value="county-san-diego">County: San Diego</option>
								<option value="den">Den</option>
								<option value="dining-room-is-20x12">Dining room is 20x12</option>
								<option value="dining-room-is-20x20">Dining room is 20x20</option>
								<option value="family-room">Family room</option>
								<option value="family-room-is-20x12">Family room is 20x12</option>
								<option value="family-room-is-28x20">Family room is 28x20</option>
								<option value="forced-air-heat">Forced air heat</option>
								<option value="kitchen-is-11x8">Kitchen is 11x8</option>
								<option value="kitchen-is-20x14">Kitchen is 20x14</option>
								<option value="living-room-is-20x12">Living room is 20x12</option>
								<option value="living-room-is-20x20">Living room is 20x20</option>
								<option value="master-bedroom-is-12x11">Master bedroom is 12x11</option>
								<option value="master-bedroom-is-20x14">Master Bedroom is 20x14</option>
								<option value="ocean-view">Ocean view</option>
								<option value="on-site-guard">On Site Guard</option>
								<option value="roofing-concrete">Roofing: Concrete</option>
								<option value="single-story">Single story</option>
								<option value="spahot-tubs">Spa/hot tub(s)</option>
								<option value="style-contemporary">Style: Contemporary</option>
								<option value="subdivision-olde-del-mar">Subdivision: Olde Del Mar</option>
								<option value="topography-bluffcanyon-rim">Topography: Bluff/Canyon Rim</option>
								<option value="type-detached">Type: Detached</option>
								<option value="view-evening-lights">View: Evening Lights</option>
								<option value="water-view">Water view</option>
								<option value="year-built-2007">Year Built: 2007</option>
			</select>
	            </div>
            
						<div class="left">
                <label for="ct_price_from">Price From ($)</label>
				<input type="text" id="ct_price_from" class="number" name="ct_price_from" size="8" />
            </div>    

			<div class="left">
                <label for="ct_price_to">Price To ($)</label>
				<input type="text" id="ct_price_to" class="number" name="ct_price_to" size="8" />
            </div>
                        
            <div class="left">
                <label for="ct_mls">MLS #</label>
                <input type="text" id="ct_mls" name="ct_mls" size="12" />
            </div>
            
                <div class="clear"></div>
            
            <input type="hidden" name="property-search" value="true" />
            <input id="submit" class="btn right" type="submit" value="Search" />
            
                <div class="clear"></div>
            
        </form>

    </div>
</div>                
        <div class="left onethirdcol">
        	            <h2>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h2>
                                    <p>Pellentesque rhoncus. Donec consectetuer, neque nec volutpat sodales, risus tortor eleifend libero, vitae porta diam sapien non orci. Nulla mollis ligula in tortor. Suspendisse potenti. Aenean sem justo, mollis sed, malesuada vitae, viverra eu, metus. Aliquam massa justo, rutrum at, tincidunt vel, bibendum vitae, quam. Aliquam nec ante.</p><p>Mauris quis elit diam, vitae pulvinar neque. Quisque imperdiet, augue id suscipit pharetra, ante odio mollis dui, non bibendum.</p>                    </div>
        
        <div class="right onethirdcol">
        	<h4 class="divide">New Listings</h4>
            <ul id="newlistings">
                            
					<li>
                    									<div class="imgwraptn left">
		<a href="http://wp.contempographicdesign.com/wp_real_estate_2/?post_type=listings&p=741"><img class="tn left imgfade" src="http://wp.contempographicdesign.com/wp_real_estate_2/wp-content/themes/realestate_2/img_resize/timthumb.php?src=http://wp.contempographicdesign.com/wp_real_estate_2/wp-content/uploads/2011/03/land.jpg&h=68&w=118&zc=1" /></a>
    </div>
                            <h5><a href="http://wp.contempographicdesign.com/wp_real_estate_2/?post_type=listings&p=741">5301 Willows Rd</a></h5>
                        <p class="location">Alpine, CA 91901</p>
                        <p class="price"><strong>$2,888,000</strong></p>
                        <p class="propinfo">4.47 Acres</p>
                        	<div class="clear"></div>
                    </li>
				
				                
					<li>
                    			<div class="snipe-sm"><h6 class="open-house">Open House</h6></div>
    							<div class="imgwraptn left">
		<a href="http://wp.contempographicdesign.com/wp_real_estate_2/?post_type=listings&p=716"><img class="tn left imgfade" src="http://wp.contempographicdesign.com/wp_real_estate_2/wp-content/themes/realestate_2/img_resize/timthumb.php?src=http://wp.contempographicdesign.com/wp_real_estate_2/wp-content/uploads/2010/11/l6a2b6342-m0o.jpg&h=68&w=118&zc=1" /></a>
    </div>
                            <h5><a href="http://wp.contempographicdesign.com/wp_real_estate_2/?post_type=listings&p=716">963 Klish Way</a></h5>
                        <p class="location">Del Mar, CA 92014</p>
                        <p class="price"><strong>$6,995,000</strong></p>
                        <p class="propinfo">4 Bed, 6 Bath,  6,000 Sq Ft</p>
                        	<div class="clear"></div>
                    </li>
				
				                
					<li>
                    									<div class="imgwraptn left">
		<a href="http://wp.contempographicdesign.com/wp_real_estate_2/?post_type=listings&p=691"><img class="tn left imgfade" src="http://wp.contempographicdesign.com/wp_real_estate_2/wp-content/themes/realestate_2/img_resize/timthumb.php?src=http://wp.contempographicdesign.com/wp_real_estate_2/wp-content/uploads/2010/09/3313607822_f17a5c13f5_o.jpg&h=68&w=118&zc=1" /></a>
    </div>
                            <h5><a href="http://wp.contempographicdesign.com/wp_real_estate_2/?post_type=listings&p=691">100 Harbor Dr #3902</a></h5>
                        <p class="location">San Diego, CA 92101</p>
                        <p class="price">Rental - <strong>$4,275</strong></p>
                        <p class="propinfo">4 Bed, 3 Bath,  4,303 Sq Ft</p>
                        	<div class="clear"></div>
                    </li>
				
				                        </ul>
        </div>
        
    </div>
    
    <div id="rightcol-home" class="right">
    		<aside id="latestposts" class="widget left">
		<h4>Latest from the blog</h4>
        			
				<li>
					<h5><a href="http://wp.contempographicdesign.com/wp_real_estate_2/?p=522">Creating a Custom Skin in WP Structure</a></h5>
					<p class="description">Mauris quis elit diam, vitae pulvinar neque. Quisque imperdiet, augue id suscipit pharetra, ante odio mollis dui, non bibendum.</p>
						<div class="clear"></div>
				</li>
			
						
				<li>
					<h5><a href="http://wp.contempographicdesign.com/wp_real_estate_2/?p=537">Creating a Gallery Post in WP Structure</a></h5>
					<p class="description">Mauris quis elit diam, vitae pulvinar neque. Quisque imperdiet, augue id suscipit pharetra, ante odio mollis dui, non bibendum.</p>
						<div class="clear"></div>
				</li>
			
						
				<li>
					<h5><a href="#">Creating a Video Post in WP Structure</a></h5>
					<p class="description">Mauris quis elit diam, vitae pulvinar neque. Quisque imperdiet, augue id suscipit pharetra, ante odio mollis dui, non bibendum.</p>
						<div class="clear"></div>
				</li>
			
					    </aside>

    </div>
    

