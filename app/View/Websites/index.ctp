    <section id="content">
        <div class="inner">
				<div class="inner-t">
					<div class="heading">
						<h3>Top Listings in Your Area</h3>
					</div>
					<?php echo $this->Element('listings_streamer'); ?>
				</div>
        </div>

		  <div class="wrap720">

			  <div class="shady bott-27"></div>

			  <div class="inner clearfix">
					<div class="inner-t">
						 <div class="heading">
							 <h3>Find Your Next Home Right Here!</h3>
						 </div>
						<?php echo $this->Element('listings_search'); ?>
					</div>
			  </div>

			  <div class="shady bott-27"></div>

				<!-- Q&A At a Glance -->
			  <div class="inner clearfix" id="qa_glance">
					<div class="inner-t">
						<div class="post-mod">
							<a href="blog-single.html" class="date-comments">
								<div>
								20 Dec,<br />
								2011
								</div>
								<span>05</span>
							</a>
							<h6><a href="blog-single.html">Et harum quidem rerum facilis est et expedita distinctio</a></h6>
							<p>Posted by <strong><a href="#">Fireform</a></strong> in <a href="#" class="clr-link">Commercials</a><img class="ml-10" src="/img/layout/pencil.gif" alt=""></p>
						</div>
						<div class="post-mod">
							<a href="blog-single.html" class="date-comments">
								<div>27 Nov,<br />2011</div><span>79</span>
							</a>
							<h6><a href="blog-single.html">Temporibus autem quibusdam et aut officiis debitis</a></h6>
							<p>Posted by <strong><a href="#">Fireform</a></strong> in <a href="#" class="clr-link">Landed</a><img class="ml-10" src="/img/layout/pencil.gif" alt=""></p>
						</div>
						<div class="post-mod">
							<a href="blog-single.html" class="date-comments">
								<div>25 Nov,<br />2011</div><span>26</span>
							</a>
							<h6><a href="blog-single.html">Nam libero tempore, cum soluta nobis est eligendi optio cumque</a></h6>
							<p>Posted by <strong><a href="#">Fireform</a></strong> in <a href="#" class="clr-link">Landed</a><img class="ml-10" src="/img/layout/pencil.gif" alt=""></p>
						</div>
						<div class="post-mod">
							<a href="blog-single.html" class="date-comments">
								<div>25 Nov,<br />2011</div><span>26</span>
							</a>
							<h6><a href="blog-single.html">Nam libero tempore, cum soluta nobis est eligendi optio cumque</a></h6>
							<p>Posted by <strong><a href="#">Fireform</a></strong> in <a href="#" class="clr-link">Landed</a><img class="ml-10" src="/img/layout/pencil.gif" alt=""></p>
						</div>
						<div class="post-mod">
							<a href="blog-single.html" class="date-comments">
								<div>25 Nov,<br />2011</div><span>26</span>
							</a>
							<h6><a href="blog-single.html">Nam libero tempore, cum soluta nobis est eligendi optio cumque</a></h6>
							<p>Posted by <strong><a href="#">Fireform</a></strong> in <a href="#" class="clr-link">Landed</a><img class="ml-10" src="/img/layout/pencil.gif" alt=""></p>
						</div>
						<div class="post-mod">
							<a href="blog-single.html" class="date-comments">
								<div>25 Nov,<br />2011</div><span>26</span>
							</a>
							<h6><a href="blog-single.html">Nam libero tempore, cum soluta nobis est eligendi optio cumque</a></h6>
							<p>Posted by <strong><a href="#">Fireform</a></strong> in <a href="#" class="clr-link">Landed</a><img class="ml-10" src="/img/layout/pencil.gif" alt=""></p>
						</div>
						<div class="post-mod">
							<a href="blog-single.html" class="date-comments">
								<div>25 Nov,<br />2011</div><span>26</span>
							</a>
							<h6><a href="blog-single.html">Nam libero tempore, cum soluta nobis est eligendi optio cumque</a></h6>
							<p>Posted by <strong><a href="#">Fireform</a></strong> in <a href="#" class="clr-link">Landed</a><img class="ml-10" src="/img/layout/pencil.gif" alt=""></p>
						</div>
						<div class="post-mod">
							<a href="blog-single.html" class="date-comments">
								<div>25 Nov,<br />2011</div><span>26</span>
							</a>
							<h6><a href="blog-single.html">Nam libero tempore, cum soluta nobis est eligendi optio cumque</a></h6>
							<p>Posted by <strong><a href="#">Fireform</a></strong> in <a href="#" class="clr-link">Landed</a><img class="ml-10" src="/img/layout/pencil.gif" alt=""></p>
						</div>
					</div>				
				</div>
			  </div>

			  <div class="shady bott-27"></div>

			<div class="col1-4 sidebar omega">
				<div class="heading">
					<?php echo $this->Element('vertical_ads_strip'); ?>
				</div>
			</div>

	 </section>
