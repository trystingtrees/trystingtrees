<div class="listingFinancials form">
<?php echo $this->Form->create('ListingFinancial');?>
	<fieldset>
		<legend><?php echo __('Edit Listing Financial'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('listing_id');
		echo $this->Form->input('price_amount');
		echo $this->Form->input('price_currency');
		echo $this->Form->input('price_period');
		echo $this->Form->input('tax_amount');
		echo $this->Form->input('tax_currency');
		echo $this->Form->input('tax_year');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit'));?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('ListingFinancial.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('ListingFinancial.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Listing Financials'), array('action' => 'index'));?></li>
		<li><?php echo $this->Html->link(__('List Listings'), array('controller' => 'listings', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Listing'), array('controller' => 'listings', 'action' => 'add')); ?> </li>
	</ul>
</div>
