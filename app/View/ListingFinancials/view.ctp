<div class="listingFinancials view">
<h2><?php  echo __('Listing Financial');?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($listingFinancial['ListingFinancial']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Listing'); ?></dt>
		<dd>
			<?php echo $this->Html->link($listingFinancial['Listing']['id'], array('controller' => 'listings', 'action' => 'view', $listingFinancial['Listing']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Price Amount'); ?></dt>
		<dd>
			<?php echo h($listingFinancial['ListingFinancial']['price_amount']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Price Currency'); ?></dt>
		<dd>
			<?php echo h($listingFinancial['ListingFinancial']['price_currency']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Price Period'); ?></dt>
		<dd>
			<?php echo h($listingFinancial['ListingFinancial']['price_period']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Tax Amount'); ?></dt>
		<dd>
			<?php echo h($listingFinancial['ListingFinancial']['tax_amount']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Tax Currency'); ?></dt>
		<dd>
			<?php echo h($listingFinancial['ListingFinancial']['tax_currency']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Tax Year'); ?></dt>
		<dd>
			<?php echo h($listingFinancial['ListingFinancial']['tax_year']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($listingFinancial['ListingFinancial']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($listingFinancial['ListingFinancial']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Listing Financial'), array('action' => 'edit', $listingFinancial['ListingFinancial']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Listing Financial'), array('action' => 'delete', $listingFinancial['ListingFinancial']['id']), null, __('Are you sure you want to delete # %s?', $listingFinancial['ListingFinancial']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Listing Financials'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Listing Financial'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Listings'), array('controller' => 'listings', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Listing'), array('controller' => 'listings', 'action' => 'add')); ?> </li>
	</ul>
</div>
