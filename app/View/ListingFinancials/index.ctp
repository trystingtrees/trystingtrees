<div class="listingFinancials index">
	<h2><?php echo __('Listing Financials');?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id');?></th>
			<th><?php echo $this->Paginator->sort('listing_id');?></th>
			<th><?php echo $this->Paginator->sort('price_amount');?></th>
			<th><?php echo $this->Paginator->sort('price_currency');?></th>
			<th><?php echo $this->Paginator->sort('price_period');?></th>
			<th><?php echo $this->Paginator->sort('tax_amount');?></th>
			<th><?php echo $this->Paginator->sort('tax_currency');?></th>
			<th><?php echo $this->Paginator->sort('tax_year');?></th>
			<th><?php echo $this->Paginator->sort('created');?></th>
			<th><?php echo $this->Paginator->sort('modified');?></th>
			<th class="actions"><?php echo __('Actions');?></th>
	</tr>
	<?php
	$i = 0;
	foreach ($listingFinancials as $listingFinancial): ?>
	<tr>
		<td><?php echo h($listingFinancial['ListingFinancial']['id']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($listingFinancial['Listing']['id'], array('controller' => 'listings', 'action' => 'view', $listingFinancial['Listing']['id'])); ?>
		</td>
		<td><?php echo h($listingFinancial['ListingFinancial']['price_amount']); ?>&nbsp;</td>
		<td><?php echo h($listingFinancial['ListingFinancial']['price_currency']); ?>&nbsp;</td>
		<td><?php echo h($listingFinancial['ListingFinancial']['price_period']); ?>&nbsp;</td>
		<td><?php echo h($listingFinancial['ListingFinancial']['tax_amount']); ?>&nbsp;</td>
		<td><?php echo h($listingFinancial['ListingFinancial']['tax_currency']); ?>&nbsp;</td>
		<td><?php echo h($listingFinancial['ListingFinancial']['tax_year']); ?>&nbsp;</td>
		<td><?php echo h($listingFinancial['ListingFinancial']['created']); ?>&nbsp;</td>
		<td><?php echo h($listingFinancial['ListingFinancial']['modified']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $listingFinancial['ListingFinancial']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $listingFinancial['ListingFinancial']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $listingFinancial['ListingFinancial']['id']), null, __('Are you sure you want to delete # %s?', $listingFinancial['ListingFinancial']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>

	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Listing Financial'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Listings'), array('controller' => 'listings', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Listing'), array('controller' => 'listings', 'action' => 'add')); ?> </li>
	</ul>
</div>
