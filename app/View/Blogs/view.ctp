<div class="left dashboard-main">
	<h1 style="margin-bottom: 0px; padding-bottom: 0px; line-height: 1em;"><?php echo $post['Post']['title']?></h1>

	<p><small>Created: <?php echo $post['Post']['created']?></small></p>

	<p><?php echo nl2br($post['Post']['body']); ?></p>
</div>

<!-- Tags -->
<div class="comment-tags">
	<h3>Tags</h3>
	<?php
		$tags = $post['Post']['tags'];
		$tags = explode(',', $tags);

		if ($tags == ''){
			echo 'There are no tags for this post';
		}else{
			foreach($tags as $tag){
				$tag = trim($tag);
				echo '<a href="' . $blog_base . '/tag/'. $tag .'" class="tag">' . $tag . '</a>';
			}
		}
	?>
</div>
<br />
<br />

<!-- Comments area -->
<div id="blog-comments">
	<h3>Leave a comment!</h3>
	<div class="comment-form">
	<?php
		echo $this->Form->create(false, array('id' => 'addCommentForm', 'class' => 'formify'));
		//echo $this->Form->label('name', 'Name (required)');
		//echo $this->Form->input('name', array('label' => '', 'class' => 'text-input'));
		//echo $this->Form->label('email', 'Email (will not be published / required)');
		//echo $this->Form->input('email', array('label' => '', 'class' => 'text-input'));
		echo $this->Form->textarea('message', array('label' => '', 'rows' => '6', 'cols' => '60', 'class' => '', 'style' => 'width: 95%;'));
		echo $this->Form->hidden('uuid');
		echo $this->Form->hidden('post_id');
		echo $this->Form->submit('Submit Comment', array('class' => 'btn left comment-submit', 'id' => 'addComment'));
		echo $this->Form->end();
	?>
	<div class="clear"></div>
	</div>

	<div class="clear"></div>
	<br />
	<hr></hr>
	<br />

	<?php
		if (empty($comments)){
	?>
		<h3>Showing 0 comments</h3>
	<?php
		}else{
			$num_comments = sizeof($comments);
			$s = ($num_comments > 1) ? 's' : '';
			echo '<h3>Showing ' . sizeof($comments) . ' comment' . $s . '</h3>';
			foreach($comments as $comment){
				$name = $comment['PostComment']['name'];
				$message = $comment['PostComment']['message'];
				$created = $comment['PostComment']['created'];
				$reply_to = $comment['PostComment']['reply_to'];
	?>
		<div class="comment-container">
			<div class="comment-header">
				<span class="commenter" id="commenter_<?php echo $comment['PostComment']['id']; ?>"><?php echo $name; ?></span>
				<span class="comment-elapsed-time"><?php echo $created; ?></span>
			</div>
			<div class="clear"></div>
			<div class="comment-body"><?php echo nl2br($message); ?></div>
			<div class="clear"></div>
			<div class="comment-actions">
				<div class="right">
					<a href="#" class="block-link">Like</a>
					<a href="#" class="replyToComment " id="comment_<?php echo $comment['PostComment']['id']; ?>">Reply</a>
				</div>
				<div class="reply-container left" style="display: none;"></div>
			</div>
		</div>
	<?php
			}
		}
	?>

</div>

<div id="reply_content" style="display: none;">
	<h3 style="font-size: 15px;">Replying to <span class="reply_name"></span></h3>
	<div class="comment-form">
	<?php
		echo $this->Form->create(false, array('id' => 'addCommentForm', 'class' => 'formify'));
		echo $this->Form->label('name', 'Name (required)');
		echo $this->Form->input('name', array('label' => '', 'class' => 'text-input'));
		echo $this->Form->label('email', 'Email (will not be published / required)');
		echo $this->Form->input('email', array('label' => '', 'class' => 'text-input'));
		echo $this->Form->textarea('message', array('label' => '', 'rows' => '6', 'cols' => '60', 'class' => 'text-input'));
		echo $this->Form->hidden('uuid');
		echo $this->Form->hidden('post_id');
		echo $this->Form->submit('Submit Comment', array('class' => 'btn left', 'id' => 'addComment'));
		echo $this->Form->end();
	?>
	</div>
</div>
