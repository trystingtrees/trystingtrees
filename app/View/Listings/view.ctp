<?php
	//JS and CSS
	echo '<!-- Page Specific CSS and JS -->' . "\n";
	/*
	echo $this->Html->css('lightbox') . "\n";
	echo $this->Html->script('prototype') . "\n";
	echo $this->Html->script('scriptaculous.js?load=effects,builder') . "\n";
	*/
	echo $this->Html->css('slimbox2') ."\n\n" ;
	echo $this->Html->script('slimbox2') ."\n\n" ;

	//pr($listing);
	//pr($agent_media);

	$price_period = null;

	$address = $listing['ListingLocation'][0]['address'];
	$city = $listing['ListingLocation'][0]['city'];
	$county = $listing['ListingLocation'][0]['county'];
	$postal_code = $listing['ListingLocation'][0]['postal_code'];
	$state_code = $state['State']['short_name'];
   $gmap_address = "{$address}, $city, $state_code, $postal_code";
		  
	$latitude = $listing['ListingLocation'][0]['latitude'];
	$longitude = $listing['ListingLocation'][0]['longitude'];

	//Sale Type -- Rental, Sale, Foreclosure, etc...
	$sale_type = $listing['ListingStatus']['name'];
	if ($sale_type == 'sale_pending' || $sale_type == 'for_sale') $sale_type = 'for_sale';

	//Price
	$price = $listing['ListingFinancial'][0]['price_amount'];
	$price = number_format($price, 2);
	$price_currency = $listing['ListingFinancial'][0]['price_currency'];

	if ($sale_type == 'for_rent'){
		$price_period = $listing['ListingFinancial'][0]['price_period'];
		$price_period = " / {$price_period}";
	}

	//Strip underscores and capitalize words
	$sale_type = str_replace('_', ' ', $sale_type);
	$sale_type = ucwords($sale_type);

	//Property info
	$mls_number = $listing['ListingMetaDetail'][0]['regional_mls_number'];
	$living_area = $listing['ListingFeature'][0]['living_area'];
	$living_area_units = $listing['ListingFeature'][0]['living_area_units'];
	$beds = $listing['ListingFeature'][0]['bedrooms'];
	$baths = $listing['ListingFeature'][0]['bathrooms'];

	if ($living_area_units == 'square feet') $living_area_units = 'Sq Ft';

	//Info
	$title = $listing['ListingDetail'][0]['title'];
	$details = $listing['ListingDetail'][0]['description'];
	
	$year_built = $listing['ListingFeature'][0]['year_built'];
	if (sizeof($year_built !== 4)) $year_built = null;	//Avoid showing, 0, 1, 2, etc...from xml parsing errors
	
	$garage_stalls = $listing['ListingFeature'][0]['garage_stalls'];
	$garage_type = $listing['ListingFeature'][0]['garage_style'];
	$garage_comments = $listing['ListingFeature'][0]['garage_comments'];

	//Agent
	$agent_uuid = $listing['ListingAgent']['uuid'];
	$agent_name = $listing['ListingAgent']['name'];
	$agent_cell_num = $listing['ListingAgent']['contact_cell'];
	$agent_office_num = $listing['ListingAgent']['contact_office_phone'];
	$agent_fax_num = $listing['ListingAgent']['contact_fax'];

	$agent_cell_num = ($agent_cell_num == '') ? 'n/a' : $agent_cell_num;
	$agent_office_num = ($agent_office_num == '') ? 'n/a' : $agent_office_num;
	$agent_fax_num = ($agent_fax_num == '') ? 'n/a' : $agent_fax_num;

?>

<section id="content">
	<div class="shady bott-27"></div>
	<div class="wrap640">
		<div class="inner clearfix">
			<div class="inner-t">
		  <article id="leftcol" class="left" style="width: 620px;">
        
        	<h3><?php echo $address; ?><br /><span class="location"><?php echo $city; echo (!empty($county) ? " ( {$county} )" : '') ?>, <?php echo $state_code; ?> <?php echo $postal_code; ?></span></h3>
            <p class="price"><?php echo $sale_type; ?> - <strong><?php echo $price . " {$price_currency} {$price_period}"; ?></strong></p>
            
            <p class="propinfo">
					<?php
						$prop_info_string = '';
						//Show baths / beds / sq ft
						if ($beds !== 0 && !empty($beds)){
							$prop_info_string .= "{$beds} br &nbsp;|&nbsp;";
						}

						if ($baths !== 0 && !empty($baths)){
							$prop_info_string .= "{$baths} ba &nbsp;|&nbsp;";
						}

						if ($living_area !== '' && !empty($living_area)){
							$prop_info_string .= "{$living_area} {$living_area_units} &nbsp;|&nbsp;";
						}

						if ($mls_number !== '' && !empty($mls_number)){
							$prop_info_string .= "MLS # {$mls_number} &nbsp;|&nbsp;";
						}

						echo $prop_info_string;
					?>
				</p>

				<br />
				<div class="box-type-1" style="width: 550px;">
					<a href="javascript:window.print();" class="btn">Print</a>
					<a href="#" class="btn">Follow Home</a>
					<a href="#" class="btn">Share</a>
					<a href="#" class="btn">Ask a Question</a>
					<a href="#" class="btn">Personal Note</a>
					<a href="#" class="btn">Mortgage Calculator</a>
				</div>
				<br />

				<p class="listing-image-gallery">
					<?php
						foreach ($listing['ListingMedia'] as $media):
							$id = $media['id'];
							$url = $media['media_url'];
							$caption = $media['caption'];
					?>
						<a href="<?php echo $url; ?>" style="float: left; margin: 5px;" rel="lightbox-listing-img" title="<?php echo $caption; ?>">
							<img style="height: 75px; width: 100px;" src="<?php echo $url; ?>" title="<?php echo $caption; ?>" />
						</a>
					<?php
						endforeach;
					?>
				</p>
				<div class="clear"></div>
				<br />
				<br />
            
            <div id="listing-content">
                
                <h3 class="divide">Property Information</h3>
                <p><?php echo nl2br($details); ?></p>
                
					<div class="clear"></div>
					<br />
					<br />
                <h3 class="divide">Property Features</h3>
					 <ul class="propfeatures left">
					 <?php //@TODO: Only showing garage information for now. Need to add other features ?>
					 	<?php if ($garage_stalls !== null && $garage_stalls !== ''){ ?>
					 	<li>
							<?php 
								echo ($garage_stalls > 1) ? $garage_stalls . ' garage stalls' : $garage_stalls . ' garage stall';
								echo " ( ";
								if ($garage_type !== '' && !is_null($garage_type)) echo $garage_type . " - ";
								echo " {$garage_comments} )";
							?>
						</li>
						<?php } ?>
					 </ul>
					
					 <div class="clear"></div>
					<br />
					<br />

                <h3 id="location" class="divide">Location</h3>
                <div id="map">
							<script src="http://maps.google.com/maps/api/js?sensor=true"></script>		
		<script>
        function setMapAddress(address) {
            var geocoder = new google.maps.Geocoder();
            geocoder.geocode( { address : address }, function( results, status ) {
                if( status == google.maps.GeocoderStatus.OK ) {
										var latlng = results[0].geometry.location;
					                    var options = {
                        zoom: 15,
                        center: latlng,
                        mapTypeId: google.maps.MapTypeId.ROADMAP, 
                        streetViewControl: true
                    };
                    var mymap = new google.maps.Map( document.getElementById( 'map' ), options );
                    var marker = new google.maps.Marker({
						map: mymap,
												position: results[0].geometry.location
											});
					
                }
            } );
        }
        //setMapAddress( "100 Harbor Dr #3902, San Diego, CA, 92101" );
        setMapAddress( "<?php echo $gmap_address; ?>" );
        </script>
        <div id="location">
            <div id="map">Loading...</div>
        </div>
                </div>
                
                            </div>

        </article>
        
        <div id="viewmore" class="right">
            <div class="left">
                <a href="http://wp.contempographicdesign.com/wp_real_estate_2/?post_type=listings&p=617" rel="prev"><span>Previous</span></a>            </div>
            <div class="right">
                <a href="http://wp.contempographicdesign.com/wp_real_estate_2/?post_type=listings&p=716" rel="next"><span>Next</span></a>            </div>
                <div class="clear"></div>
        </div>
        
		</div>
		</div>
	</div>
		  
	<div class="col1-4 sidebar omega" style="float: right; width: 320px">
		<div class="box-type-1"> 
		<aside id="author-widget" class="left">
			<?php 
				$img_url = $agent_media['AgentMedia']['media_url']; 
				if ($img_url == ''){
					$img_url = '/img/no_img_available.png';
				}
			?>
			<h5>Request more information</h5>
			<hr></hr>
        	
			<p class="left" id="author"><strong><a href="<?php echo $HOME; ?>/agents/view/<?php echo $agent_uuid; ?>"><?php echo $agent_name; ?></a></strong></p>
			<div class="clear"></div>
		 	<img id="authorimg" class="left" src="<?php echo $img_url; ?>" height="90" width="90" />
		 	<div id="authorinfo" class="left" style="margin-left: 10px;">
				<p id="tagline" style="display: none;"><strong>Luxury Real Estate Specialist</strong></p>
			  	<p>Mobile: <?php echo $agent_cell_num; ?></p>
			  	<p>Office: <?php echo $agent_office_num; ?></p>
			  	<p>Fax: <?php echo $agent_fax_num ?></p>
			</div>
		</aside>
		<div class="clear"></div>
		
		<hr></hr>

		<!-- Agent Quick Contact Form -->
		<?php echo $this->Element('view_listing_contact_agent'); ?>

		<div class="clear"></div>
		
		<hr></hr>

		<!-- Agent Other homes -->
		<h5>Other Listings by <?php echo $agent_name; ?></h5>
		<br />
		
		<img id="authorimg" class="left" src="<?php echo $img_url; ?>" height="90" width="90" />
		<div id="authorinfo" class="left" style="margin-left: 10px;">
			<p id="tagline" style="display: none;"><strong>Luxury Real Estate Specialist</strong></p>
			<p>1234 Montego Bay, California</p>
			<p>2 br | 1 ba</p>
			<p>$450,000</p>
		</div>
		<div class="clear"></div>
		<br />
		<br />
		
		<img id="authorimg" class="left" src="<?php echo $img_url; ?>" height="90" width="90" />
		<div id="authorinfo" class="left" style="margin-left: 10px;">
			<p id="tagline" style="display: none;"><strong>Luxury Real Estate Specialist</strong></p>
			<p>1234 Montego Bay, California</p>
			<p>2 br | 1 ba</p>
			<p>$450,000</p>
		</div>

			<div class="clear"></div>
		</div>
	</div>
</section>
