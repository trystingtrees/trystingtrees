<section id="content">
	<div class="shady bott-27"></div>
	<div class="inner clearfix">
		<div class="inner-t">
			<div class="heading">
				<h3>Top Listings in Your Area</h3>
			</div>
			<?php echo $this->Element('listings_streamer'); ?>
		</div>
	</div>
	<div class="shady bott-27"></div>
	
	<div class="col1-4 sidebar omega" style="height: 500px;">
			EXTENDED SEARCH
	</div>

	<!--
	<div class="wrap500" style="float: left;">
		<div class="inner clearfix">
			<div class="inner-t">
				TEST
			</div>
		</div>
	</div>
	-->
	
	<!-- Search -->
	<!--
	<div class="shady bott-27"></div>
	-->
	<div class="inner clearfix" style="display: none;">
		<div class="inner-t">
			 <div class="heading">
				 <h3>Find Your Next Home Right Here!</h3>
			 </div>
			<?php echo $this->Element('listings_search'); ?>
		</div>
	</div>
	
	<!-- Results -->
	<div class="shady bott-27"></div>
	<div class="wrap500" style="float: left;">
	<div class="inner clearfix">
		<div class="inner-t">
			
			<div style="margin-left: 10px;" class="heading">
				<h3><?php echo sizeof($listings); ?> results found</h3>
			</div>

			<?php foreach($listings as $listing): 
				//pr($listing);
				$listing_uuid = $listing['Listing']['uuid'];
				$city = $listing['ListingLocation'][0]['city'];
				$address = $listing['ListingLocation'][0]['address'];
				$state = '';
				$state = $listing['State']['short_name'];
				$postal_code = $listing['ListingLocation'][0]['postal_code'];
				$price = $listing['ListingFinancial'][0]['price_amount'];
				$agent_uuid = $listing['ListingAgent']['uuid'];
				$agent_id = $listing['ListingAgent']['id'];
				$agent_name = $listing['ListingAgent']['name'];
				$img_t_url = $listing['ListingMedia'][0]['thumb_url'];
				$img_i_url = $listing['ListingMedia'][0]['media_url'];
				$beds = $listing['ListingFeature'][0]['bedrooms'];
				$baths = $listing['ListingFeature'][0]['bathrooms'];
				$property_style = $listing['PropertyStyle']['name'];
				$property_style = str_replace('_', ' ', $property_style);

				//Number of pictures
				$num_pics = sizeof($listing['ListingMedia']);
			?>
				<article class="listing">
				  <div class="imgwrapmd left">
					<?php
						echo $this->Html->link(
							$this->Html->image( $img_i_url, array('width' => '278', 'height' => '163', 'class' => 'listing_result_img') ), 
							"/listings/view/{$listing_uuid}", 
							array('escape' => false)
						);
					?>

					<!-- pictures -->
					<div class="snipe-sm">
						<h6 class="open-house"><?php echo $num_pics; ?> pictures</h6>
					</div>
				 </div>

				<!-- Listings info -->
				<div class="listing-info">
					 <h5><?php echo $this->Html->link( $address, "/listings/view/{$listing_uuid}" ); ?></h5>
					 <p class="location"><?php echo "{$city}, {$state} {$postal_code}"; ?></p>
					 <p class="price">$<?php echo number_format($price, 2); ?> | 
					 <strong>Est. Mortgage:</strong> $<?php echo number_format((($price / 25) / 12) * 1.05, 2); ?></p>
					 <p class="propinfo">
						<?php
							//Show baths / beds / sq ft
							if ($beds !== 0 && !empty($beds)){
								echo "{$beds} br &nbsp;";
							}

							if ($baths !== 0 && !empty($baths)){
								echo "|&nbsp; {$baths} ba";
							}
						?>
					 </p>
					 <p class="proptype"><?php echo ( $property_style !== '' && !empty($property_style) ) ? ucfirst($property_style) : '--'; ?></p>
					 <p class="agent">Listing Agent: 
						<?php echo $this->Html->link( $agent_name, "/agents/view/{$agent_uuid}" ); ?>
					 </p>
				 </div>
				  <div class="clear"></div>
			  </article>
				<div class="shady bott-27"></div>
			<?php endforeach; ?>
			
			 </div>
		 </div>
	 </div>

	<div class="col1-4 sidebar omega">
		<div class="heading">
			<div class="box-type-1 ad-box">
				<div class="default-text"><h5>Your Ad Here</h5></div>
			</div>
			<div class="box-type-1 ad-box">
				<div class="default-text"><h5>Your Ad Here</h5></div>
			</div>
			<div class="box-type-1 ad-box">
				<div class="default-text"><h5>Your Ad Here</h5></div>
			</div>
			<div class="box-type-1 ad-box">
				<div class="default-text"><h5>Your Ad Here</h5></div>
			</div>
		</div>
		</div>
	</div>
</section>
