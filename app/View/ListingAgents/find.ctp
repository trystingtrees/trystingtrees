<?php
	$results = false;
	if (isset($agents) && isset($map_agents)){
		$results = true;
	}
?>

<section id="content">
	<div class="shady bott-27"></div>
	<div class="wrap720">
		<div class="inner clearfix">
			<div class="inner-t">
				<div class="heading">
					<h3>Find a Real Estate Agent</h3>
				</div>
				<fieldset class="form-box">
					<h4>Directory Search</h4>
					<?php
						echo $this->Form->create(false, array('type' => 'get', 'action' => '', 'class' => 'formify', 'id' => 'findAgentForm'));
						echo $this->Form->label('location');
						echo $this->Form->input('location', 
							array('class' => 'text-input autocomplete span6', 'label' => '', 'placeholder' => 'Enter a Street, Neighborhood or City'));
						echo $this->Form->label('keyword', 'Name or Keyword');
						echo $this->Form->input('keyword', array('class' => 'text-input span6', 'label' => '', 'placeholder' => 'Enter a Keyword, Tag or Name'));
						echo $this->Form->hidden('search', array('value' => 'true'));
						echo $this->Form->submit('Search', array('class' => 'btn'));
						echo $this->Form->end();
						echo '<br /><br />';
					?>
				</fieldset>

				<!--
				<fieldset class="form-box">
					<h4>Popular Cities</h4>
				</fieldset>

				<fieldset class="form-box">
					<h4>Popular Keyword Searches</h4>
				</fieldset>

				<fieldset class="form-box">
					<h4>Q&A Top Contributors</h4>
				</fieldset>
				-->
			</div>
		</div>
	</div>

	<div class="col1-4 sidebar omega">
		<div class="heading">
			<?php echo $this->Element('vertical_ads_strip'); ?>
		</div>
	</div>
</section>
