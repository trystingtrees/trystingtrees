<section id="content">
	<div class="shady bott-27"></div>
	<div class="wrap720">
		<div class="inner clearfix">
			<div class="inner-t">
				<div class="heading">
					<h3>Find a Real Estate Agent</h3>
				</div>
				<fieldset class="form-box">
					<h4>Directory Search</h4>
					<?php
						echo $this->Form->create('ListingAgent', array('type' => 'get', 'action' => 'find_agent', 'class' => 'formify'));
						echo $this->Form->label('location');
						echo $this->Form->input('location', array('class' => 'text-input autocomplete', 'label' => '', 'size' => '60'));
						echo $this->Form->label('keyword', 'Name or Keyword');
						echo $this->Form->input('keyword', array('class' => 'text-input', 'label' => '', 'size' => '60'));
						echo $this->Form->submit('Search', array('class' => 'btn'));
						echo $this->Form->end();
						echo '<br /><br />';
					?>
				</fieldset>

				<br />

				<!-- Results -->
				<?php if ($search_type == 'location'){ ?>
					<h4>Results for Real Estate Agents in "<?php echo $location; ?>"</h4>
				<?php }else{ ?>
					<h4>Results for Real Estate Agents. Keyword "<?php echo $keyword; ?>"</h4>
				<?php } ?>

				<div class="team">
				<?php
					foreach($map_agents as $row){
						foreach($row as $rs){
							$agent_id = $rs['ListingAgent']['id'];
							$agent_name = $rs['ListingAgent']['name'];
							$agent_city = $rs['ListingAgent']['city'];
							$picture_file = $rs['ListingAgent']['picture_file_name'];

							if (empty($picture_file) || is_null($picture_file)) {
								$picture_file = Configure::read('DEFAULT_PIC_URL');
							}else{
								$picture_file = Configure::read('PROFILE_PICS_BASE_URL') . DS . $rs['ListingAgent']['uuid'] . DS . $picture_file;
							}
				?>
					<!-- Start Agent Display -->
					<div class="item-holder1">
						<div class="proj-img1">
							<img class="o-t" src="<?php echo $picture_file; ?>" width="50" alt="" />
						</div>
						<div class="descr">
							<h5><?php echo $agent_name; ?></h5>
							<p class="clr">TAGLINE</p>
							<p>DESCRIPTION</p>
						</div>
						<div class="team-social">
							<a href="#" class="button-t-s"><span><img src="/img/layout/skype-icn.png" alt=""></span></a>
							<a href="#" class="button-t-s"><span><img src="/img/layout/twitter-icn.png" alt=""></span></a>
							<a href="#" class="button-t-s"><span><img src="/img/layout/facebook-icn.png" alt=""></span></a>
						</div>
					</div>

					<div class="result_row" style="margin-bottom: 10px; border-bottom: 1px solid black;">
						<span class="agent_name"><?php echo $agent_name; ?></span>
						<div class="clear"></div>
						<div class="left" style="margin-right: 10px;">
							<img src="<?php echo $picture_file; ?>" width="50" />
						</div>
						<div class="left">
							<div class="left"><?php echo 'agent tagline'; ?></div>
							<div class="clear"></div>
							<div class="left">
								Expert In:
								<?php
									$areas = '';
									foreach($rs['Areas'] as $area){
										$areas .= '<a href="#" class="view_area_map_popup">' . $area['Map']['nice_name'] . '</a> | ';
									}
									$areas = rtrim($areas, ' | ');
									echo $areas;
								?>
							</div>
						</div>
						<div class="clear"></div>
					</div>
					<div class="clear"></div>
					<!-- End Agent Display -->
				<?php
						}
					}
					//Display regular agents
					//@TODO: Move profile pic creation to a helper function
					//@TODO: Add snippet to about me. Shorten content if too long
					if (!empty($agents) && sizeof($agents) > 0){
						$default_pic = Configure::read('DEFAULT_PROFILE_PIC_URL');
						foreach($agents as $agent){
							$agent_id = $agent['ListingAgent']['id'];
							$about_me = $agent['ListingAgent']['about'];
							$agent_name = $agent['ListingAgent']['name'];
							$agent_city = $agent['ListingAgent']['city'];
							$uuid = $agent['ListingAgent']['uuid'];
							$picture_file = $agent['ListingAgent']['picture_file_name'] == '' 
								? $default_pic 
								//: AppControls->build_profile_pic_path($agent['ListingAgent']['uuid'], $agent['ListingAgent']['picture_file_name']);
								: Configure::read('PROFILE_PICS_BASE_URL') . DS . $uuid . DS . $agent['ListingAgent']['picture_file_name'];

							$about_me = ($about_me == '') ? __('This agent has no description') : $about_me;
				?>
					<!-- Start Agent Display -->
					<div class="item-holder1">
						<div class="proj-img1">
							<img class="o-t" src="<?php echo $picture_file; ?>" alt="" style="width: 100%; height: 120px;" />
						</div>
						<div class="descr">
							<h5><?php echo $agent_name; ?></h5>
							<p class="clr"><?php echo $about_me; ?></p>
						</div>
						<div class="team-social">
							<a href="/agents/view/<?php echo $uuid; ?>" class="btn" style="margin-top: 15px;">View Agent's Page</a>
						</div>
						<div class="team-social" style="display: none;">
							<a href="#" class="button-t-s"><span><img src="/img/layout/skype-icn.png" alt=""></span></a>
							<a href="#" class="button-t-s"><span><img src="/img/layout/twitter-icn.png" alt=""></span></a>
							<a href="#" class="button-t-s"><span><img src="/img/layout/facebook-icn.png" alt=""></span></a>
						</div>
					</div>
					<!-- End Agent Display -->
				<?php
						}
					}
				?>
				</div>
			</div>
		</div>
	</div>
</section>
