<?php
	//Options for states
	$state_options = $states;
	$country_options = $countries;

	foreach($states as $state){
	}
?>

<div class="left dashboard-main">
	<br />
	<h2>My Profile</h2>

	<?php
		//Profile picture
		echo $this->Html->image($picture_url, array('width' => 100, 'class' => 'left', 'id' => 'authorimg'));
		echo '<br />';

		//Profile update form
		echo $this->Form->create('ListingAgent', array('action' => 'edit', 'enctype' => 'multipart/form'));
		echo '<div class="form-row">';
		echo $this->Form->label('picture_file_name', 'Update Profile Picture');
		echo $this->Form->file('picture_file_name');
		echo '</div>';
		
		echo '<div class="form-row">';
		echo $this->Form->input('name', array('size' => '50', 'maxlength' => '255'));
		echo '</div>';
		
		echo '<div class="form-row">';
		echo $this->Form->input('tagline', array('size' => '50', 'maxlength' => '255'));
		echo '</div>';
		
		echo '<div class="form-row">';
		echo $this->Form->input('contact_cell', array('size' => '50', 'maxlength' => '255'));
		echo '</div>';
		
		echo '<div class="form-row">';
		echo $this->Form->input('contact_office_phone', array('size' => '50', 'maxlength' => '255'));
		echo '</div>';
		
		echo '<div class="form-row">';
		echo $this->Form->input('contact_fax', array('size' => '50', 'maxlength' => '255'));
		echo '</div>';
		
		echo '<div class="form-row">';
		echo $this->Form->input('city', array('size' => '50', 'maxlength' => '255'));
		echo '</div>';
		
		echo '<div class="form-row">';
		echo $this->Form->label('state_id', 'State');
		echo $this->Form->select('state_id', $state_options);
		echo '</div>';
		
		echo '<div class="form-row">';
		echo $this->Form->label('country_id', 'Country');
		echo $this->Form->select('country_id', $country_options);
		echo '</div>';
		
		echo '<div class="form-row">';
		echo $this->Form->input('website', array('size' => '50', 'maxlength' => '255'));
		echo '</div>';
		
		echo '<div class="form-row">';
		echo $this->Form->input('twitter', array('size' => '50', 'maxlength' => '255'));
		echo '</div>';
		
		echo '<div class="form-row">';
		echo $this->Form->input('facebook', array('size' => '50', 'maxlength' => '255'));
		echo '</div>';
		
		echo '<div class="form-row">';
		echo $this->Form->input('linkedin', array('size' => '50', 'maxlength' => '255'));
		echo '</div>';
		
		echo '<div class="form-row">';
		echo $this->Form->input('youtube', array('size' => '50', 'maxlength' => '255'));
		echo '</div>';
		
		echo '<div class="form-row">';
		echo $this->Form->label('My Experience');
		echo $this->Form->textarea('experience', array('cols' => '50', 'rows' => '8'));
		echo '</div>';
		
		echo '<div class="form-row">';
		echo $this->Form->label('My Specialties');
		echo $this->Form->textarea('specialties', array('cols' => '50', 'rows' => '8'));
		echo '</div>';
		
		echo '<div class="form-row">';
		echo $this->Form->label('My Certifications and Awards');
		echo $this->Form->textarea('certifications_awards', array('cols' => '50', 'rows' => '8'));
		echo '</div>';
		
		echo '<div class="form-row">';
		echo $this->Form->label('My Interests');
		echo $this->Form->textarea('interests', array('cols' => '50', 'rows' => '8'));
		echo '</div>';
		
		echo '<div class="form-row">';
		echo $this->Form->label('About Me');
		echo $this->Form->textarea('about', array('cols' => '50', 'rows' => '8'));
		echo '</div>';
		
		echo '<div class="form-row">';
		echo $this->Form->button('Update Profile', array('class' => 'form-button'));
		echo '</div>';
		echo $this->Form->end();
	?>
</div>
