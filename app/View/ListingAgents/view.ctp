<?php
	//Agent
	$agent_uuid = $listingAgent['ListingAgent']['uuid'];
	$agent_name = $listingAgent['ListingAgent']['name'];
	$agent_cell_num = $listingAgent['ListingAgent']['contact_cell'];
	$agent_office_num = $listingAgent['ListingAgent']['contact_office_phone'];
	$agent_fax_num = $listingAgent['ListingAgent']['contact_fax'];

	$agent_cell_num = ($agent_cell_num == '') ? 'n/a' : $agent_cell_num;
	$agent_office_num = ($agent_office_num == '') ? 'n/a' : $agent_office_num;
	$agent_fax_num = ($agent_fax_num == '') ? 'n/a' : $agent_fax_num;

	$img_url = '';

	if ($listingAgent['ListingAgent']['picture_file_name'] !== ''){
		$img_url = $listingAgent['ListingAgent']['picture_file_name'];
		$img_url = Configure::read('PROFILE_PICS_BASE_URL') . DS . $agent_uuid . DS . $img_url;
	}

	if (isset($listingAgent['AgentMedia'][0]) && $img_url == ''){
		$img_url = $listingAgent['AgentMedia'][0]['media_url']; 
	}

	if ($img_url == ''){
		$img_url = '/img/no_img_available.png';
	}

	//@TODO: Completed company info, tagline and description
	$office_name = '';
	$office_phone = '';
	$tagline = $listingAgent['ListingAgent']['tagline'];
	$descr = '';

	//Format telephone numbers
	$agent_cell_num = $this->Formatter->format_telephone($agent_cell_num);
	$agent_office_num = $this->Formatter->format_telephone($agent_office_num);
	$agent_fax_num = $this->Formatter->format_telephone($agent_fax_num);
?>

<section id="content">
	<div class="shady bott-27"></div>
	<div class="wrap720">
		<div class="inner clearfix">
			<div class="inner-t">
				<!-- Profile Views -->
				<div style="float: right;">
					<img src="/img/eye_icon.png" width="50" class="left" style="margin-top: -10px;" /> 
					<span class="left" style="margin-top: 8px;"><strong>25</strong> Views</span>
				</div>

				<img id="authorimg" class="left" src="<?php echo $img_url; ?>" height="100" width="100" style="margin-right: 20px;" />
				<h1><?php echo $agent_name; ?></h1>
				<p id="tagline"><strong><?php echo $tagline; ?></strong></p>
				<div class="contact-info left">
					<p><strong>Mobile:</strong> <?php echo $agent_cell_num; ?> 
					| <strong>Office:</strong> <?php echo $agent_office_num; ?> 
					| <strong>Fax:</strong> <?php echo $agent_fax_num ?></p>
				</div>
	
				<div class="clear"></div>
				<br />
				<br />

				<div class="agent_about" style="padding: 5px 15px; border-left: 1px solid #EEE; width: 500px;">
					<?php
						if (isset($listingAgent['ListingAgent']['about'])){
							echo $listingAgent['ListingAgent']['about'];
						}else{
							echo __('No description on file yet for this agent');
						}
					?>
				</div>
				<br />
				<br />

				<!-- Expert Areas -->
				<div class="box-type-1">
					<h5>Expert Areas</h5>
					<div class="mini-content">
						<?php
							if (sizeof($listingAgent['Maps'])> 0){
								foreach($listingAgent['Maps'] as $map){
									if (empty($map)) continue;
									$map_name = $map['Map']['nice_name'] . '( ' . $map['Map']['city'] . ' )';
									echo '<a href="#" class="tag2" style="float: left;">' . $map_name . '</a>';
								}
							}
						?>
						<div class="clear"></div>
					</div>
				</div>
				<br />
				<br />

				<!-- Links -->
				<div class="box-type-1">
					<h5>Links</h5>
					<div class="mini-content">
						<strong>Personal: </strong> http://wwww.trystingtrees.com/agent/agent2 &nbsp;&nbsp;&nbsp; <a href="#">Bookmnark Link!</a>
						<br />
						<br />
						<br />
						<strong>Blog: </strong> http://wwww.trystingtrees.com/blogs/agent2 &nbsp;&nbsp;&nbsp; <a href="#">Bookmnark Link!</a>
						<br />
					</div>
				</div>
				<br />
				<br />

				<!-- Social -->
				<div class="box-type-1">
					<h5>Connect with <?php echo $agent_name; ?></h5>
					<div class="mini-content">
						<div class="agent-connect-bar">
							<!-- Facebook -->
							<?php if (isset($listingAgent['ListingAgent']['facebook'])){ ?>
							<a href="<?php echo $this->Social->get_social_profile_url('facebook', $listingAgent['ListingAgent']['facebook']); ?>">
								<img src="/img/icons/small/facebook.png" />
							</a>
							<?php } ?>
							
							<!-- Twitter -->
							<?php if (isset($listingAgent['ListingAgent']['twitter'])){ ?>
							<a href="<?php echo $this->Social->get_social_profile_url('twitter', $listingAgent['ListingAgent']['twitter']); ?>">
								<img src="/img/icons/small/twitter.png" />
							</a>
							<?php } ?>
							
							<!-- LinkedIn -->
							<?php if (isset($listingAgent['ListingAgent']['linkedin'])){ ?>
							<a href="<?php echo $this->Social->get_social_profile_url('linkedin', $listingAgent['ListingAgent']['linkedin']); ?>">
								<img src="/img/icons/small/linkedin.png" />
							</a>
							<?php } ?>
							
							<!-- Google+ -->
							<?php if (isset($listingAgent['ListingAgent']['googleplus'])){ ?>
							<a href="<?php echo $this->Social->get_social_profile_url('googleplus', $listingAgent['ListingAgent']['googleplus']); ?>">
								<img src="/img/icons/small/google+.png" />
							</a>
							<?php } ?>
							
							<!-- Youtube -->
							<?php if (isset($listingAgent['ListingAgent']['youtube'])){ ?>
							<a href="<?php echo $this->Social->get_social_profile_url('youtube', $listingAgent['ListingAgent']['youtube']); ?>">
								<img src="/img/icons/small/youtube.png" />
							</a>
							<?php } ?>

							<!-- Blog RSS -->
							<a href=""><img src="/img/icons/small/rss.png" /></a>
							<div class="clear"></div>
						</div>
					</div>
				</div>
				<br />

				<div class="contact-info left">
					<p><strong><?php echo $office_name; ?></strong></p>
					<p><?php echo $office_phone; ?></p>
				</div>
				<div class="clear"></div>
				<p id="bio"><?php echo $descr; ?></p>
			</div>
		</div>
	</div>

	<div class="col1-4 sidebar omega">
		<div class="heading">
			<h5>Agent Stats</h5>
		</div>
		<div>
			<ul class="agent-tags">
				<li class="tag-bg">10 Listings</li>
				<li class="tag-bg">5,6700 Answers</li>
				<li class="tag-bg">2,0000 Useful Answers</li>
				<li class="tag-bg">1,5900 Blog Posts</li>
				<li class="tag-bg">3,4000 Recommendations</li>
			</ul>
		</div>
	</div>

</section>
