<div class="listingAgents form">
<?php echo $this->Form->create('ListingAgent');?>
	<fieldset>
		<legend><?php echo __('Add Listing Agent'); ?></legend>
	<?php
		echo $this->Form->input('user_id');
		echo $this->Form->input('uuid');
		echo $this->Form->input('src_agent_uid');
		echo $this->Form->input('src_office_uid');
		echo $this->Form->input('name');
		echo $this->Form->input('contact_cell');
		echo $this->Form->input('contact_office_phone');
		echo $this->Form->input('contact_fax');
		echo $this->Form->input('website');
		echo $this->Form->input('twitter');
		echo $this->Form->input('facebook');
		echo $this->Form->input('linkedin');
		echo $this->Form->input('youtube');
		echo $this->Form->input('created_at');
		echo $this->Form->input('updated_at');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit'));?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Listing Agents'), array('action' => 'index'));?></li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
	</ul>
</div>
