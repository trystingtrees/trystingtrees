<h1>Your selected maps</h1>
<table>
    <tr>
        <th>Area Name</th>
        <th>Actions</th>
        <th>Selected On</th>
    </tr>

    <!-- Here is where we loop through our $posts array, printing out post info -->

    <?php foreach ($maps as $map): ?>
    <tr>
        <td><?php echo $map['Map']['name']; ?></td>
        <td>
            <?php echo $this->Html->link('View', array('controller' => 'maps', 'action' => 'view', $map['Map']['id'])); ?> | 
            <?php echo $this->Html->link('View', array('controller' => 'areas', 'action' => 'unselect', $map['Map']['id'])); ?>
        </td>
        <td><?php echo substr($map['Map']['created'], 0, 11); ?></td>
    </tr>
    <?php endforeach; ?>

</table>
