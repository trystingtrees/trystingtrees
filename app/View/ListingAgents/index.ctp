<div class="listingAgents index">
	<h2><?php echo __('Listing Agents');?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id');?></th>
			<th><?php echo $this->Paginator->sort('user_id');?></th>
			<th><?php echo $this->Paginator->sort('uuid');?></th>
			<th><?php echo $this->Paginator->sort('src_agent_uid');?></th>
			<th><?php echo $this->Paginator->sort('src_office_uid');?></th>
			<th><?php echo $this->Paginator->sort('name');?></th>
			<th><?php echo $this->Paginator->sort('contact_cell');?></th>
			<th><?php echo $this->Paginator->sort('contact_office_phone');?></th>
			<th><?php echo $this->Paginator->sort('contact_fax');?></th>
			<th><?php echo $this->Paginator->sort('website');?></th>
			<th><?php echo $this->Paginator->sort('twitter');?></th>
			<th><?php echo $this->Paginator->sort('facebook');?></th>
			<th><?php echo $this->Paginator->sort('linkedin');?></th>
			<th><?php echo $this->Paginator->sort('youtube');?></th>
			<th><?php echo $this->Paginator->sort('created_at');?></th>
			<th><?php echo $this->Paginator->sort('updated_at');?></th>
			<th class="actions"><?php echo __('Actions');?></th>
	</tr>
	<?php
	$i = 0;
	foreach ($listingAgents as $listingAgent): ?>
	<tr>
		<td><?php echo h($listingAgent['ListingAgent']['id']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($listingAgent['User']['id'], array('controller' => 'users', 'action' => 'view', $listingAgent['User']['id'])); ?>
		</td>
		<td><?php echo h($listingAgent['ListingAgent']['uuid']); ?>&nbsp;</td>
		<td><?php echo h($listingAgent['ListingAgent']['src_agent_uid']); ?>&nbsp;</td>
		<td><?php echo h($listingAgent['ListingAgent']['src_office_uid']); ?>&nbsp;</td>
		<td><?php echo h($listingAgent['ListingAgent']['name']); ?>&nbsp;</td>
		<td><?php echo h($listingAgent['ListingAgent']['contact_cell']); ?>&nbsp;</td>
		<td><?php echo h($listingAgent['ListingAgent']['contact_office_phone']); ?>&nbsp;</td>
		<td><?php echo h($listingAgent['ListingAgent']['contact_fax']); ?>&nbsp;</td>
		<td><?php echo h($listingAgent['ListingAgent']['website']); ?>&nbsp;</td>
		<td><?php echo h($listingAgent['ListingAgent']['twitter']); ?>&nbsp;</td>
		<td><?php echo h($listingAgent['ListingAgent']['facebook']); ?>&nbsp;</td>
		<td><?php echo h($listingAgent['ListingAgent']['linkedin']); ?>&nbsp;</td>
		<td><?php echo h($listingAgent['ListingAgent']['youtube']); ?>&nbsp;</td>
		<td><?php echo h($listingAgent['ListingAgent']['created_at']); ?>&nbsp;</td>
		<td><?php echo h($listingAgent['ListingAgent']['updated_at']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $listingAgent['ListingAgent']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $listingAgent['ListingAgent']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $listingAgent['ListingAgent']['id']), null, __('Are you sure you want to delete # %s?', $listingAgent['ListingAgent']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>

	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Listing Agent'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
	</ul>
</div>
