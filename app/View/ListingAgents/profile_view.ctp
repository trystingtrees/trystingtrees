<?php
	//Agent
	//pr($listings);
	$agent_uuid = $agent['ListingAgent']['uuid'];
	$agent_name = $agent['ListingAgent']['name'];
	$agent_cell_num = $agent['ListingAgent']['contact_cell'];
	$agent_office_num = $agent['ListingAgent']['contact_office_phone'];
	$agent_fax_num = $agent['ListingAgent']['contact_fax'];

	$agent_cell_num = ($agent_cell_num == '') ? 'n/a' : $agent_cell_num;
	$agent_office_num = ($agent_office_num == '') ? 'n/a' : $agent_office_num;
	$agent_fax_num = ($agent_fax_num == '') ? 'n/a' : $agent_fax_num;

	$img_url = '';

	if (isset($agent['AgentMedia'][0])){
		$img_url = $agent['AgentMedia'][0]['media_url']; 
	}
	if ($img_url == ''){
		$img_url = '/img/no_img_available.png';
	}

	//@TODO: Completed company info, tagline and description
	$office_name = '';
	$office_phone = '';
	$tagline = 'Real Estate Agent';
	$descr = '';
?>
    <div id="leftcol" class="left">
    	        
        <div id="author-info">
                <img id="authorimg" class="left" src="<?php echo $HOME . $picture_url; ?>" height="100" width="100" />
					 <h1>
					 	<?php echo $agent_name; ?> 
						<?php if ($edit){ ?>
						 	<span class="right"><a href="<?php echo $HOME; ?>/profile/edit">Edit My Profile</a></span>
						<?php } ?>
					</h1>
					 <p id="tagline"><strong><?php echo $tagline; ?></strong></p>
					 <div class="contact-info left">
						  <p>Mobile: <?php echo $agent_cell_num; ?></p>
						  <p>Office: <?php echo $agent_office_num; ?></p>
						  <p>Fax: <?php echo $agent_fax_num ?></p>
					</div>
					 <div class="contact-info left">
						  <p><strong><?php echo $office_name; ?></strong></p>
						  <p><?php echo $office_phone; ?></p>
					</div>
            	<div class="clear"></div>
            <p id="bio"><?php echo $descr; ?></p>
			</div>
		        
			<?php 
			if (!empty($listings)){
				foreach($listings as $listing):
					$listing_uuid = $listing['Listing']['uuid'];
					$city = $listing['ListingLocation'][0]['city'];
					$address = $listing['ListingLocation'][0]['address'];
					$state = '';
					$state = $listing['State']['short_name'];
					$postal_code = $listing['ListingLocation'][0]['postal_code'];
					$price = $listing['ListingFinancial'][0]['price_amount'];
					$agent_uuid = $listing['ListingAgent']['uuid'];
					$agent_id = $listing['ListingAgent']['id'];
					$agent_name = $listing['ListingAgent']['name'];
					$img_t_url = $listing['ListingMedia'][0]['thumb_url'];
					$img_i_url = $listing['ListingMedia'][0]['media_url'];
					$beds = $listing['ListingFeature'][0]['bedrooms'];
					$baths = $listing['ListingFeature'][0]['bathrooms'];
					$property_style = $listing['PropertyStyle']['name'];
					$property_style = str_replace('_', ' ', $property_style);

					//Number of pictures
					$num_pics = sizeof($listing['ListingMedia']);
			?>	
				<article class="listing">
			  <div class="imgwrapmd left">
				<?php
					echo $this->Html->link(
						$this->Html->image( $img_i_url, array('width' => '278', 'height' => '163', 'class' => 'med imgfade') ), 
						"/listings/view/{$listing_uuid}", 
						array('escape' => false)
					);
				?>
				<div class="snipe-sm">
					<h6 class="open-house"><?php echo $num_pics; ?> pictures</h6>
				</div>
			 </div>
				  <h2><?php echo $this->Html->link( $address, "/listings/view/{$listing_uuid}" ); ?></h2>
			 <p class="location"><?php echo "{$city}, {$state} {$postal_code}"; ?></p>
			 <p class="price"><strong>$<?php echo number_format($price, 2); ?></strong></p>
			 <p class="propinfo">
				<?php
					//Show baths / beds / sq ft
					if ($beds !== 0 && !empty($beds)){
						echo "{$beds} br &nbsp;";
					}

					if ($baths !== 0 && !empty($baths)){
						echo "|&nbsp; {$baths} ba";
					}
				?>
			 </p>
			 <p class="proptype"><?php echo ( $property_style !== '' && !empty($property_style) ) ? $property_style : '--'; ?></p>
			 <p class="agent">Listing Agent: 
				<?php echo $this->Html->link( $agent_name, "/agents/view/{$agent_uuid}" ); ?>
			 </p>
			  <div class="clear"></div>
		  </article>
			<?php 
				endforeach; 
			}else{
				echo 'This agent currently has no listings';
			}
			?>
    </div>
    
    <div id="sidebar" class="right">
	  	
			<!-- Twitter -->
    <script src="http://wp.contempographicdesign.com/wp_real_estate_2/wp-content/themes/realestate_2/js/jquery.tweet.js" type="text/javascript"></script>
    <script type="text/javascript">
	jQuery(document).ready(function(){
		jQuery("#twitter").tweet({
			join_text: "auto",
			username: "contempoinc",
			count: 3, //amount of tweets to display
			loading_text: "loading tweets..."
		});
	});
	</script> 
	<div class="widget left onethirdcol">
        <h4>Latest From Twitter</h4>
        <div id="twitter"></div>
    </div>
    
        <div class="clear"></div>

	<div id="flickr" class="widget left onethirdcol" style="display: none;">
        <h4>Latest On Flickr</h4>
        <script type="text/javascript" src="http://www.flickr.com/badge_code_v2.gne?count=6&amp;display=latest&amp;size=s&amp;layout=x&amp;source=user&amp;user=8689981@N07"></script> 
        <div class="clear"></div>
    </div>
    
        <div class="clear"></div>

<aside id="text-4" class="widget widget_text left" style="display: none;">
	<h4>Plain Text Widget</h4>			<div class="textwidget"><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque pretium, ipsum eu placerat viverra, orci urna convallis diam, non eleifend nisi turpis non ligula. Sed semper, massa sed tristique malesuada, est sapien sollicitudin tellus, accumsan laoreet nisl arcu at eros.</p>
</div>
		</aside><div class="clear"></div><aside id="propsearch" class="widget left">
<form id="advanced_search" name="property-search" action="http://wp.contempographicdesign.com/wp_real_estate_2">
    <h4>Find your new home</h4>
    <p>5  Real Estate Listings, Homes for Sale &amp; Rental Properties.</p>

    <div id="prop-type" class="left">
        <label for="ct_type">Type</label>
        	<select id="ct_property_type" name="ct_property_type">
		<option value="0">Any</option>
								<option value="condotownhome">Condo/Townhome</option>
								<option value="land">Land</option>
								<option value="single-family-home">Single Family Home</option>
			</select>
	    </div>
    
        <div class="clear"></div>
    
    <div id="beds" class="left">
        <label for="ct_beds">Beds</label>
        	<select id="ct_beds" name="ct_beds">
		<option value="0">Any</option>
								<option value="2">2</option>
								<option value="3">3</option>
								<option value="4">4</option>
			</select>
	    </div>
    
    <div id="baths" class="left">
        <label for="ct_baths">Baths</label>
        	<select id="ct_baths" name="ct_baths">
		<option value="0">Any</option>
								<option value="1-5">1.5</option>
								<option value="2">2</option>
								<option value="3">3</option>
								<option value="6">6</option>
			</select>
	    </div>
    
    <div id="city" class="left">
        <label for="ct_city">City</label>
        	<select id="ct_city" name="ct_city">
		<option value="0">Any</option>
								<option value="alpine">Alpine</option>
								<option value="del-mar">Del Mar</option>
								<option value="san-diego">San Diego</option>
			</select>
	    </div>
    
    <div id="state" class="left">
        <label for="ct_state">State</label>
        	<select id="ct_state" name="ct_state">
		<option value="0">Any</option>
								<option value="ca">CA</option>
			</select>
	    </div>
    
    <div id="zipcode" class="left">
        <label for="ct_zipcode">Zipcode</label>
        	<select id="ct_zipcode" name="ct_zipcode">
		<option value="0">Any</option>
								<option value="91901">91901</option>
								<option value="92014">92014</option>
								<option value="92101">92101</option>
			</select>
	    </div>
    
    <div id="status" class="left">
        <label for="ct_status">Status</label>
        	<select id="ct_status" name="ct_status">
		<option value="0">Any</option>
								<option value="active">Active</option>
								<option value="featured">Featured</option>
								<option value="for-rent">For Rent</option>
								<option value="for-sale">For Sale</option>
								<option value="open-house">Open House</option>
								<option value="reduced">Reduced</option>
								<option value="rental">Rental</option>
								<option value="sold">Sold</option>
			</select>
	    </div>
    
    <div id="addfeat" class="left">
        <label for="ct_additional_features">Addtional Features</label>
        	<select id="ct_additional_features" name="ct_additional_features">
		<option value="0">Any</option>
								<option value="1-car-garage">1 car garage</option>
								<option value="1-total-half-bath">1 total half bath</option>
								<option value="2-stories">2 stories</option>
								<option value="3-car-garages">3 car garage(s)</option>
								<option value="3-fireplaces">3 fireplaces</option>
								<option value="area-coastal-north">Area: Coastal North</option>
								<option value="area-metro-central">Area: Metro Central</option>
								<option value="automatic-security-gate">Automatic Security Gate</option>
								<option value="city-view">City view</option>
								<option value="closed-circuit-security-tv">Closed Circuit Security TV</option>
								<option value="community-clubhouses">Community clubhouse(s)</option>
								<option value="community-exercise-areas">Community exercise area(s)</option>
								<option value="community-security-features">Community security features</option>
								<option value="community-spahot-tubs">Community spa/hot tub(s)</option>
								<option value="complex-name-the-legend">Complex Name: The Legend</option>
								<option value="concierge">Concierge</option>
								<option value="county-san-diego">County: San Diego</option>
								<option value="den">Den</option>
								<option value="dining-room-is-20x12">Dining room is 20x12</option>
								<option value="dining-room-is-20x20">Dining room is 20x20</option>
								<option value="family-room">Family room</option>
								<option value="family-room-is-20x12">Family room is 20x12</option>
								<option value="family-room-is-28x20">Family room is 28x20</option>
								<option value="forced-air-heat">Forced air heat</option>
								<option value="kitchen-is-11x8">Kitchen is 11x8</option>
								<option value="kitchen-is-20x14">Kitchen is 20x14</option>
								<option value="living-room-is-20x12">Living room is 20x12</option>
								<option value="living-room-is-20x20">Living room is 20x20</option>
								<option value="master-bedroom-is-12x11">Master bedroom is 12x11</option>
								<option value="master-bedroom-is-20x14">Master Bedroom is 20x14</option>
								<option value="ocean-view">Ocean view</option>
								<option value="on-site-guard">On Site Guard</option>
								<option value="roofing-concrete">Roofing: Concrete</option>
								<option value="single-story">Single story</option>
								<option value="spahot-tubs">Spa/hot tub(s)</option>
								<option value="style-contemporary">Style: Contemporary</option>
								<option value="subdivision-olde-del-mar">Subdivision: Olde Del Mar</option>
								<option value="topography-bluffcanyon-rim">Topography: Bluff/Canyon Rim</option>
								<option value="type-detached">Type: Detached</option>
								<option value="view-evening-lights">View: Evening Lights</option>
								<option value="water-view">Water view</option>
								<option value="year-built-2007">Year Built: 2007</option>
			</select>
	    </div>
    
        <div id="price-from" class="left">
        <label for="ct_price_from">Price From ($)</label>
        <input type="text" id="ct_price_from" class="number" name="ct_price_from" size="8" />
    </div>    

    <div id="price-to" class="left">
        <label for="ct_price_to">Price To ($)</label>
        <input type="text" id="ct_price_to" class="number" name="ct_price_to" size="8" />
    </div>
        
    <input type="hidden" name="property-search" value="true" />
    <input id="submit" class="btn right" type="submit" value="Search" />
    
        <div class="clear"></div>
</form>
</aside>
	
	</div>
