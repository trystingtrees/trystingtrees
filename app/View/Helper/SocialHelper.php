<?php
class SocialHelper extends AppHelper {
	public function get_social_profile_url($service, $username){
		switch($service){
			case 'facebook':
				return 'http://www.facebook.com/' . $username;
				break;
			case 'twitter':
				//Remove the @ sign if it got in there somewhow
				if ($username[0] == '@') $username = substr($username, 1);
				return 'https://twitter.com/#!/' . $username;
				break;
			case 'googleplus':
				return 'https://plus.google.com/u/0/' . $username . '/about';
				break;
			case 'youtube':
				return 'http://www.youtube.com/user/' . $username;
				break;
			case 'linkedin':
				return 'http://ca.linkedin.com/in/' . $username;
				break;
			default:
				return false;
		}
	}
}
