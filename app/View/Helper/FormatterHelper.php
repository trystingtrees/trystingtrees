<?php
/**
 * FormatterHelper
 *
 */

class FormatterHelper extends AppHelper {
	public function format_telephone($phone, $delimiter = '-'){
		$valid_delimiters = array(' ', '.', '-');

		$phone = preg_replace("/[^0-9]/", "", $phone);
 
		if(strlen($phone) == 7)
			return preg_replace("/([0-9]{3})([0-9]{4})/", "$1{$delimiter}$2", $phone);
		elseif(strlen($phone) == 10)
			return preg_replace("/([0-9]{3})([0-9]{3})([0-9]{4})/", "($1) $2{$delimiter}$3", $phone);
		elseif(strlen($phone) == 11)
			return preg_replace("/([0-9]{1})([0-9]{3})([0-9]{3})([0-9]{4})/", "$1{$delimiter}($2) $3{$delimiter}$4", $phone);

		return $phone;

	}

	private function _insert_into_string($value, $needle, $haystack, $insertAfter = true){
		$return_string = $haystack;

		if (substr_count($haystack, $needle) > 0){
			if ($insertAfter){
				$return_string = str_replace($needle, $needle.$value, $haystack);
			}else{
				$return_string = str_replace($needle, $value.$needle, $haystack);
			}
		}

		return $return_string;
	}
}
