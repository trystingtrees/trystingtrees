<?php
	$session = $this->Session->read();
?>

<?php echo $this->Html->doctype('xhtml-trans') . "\n"; ?>
<?php echo $this->Facebook->html(); ?>
<head>
    <title>Home</title>
    <meta charset="utf-8">
	
	<?php
		echo $this->Html->css( 'http://fonts.googleapis.com/css?family=PT+Sans:400,700,700italic,400italic' );
		echo $this->Html->css( 'layout/reset' );
		echo $this->Html->css( 'layout/normalize' );
		echo $this->Html->css( 'layout/styles1' );
		echo $this->Html->css( 'main' );
		echo $this->Html->css( 'bootstrap/trystingtrees-bootstrap-v1' );
		echo $this->Html->css( '/js/layout/pretty/prettyPhoto' );
		echo $this->Html->css( '/js/layout/tipSwift/tipSwift' );
		echo $this->Html->css( '/js/layout/nivo/nivo-slider' );
		echo $this->Html->css( '/js/layout/jFlickr/jflickr_css/style' );
	?>
    <!--[if IE 7]><link rel="stylesheet" href="style/ie7.css" type="text/css" /><![endif]-->
</head>

<body>
    <div id="top-container">
    	<div class="shine-top"></div>
        <div class="top-wrap">
            <!-- header -->

            <header>
                <div class="top-info">
                    <div class="mini-menu">
                        <span class="mini float-l"><span><a href="/">Home</a><img src="/img/layout/v-sep.gif" alt=""><a href="#">Sitemap</a></span></span> 
								<span class="phone">Toll free: +012 345 678 001</span>
								<?php
									if ($user === false){
								?>
                        <span class="mini float-r"><span><a href="/login#register">Signup</a><img src="/img/layout/v-sep.gif" alt=""><a href="/login#sign-in">Login</a></span></span>
								<?php
									}
								?>
                    </div>
                </div>

                <div class="logo-menu">
                    <a class="logo" href="index.html"><img src="/img/layout/content/logo.png" alt=""></a>
										
							<?php 
								if ($user !== false){
									echo $this->Element('user_card_template');
								}
							?>

                    <ul class="navmenu">
                        <li class="curr"><a href="/">Home</a></li>

                        <li><a href="about.html">AgentLocator</a></li>

                        <li><a href="full-width.html">Q & A</a></li>

                        <li><a href="blog.trystingtrees.com">Agent Blogs</a></li>
                        
								<li><a href="blog.trystingtrees.com">Company Blog</a></li>
                        
								<li><a href="blog.trystingtrees.com">Contact</a></li>
                    </ul>
                </div>

                <div class="inner-pages-slider">
                    <div id="slider" class="nivoSlider"><img src="/img/layout/content/slide-1.jpg" alt="" title="#htmlcaption"> <img src="/img/layout/content/slide-2.jpg" alt="" title="#htmlcaption1"> <img src="/img/layout/content/slide-3.jpg" alt="" title="#htmlcaption2"> <img src="/img/layout/content/slide-4.jpg" alt="" title="#htmlcaption3"></div>

                    <div id="htmlcaption" class="nivo-html-caption">
                        <p>Modern, sleek, two-color design</p><span>This is an example of a caption</span>
                    </div>

                    <div id="htmlcaption1" class="nivo-html-caption">
                        <p>2 portfolio styles</p><span>This is an example of a caption</span>
                    </div>

                    <div id="htmlcaption2" class="nivo-html-caption">
                        <p>Unique functionality</p><span>This is an example of a caption</span>
                    </div>

                    <div id="htmlcaption3" class="nivo-html-caption">
                        <p>jQuery powered</p><span>This is an example of a caption</span>
                    </div>
                </div>
            </header>
        </div>

        <div class="bottom-mask"></div>
    </div>
    
    <!-- content -->

	 <?php echo $content_for_layout; ?>
    
    <!-- footer -->

    <footer id="footer-wrap">
		<div class="inner-blank clearfix">
    		<div class="shine"></div>
    		<div class="top-mask"></div>
		 	<div class="col1-4">
		 		<h4>Contact Us<img src="/img/layout/heading-bg-footer.gif" alt=""></h4>
		 		<ul class="contacts">
		 			<li class="cont-phone">375 17 123-12-12</li>
		 			<li class="cont-email"><a href="mailto:info@website.com">info@website.com</a></li>
		 			<li class="cont-adress">Commercial Interiors<br />
		 			234 Interior Avenue<br />
		 			Montenegro, CA 6458</li>
		 		</ul>
		 	</div>
		 	<div class="col1-4">
		 		<h4>Recent posts<img src="/img/layout/heading-bg-footer.gif" alt=""></h4>
				<ul class="recent-posts">
					<li><a href="#">Ut enim ad minima veniam</a></li>
					<li><a href="#">Quis nostrum</a></li>
					<li><a href="#">Exercitationem ullam corporis</a></li>
					<li><a href="#">Laboriosam, nisi ut aliquid ex</a></li>
					<li><a href="#">Consequatur quis autem vel</a></li>
					<li><a href="#">Reprehenderit qui in ea</a></li>
				</ul>
		 	</div>
		 	<div class="col1-4">
		 		<h4>Categories<img src="/img/layout/heading-bg-footer.gif" alt=""></h4>
				<ul class="arrows">
					<li><a href="#">Ut enim ad minima veniam</a></li>
					<li><a href="#">Quis nostrum</a></li>
					<li><a href="#">Exercitationem ullam corporis</a></li>
					<li><a href="#">Laboriosam, nisi ut aliquid ex</a></li>
					<li><a href="#">Consequatur quis autem vel</a></li>
				</ul>
		 	</div>
		 	<div class="col1-4 omega">
		 		<h4>Text widget<img src="/img/layout/heading-bg-footer.gif" alt=""></h4>
		 		<p>Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere.</p>
		 	</div>
		 </div>
		 	<div class="copyr-top"></div>
		 	<div class="copyr-spacer">
		 		<div class="inner-blank clearfix">
		 			<a href="#" class="totop" title="To top"></a>
		 			<span class="float-l">&copy; 2011 Felis - Flexible & Multipurpose theme by <a href="http://themeforest.net/user/fireform?ref=fireform">Fireform</a></span>
		 		<div class="float-r social">
					<span class="float-l">Exercitationem ullam corporis suscipit</span>
					<ul class="float-r">
		 				<li><a href="#" title="Twitter"><img src="/img/layout/twitter.png" alt=""></a></li>
		 				<li><a href="#" title="Facebook"><img src="/img/layout/facebook.png" alt=""></a></li>
		 				<li><a href="#" title="LinkedIn"><img src="/img/layout/in.png" alt=""></a></li>
		 				<li><a href="#" title="Digg"><img src="/img/layout/digg.png" alt=""></a></li>
		 				<li><a href="#" title="Flickr"><img src="/img/layout/flickr.png" alt=""></a></li>
		 				<li><a href="#" title="Vimeo"><img src="/img/layout/vimeo.png" alt=""></a></li>
		 				<li><a href="#" title="YouTube"><img src="/img/layout/youtube.png" alt=""></a></li>
		 			</ul>
		 		</div>
			</div>
		 </div>
    </footer>

	<?php
		//Javascripts

		//echo $this->Html->script('layout/jquery-1.6.1.min');
		echo $this->Html->script('jquery.js?time=' . time());
		echo $this->Html->script('layout/jquery.easing.1.3');
		echo $this->Html->script('layout/jquery-ui-1.8.16.custom.min');
		echo $this->Html->script('layout/jquery.effects.core');
		echo $this->Html->script('layout/cufon/cufon-yui');
		echo $this->Html->script('layout/cufon/generated-fonts');
		/*
		echo $this->Html->script('http://maps.google.com/maps/api/js?v=3.5&amp;sensor=false');
		echo $this->Html->script('layout/maps/vendor/markermanager');
		echo $this->Html->script('layout/maps/vendor/StyledMarker');
		echo $this->Html->script('layout/maps/vendor/jquery.metadata');
		echo $this->Html->script('layout/maps/jquery.jmapping');
		*/
		echo $this->Html->script('layout/jPreloader/jquery.preloader');
		echo $this->Html->script('layout/nivo/jquery.nivo.slider.custom');
		echo $this->Html->script('layout/contact-form/contactform');
		echo $this->Html->script('layout/content-slider/jquery.flow.1.2');
		echo $this->Html->script('layout/tweet/jquery.tweet');
		echo $this->Html->script('layout/jFlickr/jflickr_js/jflickr_0.3_min');
		echo $this->Html->script('layout/pretty/jquery.prettyPhoto');
		echo $this->Html->script('layout/tipSwift/tipSwift');
		echo $this->Html->script('layout/scrollTo/jquery.scrollTo-min');
		echo $this->Html->script('layout/column-height');
		echo $this->Html->script('layout/custom');
		echo $this->Html->script('layout/html5');
		echo $this->Html->script('bootstrap/bootstrap-tab');
		echo $this->Html->script('app');
		echo $this->Html->script('application');
	?>

    <script type="text/javascript">
//<![CDATA[
    Cufon.now(); 
    //]]>
    </script>
</body>
<?php echo $this->Facebook->init(); ?>
</html>
