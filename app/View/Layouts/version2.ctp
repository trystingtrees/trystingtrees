<?php
	$user = null;
?>

<!DOCTYPE html>
<html>
<head>
  <title>TechProfiles - Showcase yourself!</title>
	<?php
		echo $this->Html->css('bootstrap/bootstrap-v2');
		//echo $this->Html->css('style');
		echo $this->Html->css('main');
		echo $this->Html->css('ui-lightness/jquery-ui-1.8.16.custom');
	?>

<style type="text/css">
  /* Override some defaults */
  html, body {
    background-color: #eee;
  }
  body {
    padding-top: 40px; /* 40px to make the container go all the way to the bottom of the topbar */
  }
  .container > footer p {
    text-align: center; /* center align it with the container */
  }
  .container {
    /*width: 820px;*/ /* downsize our container to make the content feel a bit tighter and more cohesive. NOTE: this removes two full columns from the grid, meaning you only go to 14 columns and not 16. */
  }

  /* The white background content wrapper */
  .content {
    background-color: #fff;
    padding: 20px;
    margin: 0 -20px; /* negative indent the amount of the padding to maintain the grid system */
    -webkit-border-radius: 0 0 6px 6px;
       -moz-border-radius: 0 0 6px 6px;
            border-radius: 0 0 6px 6px;
    -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.15);
       -moz-box-shadow: 0 1px 2px rgba(0,0,0,.15);
            box-shadow: 0 1px 2px rgba(0,0,0,.15);
  }

  /* Page header tweaks */
  .page-header {
    background-color: #f5f5f5;
    padding: 20px 20px 10px;
    margin: -20px -20px 20px;
  }

  /* Styles you shouldn't keep as they are for displaying this base example only */
  .content .span10,
  .content .span4 {
    min-height: 500px;
  }
  /* Give a quick and non-cross-browser friendly divider */
  .content .span4 {
    margin-left: 0;
    padding-left: 19px;
    border-left: 1px solid #eee;
  }

  .topbar .btn {
    border: 0;
  }

</style>

</head>

<body>
  <div class="topbar" style="height: 90px;">
    <div class="fill1" style="height: 100%; background: none;">
      <div class="container" style="padding-top: 20px;">
        <a class="brand" href="<?php echo $HOME; ?>/">Trystingtrees</a>
        <ul class="nav">
          <li class="active"><a href="<?php echo $HOME; ?>/">Home</a></li>
          <li><a href="<?php echo $HOME; ?>/about">AgentLocator <sup>&copy;</sup></a></li>
          <li><a href="<?php echo $HOME; ?>/signup">Q & A</a></li>
          <li><a href="<?php echo $HOME; ?>/signup">Signup</a></li>
          <li><a href="<?php echo $HOME; ?>/signup">Help</a></li>
          <?php if($user){ ?>
          <li><?php echo $this->Html->link('Logout', array('controller' => 'logins', 'action' => 'logout')); ?></li>
          <?php } ?>
        </ul>
        <?php if($user){ 
			  $shop_url = 'test';
		  ?>
				<?php echo $this->Html->link($shop_url, 'https://' . $shop_url, array('class' => 'shop_name', 'style' => 'float: right; text-align: right; margin-top: 10px;')); ?>
        <?php }else{ ?>
        <form action="" class="pull-right">
          <input class="input-small" type="text" placeholder="Username">
          <input class="input-small" type="password" placeholder="Password">
          <button class="btn" type="submit">Sign in</button>
        </form>
        <?php } ?>
      </div>
    </div>
  </div>

  <div class="container" style="margin-top: 50px;">
    <div class="content drop-shadow curved curved-hz-2">
      <div class="page-header" style="display: none;">
      </div>
      <div class="row">
        <div class="span16">
		  	<?php echo $content_for_layout; ?>
        </div>
      </div>
    </div>

    <footer>
      <p>&copy; AtCost, a <a href="http://www.tekcastdesigns.ca">TekCast Designs</a> Shopify application, 2011</p>
    </footer>
  </div>

	<?php
		echo $this->Html->script('https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js');
		echo $this->Html->script('https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.16/jquery-ui.min.js');
		//echo $this->Html->script('app');
	?>
</body>
</html>
