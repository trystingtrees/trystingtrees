<?php
	$sess = $this->Session->read();
	if (isset($sess['Auth']['User'])){
		$user = $sess['Auth']['User'];
	}else{
		$user = FALSE;
	}
?>

<!DOCTYPE html>
<!--<html dir="ltr" lang="en-US">-->
<?php echo $this->Facebook->html(); ?>
<head>

	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>Trystingtrees</title>
	<meta name="description" content="A Premium WordPress Theme" />

	<?php
		echo $this->Html->css( 'style' );
		echo $this->Html->css( 'main' );
		echo $this->Html->css( 'bootstrap/trystingtrees-bootstrap-v1' );
		echo $this->Html->css( 'nivo-slider' );
		echo $this->Html->css( 'custom-nivo-slider' );
		echo $this->Html->css( 'archive_list_view' );
		echo $this->Html->css( 'author' );
		echo $this->Html->css( 'ui-lightness/jquery-ui-1.8.16.custom' );
		echo $this->Html->css( 'simplemodal/osx' );
		//echo $this->Html->css( 'listings_print' );

		if (isset($cssIncludes)){
			foreach($cssIncludes as $css){
				echo $this->Html->css($css);
			}
		}
	?>
        
        
       	
    <!-- Skin Stylesheet -->
	    
</head>

<body <?php echo (isset($body_id) ? " id='" . $body_id . "'" : ''); echo (isset($body_classes) ? " class='" . $body_classes . "'" : ''); ?> <?php echo (isset($body_function) ? "onload={$body_function}" : ''); ?>
>

<!--
<div id="fb-root"></div>
<script>
  window.fbAsyncInit = function() {
    FB.init({
      appId      : '319420798103380', // App ID
      channelUrl : '//www.thewebsandbox.com/channel.html', // Channel File
      status     : true, // check login status
      cookie     : true, // enable cookies to allow the server to access the session
      xfbml      : true  // parse XFBML
    });

    // Additional initialization code here
  };

  // Load the SDK Asynchronously
  (function(d){
     var js, id = 'facebook-jssdk'; if (d.getElementById(id)) {return;}
     js = d.createElement('script'); js.id = id; js.async = true;
     js.src = "//connect.facebook.net/en_US/all.js";
     d.getElementsByTagName('head')[0].appendChild(js);
   }(document));
</script>
-->

<!--
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=APP_ID";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
-->

	<div id="container">

        <a name="top"></a>

			<?php 
			if ($user){ 
				$group = $sess['Auth']['User']['group_id'];
				if (($group == 1 || $group == 2)){
			?>
			<div id="user_card" class="right" style="position: relative;">
				<ul>
					<li class="trigger">
						<a href="#">
							<?php echo $user['username']; ?>
							&#9660;
						</a>
						<ul class="user_card_menu">
							<?php 
								switch($group){
									case 1: //Users
										echo $this->Element('dashboard/users/card_menu', array('HOME' => $HOME));
										break;
									case 2: //Agents
										echo $this->Element('dashboard/agents/card_menu', array('HOME' => $HOME));
										break;
								}
							?>
						</ul>
					</li>
				</ul>
			</div>
			<?php 
				}
			} 
			?>
    
       
		 	
        <header>
          <a href="http://wp.contempographicdesign.com/wp_real_estate_2">
			 	<?php
					echo $this->Html->image( 're_logo.png', array('height' => 82, 'widht' => '320', 'alt' => 'Trystingtrees.com', 'class' => 'left', 'id' => 'logo') );
				?>
          </a>
          
			 <?php
			 	echo $this->Element('layouts/main_menu', array('user' => $user, 'fb_user' => $fb_user));
			 ?>
			  
			<div class="clear"></div>
                
        </header>
      <div class="clear"></div>
		 
		 <?php
		 	if ($user){
				$group = $sess['Auth']['User']['group_id'];
				switch($group){
					/*
					case 1:	//Users
						echo $this->Element('dashboard/users/main_menu', array('HOME' => $HOME));
						break;
					case 2:	//Agents
						echo $this->Element('dashboard/agents/main_menu', array('HOME' => $HOME));
						break;
					*/
					case 3:	//Admins
						echo $this->Element('dashboard/agents/main_menu');
						break;
					case 4:	//Superusers
						echo $this->Element('dashboard/superusers/main_menu');
						break;
					default:
				}
			}
		 ?>

		<div class="cleartext"></div>
		<br />
		<div id="error_message_box" style="display: none; background: red; color: white; padding: 15px;"></div>
		<div id="success_message_box" style="display: none; background: green; color: white; padding: 15px;"></div>
		<?php echo $this->Session->flash(); ?>

	<?php //echo $this->Facebook->picture($fb_user['id']); ?>
	<?php
		//pr($fb_user);
	?>
    <?php echo $content_for_layout; ?>

    	<div class="clear"></div>

		<footer class="fourcol">
			                <p class="left">&copy; 2011 Trystingtrees, All Rights Reserved. .</p>
                            <p class="right"><a href="#top">Back to top</a></p>
                
                	<div class="clear"></div>
        </footer>
    	
                
		
	</div>

<?php
		//echo $this->Html->script( 'https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js' );
		echo $this->Html->script( 'jquery' );
		echo $this->Html->script( 'jquery-nivo-slider-pack' );
		echo $this->Html->script( 'app' );
		echo $this->Html->script( 'jquery-ui-1.8.16.custom.min' );
		echo $this->Html->script( 'application' );
		echo $this->Html->script( 'simplemodal/jquery.simplemodal' );
		echo $this->Html->script( 'simplemodal/osx.js' );

		//Load controller specific css
		if (isset($jsIncludes)){
			foreach($jsIncludes as $js){
				echo $this->Html->script($js);
			}
		}
?>
    <script>
    jQuery(window).load(function() {
        jQuery('#slider').nivoSlider({
            effect:'sliceDown',
            slices:15,
            animSpeed:500,
            pauseTime:3000,
            directionNav:true, //Next & Prev
            directionNavHide:true, //Only show on hover
            controlNav:true, //1,2,3...
            pauseOnHover:true, //Stop animation while hovering
            beforeChange: function(){},
            afterChange: function(){}, 
				fauxBackground: "#FFFFFF"
        });
    });
    </script>
            
	<!-- Core Javascript -->
	<!--
    <script src="http://wp.contempographicdesign.com/wp_real_estate_2/wp-content/themes/realestate_2/js/core.js"></script>
	-->

<!--

http://net.tutsplus.com/tutorials/javascript-ajax/how-to-use-the-jquery-ui-autocomplete-widget/

-->


<script type="text/javascript">
/*
	$(function(){

		//attach autocomplete
		$("#ListingAgentLocation").autocomplete({

			//define callback to format results
			source: function(req, add){

				//pass request to server
				$.getJSON("http://trystingtrees.localhost/states/autocomplete/", req, function(data) {

					console.log(data);

					//create array for response objects
					var suggestions = [];

					//process response
					$.each(data, function(i, val){
					suggestions.push(val.name);
				});

				//pass array to callback
				add(suggestions);
			});
		},

		//define select handler
		select: function(e, ui) {

			//create formatted friend
			var friend = ui.item.value,
				span = $("<span>").text(friend),
				a = $("<a>").addClass("remove").attr({
					href: "javascript:",
					title: "Remove " + friend
				}).text("x").appendTo(span);

				//add friend to friend div
				span.insertBefore("#ListingAgentLocation");
			},

			//define select handler
			change: function() {

				//prevent 'to' field being updated and correct position
				$("#ListingAgentLocation").val("").css("top", 2);
			}
		});
	});
*/
</script>

<?php
	//Social integration
	echo $fb_flag;
	if ($fb_flag == true){
		//Show welcome message as well as message about switching to being an agent
	}
?>

<!-- modal content -->
<div id="osx-login" style="display: none;">
	<div id="osx-modal-title"><h3>Login to Trystingtrees!</h3></div>
	<div class="close"><a href="#" class="simplemodal-close">x</a></div>
	<div id="osx-modal-data">
		<p><button class="simplemodal-close">Close</button> <span>(or press ESC or click the overlay)</span></p>
	</div>
</div>

<input type="hidden" id="cur_page_url" value="<?php echo $cur_page_url; ?>" />

</body>
<?php echo $this->Facebook->init(); ?>
</html>
