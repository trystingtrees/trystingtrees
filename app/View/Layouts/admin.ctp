<?php echo $this->Html->doctype('xhtml-trans'); ?>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Bootstrap, from Twitter</title>
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Le HTML5 shim, for IE6-8 support of HTML elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- Le styles -->
	<?php echo $this->Html->css('bootstrap/bootstrap-v2'); ?>
    <style type="text/css">
      body {
        padding-top: 60px;
        padding-bottom: 40px;
      }
      
		.sidebar-nav {
        padding: 9px 0;
      }
		
		#map img {
			max-width: none;
		}

		.left {
			float: left;
		}
    </style>
	<?php 
		echo $this->Html->css('bootstrap/bootstrap-responsive'); 
		echo $this->Html->css( 'ui-lightness/jquery-ui-1.8.16.custom' );

		//Load action specific CSS
		if (isset($cssIncludes)){
			foreach($cssIncludes as $css){
				echo $this->Html->css($css);
			}
		}
	?>

    <!-- Le fav and touch icons -->
    <link rel="shortcut icon" href="images/favicon.ico">
    <link rel="apple-touch-icon" href="images/apple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="images/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="images/apple-touch-icon-114x114.png">
  </head>

	<body <?php echo (isset($body_id) ? " id='" . $body_id . "'" : ''); echo (isset($body_classes) ? " class='" . $body_classes . "'" : ''); ?> <?php echo (isset($body_function) ? "onload={$body_function}" : ''); ?>>

    <div class="navbar navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container-fluid">
          <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </a>
          <a class="brand" href="#">Trystingtrees Admin Control Panel</a>
          <div class="nav-collapse">
            <ul class="nav">
              <li class="active"><a href="#">Home</a></li>
              <li><a href="#about">About</a></li>
              <li><a href="#contact">Contact</a></li>
            </ul>
            <p class="navbar-text pull-right">Logged in as <a href="#">username</a></p>
          </div><!--/.nav-collapse -->
        </div>
      </div>
    </div>

    <div class="container-fluid">
      <div class="row-fluid">
        <div class="span3">
          <div class="well sidebar-nav">
            <ul class="nav nav-list">
              <li class="active"><a href="#">Dashboard</a></li>
              <li class="nav-header">Listings</li>
              <li><a href="#">Manage Listings</a></li>
              <li><a href="#">Manage Syndications</a></li>
              <li class="nav-header">Maps</li>
              <li><a href="<?php echo $this->Html->url('/admin/maps'); ?>">Manage Maps</a></li>
              <li><a href="<?php echo $this->Html->url('/admin/maps/create'); ?>">Add Map</a></li>
              <li class="nav-header">Payments</li>
              <li><a href="#">Manage Providers</a></li>
              <li><a href="#">Manage Packages</a></li>
              <li><a href="#">Payment History</a></li>
              <li class="nav-header">Users</li>
              <li><a href="#">Manage Agents</a></li>
              <li><a href="#">Manage Users</a></li>
              <li><a href="#">Manage Roles</a></li>
              <li class="nav-header">Website</li>
              <li><a href="#">Manage FAQ</a></li>
              <li><a href="#">Manage Help Center</a></li>
              <li><a href="#">Manage Site Messages</a></li>
              <li class="nav-header">Reports</li>
              <li><a href="#">Listing Reports</a></li>
              <li><a href="#">Map Reports</a></li>
              <li><a href="#">Payment Reports</a></li>
              <li><a href="#">User Reports</a></li>
              <li class="nav-header">Tools & Settings</li>
              <li><a href="#">General Settings</a></li>
              <li><a href="#">Email Settings</a></li>
              <li><a href="#">Payment Settings</a></li>
            </ul>
          </div><!--/.well -->
        </div><!--/span-->
        <div class="span9">
		  	<?php echo $content_for_layout; ?>
        </div><!--/span-->
      </div><!--/row-->

      <hr>

      <footer>
        <p>&copy; Company 2012</p>
      </footer>

    </div><!--/.fluid-container-->
    
    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
	 <?php
	 	echo $this->Html->script('jquery.js?time=' . time()) . "\n";
	 	echo $this->Html->script('bootstrap/bootstrap-transition') . "\n";
	 	echo $this->Html->script('bootstrap/bootstrap-alert') . "\n";
	 	echo $this->Html->script('bootstrap/bootstrap-modal') . "\n";
	 	echo $this->Html->script('bootstrap/bootstrap-dropdown') . "\n";
	 	echo $this->Html->script('bootstrap/bootstrap-scrollspy') . "\n";
	 	echo $this->Html->script('bootstrap/bootstrap-tab') . "\n";
	 	echo $this->Html->script('bootstrap/bootstrap-tooltip') . "\n";
	 	echo $this->Html->script('bootstrap/bootstrap-popover') . "\n";
	 	echo $this->Html->script('bootstrap/bootstrap-button') . "\n";
	 	echo $this->Html->script('bootstrap/bootstrap-collapse') . "\n";
	 	echo $this->Html->script('bootstrap/bootstrap-carousel') . "\n";
	 	echo $this->Html->script('bootstrap/bootstrap-typeahead') . "\n";
		echo $this->Html->script( 'app' );
		echo $this->Html->script( 'jquery-ui-1.8.16.custom.min' );
		echo $this->Html->script( 'application' );

		//Load controller/action specific js
		if (isset($jsIncludes)){
			foreach($jsIncludes as $js){
				echo $this->Html->script($js) . "\n";
			}
		}
	 ?>

  </body>
</html>
