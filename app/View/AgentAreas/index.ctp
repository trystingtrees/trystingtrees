<h1>Your selected areas</h1>
<table>
    <tr>
        <th>Area Name</th>
        <th>Actions</th>
        <th>Selected</th>
    </tr>

    <!-- Here is where we loop through our $posts array, printing out post info -->

    <?php foreach ($areas as $area): ?>
    <tr>
        <td><?php echo $area['name']; ?></td>
        <td>
            <?php echo $this->Html->link($area['Post']['title'],
array('controller' => 'maps', 'action' => 'view', $area['id'])); ?>
        </td>
        <td><?php echo $area['created']; ?></td>
    </tr>
    <?php endforeach; ?>

</table>
