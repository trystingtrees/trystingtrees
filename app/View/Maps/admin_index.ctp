<div class="left dashboard-main">
	<br />
	<h2>Manage Maps</h2>

	<table id="map-listings-table" class="table table-condensed">
		<thead>
			<tr>
				<th>Map ID</th>
				<th>Map Title</th>
				<th>City</th>
				<th>State</th>
				<th>Country</th>
				<th>Actions</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach($maps as $map): 
				$title = $map['Map']['name'];
				$date = substr($map['Map']['created'], 0, 11);
				$id = $map['Map']['id'];
			?>
			<tr>
				<td><?php echo $map['Map']['id'] ;?></td>
				<td><?php echo $title; ?></td>
				<td><?php echo $map['Map']['city']; ?></td>
				<td><?php echo $map['State']['long_name']; ?></td>
				<td><?php echo $map['Country']['long_name']; ?></td>
				<td>
					<?php
						echo $this->Html->link('Edit', array('action' => 'edit/' . $id)) . ' | ';
						echo $this->Html->link('View', array('action' => 'view/' . $id)) . ' | ';
						echo $this->Form->postLink('Delete', array('action' => 'delete', $id), null, 'Are you sure you wish to delete this map?', $id);
					?>
			</td>
		</tr>
		<?php endforeach; ?>
		</tbody>
	</table>
</div>

