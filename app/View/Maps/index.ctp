<div class="left dashboard-main">
	<br />
	<h2>Manage Maps</h2>

	<table>
		<thead>
			<tr>
				<th>Map ID</th>
				<th>Map Title</th>
				<th>City</th>
				<th>State</th>
				<th>Country</th>
				<th>Actions</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach($maps as $map): 
				$title = $map['Map']['name'];
				$date = substr($map['Map']['created'], 0, 11);
				$id = $map['Map']['id'];
			?>
			<tr>
				<td><?php echo $map['Map']['id'] ;?></td>
				<td><?php echo $title; ?></td>
				<td><?php echo $map['Map']['city']; ?></td>
				<td><?php echo $map['State']['long_name']; ?></td>
				<td><?php echo $map['Country']['long_name']; ?></td>
				<td>
					<?php
						echo $this->Html->link('Edit', array('action' => 'edit/' . $id)) . ' | ';
						echo $this->Html->link('View', array('action' => 'view/' . $id)) . ' | ';
						echo $this->Html->link('Delete', array('action' => 'delete/' . $id));
					?>
			</td>
		</tr>
		<?php endforeach; ?>
		</tbody>
	</table>
</div>

