<style>
.ui-autocomplete {
	background-color: white;
   width: 300px;
   border: 1px solid #cfcfcf;
   list-style-type: none;
   padding-left: 0px;
}
</style>
<div class="left dashboard-main">
	<h2>Create new Area</h2>

	<div id="area_form" style="float: left; margin-right: 50px;">
	<?php
		echo $this->Form->create('Map', array('action' => 'add'));
		echo '<div class="form-row">';
		echo $this->Form->input('name', array('size' => '20', 'maxlength' => '50'));
		echo $this->Form->input('nice_name', array('size' => '20', 'maxlength' => '50'));
		echo '</div>';
		echo '<div class="form-row">';
		echo $this->Form->input('city', array('size' => '20', 'maxlength' => '50'));
		echo '</div>';
		echo '<div class="form-row">';
		echo $this->Form->label('State');
		echo $this->Form->select('state_id', $states);
		echo '</div>';
		echo '<div class="form-row">';
		echo $this->Form->label('Country');
		echo $this->Form->select('country_id', $countries);
		echo '</div>';
		echo '<div class="form-row">';
		echo $this->Form->hidden('coordinates');
		echo $this->Form->button('Create Map', array('style' => 'padding: 10px;', 'id' => 'create_map'));
		echo '</div>';
		echo $this->Form->end();
	?>
	</div>
	<br class="cleartext">
	<br />
	<br />
	<br />
	<br />

	<div id="map_canvas" style"float: left;">
		<button id="reset_map" onclick="initialize(); return false;" style="float: right; padding: 10px;">Reset Map</button>
		<input type="text" id="gmap_address" value="" /> <button id="find_gmap_address" style="padding: 10px;" onclick="find_gmap_address();">Find Address</button>
		<div id="map" style="width: 900px; height: 500px;"></div>
	</div>

</div>
