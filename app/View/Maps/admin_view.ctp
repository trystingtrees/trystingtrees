<?php
	$group = $this->Session->read('Auth.User.group_id');
?>

<div class="left dashboard-main">
	<h2>View Map Area - [ <?php echo $map['Map']['name']; ?> ]</h2>

	<div id="sub-menu">
		<?php
			//Admin
			if ($group == 3){
				echo $this->Html->link('Edit Map', array('action' => 'edit', $map['Map']['id']));
				echo ' | ';
				echo $this->Html->link('Back to List', array('action' => 'index'));
			//Agent
			}else if ($group == 2){
				echo $this->Html->link('Back to My Areas', array('controller' => 'agent', 'action' => 'areas'));
			}
		?>
	</div>
	<br />

	<div id="map_canvas" style"float: left;">
		<div id="map" style="width: 900px; height: 500px;">Loading Map</div>
		<input type="hidden" id="map_coordinates" value="<?php echo $map['Map']['coordinates']; ?>">
	</div>
</div>
