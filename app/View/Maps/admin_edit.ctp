<div class="left dashboard-main">
	<h2>Edit Map Area - [ <?php echo $this->data['Map']['name']; ?> ]</h2>

	<div id="area_form" style="float: left; margin-right: 50px;">
	<?php
		echo $this->Form->create('Map', array('action' => 'edit'));
		echo '<div class="form-row left">';
		echo $this->Form->input('name', array('size' => '20', 'maxlength' => '50'));
		echo '</div>';
		echo '<div class="form-row left">';
		echo $this->Form->input('city', array('size' => '20', 'maxlength' => '50'));
		echo '</div>';
		echo '<div class="form-row left">';
		echo $this->Form->label('State');
		echo $this->Form->select('state_id', $states);
		echo '</div>';
		echo '<div class="form-row left">';
		echo $this->Form->label('Country');
		echo $this->Form->select('country_id', $countries);
		echo '</div>';
		echo '<div class="form-row">';
		echo $this->Form->hidden('coordinates');
		echo $this->Form->hidden('id');
		echo $this->Form->button('Update Map', array('style' => 'padding: 10px;', 'id' => 'create_map'));
		echo '</div>';
		echo $this->Form->end();
	?>
	<button style="display: none;" id="create_map2">CHECK COORDS</button>
	</div>
	<br class="cleartext">
	<div id="map_canvas" style"float: left;">
		<div id="map" style="width: 900px; height: 500px;">Loading Map</div>
		<input type="hidden" id="map_coordinates" value="<?php echo $this->data['Map']['coordinates']; ?>">
	</div>
</div>
