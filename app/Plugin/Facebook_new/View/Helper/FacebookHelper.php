<?php
//App::uses('Facebook.Lib', 'FacebookInfo');
App::uses('FacebookInfo', 'Facebook.Lib');
App::uses('AppHelper', 'View/Helper');
class FacebookHelper extends AppHelper {
	public $helpers = array('Html', 'Session');
	
	/*
	public function __construct(View $View, $settings = array()){
		$config = FacebookInfo::getConfig();
		pr($config);

		parent::__construct($View, $settings);
	}
	*/

	/**
   * HTML XMLNS tag (required)
   * @return string of html header
   * @access public
   */
   public function html(){
      return '<html xmlns="http://www.w3.org/1999/xhtml" xmlns:fb="http://ogp.me/ns/fb#">';
   }

	public function login(){
		$config = Configure::read('FacebookConfig');
		$login_img = '/img/auth/facebook_login_button_small.png';
		$login_url = $config['Facebook']['login_url'];
		return '<a href="' . $login_url . '"><img src="' . $login_img . '"/></a>';
	}

	public function logout(){
		$config = Configure::read('FacebookConfig');
		$token = $this->Session->read();
		$redir = $config['Facebook']['logout_redir'];
		$logout_url = 'https://www.facebook.com/logout.php?next=' . $redir . '&access_token=' . $token;
		$logout_img = '/img/auth/facebook_logout_button.png';
		return '<a href="' . $logout_url . '"><img src="' . $logout_img . '"/></a>';
	}
}
